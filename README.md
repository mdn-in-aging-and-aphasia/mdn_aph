# Stimulation of pre-SMA in healthy aging

This project contains the code for the project "Facilitatory stimulation of the pre-SMA in healthy aging has distinct effects on activity and connectivity" (https://www.biorxiv.org/content/10.1101/2022.10.21.513185v1).


