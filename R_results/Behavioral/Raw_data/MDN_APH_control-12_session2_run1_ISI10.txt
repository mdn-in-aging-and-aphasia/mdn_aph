MDN_APH

Participant:	12
Date: 05/10/2021 11:41:49
Session: 2
Run: 1
Buttons: 2/1

block_number	stimulus_audio	time_audio	length_audio	stimulus_picture	time_picture	response_time	relative_response_time	button_response	accuracy	condition	correctness	category	ISI	time_ISI
1	hobel_word	26900	1190	meissel	28098	1606	29704	1	incorrect	WPM	incorrect	werkzeug	4750	30414
1	moehre_word	35198	1170	moehre	36381	889	37270	1	correct	WPM	correct	gemuese	2500	38714
1	reiher_incorrect	41264	1110	reiher	42380	2262	44643	1	incorrect	FPM	incorrect	vogel	7000	44780
1	weisskohl_correct	51847	1080	weisskohl	52930	1170	54100	1	correct	FPM	correct	gemuese	3750	59146
2	375_625	60679	1200	pfeil_runter	61896	1165	63061	2	correct	tone	incorrect	tone	2500	64196
2	525_775	66729	1200	pfeil_hoch	67946	1956	69902	1	correct	tone	correct	tone	7000	70245
2	325_575	77278	1200	pfeil_hoch	78495	1011	79506	1	correct	tone	correct	tone	4750	80795
2	475_725	85578	1200	pfeil_runter	86794	1447	88242	2	correct	tone	incorrect	tone	16250	105377
3	meise_correct	106927	1190	meise	108126	1716	109843	1	correct	FPM	correct	vogel	7000	110443
3	paprika_incorrect	117476	1130	paprika	118609	1293	119903	2	correct	FPM	incorrect	gemuese	4750	120992
3	tiger_word	125809	1210	bueffel	127025	1137	128163	2	correct	WPM	incorrect	saeugetier	2500	129325
3	rollschuh_incorrect	131875	1070	rollschuh	132958	1379	134338	2	correct	FPM	incorrect	fortbewegungsmittel	3750	139175
4	hut_word	140741	1120	hut	141874	919	142794	1	correct	WPM	correct	kleidungsstueck	2500	144258
4	seehund_incorrect	146791	990	seehund	147791	1716	149507	2	correct	FPM	incorrect	saeugetier	7000	150307
4	flugzeug_incorrect	157340	870	flugzeug	158223	1838	160062	2	correct	FPM	incorrect	fortbewegungsmittel	4750	160857
4	traktor_word	165673	1320	schiff	167006	995	168002	2	correct	WPM	incorrect	fortbewegungsmittel	16250	185472
5	kokosnuss_correct	187005	980	kokosnuss	187988	1517	189505	1	correct	FPM	correct	frucht	7000	190522
5	kiwi_incorrect	197555	1250	kiwi	198821	1841	200663	2	correct	FPM	incorrect	frucht	4750	201071
5	sellerie_correct	205854	970	sellerie	206837	1706	208544	2	incorrect	FPM	correct	gemuese	2500	209370
5	sichel_correct	211904	1230	sichel	213137	2043	215180	1	correct	FPM	correct	werkzeug	3750	219203
6	700_450	220736	1200	pfeil_runter	221953	1246	223200	1	correct	tone	correct	tone	4750	224253
6	350_600	229036	1200	pfeil_runter	230252	1427	231680	2	correct	tone	incorrect	tone	2500	232552
6	400_650	235086	1200	pfeil_hoch	236302	1091	237394	1	correct	tone	correct	tone	7000	238602
6	425_675	245635	1200	pfeil_hoch	246851	939	247791	1	correct	tone	correct	tone	3750	252934
7	faehre_correct	254468	1320	faehre	255801	1508	257309	1	correct	FPM	correct	fortbewegungsmittel	7000	257984
7	schal_word	265017	1040	schal	266067	1075	267142	1	correct	WPM	correct	kleidungsstueck	2500	268534
7	ananas_word	271067	1320	dattel	272400	1008	273409	2	correct	WPM	incorrect	frucht	4750	274583
7	moewe_correct	279366	1090	moewe	280466	1383	281849	1	correct	FPM	correct	vogel	3750	286666
8	handschuh_correct	288232	920	handschuh	289166	2016	291182	1	correct	FPM	correct	kleidungsstueck	4750	291749
8	sofa_incorrect	296532	1220	sofa	297765	1325	299091	2	correct	FPM	incorrect	moebelstueck	7000	300048
8	weintraube_correct	307098	1000	weintraube	308114	2668	310783	2	incorrect	FPM	correct	frucht	2500	310614
8	kuerbis_word	313148	1300	radieschen	314464	1221	315686	2	correct	WPM	incorrect	gemuese	16250	332946
9	truhe_incorrect	334496	1210	truhe	335713	1280	336993	2	correct	FPM	incorrect	moebelstueck	2500	338013
9	aprikose_word	340546	1440	aprikose	341996	859	342855	1	correct	WPM	correct	frucht	4750	344062
9	tisch_word	348845	1040	tisch	349895	1071	350967	1	correct	WPM	correct	moebelstueck	7000	352362
9	ente_word	359411	1220	eule	360645	943	361588	2	correct	WPM	incorrect	vogel	3750	366711
10	mantel_correct	368244	1130	mantel	369377	1494	370872	2	incorrect	FPM	correct	kleidungsstueck	7000	371761
10	avocado_incorrect	378794	940	avocado	379744	1150	380894	2	correct	FPM	incorrect	gemuese	4750	382310
10	sessel_correct	387093	990	sessel	388093	1176	389270	1	correct	FPM	correct	moebelstueck	2500	390610
10	wiege_word	393193	1190	wiege	394393	1168	395561	1	correct	WPM	correct	moebelstueck	3750	400492
11	schrank_correct	402042	1040	schrank	403092	1195	404288	1	correct	FPM	correct	moebelstueck	4750	405559
11	hemd_incorrect	410342	1080	hemd	411425	1271	412696	2	correct	FPM	incorrect	kleidungsstueck	7000	413858
11	baer_word	420891	1130	lama	422024	915	422940	2	correct	WPM	incorrect	saeugetier	2500	424408
11	pullover_word	426974	1290	schlafanzug	428274	937	429211	1	incorrect	WPM	incorrect	kleidungsstueck	16250	446773
12	550_300	448306	1200	pfeil_hoch	449523	1173	450697	2	correct	tone	incorrect	tone	7000	451823
12	725_475	458856	1200	pfeil_runter	460072	1139	461212	1	correct	tone	correct	tone	2500	462372
12	825_575	464905	1200	pfeil_runter	466122	2079	468202	1	correct	tone	correct	tone	4750	468422
12	650_400	473205	1200	pfeil_hoch	474421	1677	476098	2	correct	tone	incorrect	tone	3750	480504
13	pinguin_word	482037	1200	pinguin	483254	1102	484357	1	correct	WPM	correct	vogel	7000	485554
13	orange_incorrect	492620	1670	orange	494303	1563	495866	2	correct	FPM	incorrect	frucht	2500	496137
13	spatz_word	498670	1150	geier	499836	1132	500969	2	correct	WPM	incorrect	vogel	4750	502186
13	zug_word	506969	960	zug	507936	1138	509074	1	correct	WPM	correct	fortbewegungsmittel	16250	526768
14	300_550	528301	1200	pfeil_hoch	529518	1179	530697	1	correct	tone	correct	tone	4750	531818
14	750_500	536601	1200	pfeil_hoch	537817	1129	538947	2	correct	tone	incorrect	tone	7000	540117
14	450_700	547150	1200	pfeil_runter	548367	1293	549661	2	correct	tone	incorrect	tone	2500	550667
14	625_375	553200	1200	pfeil_runter	554416	1940	556357	1	correct	tone	correct	tone	3750	560499
15	adler_incorrect	562033	1210	adler	563249	1586	564836	2	correct	FPM	incorrect	vogel	7000	565549
15	wellensittich_word	572599	1510	wellensittich	574115	878	574994	1	correct	WPM	correct	vogel	4750	576115
15	bluse_word	580898	1340	hose	582248	1130	583378	2	correct	WPM	incorrect	kleidungsstueck	2500	584415
15	kamel_correct	586948	960	kamel	587914	1148	589063	1	correct	FPM	correct	saeugetier	16250	606747
16	575_325	608280	1200	pfeil_hoch	609496	1201	610697	2	correct	tone	incorrect	tone	7000	611796
16	500_750	618829	1200	pfeil_hoch	620046	1095	621141	1	correct	tone	correct	tone	2500	622346
16	600_350	624879	1200	pfeil_runter	626095	1513	627609	1	correct	tone	correct	tone	4750	628395
16	675_425	633178	1200	pfeil_hoch	634395	1213	635609	2	correct	tone	incorrect	tone	3750	640478
17	axt_incorrect	642045	1170	axt	643228	1311	644539	2	correct	FPM	incorrect	werkzeug	4750	645561
17	harke_word	650361	1240	harke	651611	977	652588	1	correct	WPM	correct	werkzeug	2500	653877
17	kirsche_word	656410	1190	kirsche	657610	976	658586	1	correct	WPM	correct	frucht	7000	659927
17	fuchs_incorrect	666976	1360	fuchs	668343	1140	669484	2	correct	FPM	incorrect	saeugetier	16250	686775
18	575_825	688309	1200	pfeil_runter	689525	1172	690698	2	correct	tone	incorrect	tone	4750	691825
18	800_550	696608	1200	pfeil_runter	697824	1368	699193	1	correct	tone	correct	tone	2500	700124
18	775_525	702658	1200	pfeil_hoch	703874	1191	705065	2	correct	tone	incorrect	tone	7000	706174
18	550_800	713207	1200	pfeil_runter	714423	1471	715895	2	correct	tone	incorrect	tone	3750	720506
19	schraubenzieher_word	722040	1420	schleifpapier	723473	992	724465	2	correct	WPM	incorrect	werkzeug	2500	725556
19	brokkoli_word	728106	1100	kohlrabi	729223	928	730151	2	correct	WPM	incorrect	gemuese	7000	731622
19	wal_word	738655	1100	wal	739772	1410	741182	2	incorrect	WPM	correct	saeugetier	4750	742172
19	kutsche_correct	746988	940	kutsche	747938	1156	749094	1	correct	FPM	correct	fortbewegungsmittel	3750	754288
20	fahrrad_word	755838	1350	fahrrad	757204	939	758144	1	correct	WPM	correct	fortbewegungsmittel	2500	759354
20	saege_incorrect	761887	1180	saege	763071	1231	764302	2	correct	FPM	incorrect	werkzeug	4750	765404
20	taxi_word	770187	1230	schlitten	771420	1073	772494	2	correct	WPM	incorrect	fortbewegungsmittel	7000	773703
20	walze_correct	780753	1170	walze	781936	1277	783214	1	correct	FPM	correct	werkzeug	16250	800552
21	eichhoernchen_word	802085	1430	eichhoernchen	803518	1141	804660	1	correct	WPM	correct	saeugetier	2500	805601
21	kleid_incorrect	808151	1290	kleid	809451	950	810402	2	correct	FPM	incorrect	kleidungsstueck	7000	811668
21	bohrmaschine_word	818701	1530	bohrmaschine	820234	1078	821313	1	correct	WPM	correct	werkzeug	4750	822217
21	bank_word	827000	1160	kommode	828167	1115	829282	2	correct	WPM	incorrect	moebelstueck	3750	834300
22	blaubeere_word	835850	1450	birne	837316	1028	838344	2	correct	WPM	incorrect	frucht	2500	839366
22	biber_correct	841916	1240	biber	843166	1532	844699	1	correct	FPM	correct	saeugetier	4750	845432
22	hocker_word	850249	1110	regal	851365	1036	852402	2	correct	WPM	incorrect	moebelstueck	7000	853765
22	zwiebel_word	860798	1250	zwiebel	862065	899	862964	1	correct	WPM	correct	gemuese	0	862065
