MDN_APH

Participant:	14
Date: 07/01/2021 14:53:13
Session: 3
Run: 1
Buttons: 2/1

block_number	stimulus_audio	time_audio	length_audio	stimulus_picture	time_picture	response_time	relative_response_time	button_response	accuracy	condition	correctness	category	ISI	time_ISI
1	harke_word	25052	1240	axt	26315	1228	27544	2	correct	WPM	incorrect	werkzeug	2500	28564
1	banane_incorrect	31098	890	banane	31998	1305	33303	2	correct	FPM	incorrect	frucht	7000	34614
1	avocado_word	41647	1440	zwiebel	43097	827	43924	2	correct	WPM	incorrect	gemuese	4750	45163
1	biber_correct	49947	1240	biber	51196	1951	53148	1	correct	FPM	correct	saeugetier	16250	69745
2	rollschuh_word	71279	1150	fahrrad	72445	1095	73540	2	correct	WPM	incorrect	fortbewegungsmittel	2500	74795
2	tiger_word	77328	1210	tiger	78545	1316	79861	1	correct	WPM	correct	saeugetier	4750	80845
2	sichel_word	85628	1210	sichel	86844	1313	88158	1	correct	WPM	correct	werkzeug	7000	89144
2	regal_correct	96177	980	regal	97160	1439	98600	1	correct	FPM	correct	moebelstueck	16250	115976
3	550_800	117509	1200	pfeil_hoch	118726	1135	119861	1	correct	tone	correct	tone	7000	121026
3	375_625	128059	1200	pfeil_hoch	129275	883	130158	1	correct	tone	correct	tone	4750	131575
3	525_775	136358	1200	pfeil_runter	137575	2037	139612	2	correct	tone	incorrect	tone	2500	139874
3	350_600	142408	1200	pfeil_hoch	143624	1074	144699	1	correct	tone	correct	tone	3750	149707
4	meissel_correct	151240	1170	meissel	152424	1381	153805	1	correct	FPM	correct	werkzeug	7000	154757
4	ziege_incorrect	161790	1000	ziege	162806	1299	164105	2	correct	FPM	incorrect	saeugetier	4750	165306
4	traktor_word	170089	1320	traktor	171423	934	172357	1	correct	WPM	correct	fortbewegungsmittel	2500	173606
4	fahrrad_word	176139	1350	rollschuh	177505	1097	178603	2	correct	WPM	incorrect	fortbewegungsmittel	3750	183438
5	schlafanzug_correct	184972	1030	schlafanzug	186005	2221	188226	1	correct	FPM	correct	kleidungsstueck	4750	188488
5	birne_correct	193271	1310	birne	194588	947	195536	1	correct	FPM	correct	frucht	7000	196788
5	wiege_word	203821	1190	truhe	205020	1217	206238	2	correct	WPM	incorrect	moebelstueck	2500	207337
5	kutsche_correct	209870	940	kutsche	210820	1041	211862	1	correct	FPM	correct	fortbewegungsmittel	3750	217170
6	bohrmaschine_incorrect	218703	1030	bohrmaschine	219736	1483	221220	2	correct	FPM	incorrect	werkzeug	7000	222219
6	sellerie_word	229252	1110	sellerie	230369	1671	232040	1	correct	WPM	correct	gemuese	2500	232769
6	kirsche_incorrect	235302	900	kirsche	236219	1090	237309	2	correct	FPM	incorrect	frucht	4750	238818
6	faehre_word	243602	1240	faehre	244851	1307	246159	1	correct	WPM	correct	fortbewegungsmittel	16250	263400
7	kleid_word	264934	1210	hut	266150	998	267149	2	correct	WPM	incorrect	kleidungsstueck	7000	268450
7	weisskohl_correct	275483	1080	weisskohl	276566	1593	278159	1	correct	FPM	correct	gemuese	2500	278999
7	kokosnuss_word	281533	1510	kokosnuss	283049	813	283862	1	correct	WPM	correct	frucht	4750	285049
7	wal_word	289832	1100	fuchs	290949	976	291925	2	correct	WPM	incorrect	saeugetier	16250	309631
8	mantel_word	311164	1020	mantel	312197	1364	313562	1	correct	WPM	correct	kleidungsstueck	7000	314681
8	eichhoernchen_incorrect	321714	1210	eichhoernchen	322930	1227	324158	2	correct	FPM	incorrect	saeugetier	2500	325230
8	schal_incorrect	327763	1360	schal	329130	1128	330258	2	correct	FPM	incorrect	kleidungsstueck	4750	331280
8	weintraube_correct	336063	1000	weintraube	337079	1773	338852	1	correct	FPM	correct	frucht	3750	343362
9	625_375	344895	1200	pfeil_hoch	346112	1354	347467	2	correct	tone	incorrect	tone	2500	348412
9	725_475	350945	1200	pfeil_hoch	352162	1206	353368	2	correct	tone	incorrect	tone	4750	354461
9	475_725	359245	1200	pfeil_hoch	360461	906	361368	1	correct	tone	correct	tone	7000	362761
9	550_300	369794	1200	pfeil_runter	371010	1504	372515	1	correct	tone	correct	tone	16250	389593
10	reiher_word	391126	1180	wellensittich	392309	1535	393844	2	correct	WPM	incorrect	vogel	2500	394642
10	sessel_word	397176	1130	sessel	398309	1158	399467	1	correct	WPM	correct	moebelstueck	7000	400692
10	jeans_incorrect	407725	1300	jeans	409041	1217	410259	2	correct	FPM	incorrect	kleidungsstueck	4750	411241
10	pinguin_incorrect	416024	940	pinguin	416974	1261	418235	2	correct	FPM	incorrect	vogel	3750	423324
11	675_425	424857	1200	pfeil_runter	426074	1219	427293	1	correct	tone	correct	tone	2500	428374
11	600_350	430907	1200	pfeil_hoch	432123	1125	433249	2	correct	tone	incorrect	tone	7000	434423
11	775_525	441456	1200	pfeil_runter	442673	1144	443817	1	correct	tone	correct	tone	4750	444973
11	825_575	449756	1200	pfeil_hoch	450972	1058	452031	2	correct	tone	incorrect	tone	3750	457055
12	750_500	458589	1200	pfeil_runter	459805	948	460754	1	correct	tone	correct	tone	4750	462105
12	575_325	466888	1200	pfeil_runter	468105	1517	469622	1	correct	tone	correct	tone	2500	470404
12	575_825	472938	1200	pfeil_hoch	474154	1043	475197	1	correct	tone	correct	tone	7000	476454
12	325_575	483487	1200	pfeil_runter	484704	1201	485906	2	correct	tone	incorrect	tone	3750	490787
13	bueffel_correct	492320	1410	bueffel	493736	1556	495292	1	correct	FPM	correct	saeugetier	4750	495836
13	tisch_incorrect	500619	1110	tisch	501736	1216	502952	2	correct	FPM	incorrect	moebelstueck	7000	504136
13	gans_incorrect	511169	950	gans	512135	1688	513824	2	correct	FPM	incorrect	vogel	2500	514685
13	zug_incorrect	517218	930	zug	518152	1415	519567	2	correct	FPM	incorrect	fortbewegungsmittel	3750	524518
14	schrank_correct	526051	1040	schrank	527101	1227	528329	1	correct	FPM	correct	moebelstueck	7000	529568
14	fuchs_word	536601	1100	wal	537717	1334	539052	2	correct	WPM	incorrect	saeugetier	4750	540117
14	pullover_word	544900	1290	pullover	546200	1463	547663	1	correct	WPM	correct	kleidungsstueck	2500	548416
14	hubschrauber_incorrect	550950	1180	hubschrauber	552133	1017	553151	2	correct	FPM	incorrect	fortbewegungsmittel	16250	570748
15	500_750	572282	1200	pfeil_runter	573498	1256	574755	2	correct	tone	incorrect	tone	7000	575798
15	650_400	582831	1200	pfeil_runter	584048	958	585006	1	correct	tone	correct	tone	2500	586347
15	700_450	588881	1200	pfeil_hoch	590097	1231	591329	2	correct	tone	incorrect	tone	4750	592397
15	450_700	597180	1200	pfeil_hoch	598397	1137	599534	1	correct	tone	correct	tone	3750	604480
16	schaukelstuhl_incorrect	606013	940	schaukelstuhl	606963	1396	608359	2	correct	FPM	incorrect	moebelstueck	4750	609529
16	schiff_correct	614312	1150	schiff	615479	1672	617151	1	correct	FPM	correct	fortbewegungsmittel	2500	617829
16	moehre_incorrect	620362	1000	moehre	621379	1067	622446	2	correct	FPM	incorrect	gemuese	7000	623878
16	zwiebel_word	630911	1250	avocado	632178	972	633151	1	incorrect	WPM	incorrect	gemuese	3750	638211
17	ente_word	639744	1220	ente	640977	1362	642340	1	correct	WPM	correct	vogel	7000	643261
17	axt_word	650294	1140	harke	651443	1297	652741	2	correct	WPM	incorrect	werkzeug	4750	653810
17	wellensittich_word	658593	1510	reiher	660110	987	661097	2	correct	WPM	incorrect	vogel	2500	662109
17	eule_correct	664643	1450	eule	666109	1256	667366	1	correct	FPM	correct	vogel	16250	684441
18	blaubeere_word	685975	1450	blaubeere	687441	1115	688557	1	correct	WPM	correct	frucht	2500	689491
18	handschuh_correct	692024	920	handschuh	692958	962	693920	1	correct	FPM	correct	kleidungsstueck	7000	695541
18	kamel_word	702574	1190	kamel	703774	863	704637	1	correct	WPM	correct	saeugetier	4750	706090
18	moewe_word	710873	1240	moewe	712123	1390	713514	2	incorrect	WPM	correct	vogel	3750	718173
19	400_650	719706	1200	pfeil_runter	720923	1300	722224	2	correct	tone	incorrect	tone	4750	723222
19	300_550	728006	1200	pfeil_runter	729222	1137	730360	2	correct	tone	incorrect	tone	7000	731522
19	800_550	738555	1200	pfeil_hoch	739771	1455	741227	2	correct	tone	incorrect	tone	2500	742071
19	425_675	744605	1200	pfeil_runter	745821	1510	747332	2	correct	tone	incorrect	tone	3750	751904
20	orange_word	753437	1230	aprikose	754671	1445	756116	2	correct	WPM	incorrect	frucht	4750	756954
20	truhe_word	761737	1190	wiege	762937	1193	764130	2	correct	WPM	incorrect	moebelstueck	2500	765253
20	feile_incorrect	767787	870	feile	768670	1888	770558	2	correct	FPM	incorrect	werkzeug	7000	771303
20	meise_correct	778336	1190	meise	779536	2230	781766	2	incorrect	FPM	correct	vogel	3750	785635
21	aprikose_word	787169	1440	orange	788618	1345	789964	2	correct	WPM	incorrect	frucht	4750	790685
21	hobel_word	795468	1190	hobel	796668	1063	797732	1	correct	WPM	correct	werkzeug	2500	798985
21	brokkoli_word	801518	1100	brokkoli	802634	901	803536	1	correct	WPM	correct	gemuese	7000	805034
21	hut_word	812067	1120	kleid	813200	1121	814322	2	correct	WPM	incorrect	kleidungsstueck	16250	831866
22	mais_incorrect	833399	1440	mais	834849	1610	836459	1	incorrect	FPM	incorrect	gemuese	2500	836916
22	kohlrabi_correct	839449	1250	kohlrabi	840715	1531	842246	2	incorrect	FPM	correct	gemuese	4750	842965
22	walze_correct	847748	1170	walze	848931	1527	850459	1	correct	FPM	correct	werkzeug	7000	851265
22	hocker_word	858298	1110	hocker	859414	1101	860515	1	correct	WPM	correct	moebelstueck	0	859414
