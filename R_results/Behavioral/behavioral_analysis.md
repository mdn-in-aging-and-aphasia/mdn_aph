# Load packages

``` r
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(ggplot2)
library(lme4)
library(bbmle)
library(emmeans)
library(ggeffects)
library(stringr)
library(fitdistrplus)
library(see)
library(gghalves)
library(sjPlot)
```

# Load data

``` r
# full df including all sessions
load(here::here("Behavioral/df_behav_06_2022.RData"))

# df only with TMS sessions
load(here::here("Behavioral/df_TMS_06_2022.RData"))
```

``` r
## Count NA values
df_na <- df_behav %>% 
  filter(is.na(response_time) == TRUE) %>% 
  group_by(sub, stim_type, condition) %>% 
  tally(name = "missed")

# remove NAs 
df_behav <- df_behav[!is.na(df_behav$response_time),]
df_TMS <- df_TMS[!is.na(df_TMS$response_time),]
```

# Reaction time

Data frame with only correct reactions for RT analyses

``` r
df_RT <- df_behav[df_behav$correct !=0, ]
```

## Plots

### Plot for all sessions

``` r
# relevel factors
df_RT$stim_type <- factor(df_RT$stim_type, levels = c("baseline", "active", "sham"))
df_RT$condition <- factor(df_RT$condition, levels = c("WPM", "FPM", "tone"))

# Create summary df for RT data incl baseline
summary_rt_full <- df_RT %>% group_by(stim_type, task) %>% 
  summarise(mean_rt = mean(response_time, na.rm = TRUE), 
            md_rt = median(response_time, na.rm = TRUE),
            SD_rt = sd(response_time, na.rm = TRUE), 
            var_rt = var(response_time, na.rm = TRUE),
            std.error_rt = std.error(response_time, na.rm = TRUE))

# Plot by Task
RT_allSessions_tasks <- ggplot(df_RT, aes(x = stim_type, y = response_time, fill = task)) + 
  geom_violinhalf(aes(fill = task), position = position_nudge(x = .2, y = 0), adjust = 1.5, trim = FALSE, alpha = .6, colour = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_rt_full, aes(x = stim_type, y = mean_rt, group = task, colour = task), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_rt_full, aes(x = stim_type, y = mean_rt, group = task, colour = task), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_rt_full, aes(x = stim_type, y = mean_rt, group = task, colour = task, ymin = mean_rt - std.error_rt, ymax = mean_rt + std.error_rt), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values =palet_task, labels = c("Semantic", "Tone")) + 
  scale_fill_manual(values = palet_task, labels = c("Semantic", "Tone")) +
  scale_x_discrete(labels = c("Baseline", "Active", "Sham")) +
  coord_cartesian(ylim = c(0, 3500)) +
  labs(y = "Reaction time in ms") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=12),
        axis.title.x = element_blank())
#facet_wrap(~ stim_order)
RT_allSessions_tasks
```

![](behavioral_analysis_files/figure-markdown_github/RT%20plot%20-%20all%20sessions-1.png)

``` r
###ggsave(plot = RT_allSessions_tasks, filename = paste0(output_path, "Plots/Behavioral/RT_allSessions_byTasks_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")


# Create summary df for RT data incl baseline
summary_rt_full <- df_RT %>% group_by(stim_type, condition) %>% 
  summarise(mean_rt = mean(response_time, na.rm = TRUE), 
            md_rt = median(response_time, na.rm = TRUE),
            SD_rt = sd(response_time, na.rm = TRUE), 
            var_rt = var(response_time, na.rm = TRUE),
            std.error_rt = std.error(response_time, na.rm = TRUE))

# Plot by condition
RT_allSessions_cond <- ggplot(df_RT, aes(x = stim_type, y = response_time, fill = condition)) + 
  geom_violinhalf(aes(fill = condition), position = position_nudge(x = .2, y = 0), adjust = 1.5, trim = FALSE, alpha = .6, colour = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_rt_full, aes(x = stim_type, y = mean_rt, group = condition, colour = condition), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_rt_full, aes(x = stim_type, y = mean_rt, group = condition, colour = condition), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_rt_full, aes(x = stim_type, y = mean_rt, group = condition, colour = condition, ymin = mean_rt - std.error_rt, ymax = mean_rt + std.error_rt), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values =palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Baseline", "Active", "Sham")) +
  coord_cartesian(ylim = c(0, 3500)) +
  labs(y = "Reaction time in ms") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
#facet_wrap(~ stim_order)
RT_allSessions_cond
```

![](behavioral_analysis_files/figure-markdown_github/RT%20plot%20-%20all%20sessions-2.png)

``` r
##ggsave(plot = RT_allSessions_cond, filename = paste0(output_path, "Plots/Behavioral/RT_allSessions_byCond_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")
```

### Plot only TMS sessions

``` r
df_TMS <- df_TMS[df_TMS$correct != 0, ]
df_TMS <- droplevels(df_TMS)
# relevel factors
df_TMS$stim_type <- factor(df_TMS$stim_type, levels = c("active", "sham"))
df_TMS$condition <- factor(df_TMS$condition, levels = c("WPM", "FPM", "tone"))

# Create summary df for RT data only TMS sessions
summary_rt_TMS <- df_TMS %>% group_by(stim_type, task) %>% 
  summarise(mean_rt = mean(response_time, na.rm = TRUE), 
            md_rt = median(response_time, na.rm = TRUE),
            SD_rt = sd(response_time, na.rm = TRUE), 
            var_rt = var(response_time, na.rm = TRUE), 
            std.error_rt = std.error(response_time, na.rm = TRUE))

# Plot by task
RT_TMS_task <- ggplot(df_TMS, aes(x = stim_type, y = response_time, fill = task)) + 
  geom_violinhalf(aes(fill = task), position = position_nudge(x = .2, y = 0), adjust = 1.5, trim = FALSE, alpha = .6, colour = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_rt_TMS, aes(x = stim_type, y = mean_rt, group = task, colour = task), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_rt_TMS, aes(x = stim_type, y = mean_rt, group = task, colour = task), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_rt_TMS, aes(x = stim_type, y = mean_rt, group = task, colour = task, ymin = mean_rt - std.error_rt, ymax = mean_rt + std.error_rt), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_task, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_task, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 3500)) +
  labs(y = "Reaction time in ms") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(),
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
#facet_wrap(~ stim_order)
RT_TMS_task
```

![](behavioral_analysis_files/figure-markdown_github/RT%20plot%20-%20TMS%20sessions-1.png)

``` r
#ggsave(plot = RT_TMS_task, filename = paste0(output_path, "Plots/Behavioral/RT_TMS_byTask_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


  
# Create summary df for RT data only TMS sessions
summary_rt_TMS <- df_TMS %>% group_by(stim_type, condition) %>% 
  summarise(mean_rt = mean(response_time, na.rm = TRUE), 
            md_rt = median(response_time, na.rm = TRUE),
            SD_rt = sd(response_time, na.rm = TRUE), 
            var_rt = var(response_time, na.rm = TRUE), 
            std.error_rt = std.error(response_time, na.rm = TRUE))
  
# Plot by condition
RT_TMS_cond <- ggplot(df_TMS, aes(x = stim_type, y = response_time, fill = condition)) + 
  geom_violinhalf(aes(fill = condition), position = position_nudge(x = .2, y = 0), adjust = 1.5, trim = FALSE, alpha = .6, colour = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_rt_TMS, aes(x = stim_type, y = mean_rt, group = condition, colour = condition), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_rt_TMS, aes(x = stim_type, y = mean_rt, group = condition, colour = condition), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_rt_TMS, aes(x = stim_type, y = mean_rt, group = condition, colour = condition, ymin = mean_rt - std.error_rt, ymax = mean_rt + std.error_rt), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 3500)) +
  labs(y = "Reaction time in ms") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(),
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
#facet_wrap(~ stim_order)
RT_TMS_cond
```

![](behavioral_analysis_files/figure-markdown_github/RT%20plot%20-%20TMS%20sessions-2.png)

``` r
#ggsave(plot = RT_TMS_cond, filename = paste0(output_path, "Plots/Behavioral/RT_TMS_byCond_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")
```

### Spaghetti plot for individual data

``` r
# Create summary data
df_summary_subs <- df_TMS %>% 
  group_by(sub, stim_type, condition) %>% 
  summarise(mean_rt = mean(response_time))

RT_TMS_indivData_WPM <- ggplot(df_summary_subs, aes(x = stim_type, y = mean_rt)) + 
  geom_line(data = df_summary_subs %>% filter(condition == "WPM"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = df_summary_subs %>% filter(condition == "WPM"), color = "#DC964F", size = 3, alpha = 0.7) +
  geom_half_violin(data = df_summary_subs %>% filter(condition == "WPM") %>% filter(stim_type == "active"), fill = "#DC964F", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA, trim = F) +
  geom_half_violin(data = df_summary_subs %>% filter(condition == "WPM") %>% filter(stim_type == "sham"), fill = "#DC964F", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA, trim = F) +
  geom_half_boxplot(data = df_summary_subs %>% filter(condition == "WPM") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = df_summary_subs %>% filter(condition == "WPM") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 1700)) +
  labs(y = "Reaction time in ms") +
  #geom_text(data = df_summary_subs %>% filter(condition == "WPM"), aes(label = sub), position = position_jitter(width = 0.05, height = 0.1)) +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
RT_TMS_indivData_WPM
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-2-1.png)

``` r
#ggsave(plot = RT_TMS_indivData_WPM, filename = paste0(output_path, "Plots/Behavioral/RT_TMS_indivData_WPM_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# FPM
RT_TMS_indivData_FPM <- ggplot(df_summary_subs, aes(x = stim_type, y = mean_rt)) + 
  geom_line(data = df_summary_subs %>% filter(condition == "FPM"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = df_summary_subs %>% filter(condition == "FPM"), color = "gray65", size = 3, alpha = 0.7) +
  geom_half_violin(data = df_summary_subs %>% filter(condition == "FPM") %>% filter(stim_type == "active"), fill = "gray65", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA, trim = F) +
  geom_half_violin(data = df_summary_subs %>% filter(condition == "FPM") %>% filter(stim_type == "sham"), fill = "gray65", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA, trim = F) +
  geom_half_boxplot(data = df_summary_subs %>% filter(condition == "FPM") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = df_summary_subs %>% filter(condition == "FPM") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 1700)) +
  labs(y = "Reaction time in ms") +
  #geom_text(data = df_summary_subs %>% filter(condition == "FPM"), aes(label = sub), position = position_jitter(width = 0.05, height = 0.1)) +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
RT_TMS_indivData_FPM
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-2-2.png)

``` r
#ggsave(plot = RT_TMS_indivData_FPM, filename = paste0(output_path, "Plots/Behavioral/RT_TMS_indivData_FPM_", today, ".pdf"), dpi = 300, width = 20, height = 10, device = "pdf")


# Tone-picture matching
RT_TMS_indivData_tone <- ggplot(df_summary_subs, aes(x = stim_type, y = mean_rt)) + 
  geom_line(data = df_summary_subs %>% filter(condition == "tone"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = df_summary_subs %>% filter(condition == "tone"), color = "#4477AA", size = 3, alpha = 0.7) +
  geom_half_violin(data = df_summary_subs %>% filter(condition == "tone") %>% filter(stim_type == "active"), fill = "#4477AA", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA, trim = F) +
  geom_half_violin(data = df_summary_subs %>% filter(condition == "tone") %>% filter(stim_type == "sham"), fill = "#4477AA", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA, trim = F) +
  geom_half_boxplot(data = df_summary_subs %>% filter(condition == "tone") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = df_summary_subs %>% filter(condition == "tone") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 1700)) +
  labs(y = "Reaction time in ms") +
  #geom_text(data = df_summary_subs %>% filter(condition == "tone"), aes(label = sub), position = position_jitter(width = 0.05, height = 0.1)) +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
RT_TMS_indivData_tone
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-2-3.png)

``` r
#ggsave(plot = RT_TMS_indivData_tone, filename = paste0(output_path, "Plots/Behavioral/RT_TMS_indivData_tone_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")
```

### Arrange spaghetti plots in one plot

``` r
plot_RT <- egg::ggarrange(RT_TMS_indivData_WPM +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          RT_TMS_indivData_FPM +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    axis.title.x = element_blank(),
                    #legend.position = "bottom",
                    #legend.direction = "horizontal"
                    legend.position = "none"),
          RT_TMS_indivData_tone +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1)
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-3-1.png)

``` r
#ggsave(plot = plot_RT, filename = paste0(output_path, "Plots/Behavioral/RT_spaghettiPlots_", today, ".pdf"), dpi = 300, width = 7 , height = 3, device = "pdf")
```

## Mixed-effects regression

### Describe distribution of RT and check if log-transformation is good

``` r
descdist(df_RT$response_time)
```

![](behavioral_analysis_files/figure-markdown_github/Describe%20distribution%20of%20RT-1.png)

    ## summary statistics
    ## ------
    ## min:  316   max:  4587 
    ## median:  987 
    ## mean:  1044.571 
    ## estimated sd:  349.8242 
    ## estimated skewness:  1.179587 
    ## estimated kurtosis:  5.675497

``` r
descdist(df_RT$logRT) # log-transform leads to perfect normal dist
```

![](behavioral_analysis_files/figure-markdown_github/Describe%20distribution%20of%20RT-2.png)

    ## summary statistics
    ## ------
    ## min:  5.755742   max:  8.430981 
    ## median:  6.89467 
    ## mean:  6.899359 
    ## estimated sd:  0.3209043 
    ## estimated skewness:  0.08792711 
    ## estimated kurtosis:  3.075752

### Model including baseline session

Based on our a priori hypotheses, we included fixed effects for session
and condition.

``` r
# relevel factors
df_RT$stim_type <- factor(df_RT$stim_type, levels = c("baseline", "active", "sham"))
df_RT$condition <- factor(df_RT$condition, levels = c("WPM", "FPM", "tone"))
df_RT$age_z <- scale(df_RT$age, scale = F)

table(unlist(df_RT$stim_order))
```

    ## 
    ## Active – Sham Sham – Active 
    ##          7556          7472

``` r
# Sum-coding
contrasts(df_RT$stim_type) <- contr.sum(3)/3
contrasts(df_RT$condition) <- contr.sum(3)/3
contrasts(df_RT$congruency) <- contr.sum(2)/2

# Hierarchichal model selection of random effects structure
RT_RE_0 <- lm(log(response_time) ~ stim_type * condition, data = df_RT)
RT_RE_1 <- lmer(log(response_time) ~ stim_type * condition + (1|sub), data = df_RT, REML = F)
RT_RE_2 <- lmer(log(response_time) ~ stim_type * condition + (1 + stim_type|sub), data = df_RT, REML = F)
RT_RE_3 <- lmer(log(response_time) ~ stim_type * condition + (1 + stim_type|sub) + (1|stimulus_picture), data = df_RT, REML = F)
RT_RE_4 <- lmer(log(response_time) ~ stim_type * condition + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F) # best RE structure
#RT_RE_5 <- lmer(log(response_time) ~ stim_type * condition + (1 + stim_type|sub) + (1|stimulus_audio) + (1|stimulus_picture), data = df_RT, REML = F) #conv warning
AICtab(RT_RE_0, RT_RE_1, RT_RE_2, RT_RE_3, RT_RE_4)
```

    ##         dAIC   df
    ## RT_RE_4    0.0 17
    ## RT_RE_3 1058.4 17
    ## RT_RE_2 1971.3 16
    ## RT_RE_1 2644.0 11
    ## RT_RE_0 8421.5 10

``` r
anova(RT_RE_1, RT_RE_0, RT_RE_2, RT_RE_3, RT_RE_4)
```

    ## Data: df_RT
    ## Models:
    ## RT_RE_0: log(response_time) ~ stim_type * condition
    ## RT_RE_1: log(response_time) ~ stim_type * condition + (1 | sub)
    ## RT_RE_2: log(response_time) ~ stim_type * condition + (1 + stim_type | sub)
    ## RT_RE_3: log(response_time) ~ stim_type * condition + (1 + stim_type | sub) + (1 | stimulus_picture)
    ## RT_RE_4: log(response_time) ~ stim_type * condition + (1 + stim_type | sub) + (1 | stimulus_audio)
    ##         npar     AIC     BIC  logLik deviance   Chisq Df            Pr(>Chisq)
    ## RT_RE_0   10  6767.5  6843.7 -3373.8   6747.5                                 
    ## RT_RE_1   11   990.0  1073.8  -484.0    968.0 5779.50  1 < 0.00000000000000022
    ## RT_RE_2   16   317.4   439.2  -142.7    285.4  682.66  5 < 0.00000000000000022
    ## RT_RE_3   17  -595.6  -466.1   314.8   -629.6  914.96  1 < 0.00000000000000022
    ## RT_RE_4   17 -1654.0 -1524.5   844.0  -1688.0 1058.37  0                      
    ##            
    ## RT_RE_0    
    ## RT_RE_1 ***
    ## RT_RE_2 ***
    ## RT_RE_3 ***
    ## RT_RE_4    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# Hierarchichal model selection of fixed effects structure
RT_FE_0 <-  lmer(log(response_time) ~ stim_type * condition + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_FE_stimOrder <- lmer(log(response_time) ~ stim_type * condition + stim_order + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_FE_task <- lmer(log(response_time) ~ stim_type * condition + task + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_FE_congruency <- lmer(log(response_time) ~ stim_type * condition + congruency + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_FE_age <- lmer(log(response_time) ~ stim_type * condition + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_FE_full <- lmer(log(response_time) ~ stim_type * condition + congruency + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)# winning model
AICtab(RT_FE_0, RT_FE_stimOrder, RT_FE_congruency, RT_FE_age, RT_FE_task, RT_FE_full)
```

    ##                  dAIC  df
    ## RT_FE_full         0.0 19
    ## RT_FE_congruency   7.4 18
    ## RT_FE_age        222.8 18
    ## RT_FE_0          230.3 17
    ## RT_FE_task       230.3 17
    ## RT_FE_stimOrder  232.3 18

``` r
anova(RT_FE_0, RT_FE_stimOrder, RT_FE_congruency, RT_FE_age, RT_FE_full)
```

    ## Data: df_RT
    ## Models:
    ## RT_FE_0: log(response_time) ~ stim_type * condition + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_FE_stimOrder: log(response_time) ~ stim_type * condition + stim_order + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_FE_congruency: log(response_time) ~ stim_type * condition + congruency + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_FE_age: log(response_time) ~ stim_type * condition + age + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_FE_full: log(response_time) ~ stim_type * condition + congruency + age + (1 + stim_type | sub) + (1 | stimulus_audio)
    ##                  npar     AIC     BIC logLik deviance    Chisq Df
    ## RT_FE_0            17 -1654.0 -1524.5 843.99  -1688.0            
    ## RT_FE_stimOrder    18 -1652.0 -1514.9 844.00  -1688.0   0.0275  1
    ## RT_FE_congruency   18 -1876.9 -1739.7 956.43  -1912.9 224.8533  0
    ## RT_FE_age          18 -1661.4 -1524.3 848.70  -1697.4   0.0000  0
    ## RT_FE_full         19 -1884.2 -1739.5 961.13  -1922.2 224.8462  1
    ##                           Pr(>Chisq)    
    ## RT_FE_0                                 
    ## RT_FE_stimOrder               0.8683    
    ## RT_FE_congruency                        
    ## RT_FE_age                               
    ## RT_FE_full       <0.0000000000000002 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# Hierarchichal model selection of interaction structure
RT_INT_0 <- lmer(log(response_time) ~ stim_type * condition + congruency + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_INT_1 <- lmer(log(response_time) ~ stim_type * condition * congruency + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_INT_2 <- lmer(log(response_time) ~ stim_type * condition + stim_type * congruency + condition * congruency + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
#RT_INT_3 <- lmer(log(response_time) ~ stim_type * condition + congruency + stim_type:congruency + condition:congruency + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F)
RT_INT_4 <- lmer(log(response_time) ~ stim_type + condition + congruency + condition:congruency + condition:stim_type + scale(age) + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F) # winning model
AICtab(RT_INT_0, RT_INT_1, RT_INT_2, RT_INT_4)
```

    ##          dAIC  df
    ## RT_INT_4   0.0 21
    ## RT_INT_2   2.1 23
    ## RT_INT_1   8.3 27
    ## RT_INT_0 302.5 19

``` r
anova(RT_INT_0, RT_INT_1, RT_INT_2, RT_INT_4)
```

    ## Data: df_RT
    ## Models:
    ## RT_INT_0: log(response_time) ~ stim_type * condition + congruency + age + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_INT_4: log(response_time) ~ stim_type + condition + congruency + condition:congruency + condition:stim_type + scale(age) + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_INT_2: log(response_time) ~ stim_type * condition + stim_type * congruency + condition * congruency + age + (1 + stim_type | sub) + (1 | stimulus_audio)
    ## RT_INT_1: log(response_time) ~ stim_type * condition * congruency + age + (1 + stim_type | sub) + (1 | stimulus_audio)
    ##          npar     AIC     BIC  logLik deviance    Chisq Df          Pr(>Chisq)
    ## RT_INT_0   19 -1884.2 -1739.5  961.13  -1922.2                                
    ## RT_INT_4   21 -2186.8 -2026.8 1114.39  -2228.8 306.5258  2 <0.0000000000000002
    ## RT_INT_2   23 -2184.7 -2009.5 1115.35  -2230.7   1.9189  2              0.3831
    ## RT_INT_1   27 -2178.5 -1972.8 1116.23  -2232.5   1.7672  4              0.7785
    ##             
    ## RT_INT_0    
    ## RT_INT_4 ***
    ## RT_INT_2    
    ## RT_INT_1    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
RT_model <- RT_INT_4
summary(RT_model)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: 
    ## log(response_time) ~ stim_type + condition + congruency + condition:congruency +  
    ##     condition:stim_type + scale(age) + (1 + stim_type | sub) +  
    ##     (1 | stimulus_audio)
    ##    Data: df_RT
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##  -2186.8  -2026.8   1114.4  -2228.8    15007 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -6.5106 -0.6464 -0.0727  0.5525  6.4906 
    ## 
    ## Random effects:
    ##  Groups         Name        Variance Std.Dev. Corr       
    ##  stimulus_audio (Intercept) 0.01365  0.1168              
    ##  sub            (Intercept) 0.02165  0.1471              
    ##                 stim_type1  0.04547  0.2132   -0.31      
    ##                 stim_type2  0.01465  0.1211    0.30 -0.64
    ##  Residual                   0.04689  0.2165              
    ## Number of obs: 15028, groups:  stimulus_audio, 312; sub, 30
    ## 
    ## Fixed effects:
    ##                         Estimate Std. Error t value
    ## (Intercept)             6.904767   0.028491 242.352
    ## stim_type1              0.188394   0.039669   4.749
    ## stim_type2             -0.087582   0.023376  -3.747
    ## condition1             -0.302728   0.035460  -8.537
    ## condition2              0.388163   0.032395  11.982
    ## congruency1            -0.042445   0.006671  -6.363
    ## scale(age)              0.086550   0.025458   3.400
    ## condition1:congruency1  0.100824   0.022358   4.510
    ## condition2:congruency1  0.241638   0.036934   6.542
    ## stim_type1:condition1  -0.139463   0.031128  -4.480
    ## stim_type2:condition1   0.040516   0.031229   1.297
    ## stim_type1:condition2  -0.079500   0.031654  -2.512
    ## stim_type2:condition2   0.061330   0.031986   1.917

``` r
drop1(RT_model, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(response_time) ~ stim_type + condition + congruency + condition:congruency + 
    ##     condition:stim_type + scale(age) + (1 + stim_type | sub) + 
    ##     (1 | stimulus_audio)
    ##                      npar     AIC     LRT               Pr(Chi)    
    ## <none>                    -2186.8                                  
    ## scale(age)              1 -2179.4   9.399              0.002171 ** 
    ## condition:congruency    2 -1884.2 306.526 < 0.00000000000000022 ***
    ## stim_type:condition     4 -2150.4  44.397        0.000000005306 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emmeans(RT_model, pairwise ~ stim_type | condition, adjust = "holm", type = "response")
```

    ## $emmeans
    ## condition = WPM:
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline       945 27.4 Inf       893      1000
    ##  active         879 28.6 Inf       825       937
    ##  sham           881 29.0 Inf       826       940
    ## 
    ## condition = FPM:
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline      1198 33.3 Inf      1134      1265
    ##  active        1110 35.0 Inf      1043      1180
    ##  sham          1099 35.0 Inf      1033      1170
    ## 
    ## condition = tone:
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline      1057 37.8 Inf       986      1134
    ##  active         931 36.0 Inf       863      1004
    ##  sham           925 36.1 Inf       857       998
    ## 
    ## Results are averaged over the levels of: congruency 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ## condition = WPM:
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active 1.075 0.0214 Inf    1   3.618  0.0009
    ##  baseline / sham   1.072 0.0247 Inf    1   3.033  0.0048
    ##  active / sham     0.998 0.0137 Inf    1  -0.151  0.8797
    ## 
    ## condition = FPM:
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active 1.079 0.0216 Inf    1   3.816  0.0004
    ##  baseline / sham   1.089 0.0252 Inf    1   3.697  0.0004
    ##  active / sham     1.009 0.0141 Inf    1   0.659  0.5101
    ## 
    ## condition = tone:
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active 1.136 0.0232 Inf    1   6.264  <.0001
    ##  baseline / sham   1.143 0.0268 Inf    1   5.696  <.0001
    ##  active / sham     1.006 0.0145 Inf    1   0.425  0.6710
    ## 
    ## Results are averaged over the levels of: congruency 
    ## Degrees-of-freedom method: asymptotic 
    ## P value adjustment: holm method for 3 tests 
    ## Tests are performed on the log scale

``` r
emmeans(RT_model, pairwise ~ congruency | condition, adjust = "holm", type = "response")
```

    ## $emmeans
    ## condition = WPM:
    ##  congruency  response   SE  df asymp.LCL asymp.UCL
    ##  congruent        897 26.6 Inf       847       951
    ##  incongruent      905 26.8 Inf       854       959
    ## 
    ## condition = FPM:
    ##  congruency  response   SE  df asymp.LCL asymp.UCL
    ##  congruent       1157 34.4 Inf      1091      1226
    ##  incongruent     1113 33.0 Inf      1050      1180
    ## 
    ## condition = tone:
    ##  congruency  response   SE  df asymp.LCL asymp.UCL
    ##  congruent        896 32.5 Inf       835       962
    ##  incongruent     1048 38.0 Inf       976      1125
    ## 
    ## Results are averaged over the levels of: stim_type 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ## condition = WPM:
    ##  contrast                ratio      SE  df null z.ratio p.value
    ##  congruent / incongruent 0.991 0.00571 Inf    1  -1.533  0.1252
    ## 
    ## condition = FPM:
    ##  contrast                ratio      SE  df null z.ratio p.value
    ##  congruent / incongruent 1.039 0.01862 Inf    1   2.126  0.0335
    ## 
    ## condition = tone:
    ##  contrast                ratio      SE  df null z.ratio p.value
    ##  congruent / incongruent 0.855 0.00579 Inf    1 -23.111  <.0001
    ## 
    ## Results are averaged over the levels of: stim_type 
    ## Degrees-of-freedom method: asymptotic 
    ## Tests are performed on the log scale

``` r
RT_model_mainEffects <- lmer(log(response_time) ~ stim_type + condition + congruency + age + (1 + stim_type|sub) + (1|stimulus_audio), data = df_RT, REML = F) 
drop1(RT_model_mainEffects, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(response_time) ~ stim_type + condition + congruency + age + 
    ##     (1 + stim_type | sub) + (1 | stimulus_audio)
    ##            npar     AIC     LRT               Pr(Chi)    
    ## <none>          -1848.9                                  
    ## stim_type     2 -1836.6  16.349             0.0002817 ***
    ## condition     2 -1690.8 162.111 < 0.00000000000000022 ***
    ## congruency    1 -1626.6 224.291 < 0.00000000000000022 ***
    ## age           1 -1841.6   9.362             0.0022150 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emmeans(RT_model, pairwise ~ stim_type, adjust = "holm", type = "response")
```

    ## $emmeans
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline      1062 29.5 Inf      1005      1121
    ##  active         968 30.5 Inf       910      1030
    ##  sham           964 30.7 Inf       906      1026
    ## 
    ## Results are averaged over the levels of: condition, congruency 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active   1.1 0.0210 Inf    1   4.814  <.0001
    ##  baseline / sham     1.1 0.0246 Inf    1   4.308  <.0001
    ##  active / sham       1.0 0.0126 Inf    1   0.350  0.7260
    ## 
    ## Results are averaged over the levels of: condition, congruency 
    ## Degrees-of-freedom method: asymptotic 
    ## P value adjustment: holm method for 3 tests 
    ## Tests are performed on the log scale

``` r
RT_age_effect <- ggpredict(RT_model, terms = c("age[all]"))
RT_age <- plot(RT_age_effect) +
  apatheme +
  ylab("Response time in ms") +
  xlab("Age") +
  coord_cartesian(ylim = c(650, 1350)) 
#ggsave(plot = RT_age, filename = paste0(output_path, "Plots/Behavioral/RT_age_effect_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


RT_condCong_effect <- ggpredict(RT_model, terms = c("congruency", "condition"))
RT_condCong <- plot(RT_condCong_effect) +
  scale_fill_manual(values = palet_cond) +
  scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  coord_cartesian(ylim = c(650, 1350)) +
  ylab("Response time in ms") +
  #scale_x_discrete(labels = c("congruent" = "Congruent", "incongruent" = "Incongruent")) +
  apatheme +
  theme(axis.title.x = element_blank(),
        legend.position = "none",
        legend.title = element_blank(),
        legend.text=element_text(size=6))
#ggsave(plot = RT_condCong, filename = paste0(output_path, "Plots/Behavioral/RT_condCong_effect_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")

RT_condStim_effect <- ggpredict(RT_model, terms = c("stim_type", "condition"))
RT_condStim <- plot(RT_condStim_effect) +
  scale_fill_manual(values = palet_cond) +
  scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  coord_cartesian(ylim = c(650, 1350)) +
  ylab("Response time in ms") +
  #scale_x_discrete(labels = c("baseline" = "Baseline", "active" = "Active", "Sham" = "sham")) +
  apatheme +
  theme(axis.title.x = element_blank(),
        legend.position = "none",
        legend.title = element_blank(),
        legend.text=element_text(size=6))
#ggsave(plot = RT_condStim, filename = paste0(output_path, "Plots/Behavioral/RT_condStim_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")

plot_RT_model <- egg::ggarrange(RT_age +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          RT_condCong +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    axis.title.x = element_blank(),
                    #legend.position = "bottom",
                    #legend.direction = "horizontal"
                    legend.position = "none"),
          RT_condStim +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1,
          widths = c(1,0.6,1))
```

![](behavioral_analysis_files/figure-markdown_github/RT%20-%20Model%20including%20baseline%20session-1.png)

``` r
#ggsave(plot = plot_RT_model, filename = paste0(output_path, "Plots/Behavioral/RT_model_plots_", today, ".pdf"), dpi = 300, width = 7, height = 3, device = "pdf")
```

### Model controlling for baseline: active-baseline & sham-baseline and then compare

``` r
# Model controlling for baseline: active-baseline & sham-baseline
# prepare df
df_diff <- df_behav
df_diff <- df_diff %>% 
  group_by(sub, stim_type, condition, category) %>% 
  summarise(meanRT = mean(response_time))
df_diff <- df_diff %>% 
  pivot_wider(names_from = stim_type, values_from = meanRT)
df_diff$active_diff <- df_diff$active - df_diff$baseline
df_diff$sham_diff <- df_diff$sham - df_diff$baseline
df_diff <- df_diff %>% 
  pivot_longer(cols = 7:8, names_to = "stim_type", values_to = "meanRT_diff")

# relevel factors
df_diff$stim_type <- factor(df_diff$stim_type, levels = c("active_diff", "sham_diff"))
df_diff$condition <- factor(df_diff$condition, levels = c("WPM", "FPM", "tone"))

m_RT <- lmer(meanRT_diff ~ stim_type + condition + (1 + stim_type|sub), data = df_diff, REML = F)
summary(m_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: meanRT_diff ~ stim_type + condition + (1 + stim_type | sub)
    ##    Data: df_diff
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##  13050.2  13089.6  -6517.1  13034.2     1012 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -3.5299 -0.5776  0.0361  0.5382  5.7592 
    ## 
    ## Random effects:
    ##  Groups   Name               Variance Std.Dev. Corr 
    ##  sub      (Intercept)        10224    101.11        
    ##           stim_typesham_diff  3740     61.16   -0.09
    ##  Residual                    18457    135.86        
    ## Number of obs: 1020, groups:  sub, 30
    ## 
    ## Fixed effects:
    ##                    Estimate Std. Error t value
    ## (Intercept)         -72.081     19.933  -3.616
    ## stim_typesham_diff   -1.492     14.038  -0.106
    ## conditionFPM        -19.986      8.770  -2.279
    ## conditiontone       -67.838     18.603  -3.647
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) stm_t_ cndFPM
    ## stm_typshm_ -0.194              
    ## conditinFPM -0.220  0.000       
    ## conditiontn -0.104  0.000  0.236

``` r
drop1(m_RT, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## meanRT_diff ~ stim_type + condition + (1 + stim_type | sub)
    ##           npar   AIC     LRT  Pr(Chi)    
    ## <none>         13050                     
    ## stim_type    1 13048  0.0113 0.915354    
    ## condition    2 13062 15.3085 0.000474 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emmeans(m_RT, pairwise ~ stim_type | condition, adjust = "holm", type = "response")
```

    ## $emmeans
    ## condition = WPM:
    ##  stim_type   emmean   SE   df lower.CL upper.CL
    ##  active_diff  -72.1 20.3 34.5     -113    -30.9
    ##  sham_diff    -73.6 22.4 33.8     -119    -28.0
    ## 
    ## condition = FPM:
    ##  stim_type   emmean   SE   df lower.CL upper.CL
    ##  active_diff  -92.1 20.3 34.5     -133    -50.9
    ##  sham_diff    -93.6 22.4 33.8     -139    -48.0
    ## 
    ## condition = tone:
    ##  stim_type   emmean   SE   df lower.CL upper.CL
    ##  active_diff -139.9 26.1 95.2     -192    -88.1
    ##  sham_diff   -141.4 27.8 80.7     -197    -86.1
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## condition = WPM:
    ##  contrast                estimate   SE df t.ratio p.value
    ##  active_diff - sham_diff     1.49 14.3 31   0.105  0.9174
    ## 
    ## condition = FPM:
    ##  contrast                estimate   SE df t.ratio p.value
    ##  active_diff - sham_diff     1.49 14.3 31   0.105  0.9174
    ## 
    ## condition = tone:
    ##  contrast                estimate   SE df t.ratio p.value
    ##  active_diff - sham_diff     1.49 14.3 31   0.105  0.9174
    ## 
    ## Degrees-of-freedom method: kenward-roger

``` r
plot(ggpredict(m_RT, terms = c("stim_type", "condition")))
```

![](behavioral_analysis_files/figure-markdown_github/RT%20-%20Model%20controlling%20for%20baseline%20session-1.png)

``` r
#emtrends(mdl, pairwise ~ stim, var = "age", transform = "response", adjust = "bonferroni")
```

### Model only for TMS sessions with difference score as DV

``` r
# relevel factors
df_TMS$stim_type <- factor(df_TMS$stim_type, levels = c("active", "sham"))
df_TMS$condition <- factor(df_TMS$condition, levels = c("WPM", "FPM", "tone"))

df_diff_TMS <- df_TMS
df_diff_TMS <- df_diff_TMS %>% 
  group_by(sub, stim_type, condition, category) %>% 
  summarise(meanRT = mean(response_time))
df_diff_TMS <- df_diff_TMS %>% 
  pivot_wider(names_from = stim_type, values_from = meanRT)
df_diff_TMS$RT_diff <- df_diff_TMS$active - df_diff_TMS$sham
df_diff_TMS <- df_diff_TMS %>% 
  pivot_longer(cols = 4:5, names_to = "stim_type", values_to = "meanRT")

m_RT <- lmer(RT_diff ~ condition + (1|sub), data = df_diff_TMS, REML = F)
summary(m_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: RT_diff ~ condition + (1 | sub)
    ##    Data: df_diff_TMS
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##  12716.1  12740.7  -6353.0  12706.1     1015 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -3.7178 -0.6174 -0.0091  0.5122  3.8307 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept)  5268     72.58  
    ##  Residual             13929    118.02  
    ## Number of obs: 1020, groups:  sub, 30
    ## 
    ## Fixed effects:
    ##               Estimate Std. Error t value
    ## (Intercept)     -5.086     14.305  -0.356
    ## conditionFPM    12.821      7.618   1.683
    ## conditiontone   17.185     16.161   1.063
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) cndFPM
    ## conditinFPM -0.266       
    ## conditiontn -0.126  0.236

``` r
drop1(m_RT, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## RT_diff ~ condition + (1 | sub)
    ##           npar   AIC    LRT Pr(Chi)
    ## <none>         12716               
    ## condition    2 12715 3.2975  0.1923

``` r
emmeans(m_RT, pairwise ~ condition, adjust = "holm", type = "response")
```

    ## $emmeans
    ##  condition emmean   SE    df lower.CL upper.CL
    ##  WPM        -5.09 14.5  36.3    -34.6     24.4
    ##  FPM         7.74 14.5  36.3    -21.7     37.2
    ##  tone       12.10 20.4 138.3    -28.2     52.4
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ##  contrast   estimate    SE  df t.ratio p.value
    ##  WPM - FPM    -12.82  7.63 992  -1.681  0.2791
    ##  WPM - tone   -17.19 16.18 992  -1.062  0.5767
    ##  FPM - tone    -4.36 16.18 992  -0.270  0.7874
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## P value adjustment: holm method for 3 tests

``` r
RT_cond_effect <- ggpredict(m_RT, terms = c("condition"))
plot(RT_cond_effect)
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-4-1.png)

``` r
emmeans(RT_model, pairwise ~ stim_type | condition, adjust = "holm", type = "response")
```

    ## $emmeans
    ## condition = WPM:
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline       945 27.4 Inf       893      1000
    ##  active         879 28.6 Inf       825       937
    ##  sham           881 29.0 Inf       826       940
    ## 
    ## condition = FPM:
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline      1198 33.3 Inf      1134      1265
    ##  active        1110 35.0 Inf      1043      1180
    ##  sham          1099 35.0 Inf      1033      1170
    ## 
    ## condition = tone:
    ##  stim_type response   SE  df asymp.LCL asymp.UCL
    ##  baseline      1057 37.8 Inf       986      1134
    ##  active         931 36.0 Inf       863      1004
    ##  sham           925 36.1 Inf       857       998
    ## 
    ## Results are averaged over the levels of: congruency 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ## condition = WPM:
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active 1.075 0.0214 Inf    1   3.618  0.0009
    ##  baseline / sham   1.072 0.0247 Inf    1   3.033  0.0048
    ##  active / sham     0.998 0.0137 Inf    1  -0.151  0.8797
    ## 
    ## condition = FPM:
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active 1.079 0.0216 Inf    1   3.816  0.0004
    ##  baseline / sham   1.089 0.0252 Inf    1   3.697  0.0004
    ##  active / sham     1.009 0.0141 Inf    1   0.659  0.5101
    ## 
    ## condition = tone:
    ##  contrast          ratio     SE  df null z.ratio p.value
    ##  baseline / active 1.136 0.0232 Inf    1   6.264  <.0001
    ##  baseline / sham   1.143 0.0268 Inf    1   5.696  <.0001
    ##  active / sham     1.006 0.0145 Inf    1   0.425  0.6710
    ## 
    ## Results are averaged over the levels of: congruency 
    ## Degrees-of-freedom method: asymptotic 
    ## P value adjustment: holm method for 3 tests 
    ## Tests are performed on the log scale

# Accuracy

## Plots

### Plots by accuracy

#### Prepare summary df

``` r
# relevel factors
df_behav$stim_type <- factor(df_behav$stim_type, levels = c("baseline", "active", "sham"))
df_behav$condition <- factor(df_behav$condition, levels = c("WPM", "FPM", "tone"))
df_behav$incorrect <- ifelse(df_behav$accuracy == "correct", 0, 1) 

summary_acc <- df_behav %>% 
  group_by(sub, stim_type, task, condition) %>% 
  summarise(correct = sum(correct, na.rm = T)) # build df with summarized values per subject, stimulation, and condition

summary_acc <- merge(summary_acc, df_na, all = T)
summary_acc[is.na(summary_acc)] <- 0
summary_acc <- summary_acc %>% 
  mutate(perc = case_when(condition == "WPM" ~ correct*100/(64-missed),
                          condition == "FPM" ~ correct*100/(64-missed),
                          condition == "tone" ~ correct*100/(48-missed)))
summary_acc <- summary_acc[summary_acc$perc > 70,]

## Create summary df for RT data incl baseline
summary_acc_group_task <- summary_acc %>% group_by(stim_type, task) %>% 
  summarise(mean_acc = mean(perc), 
            std.error_acc = std.error(perc))

summary_acc_group_condition <- summary_acc %>% group_by(stim_type, condition) %>% 
  summarise(mean_acc = mean(perc), 
            std.error_acc = std.error(perc))

############ Same procedure for errors
summary_errors <- df_behav %>% 
  group_by(sub, stim_type, task, condition) %>% 
  summarise(errors = sum(incorrect, na.rm = T))

summary_errors <- merge(summary_errors, df_na, all = T)
summary_errors[is.na(summary_errors)] <- 0
summary_errors <- summary_errors %>% 
  mutate(perc = case_when(condition == "WPM" ~ errors*100/(64-missed),
                          condition == "FPM" ~ errors*100/(64-missed),
                          condition == "tone" ~ errors*100/(48-missed)))
summary_errors <- summary_errors[summary_errors$perc < 25,]

## Create summary df for RT data incl baseline
summary_err_group_task <- summary_errors %>% group_by(stim_type, task) %>% 
  summarise(mean_err = mean(perc), 
            std.error_err = std.error(perc))

summary_err_group_condition <- summary_errors %>% group_by(stim_type, condition) %>% 
  summarise(mean_err = mean(perc), 
            std.error_err = std.error(perc))
```

#### Plot for all sessions

``` r
# Plot by task
Acc_allSessions_tasks <- ggplot(summary_acc, aes(x = stim_type, y = perc, fill = task)) +
  geom_violinhalf(aes(fill = task), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_acc_group_task, aes(x = stim_type, y = mean_acc, group = task, colour = task), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_acc_group_task, aes(x = stim_type, y = mean_acc, group = task, colour = task), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_acc_group_task, aes(x = stim_type, y = mean_acc, group = task, colour = task, ymin = mean_acc - std.error_acc, ymax = mean_acc + std.error_acc), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_task, labels = c("Semantic", "Tone")) + 
  scale_fill_manual(values = palet_task, labels = c("Semantic", "Tone")) +
  scale_x_discrete(labels = c("Baseline", "Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank())
Acc_allSessions_tasks  
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20all%20sessions-1.png)

``` r
#ggsave(plot = Acc_allSessions_tasks, filename = paste0(output_path, "Plots/Behavioral/Acc_allSessions_byTask_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")


# Plot by condition
Acc_allSessions_cond <- ggplot(summary_acc, aes(x = stim_type, y = perc, fill = condition)) +
  geom_violinhalf(aes(fill = condition), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_acc_group_condition, aes(x = stim_type, y = mean_acc, group = condition, colour = condition), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_acc_group_condition, aes(x = stim_type, y = mean_acc, group = condition, colour = condition), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_acc_group_condition, aes(x = stim_type, y = mean_acc, group = condition, colour = condition, ymin = mean_acc - std.error_acc, ymax = mean_acc + std.error_acc), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Baseline", "Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=10),
        axis.title.x = element_blank())
Acc_allSessions_cond
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20all%20sessions-2.png)

``` r
#ggsave(plot = Acc_allSessions_cond, filename = paste0(output_path, "Plots/Behavioral/Acc_allSessions_byCond_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")
```

#### Plot only TMS sessions

``` r
summary_acc_TMS <- filter(summary_acc, stim_type != "baseline")
summary_acc_TMS <- droplevels(summary_acc_TMS)

## Create summary df for accuracy data
summary_acc_group_TMS_task <- summary_acc_TMS %>% group_by(stim_type, task) %>% 
  summarise(mean_acc = mean(perc), 
            std.error_acc = std.error(perc))

summary_acc_group_TMS_condition <- summary_acc_TMS %>% group_by(stim_type, condition) %>% 
  summarise(mean_acc = mean(perc), 
            std.error_acc = std.error(perc))

# Plot by task
Acc_TMS_task <- ggplot(summary_acc_TMS, aes(x = stim_type, y = perc, fill = task)) +
  geom_violinhalf(aes(fill = task), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_acc_group_TMS_task, aes(x = stim_type, y = mean_acc, group = task, colour = task), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_acc_group_TMS_task, aes(x = stim_type, y = mean_acc, group = task, colour = task), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_acc_group_TMS_task, aes(x = stim_type, y = mean_acc, group = task, colour = task, ymin = mean_acc - std.error_acc, ymax = mean_acc + std.error_acc), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_task, labels = c("Semantic", "Tone")) + 
  scale_fill_manual(values = palet_task, labels = c("Semantic", "Tone")) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank())
Acc_TMS_task 
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20TMS%20sessions-1.png)

``` r
#ggsave(plot = Acc_TMS_task, filename = paste0(output_path, "Plots/Behavioral/Acc_TMS_byTask_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# Plot by condition
Acc_TMS_cond <- ggplot(summary_acc_TMS, aes(x = stim_type, y = perc, fill = condition)) +
  geom_violinhalf(aes(fill = condition), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_acc_group_TMS_condition, aes(x = stim_type, y = mean_acc, group = condition, colour = condition), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_acc_group_TMS_condition, aes(x = stim_type, y = mean_acc, group = condition, colour = condition), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_acc_group_TMS_condition, aes(x = stim_type, y = mean_acc, group = condition, colour = condition, ymin = mean_acc - std.error_acc, ymax = mean_acc + std.error_acc), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values =palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=10),
        axis.title.x = element_blank())
Acc_TMS_cond
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20TMS%20sessions-2.png)

``` r
#ggsave(plot = Acc_TMS_cond, filename = paste0(output_path, "Plots/Behavioral/Acc_TMS_byCond_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")
```

#### Spaghetti plot for individual data

``` r
# WPM
Acc_TMS_indivData_WPM <- ggplot(summary_acc_TMS, aes(x = stim_type, y = perc)) + 
  geom_line(data = summary_acc_TMS %>% filter(condition == "WPM"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = summary_acc_TMS %>% filter(condition == "WPM"), color = "#DC964F", size = 3, alpha = 0.7) +
  geom_half_violin(data = summary_acc_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "active"), fill = "#DC964F", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA) +
  geom_half_violin(data = summary_acc_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "sham"), fill = "#DC964F", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA) +
  geom_half_boxplot(data = summary_acc_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = summary_acc_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
Acc_TMS_indivData_WPM
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-6-1.png)

``` r
#ggsave(plot = Acc_TMS_indivData_WPM, filename = paste0(output_path, "Plots/Behavioral/Acc_TMS_indivData_WPM_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# FPM
Acc_TMS_indivData_FPM <- ggplot(summary_acc_TMS, aes(x = stim_type, y = perc)) + 
  geom_line(data = summary_acc_TMS %>% filter(condition == "FPM"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = summary_acc_TMS %>% filter(condition == "FPM"), color = "gray60", size = 3, alpha = 0.7) +
  geom_half_violin(data = summary_acc_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "active"), fill = "gray60", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA) +
  geom_half_violin(data = summary_acc_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "sham"), fill = "gray60", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA) +
  geom_half_boxplot(data = summary_acc_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = summary_acc_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
Acc_TMS_indivData_FPM
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-6-2.png)

``` r
#ggsave(plot = Acc_TMS_indivData_FPM, filename = paste0(output_path, "Plots/Behavioral/Acc_TMS_indivData_FPM_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# Tone-picture matching
Acc_TMS_indivData_tone <- ggplot(summary_acc_TMS, aes(x = stim_type, y = perc)) + 
  geom_line(data = summary_acc_TMS %>% filter(condition == "tone"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = summary_acc_TMS %>% filter(condition == "tone"), color = "#4477AA", size = 3, alpha = 0.7) +
  geom_half_violin(data = summary_acc_TMS %>% filter(condition == "tone") %>% filter(stim_type == "active"), fill = "#4477AA", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA) +
  geom_half_violin(data = summary_acc_TMS %>% filter(condition == "tone") %>% filter(stim_type == "sham"), fill = "#4477AA", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA) +
  geom_half_boxplot(data = summary_acc_TMS %>% filter(condition == "tone") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = summary_acc_TMS %>% filter(condition == "tone") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Accuracy in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
Acc_TMS_indivData_tone
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-6-3.png)

``` r
#ggsave(plot = Acc_TMS_indivData_tone, filename = paste0(output_path, "Plots/Behavioral/Acc_TMS_indivData_tone_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")
```

#### Arrange spaghetti plots in one plot

``` r
plot_Acc <- egg::ggarrange(Acc_TMS_indivData_WPM +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          Acc_TMS_indivData_FPM +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    axis.title.x = element_blank(),
                    #legend.position = "bottom",
                    #legend.direction = "horizontal"
                    legend.position = "none"),
          Acc_TMS_indivData_tone +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1)
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-7-1.png)

``` r
#ggsave(plot = plot_Acc, filename = paste0(output_path, "Plots/Behavioral/Acc_spaghettiPlots_", today, ".pdf"), dpi = 300, width = 7 , height = 3, device = "pdf")
```

### Plot by errors

#### Plot for all sessions

``` r
# Plot by task
Err_allSessions_tasks <- ggplot(summary_errors, aes(x = stim_type, y = perc, fill = task)) +
  geom_violinhalf(aes(fill = task), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_err_group_task, aes(x = stim_type, y = mean_err, group = task, colour = task), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_err_group_task, aes(x = stim_type, y = mean_err, group = task, colour = task), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_err_group_task, aes(x = stim_type, y = mean_err, group = task, colour = task, ymin = mean_err - std.error_err, ymax = mean_err + std.error_err), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_task, labels = c("Semantic", "Tone")) + 
  scale_fill_manual(values = palet_task, labels = c("Semantic", "Tone")) +
  scale_x_discrete(labels = c("Baseline", "Active", "Sham")) +
  #coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank())
Err_allSessions_tasks  
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20all%20sessions,%20by%20errors-1.png)

``` r
#ggsave(plot = Err_allSessions_tasks, filename = paste0(output_path, "Plots/Behavioral/Errors_allSessions_byTask_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")


# Plot by condition
Err_allSessions_cond <- ggplot(summary_errors, aes(x = stim_type, y = perc, fill = condition)) +
  geom_violinhalf(aes(fill = condition), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_err_group_condition, aes(x = stim_type, y = mean_err, group = condition, colour = condition), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_err_group_condition, aes(x = stim_type, y = mean_err, group = condition, colour = condition), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_err_group_condition, aes(x = stim_type, y = mean_err, group = condition, colour = condition, ymin = mean_err - std.error_err, ymax = mean_err + std.error_err), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Baseline", "Active", "Sham")) +
  #coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=10),
        axis.title.x = element_blank())
Err_allSessions_cond
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20all%20sessions,%20by%20errors-2.png)

``` r
#ggsave(plot = Err_allSessions_cond, filename = paste0(output_path, "Plots/Behavioral/Err_allSessions_byCond_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")
```

#### Plot only TMS sessions

``` r
summary_err_TMS <- filter(summary_errors, stim_type != "baseline")
summary_err_TMS <- droplevels(summary_err_TMS)

## Create summary df for accuracy data
summary_err_group_TMS_task <- summary_err_TMS %>% group_by(stim_type, task) %>% 
  summarise(mean_err = mean(perc), 
            std.error_err = std.error(perc))

summary_err_group_TMS_condition <- summary_err_TMS %>% group_by(stim_type, condition) %>% 
  summarise(mean_err = mean(perc), 
            std.error_err = std.error(perc))

# Plot by task
Err_TMS_task <- ggplot(summary_err_TMS, aes(x = stim_type, y = perc, fill = task)) +
  geom_violinhalf(aes(fill = task), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_err_group_TMS_task, aes(x = stim_type, y = mean_err, group = task, colour = task), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_err_group_TMS_task, aes(x = stim_type, y = mean_err, group = task, colour = task), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_err_group_TMS_task, aes(x = stim_type, y = mean_err, group = task, colour = task, ymin = mean_err - std.error_err, ymax = mean_err + std.error_err), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values = palet_task, labels = c("Semantic", "Tone")) + 
  scale_fill_manual(values = palet_task, labels = c("Semantic", "Tone")) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  #coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank())
Err_TMS_task 
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20TMS%20sessions,%20by%20errors-1.png)

``` r
#ggsave(plot = Err_TMS_task, filename = paste0(output_path, "Plots/Behavioral/Errors_TMS_byTask_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# Plot by condition
Err_TMS_cond <- ggplot(summary_err_TMS, aes(x = stim_type, y = perc, fill = condition)) +
  geom_violinhalf(aes(fill = condition), position = position_nudge(x = 0.2, y = 0), alpha = 0.6, color = NA) +
  geom_boxplot(alpha = 1, width = .3, colour = "black", outlier.shape = NA, position = position_dodge(width = 0.4)) +
  geom_line(data = summary_err_group_TMS_condition, aes(x = stim_type, y = mean_err, group = condition, colour = condition), linetype = 2, position = position_nudge(x = .2, y = 0)) +
  geom_point(data=summary_err_group_TMS_condition, aes(x = stim_type, y = mean_err, group = condition, colour = condition), shape=18, position = position_nudge(x = .2, y = 0)) +
  geom_errorbar(data=summary_err_group_TMS_condition, aes(x = stim_type, y = mean_err, group = condition, colour = condition, ymin = mean_err - std.error_err, ymax = mean_err + std.error_err), width = .05, position = position_nudge(x = .2, y = 0)) +
  scale_colour_manual(values =palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) + 
  scale_fill_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  #coord_cartesian(ylim = c(70, 100)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=10),
        axis.title.x = element_blank())
Err_TMS_cond
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20plot%20-%20TMS%20sessions,%20by%20errors-2.png)

``` r
#ggsave(plot = Err_TMS_cond, filename = paste0(output_path, "Plots/Behavioral/Errors_TMS_byCond_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")
```

#### Spaghetti plot for individual data

``` r
# WPM
Err_TMS_indivData_WPM <- ggplot(summary_err_TMS, aes(x = stim_type, y = perc)) + 
  geom_line(data = summary_err_TMS %>% filter(condition == "WPM"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = summary_err_TMS %>% filter(condition == "WPM"), color = "#DC964F", size = 3, alpha = 0.7) +
  geom_half_violin(data = summary_err_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "active"), fill = "#DC964F", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA) +
  geom_half_violin(data = summary_err_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "sham"), fill = "#DC964F", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA) +
  geom_half_boxplot(data = summary_err_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = summary_err_TMS %>% filter(condition == "WPM") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 25)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
Err_TMS_indivData_WPM
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-8-1.png)

``` r
#ggsave(plot = Err_TMS_indivData_WPM, filename = paste0(output_path, "Plots/Behavioral/Err_TMS_indivData_WPM_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# FPM
Err_TMS_indivData_FPM <- ggplot(summary_err_TMS, aes(x = stim_type, y = perc)) + 
  geom_line(data = summary_err_TMS %>% filter(condition == "FPM"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = summary_err_TMS %>% filter(condition == "FPM"), color = "gray60", size = 3, alpha = 0.7) +
  geom_half_violin(data = summary_err_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "active"), fill = "gray60", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA) +
  geom_half_violin(data = summary_err_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "sham"), fill = "gray60", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA) +
  geom_half_boxplot(data = summary_err_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = summary_err_TMS %>% filter(condition == "FPM") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 25)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
Err_TMS_indivData_FPM
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-8-2.png)

``` r
#ggsave(plot = Err_TMS_indivData_FPM, filename = paste0(output_path, "Plots/Behavioral/Err_TMS_indivData_FPM_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# Tone-picture matching
Err_TMS_indivData_tone <- ggplot(summary_err_TMS, aes(x = stim_type, y = perc)) + 
  geom_line(data = summary_err_TMS %>% filter(condition == "tone"), aes(group = sub), colour = "#BBBBBB", alpha = 0.6) +
  geom_point2(data = summary_err_TMS %>% filter(condition == "tone"), color = "#4477AA", size = 3, alpha = 0.7) +
  geom_half_violin(data = summary_err_TMS %>% filter(condition == "tone") %>% filter(stim_type == "active"), fill = "#4477AA", side = "l", position = position_nudge(x = -0.09), width = 0.35, color = NA) +
  geom_half_violin(data = summary_err_TMS %>% filter(condition == "tone") %>% filter(stim_type == "sham"), fill = "#4477AA", side = "r", position = position_nudge(x = 0.09), width = 0.35, color = NA) +
  geom_half_boxplot(data = summary_err_TMS %>% filter(condition == "tone") %>% filter(stim_type == "active"), side = "l", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = -0.07)) +
  geom_half_boxplot(data = summary_err_TMS %>% filter(condition == "tone") %>% filter(stim_type == "sham"), side = "r", center = T, outlier.shape = NA, width = 0.1, fill = NA, color = "black", errorbar.draw = F, position = position_nudge(x = 0.07)) +
  scale_x_discrete(labels = c("Active", "Sham")) +
  coord_cartesian(ylim = c(0, 25)) +
  labs(y = "Errors in %") +
  apatheme +
  theme(legend.position=("bottom"), 
        legend.title=element_blank(), 
        legend.text=element_text(size=6),
        axis.title.x = element_blank())
Err_TMS_indivData_tone
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-8-3.png)

``` r
#ggsave(plot = Err_TMS_indivData_tone, filename = paste0(output_path, "Plots/Behavioral/Err_TMS_indivData_tone_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")
```

#### Arrange spaghetti plots in one plot

``` r
plot_Errors <- egg::ggarrange(Err_TMS_indivData_WPM +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          Err_TMS_indivData_FPM +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    axis.title.x = element_blank(),
                    #legend.position = "bottom",
                    #legend.direction = "horizontal"
                    legend.position = "none"),
          Err_TMS_indivData_tone +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1)
```

![](behavioral_analysis_files/figure-markdown_github/unnamed-chunk-9-1.png)

``` r
#ggsave(plot = plot_Errors, filename = paste0(output_path, "Plots/Behavioral/Err_spaghettiPlots_", today, ".pdf"), dpi = 300, width = 7 , height = 3, device = "pdf")
```

## Mixed-effects regression

### Model including baseline session

``` r
# relevel factors
df_behav$stim_type <- factor(df_behav$stim_type, levels = c("baseline", "active", "sham"))
df_behav$condition <- factor(df_behav$condition, levels = c("WPM", "FPM", "tone"))
df_behav$age_z <- scale(df_behav$age, scale = F)

table(unlist(df_behav$stim_order))
```

    ## 
    ## Active – Sham Sham – Active 
    ##          7888          7862

``` r
# Sum-coding
contrasts(df_behav$stim_type) <- contr.sum(3)/3
contrasts(df_behav$condition) <- contr.sum(3)/3
contrasts(df_behav$congruency) <- contr.sum(2)/2

# Hierarchichal model selection of random effects structure
Acc_RE_0 <- glm(correct ~ stim_type * condition, data = df_behav, family = binomial(link = "logit"))
Acc_RE_1 <- glmer(correct ~ stim_type * condition + (1|sub), data = df_behav, family = binomial(link = "logit"))
#Acc_RE_2 <- glmer(correct ~ stim_type * condition + (1 + stim_type|sub), data = df_behav, family = binomial(link = "logit")) # conv warning
#Acc_RE_3 <- glmer(correct ~ stim_type * condition + (1|sub) + (1|stimulus_picture), data = df_behav, family = binomial(link = "logit")) # conv warning
#Acc_RE_4 <- glmer(correct ~ stim_type * condition + (1|sub) + (1|stimulus_audio), data = df_behav, family = binomial(link = "logit")) # conv warning
#Acc_RE_5 <- glmer(correct ~ stim_type * condition + (1 + stim_type|sub) + (1|stimulus_audio) + (1|stimulus_picture), data = df_behav, family = binomial(link = "logit")) # conv warning
#Acc_RE_5 <- glmer(correct ~ stim_type * condition + (1|sub) + (1|stimulus_audio) + (1|stimulus_picture), data = df_behav, family = binomial(link = "logit")) # conv warning
AICtab(Acc_RE_0, Acc_RE_1)
```

    ##          dAIC df
    ## Acc_RE_1  0.0 10
    ## Acc_RE_0 71.4 9

``` r
#anova(Acc_RE_1, Acc_RE_0, Acc_RE_2, Acc_RE_3, Acc_RE_4, Acc_RE_5)

# Hierarchichal model selection of fixed effects structure
Acc_FE_0 <-  glmer(correct ~ stim_type * condition + (1|sub), data = df_behav, family = binomial(link = "logit"))
Acc_FE_stimOrder <- glmer(correct ~ stim_type * condition + stim_order + (1|sub), data = df_behav, family = binomial(link = "logit"))
Acc_FE_task <- glmer(correct ~ stim_type * condition + task + (1|sub), data = df_behav, family = binomial(link = "logit"))
Acc_FE_congruency <- glmer(correct ~ stim_type * condition + congruency + (1|sub), data = df_behav, family = binomial(link = "logit"))
Acc_FE_age <- glmer(correct ~ stim_type * condition + age_z + (1|sub), data = df_behav, family = binomial(link = "logit"))
Acc_FE_full <- glmer(correct ~ stim_type * condition + congruency + age_z + (1|sub), data = df_behav, family = binomial(link = "logit"))
Acc_FE_stimCondCong <- glmer(correct ~ stim_type * condition * congruency + age_z + (1|sub), data = df_behav, family = binomial(link = "logit"))
AICtab(Acc_FE_0, Acc_FE_stimOrder, Acc_FE_task, Acc_FE_congruency, Acc_FE_age, Acc_FE_full, Acc_FE_stimCondCong)
```

    ##                     dAIC  df
    ## Acc_FE_stimCondCong   0.0 20
    ## Acc_FE_congruency    55.6 11
    ## Acc_FE_full          57.6 12
    ## Acc_FE_0            131.6 10
    ## Acc_FE_task         131.6 10
    ## Acc_FE_stimOrder    133.2 11
    ## Acc_FE_age          133.6 11

``` r
anova(Acc_FE_0, Acc_FE_stimOrder, Acc_FE_task)
```

    ## Data: df_behav
    ## Models:
    ## Acc_FE_0: correct ~ stim_type * condition + (1 | sub)
    ## Acc_FE_task: correct ~ stim_type * condition + task + (1 | sub)
    ## Acc_FE_stimOrder: correct ~ stim_type * condition + stim_order + (1 | sub)
    ##                  npar    AIC    BIC  logLik deviance  Chisq Df Pr(>Chisq)
    ## Acc_FE_0           10 5500.6 5577.2 -2740.3   5480.6                     
    ## Acc_FE_task        10 5500.6 5577.2 -2740.3   5480.6 0.0000  0           
    ## Acc_FE_stimOrder   11 5502.2 5586.5 -2740.1   5480.2 0.4394  1     0.5074

``` r
Acc_model <- Acc_FE_stimCondCong
summary(Acc_model)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: correct ~ stim_type * condition * congruency + age_z + (1 | sub)
    ##    Data: df_behav
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   5369.0   5522.3  -2664.5   5329.0    15730 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -13.9609   0.1195   0.1686   0.2441   0.5909 
    ## 
    ## Random effects:
    ##  Groups Name        Variance Std.Dev.
    ##  sub    (Intercept) 0.1717   0.4144  
    ## Number of obs: 15750, groups:  sub, 30
    ## 
    ## Fixed effects:
    ##                                    Estimate Std. Error z value
    ## (Intercept)                        3.435951   0.093554  36.727
    ## stim_type1                        -0.621384   0.209253  -2.970
    ## stim_type2                         0.342532   0.224876   1.523
    ## condition1                         3.038752   0.270411  11.238
    ## condition2                        -2.122880   0.187179 -11.341
    ## congruency1                       -0.639478   0.105659  -6.052
    ## age_z                              0.000804   0.011288   0.071
    ## stim_type1:condition1              0.625075   1.067620   0.585
    ## stim_type2:condition1             -1.477262   1.116542  -1.323
    ## stim_type1:condition2              2.041986   0.746204   2.736
    ## stim_type2:condition2             -1.221243   0.793593  -1.539
    ## stim_type1:congruency1             0.701139   0.414198   1.693
    ## stim_type2:congruency1            -0.364518   0.447053  -0.815
    ## condition1:congruency1            -1.077391   0.540645  -1.993
    ## condition2:congruency1            -1.593797   0.374430  -4.257
    ## stim_type1:condition1:congruency1  0.991184   2.059729   0.481
    ## stim_type2:condition1:congruency1  1.687271   2.197470   0.768
    ## stim_type1:condition2:congruency1  2.401504   1.462611   1.642
    ## stim_type2:condition2:congruency1 -2.224871   1.557408  -1.429
    ##                                               Pr(>|z|)    
    ## (Intercept)                       < 0.0000000000000002 ***
    ## stim_type1                                     0.00298 ** 
    ## stim_type2                                     0.12771    
    ## condition1                        < 0.0000000000000002 ***
    ## condition2                        < 0.0000000000000002 ***
    ## congruency1                              0.00000000143 ***
    ## age_z                                          0.94322    
    ## stim_type1:condition1                          0.55822    
    ## stim_type2:condition1                          0.18581    
    ## stim_type1:condition2                          0.00621 ** 
    ## stim_type2:condition2                          0.12383    
    ## stim_type1:congruency1                         0.09050 .  
    ## stim_type2:congruency1                         0.41486    
    ## condition1:congruency1                         0.04628 *  
    ## condition2:congruency1                   0.00002075675 ***
    ## stim_type1:condition1:congruency1              0.63036    
    ## stim_type2:condition1:congruency1              0.44259    
    ## stim_type1:condition2:congruency1              0.10060    
    ## stim_type2:condition2:congruency1              0.15313    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
drop1(Acc_model, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## correct ~ stim_type * condition * congruency + age_z + (1 | sub)
    ##                                npar    AIC    LRT Pr(Chi)  
    ## <none>                              5369.0                 
    ## age_z                             1 5367.0 0.0050 0.94342  
    ## stim_type:condition:congruency    4 5368.8 7.8278 0.09809 .
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# two-way interactions
Acc_twoway <- glmer(correct ~ stim_type * condition + stim_type * congruency + condition * congruency + age_z + (1|sub), data = df_behav, family = binomial(link = "logit"))
drop1(Acc_twoway, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## correct ~ stim_type * condition + stim_type * congruency + condition * 
    ##     congruency + age_z + (1 | sub)
    ##                      npar    AIC    LRT           Pr(Chi)    
    ## <none>                    5368.8                             
    ## age_z                   1 5366.8  0.005         0.9453931    
    ## stim_type:condition     4 5382.6 21.798         0.0002199 ***
    ## stim_type:congruency    2 5369.8  4.971         0.0832803 .  
    ## condition:congruency    2 5418.0 53.150 0.000000000002875 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms_twoway <- emmeans(Acc_twoway, pairwise ~ congruency|condition, adjust = "holm", type = "response")
confint(emms_twoway$contrasts)
```

    ## condition = WPM:
    ##  contrast                odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  congruent / incongruent      0.379 0.0938 Inf     0.233     0.615
    ## 
    ## condition = FPM:
    ##  contrast                odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  congruent / incongruent      0.312 0.0356 Inf     0.249     0.390
    ## 
    ## condition = tone:
    ##  contrast                odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  congruent / incongruent      1.181 0.1728 Inf     0.887     1.573
    ## 
    ## Results are averaged over the levels of: stim_type 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log odds ratio scale

``` r
Acc_cond_cong <- plot(ggpredict(Acc_twoway, terms = c("condition", "congruency"))) +
  #scale_fill_manual(values = palet_cond) +
  #scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  scale_colour_manual(values = c("orangered4", "orangered")) +
  coord_cartesian(ylim = c(0.8, 1)) +
  ylab("Accuracy") +
  #scale_x_discrete(labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  apatheme +
  theme(axis.title.x = element_blank(),
        legend.position = "bottom",
        legend.title = element_blank(),
        legend.text=element_text(size=6))
#ggsave(plot = Acc_cond_cong, filename = paste0(output_path, "Plots/Behavioral/Acc_cond_cong_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")

emms_twoway2 <- emmeans(Acc_twoway, pairwise ~ stim_type|condition, adjust = "holm", type = "response")
confint(emms_twoway2$contrasts)
```

    ## condition = WPM:
    ##  contrast          odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  baseline / active      0.873 0.2377 Inf     0.455     1.675
    ##  baseline / sham        0.813 0.2264 Inf     0.418     1.583
    ##  active / sham          0.932 0.2635 Inf     0.473     1.833
    ## 
    ## condition = FPM:
    ##  contrast          odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  baseline / active      1.133 0.1519 Inf     0.822     1.562
    ##  baseline / sham        1.088 0.1459 Inf     0.790     1.500
    ##  active / sham          0.960 0.1274 Inf     0.699     1.319
    ## 
    ## condition = tone:
    ##  contrast          odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  baseline / active      0.408 0.0754 Inf     0.262     0.635
    ##  baseline / sham        0.574 0.0957 Inf     0.385     0.855
    ##  active / sham          1.407 0.2834 Inf     0.869     2.279
    ## 
    ## Results are averaged over the levels of: congruency 
    ## Confidence level used: 0.95 
    ## Conf-level adjustment: bonferroni method for 3 estimates 
    ## Intervals are back-transformed from the log odds ratio scale

``` r
sess_cond <- plot(ggpredict(Acc_twoway, terms = c("stim_type", "condition")))
Acc_sess_cond <- plot(ggpredict(Acc_twoway, terms = c("stim_type", "condition"))) +
  #scale_fill_manual(values = palet_cond) +
  scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  #scale_colour_manual(values = c("orangered4", "orangered", "orangered2")) +
  coord_cartesian(ylim = c(0.4, 1)) +
  ylab("Accuracy") +
  #scale_x_discrete(labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  apatheme +
  theme(axis.title.x = element_blank(),
        legend.position = "none")
#ggsave(plot = Acc_sess_cond, filename = paste0(output_path, "Plots/Behavioral/Acc_sess_cond_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


# main effects
Acc_mainEffect <- glmer(correct ~ stim_type + condition + congruency + age_z + (1|sub), data = df_behav, family = binomial(link = "logit"))
drop1(Acc_mainEffect, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## correct ~ stim_type + condition + congruency + age_z + (1 | sub)
    ##            npar    AIC     LRT             Pr(Chi)    
    ## <none>          5447.9                                
    ## stim_type     2 5446.7   2.787              0.2483    
    ## condition     2 5723.2 279.340 <0.0000000000000002 ***
    ## congruency    1 5523.8  77.919 <0.0000000000000002 ***
    ## age_z         1 5445.9   0.005              0.9410    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms_cond <- emmeans(Acc_mainEffect, pairwise ~ condition, adjust = "holm", type = "response")
confint(emms_cond$contrasts)
```

    ##  contrast   odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  WPM / FPM       5.772 0.7026 Inf     4.312     7.725
    ##  WPM / tone      3.550 0.4677 Inf     2.590     4.867
    ##  FPM / tone      0.615 0.0537 Inf     0.499     0.758
    ## 
    ## Results are averaged over the levels of: stim_type, congruency 
    ## Confidence level used: 0.95 
    ## Conf-level adjustment: bonferroni method for 3 estimates 
    ## Intervals are back-transformed from the log odds ratio scale

``` r
Acc_cond <- plot(ggpredict(Acc_mainEffect, terms = c("condition"))) +
  scale_fill_manual(values = palet_cond) +
  #scale_colour_manual(values = palet_cond, labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  #scale_colour_manual(values = palet_cond) +
  coord_cartesian(ylim = c(0.8, 1)) +
  ylab("Accuracy") +
  #scale_x_discrete(labels = c("Word-picture matching", "Feature-picture matching", "Tone-picture matching")) +
  apatheme +
  theme(axis.title.x = element_blank(),
        legend.position = "bottom",
        legend.title = element_blank(),
        legend.text=element_text(size=6))
##ggsave(plot = Acc_cond, filename = paste0(output_path, "Plots/Behavioral/Acc_cond_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


emms_cong <- emmeans(Acc_mainEffect, pairwise ~ congruency, adjust = "holm", type = "response")
confint(emms_cong$contrasts)
```

    ##  contrast                odds.ratio     SE  df asymp.LCL asymp.UCL
    ##  congruent / incongruent      0.497 0.0401 Inf     0.425     0.583
    ## 
    ## Results are averaged over the levels of: stim_type, condition 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log odds ratio scale

``` r
Acc_cong <- plot(ggpredict(Acc_mainEffect, terms = c("congruency"))) +
  coord_cartesian(ylim = c(0.8, 1)) +
  ylab("Accuracy") +
  apatheme +
  theme(axis.title.x = element_blank(),
        legend.position = "bottom",
        legend.title = element_blank(),
        legend.text=element_text(size=6))
##ggsave(plot = Acc_cong, filename = paste0(output_path, "Plots/Behavioral/Acc_cond_", today, ".pdf"), dpi = 300, width = 5, height = 4, device = "pdf")


plot_Acc_model <- egg::ggarrange(Acc_cond +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          Acc_cong +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    axis.title.x = element_blank(),
                    #legend.position = "bottom",
                    #legend.direction = "horizontal"
                    legend.position = "none"),
          Acc_cond_cong +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1,
          widths = c(1,0.6,1))
```

![](behavioral_analysis_files/figure-markdown_github/Accuracy%20-%20Model%20including%20baseline%20session-1.png)

``` r
#ggsave(plot = plot_Acc_model, filename = paste0(output_path, "Plots/Behavioral/Acc_model_plots_", today, ".pdf"), dpi = 300, width = 7, height = 3, device = "pdf")
```
