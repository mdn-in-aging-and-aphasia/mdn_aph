# Load packages

``` r
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(ggplot2)
library(lme4)
library(emmeans)
library(ggeffects)
library(sjPlot)
```

# Load data

``` r
# df with Localizer estimates
load(here::here("Localizer/df_localizer_estimates_n=25ROIs.RData"))

# df with Localizer estimates - only TMS sessions
load(here::here("Localizer/df_TMS.RData"))
```

# Read in data

# Plot data

``` r
df_TMS$ROI <- factor(df_TMS$ROI, levels = c("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", 
                                            "15", "16", "17", "18", "19", "20", "21", "22", "24", "25", "29"))

WPM <- ggplot(data = df_TMS, aes(x = Stimulation, y = PSC_WPM, group = Subject)) + 
    geom_line(colour = "#BBBBBB", alpha = 0.6) + 
geom_point(color = palet_cond[1]) +
facet_wrap(~ROI) + 
  apatheme +
  theme(legend.position = "none")
WPM
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-1-1.png)

``` r
#ggsave(plot = WPM, filename = paste0(output_path, "Plots/fMRI/Localizer/PSC_WPM_", today, ".pdf"), dpi = 300, width = 10, height = 7, device = "pdf")
#ggsave(plot = WPM, filename = paste0(output_path, "Plots/fMRI/Localizer/PSC_WPM_", today, ".png"), dpi = 300, width = 10, height = 7)

FPM <- ggplot(data = df_TMS, aes(x = Stimulation, y = PSC_FPM, group = Subject)) + 
    geom_line(colour = "#BBBBBB", alpha = 0.6) + 
geom_point(color = palet_cond[2]) +
facet_wrap(~ROI) + 
  apatheme +
  theme(legend.position = "none")
FPM
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-1-2.png)

``` r
#ggsave(plot = FPM, filename = paste0(output_path, "Plots/fMRI/Localizer/PSC_FPM_", today, ".pdf"), dpi = 300, width = 10, height = 7, device = "pdf")
#ggsave(plot = FPM, filename = paste0(output_path, "Plots/fMRI/Localizer/PSC_FPM_", today, ".png"), dpi = 300, width = 10, height = 7)

tone <- ggplot(data = df_TMS, aes(x = Stimulation, y = PSC_tone, group = Subject)) + 
    geom_line(colour = "#BBBBBB", alpha = 0.6) + 
geom_point(color = palet_cond[3]) +
facet_wrap(~ROI) + 
  apatheme +
  theme(legend.position = "none")
tone
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-1-3.png)

``` r
#ggsave(plot = tone, filename = paste0(output_path, "Plots/fMRI/Localizer/PSC_tone_", today, ".pdf"), dpi = 300, width = 10, height = 7, device = "pdf")
#ggsave(plot = tone, filename = paste0(output_path, "Plots/fMRI/Localizer/PSC_tone_", today, ".png"), dpi = 300, width = 10, height = 7)
```

# Exploring effect of stimulation on PSC/Activation of ROIs

## PSC

``` r
PSC_WPM = lmer(PSC_WPM ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(PSC_WPM)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: PSC_WPM ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: -102.6
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -3.8507 -0.5757 -0.0311  0.4998  4.8688 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance Std.Dev. Corr
    ##  Subject  (Intercept)       0.007073 0.08410      
    ##           Stimulationactive 0.001994 0.04465  0.50
    ##  Residual                   0.045886 0.21421      
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                           Estimate Std. Error t value
    ## (Intercept)              0.4779619  0.0420153  11.376
    ## Stimulationactive       -0.0071439  0.0559061  -0.128
    ## ROI2                     0.1655181  0.0553085   2.993
    ## ROI3                    -0.0299532  0.0553085  -0.542
    ## ROI4                     0.1727243  0.0553085   3.123
    ## ROI5                    -0.1352212  0.0553085  -2.445
    ## ROI6                     0.2651622  0.0553085   4.794
    ## ROI7                    -0.0085090  0.0553085  -0.154
    ## ROI8                     0.2179993  0.0553085   3.942
    ## ROI9                    -0.2327595  0.0553085  -4.208
    ## ROI10                    0.1767260  0.0553085   3.195
    ## ROI11                   -0.0738254  0.0557924  -1.323
    ## ROI12                   -0.1970431  0.0557924  -3.532
    ## ROI13                    0.1384669  0.0553085   2.504
    ## ROI14                    0.0463964  0.0553085   0.839
    ## ROI15                   -0.0768173  0.0553085  -1.389
    ## ROI16                   -0.1588226  0.0553085  -2.872
    ## ROI17                   -0.3577388  0.0557924  -6.412
    ## ROI18                    0.0151637  0.0553085   0.274
    ## ROI19                   -0.2545085  0.0553085  -4.602
    ## ROI20                    0.0008005  0.0557924   0.014
    ## ROI21                   -0.2842921  0.0553085  -5.140
    ## ROI22                   -0.1276960  0.0553085  -2.309
    ## ROI24                   -0.3175369  0.0557924  -5.691
    ## ROI25                   -0.2017768  0.0557924  -3.617
    ## ROI29                   -0.2160801  0.0553085  -3.907
    ## Stimulationactive:ROI2   0.0184115  0.0782181   0.235
    ## Stimulationactive:ROI3   0.0239611  0.0782181   0.306
    ## Stimulationactive:ROI4   0.0133639  0.0782181   0.171
    ## Stimulationactive:ROI5   0.0377338  0.0782181   0.482
    ## Stimulationactive:ROI6   0.0385562  0.0782181   0.493
    ## Stimulationactive:ROI7  -0.0009930  0.0782181  -0.013
    ## Stimulationactive:ROI8   0.0192667  0.0782181   0.246
    ## Stimulationactive:ROI9   0.0054764  0.0782181   0.070
    ## Stimulationactive:ROI10  0.0643268  0.0782181   0.822
    ## Stimulationactive:ROI11  0.0436315  0.0788962   0.553
    ## Stimulationactive:ROI12 -0.0029371  0.0788962  -0.037
    ## Stimulationactive:ROI13  0.0529556  0.0782181   0.677
    ## Stimulationactive:ROI14  0.0532395  0.0782181   0.681
    ## Stimulationactive:ROI15  0.0059630  0.0782181   0.076
    ## Stimulationactive:ROI16  0.0150667  0.0782181   0.193
    ## Stimulationactive:ROI17  0.0712306  0.0788962   0.903
    ## Stimulationactive:ROI18  0.0110784  0.0782181   0.142
    ## Stimulationactive:ROI19  0.0103545  0.0782181   0.132
    ## Stimulationactive:ROI20  0.0398300  0.0788962   0.505
    ## Stimulationactive:ROI21 -0.0045064  0.0782181  -0.058
    ## Stimulationactive:ROI22  0.0011577  0.0782181   0.015
    ## Stimulationactive:ROI24 -0.0071206  0.0788962  -0.090
    ## Stimulationactive:ROI25 -0.0140681  0.0788962  -0.178
    ## Stimulationactive:ROI29  0.0123600  0.0782181   0.158

``` r
plot_model(PSC_WPM, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-2-1.png)

``` r
emmeans(PSC_WPM, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.478 0.0420 676   0.3955    0.560
    ##  active       0.471 0.0442 391   0.3839    0.558
    ## 
    ## ROI = 2:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.643 0.0420 676   0.5610    0.726
    ##  active       0.655 0.0442 391   0.5678    0.742
    ## 
    ## ROI = 3:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.448 0.0420 676   0.3655    0.531
    ##  active       0.465 0.0442 391   0.3779    0.552
    ## 
    ## ROI = 4:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.651 0.0420 676   0.5682    0.733
    ##  active       0.657 0.0442 391   0.5699    0.744
    ## 
    ## ROI = 5:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.343 0.0420 676   0.2602    0.425
    ##  active       0.373 0.0442 391   0.2864    0.460
    ## 
    ## ROI = 6:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.743 0.0420 676   0.6606    0.826
    ##  active       0.775 0.0442 391   0.6876    0.862
    ## 
    ## ROI = 7:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.469 0.0420 676   0.3870    0.552
    ##  active       0.461 0.0442 391   0.3744    0.548
    ## 
    ## ROI = 8:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.696 0.0420 676   0.6135    0.778
    ##  active       0.708 0.0442 391   0.6211    0.795
    ## 
    ## ROI = 9:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.245 0.0420 676   0.1627    0.328
    ##  active       0.244 0.0442 391   0.1566    0.331
    ## 
    ## ROI = 10:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.655 0.0420 676   0.5722    0.737
    ##  active       0.712 0.0442 391   0.6249    0.799
    ## 
    ## ROI = 11:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.404 0.0427 699   0.3204    0.488
    ##  active       0.441 0.0448 407   0.3525    0.529
    ## 
    ## ROI = 12:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.281 0.0427 699   0.1972    0.365
    ##  active       0.271 0.0448 407   0.1827    0.359
    ## 
    ## ROI = 13:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.616 0.0420 676   0.5339    0.699
    ##  active       0.662 0.0442 391   0.5753    0.749
    ## 
    ## ROI = 14:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.524 0.0420 676   0.4419    0.607
    ##  active       0.570 0.0442 391   0.4835    0.657
    ## 
    ## ROI = 15:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.401 0.0420 676   0.3186    0.484
    ##  active       0.400 0.0442 391   0.3130    0.487
    ## 
    ## ROI = 16:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.319 0.0420 676   0.2366    0.402
    ##  active       0.327 0.0442 391   0.2401    0.414
    ## 
    ## ROI = 17:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.120 0.0427 699   0.0365    0.204
    ##  active       0.184 0.0448 407   0.0962    0.272
    ## 
    ## ROI = 18:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.493 0.0420 676   0.4106    0.576
    ##  active       0.497 0.0442 391   0.4101    0.584
    ## 
    ## ROI = 19:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.223 0.0420 676   0.1410    0.306
    ##  active       0.227 0.0442 391   0.1397    0.314
    ## 
    ## ROI = 20:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.479 0.0427 699   0.3950    0.563
    ##  active       0.511 0.0448 407   0.4233    0.600
    ## 
    ## ROI = 21:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.194 0.0420 676   0.1112    0.276
    ##  active       0.182 0.0442 391   0.0951    0.269
    ## 
    ## ROI = 22:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.350 0.0420 676   0.2678    0.433
    ##  active       0.344 0.0442 391   0.2573    0.431
    ## 
    ## ROI = 24:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.160 0.0427 699   0.0767    0.244
    ##  active       0.146 0.0448 407   0.0580    0.234
    ## 
    ## ROI = 25:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.276 0.0427 699   0.1924    0.360
    ##  active       0.255 0.0448 407   0.1668    0.343
    ## 
    ## ROI = 29:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.262 0.0420 676   0.1794    0.344
    ##  active       0.267 0.0442 391   0.1801    0.354
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.00714 0.0559 1305   0.128  0.8983
    ## 
    ## ROI = 2:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.01127 0.0559 1305  -0.202  0.8403
    ## 
    ## ROI = 3:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.01682 0.0559 1305  -0.301  0.7636
    ## 
    ## ROI = 4:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.00622 0.0559 1305  -0.111  0.9114
    ## 
    ## ROI = 5:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.03059 0.0559 1305  -0.547  0.5844
    ## 
    ## ROI = 6:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.03141 0.0559 1305  -0.562  0.5743
    ## 
    ## ROI = 7:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.00814 0.0559 1305   0.146  0.8843
    ## 
    ## ROI = 8:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.01212 0.0559 1305  -0.217  0.8284
    ## 
    ## ROI = 9:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.00167 0.0559 1305   0.030  0.9762
    ## 
    ## ROI = 10:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.05718 0.0559 1305  -1.023  0.3066
    ## 
    ## ROI = 11:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.03649 0.0569 1310  -0.642  0.5212
    ## 
    ## ROI = 12:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.01008 0.0569 1310   0.177  0.8593
    ## 
    ## ROI = 13:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.04581 0.0559 1305  -0.819  0.4127
    ## 
    ## ROI = 14:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.04610 0.0559 1305  -0.825  0.4098
    ## 
    ## ROI = 15:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.00118 0.0559 1305   0.021  0.9832
    ## 
    ## ROI = 16:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.00792 0.0559 1305  -0.142  0.8873
    ## 
    ## ROI = 17:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.06409 0.0569 1310  -1.127  0.2599
    ## 
    ## ROI = 18:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.00393 0.0559 1305  -0.070  0.9439
    ## 
    ## ROI = 19:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.00321 0.0559 1305  -0.057  0.9542
    ## 
    ## ROI = 20:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.03269 0.0569 1310  -0.575  0.5655
    ## 
    ## ROI = 21:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.01165 0.0559 1305   0.208  0.8350
    ## 
    ## ROI = 22:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.00599 0.0559 1305   0.107  0.9147
    ## 
    ## ROI = 24:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.01426 0.0569 1310   0.251  0.8019
    ## 
    ## ROI = 25:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active  0.02121 0.0569 1310   0.373  0.7092
    ## 
    ## ROI = 29:
    ##  contrast      estimate     SE   df t.ratio p.value
    ##  sham - active -0.00522 0.0559 1305  -0.093  0.9257
    ## 
    ## Degrees-of-freedom method: kenward-roger

``` r
PSC_FPM = lmer(PSC_FPM ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(PSC_FPM)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: PSC_FPM ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: 7.1
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -4.5859 -0.6045 -0.0411  0.5093  4.6851 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance     Std.Dev.  Corr 
    ##  Subject  (Intercept)       0.0101391526 0.1006934      
    ##           Stimulationactive 0.0000004949 0.0007035 -1.00
    ##  Residual                   0.0498201116 0.2232042      
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                          Estimate Std. Error t value
    ## (Intercept)              0.465230   0.044706  10.406
    ## Stimulationactive        0.021257   0.057631   0.369
    ## ROI2                     0.179594   0.057631   3.116
    ## ROI3                    -0.009116   0.057631  -0.158
    ## ROI4                     0.189908   0.057631   3.295
    ## ROI5                    -0.078788   0.057631  -1.367
    ## ROI6                     0.250216   0.057631   4.342
    ## ROI7                     0.075806   0.057631   1.315
    ## ROI8                     0.274730   0.057631   4.767
    ## ROI9                    -0.158540   0.057631  -2.751
    ## ROI10                    0.192149   0.057631   3.334
    ## ROI11                   -0.068272   0.058135  -1.174
    ## ROI12                   -0.160897   0.058135  -2.768
    ## ROI13                    0.105564   0.057631   1.832
    ## ROI14                    0.025981   0.057631   0.451
    ## ROI15                   -0.015532   0.057631  -0.270
    ## ROI16                   -0.149159   0.057631  -2.588
    ## ROI17                   -0.357922   0.058135  -6.157
    ## ROI18                    0.170475   0.057631   2.958
    ## ROI19                   -0.203230   0.057631  -3.526
    ## ROI20                    0.124992   0.058135   2.150
    ## ROI21                   -0.226226   0.057631  -3.925
    ## ROI22                   -0.045737   0.057631  -0.794
    ## ROI24                   -0.264207   0.058135  -4.545
    ## ROI25                   -0.035979   0.058135  -0.619
    ## ROI29                   -0.026082   0.057631  -0.453
    ## Stimulationactive:ROI2  -0.022863   0.081503  -0.281
    ## Stimulationactive:ROI3   0.012733   0.081503   0.156
    ## Stimulationactive:ROI4  -0.020597   0.081503  -0.253
    ## Stimulationactive:ROI5   0.004070   0.081503   0.050
    ## Stimulationactive:ROI6   0.012063   0.081503   0.148
    ## Stimulationactive:ROI7  -0.023066   0.081503  -0.283
    ## Stimulationactive:ROI8  -0.014253   0.081503  -0.175
    ## Stimulationactive:ROI9  -0.021857   0.081503  -0.268
    ## Stimulationactive:ROI10  0.040243   0.081503   0.494
    ## Stimulationactive:ROI11  0.032363   0.082202   0.394
    ## Stimulationactive:ROI12 -0.009603   0.082202  -0.117
    ## Stimulationactive:ROI13  0.028574   0.081503   0.351
    ## Stimulationactive:ROI14  0.029217   0.081503   0.358
    ## Stimulationactive:ROI15 -0.012943   0.081503  -0.159
    ## Stimulationactive:ROI16 -0.023674   0.081503  -0.290
    ## Stimulationactive:ROI17  0.021459   0.082202   0.261
    ## Stimulationactive:ROI18 -0.035242   0.081503  -0.432
    ## Stimulationactive:ROI19 -0.035676   0.081503  -0.438
    ## Stimulationactive:ROI20  0.013688   0.082202   0.167
    ## Stimulationactive:ROI21 -0.023855   0.081503  -0.293
    ## Stimulationactive:ROI22 -0.042263   0.081503  -0.519
    ## Stimulationactive:ROI24 -0.033752   0.082202  -0.411
    ## Stimulationactive:ROI25 -0.024415   0.082202  -0.297
    ## Stimulationactive:ROI29 -0.017890   0.081503  -0.220

``` r
plot_model(PSC_FPM, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-2-2.png)

``` r
emmeans(PSC_FPM, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.465 0.0447 533   0.3774    0.553
    ##  active       0.486 0.0447 540   0.3988    0.574
    ## 
    ## ROI = 2:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.645 0.0447 533   0.5570    0.733
    ##  active       0.643 0.0447 540   0.5555    0.731
    ## 
    ## ROI = 3:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.456 0.0447 533   0.3683    0.544
    ##  active       0.490 0.0447 540   0.4024    0.578
    ## 
    ## ROI = 4:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.655 0.0447 533   0.5673    0.743
    ##  active       0.656 0.0447 540   0.5681    0.744
    ## 
    ## ROI = 5:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.386 0.0447 533   0.2986    0.474
    ##  active       0.412 0.0447 540   0.3241    0.499
    ## 
    ## ROI = 6:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.715 0.0447 533   0.6276    0.803
    ##  active       0.749 0.0447 540   0.6610    0.836
    ## 
    ## ROI = 7:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.541 0.0447 533   0.4532    0.629
    ##  active       0.539 0.0447 540   0.4515    0.627
    ## 
    ## ROI = 8:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.740 0.0447 533   0.6521    0.828
    ##  active       0.747 0.0447 540   0.6592    0.835
    ## 
    ## ROI = 9:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.307 0.0447 533   0.2189    0.395
    ##  active       0.306 0.0447 540   0.2184    0.394
    ## 
    ## ROI = 10:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.657 0.0447 533   0.5696    0.745
    ##  active       0.719 0.0447 540   0.6312    0.807
    ## 
    ## ROI = 11:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.397 0.0454 554   0.3079    0.486
    ##  active       0.451 0.0453 561   0.3616    0.540
    ## 
    ## ROI = 12:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.304 0.0454 554   0.2152    0.393
    ##  active       0.316 0.0453 561   0.2270    0.405
    ## 
    ## ROI = 13:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.571 0.0447 533   0.4830    0.659
    ##  active       0.621 0.0447 540   0.5329    0.708
    ## 
    ## ROI = 14:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.491 0.0447 533   0.4034    0.579
    ##  active       0.542 0.0447 540   0.4540    0.629
    ## 
    ## ROI = 15:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.450 0.0447 533   0.3619    0.538
    ##  active       0.458 0.0447 540   0.3703    0.546
    ## 
    ## ROI = 16:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.316 0.0447 533   0.2282    0.404
    ##  active       0.314 0.0447 540   0.2259    0.401
    ## 
    ## ROI = 17:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.107 0.0454 554   0.0182    0.196
    ##  active       0.150 0.0453 561   0.0610    0.239
    ## 
    ## ROI = 18:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.636 0.0447 533   0.5479    0.724
    ##  active       0.622 0.0447 540   0.5340    0.709
    ## 
    ## ROI = 19:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.262 0.0447 533   0.1742    0.350
    ##  active       0.248 0.0447 540   0.1599    0.335
    ## 
    ## ROI = 20:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.590 0.0454 554   0.5011    0.679
    ##  active       0.625 0.0453 561   0.5362    0.714
    ## 
    ## ROI = 21:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.239 0.0447 533   0.1512    0.327
    ##  active       0.236 0.0447 540   0.1487    0.324
    ## 
    ## ROI = 22:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.419 0.0447 533   0.3317    0.507
    ##  active       0.398 0.0447 540   0.3108    0.486
    ## 
    ## ROI = 24:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.201 0.0454 554   0.1119    0.290
    ##  active       0.189 0.0453 561   0.0995    0.278
    ## 
    ## ROI = 25:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.429 0.0454 554   0.3402    0.518
    ##  active       0.426 0.0453 561   0.3371    0.515
    ## 
    ## ROI = 29:
    ##  Stimulation emmean     SE  df lower.CL upper.CL
    ##  sham         0.439 0.0447 533   0.3513    0.527
    ##  active       0.443 0.0447 540   0.3548    0.530
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.021257 0.0576 1382  -0.369  0.7123
    ## 
    ## ROI = 2:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.001606 0.0576 1382   0.028  0.9778
    ## 
    ## ROI = 3:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.033989 0.0576 1382  -0.590  0.5554
    ## 
    ## ROI = 4:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.000659 0.0576 1382  -0.011  0.9909
    ## 
    ## ROI = 5:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.025327 0.0576 1382  -0.439  0.6604
    ## 
    ## ROI = 6:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.033320 0.0576 1382  -0.578  0.5633
    ## 
    ## ROI = 7:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.001810 0.0576 1382   0.031  0.9750
    ## 
    ## ROI = 8:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.007004 0.0576 1382  -0.122  0.9033
    ## 
    ## ROI = 9:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.000601 0.0576 1382   0.010  0.9917
    ## 
    ## ROI = 10:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.061500 0.0576 1382  -1.067  0.2861
    ## 
    ## ROI = 11:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.053619 0.0586 1382  -0.915  0.3605
    ## 
    ## ROI = 12:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.011654 0.0586 1382  -0.199  0.8425
    ## 
    ## ROI = 13:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.049830 0.0576 1382  -0.865  0.3874
    ## 
    ## ROI = 14:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.050474 0.0576 1382  -0.876  0.3813
    ## 
    ## ROI = 15:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.008314 0.0576 1382  -0.144  0.8853
    ## 
    ## ROI = 16:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.002417 0.0576 1382   0.042  0.9666
    ## 
    ## ROI = 17:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.042716 0.0586 1382  -0.729  0.4664
    ## 
    ## ROI = 18:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.013986 0.0576 1382   0.243  0.8083
    ## 
    ## ROI = 19:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.014419 0.0576 1382   0.250  0.8025
    ## 
    ## ROI = 20:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.034944 0.0586 1382  -0.596  0.5512
    ## 
    ## ROI = 21:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.002598 0.0576 1382   0.045  0.9640
    ## 
    ## ROI = 22:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.021007 0.0576 1382   0.365  0.7155
    ## 
    ## ROI = 24:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.012495 0.0586 1382   0.213  0.8313
    ## 
    ## ROI = 25:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active  0.003158 0.0586 1382   0.054  0.9570
    ## 
    ## ROI = 29:
    ##  contrast       estimate     SE   df t.ratio p.value
    ##  sham - active -0.003366 0.0576 1382  -0.058  0.9534
    ## 
    ## Degrees-of-freedom method: kenward-roger

``` r
PSC_tone = lmer(PSC_tone ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(PSC_tone)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: PSC_tone ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: 42
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -3.4656 -0.6014 -0.0180  0.5858  3.8346 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance Std.Dev. Corr 
    ##  Subject  (Intercept)       0.01410  0.1187        
    ##           Stimulationactive 0.01071  0.1035   -0.53
    ##  Residual                   0.04972  0.2230        
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                          Estimate Std. Error t value
    ## (Intercept)              0.383215   0.046122   8.309
    ## Stimulationactive        0.023811   0.060592   0.393
    ## ROI2                     0.020847   0.057573   0.362
    ## ROI3                    -0.246704   0.057573  -4.285
    ## ROI4                    -0.127388   0.057573  -2.213
    ## ROI5                    -0.424157   0.057573  -7.367
    ## ROI6                     0.187579   0.057573   3.258
    ## ROI7                    -0.051816   0.057573  -0.900
    ## ROI8                    -0.016778   0.057573  -0.291
    ## ROI9                    -0.392525   0.057573  -6.818
    ## ROI10                   -0.160354   0.057573  -2.785
    ## ROI11                   -0.402568   0.058084  -6.931
    ## ROI12                   -0.333251   0.058084  -5.737
    ## ROI13                    0.251512   0.057573   4.369
    ## ROI14                    0.151138   0.057573   2.625
    ## ROI15                    0.113435   0.057573   1.970
    ## ROI16                    0.047801   0.057573   0.830
    ## ROI17                   -0.134093   0.058084  -2.309
    ## ROI18                    0.081268   0.057573   1.412
    ## ROI19                   -0.131033   0.057573  -2.276
    ## ROI20                    0.027038   0.058084   0.465
    ## ROI21                   -0.292767   0.057573  -5.085
    ## ROI22                    0.085273   0.057573   1.481
    ## ROI24                   -0.214960   0.058084  -3.701
    ## ROI25                   -0.303824   0.058084  -5.231
    ## ROI29                   -0.226306   0.057573  -3.931
    ## Stimulationactive:ROI2   0.007517   0.081420   0.092
    ## Stimulationactive:ROI3   0.001244   0.081420   0.015
    ## Stimulationactive:ROI4  -0.017197   0.081420  -0.211
    ## Stimulationactive:ROI5  -0.017008   0.081420  -0.209
    ## Stimulationactive:ROI6   0.002615   0.081420   0.032
    ## Stimulationactive:ROI7  -0.009631   0.081420  -0.118
    ## Stimulationactive:ROI8  -0.022869   0.081420  -0.281
    ## Stimulationactive:ROI9  -0.062004   0.081420  -0.762
    ## Stimulationactive:ROI10  0.055753   0.081420   0.685
    ## Stimulationactive:ROI11  0.050100   0.082140   0.610
    ## Stimulationactive:ROI12  0.040002   0.082140   0.487
    ## Stimulationactive:ROI13 -0.012653   0.081420  -0.155
    ## Stimulationactive:ROI14 -0.034177   0.081420  -0.420
    ## Stimulationactive:ROI15 -0.036711   0.081420  -0.451
    ## Stimulationactive:ROI16 -0.034861   0.081420  -0.428
    ## Stimulationactive:ROI17 -0.005957   0.082140  -0.073
    ## Stimulationactive:ROI18 -0.024841   0.081420  -0.305
    ## Stimulationactive:ROI19 -0.006668   0.081420  -0.082
    ## Stimulationactive:ROI20  0.037788   0.082140   0.460
    ## Stimulationactive:ROI21  0.004941   0.081420   0.061
    ## Stimulationactive:ROI22 -0.010147   0.081420  -0.125
    ## Stimulationactive:ROI24 -0.018674   0.082140  -0.227
    ## Stimulationactive:ROI25 -0.056061   0.082140  -0.683
    ## Stimulationactive:ROI29 -0.032287   0.081420  -0.397

``` r
plot_model(PSC_tone, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-2-3.png)

``` r
emmeans(PSC_tone, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.38322 0.0461 384  0.292532   0.4739
    ##  active       0.40703 0.0452 465  0.318119   0.4959
    ## 
    ## ROI = 2:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.40406 0.0461 384  0.313379   0.4947
    ##  active       0.43539 0.0452 465  0.346483   0.5243
    ## 
    ## ROI = 3:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.13651 0.0461 384  0.045828   0.2272
    ##  active       0.16157 0.0452 465  0.072659   0.2505
    ## 
    ## ROI = 4:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.25583 0.0461 384  0.165144   0.3465
    ##  active       0.26244 0.0452 465  0.173533   0.3513
    ## 
    ## ROI = 5:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham        -0.04094 0.0461 384 -0.131624   0.0497
    ##  active      -0.03414 0.0452 465 -0.123046   0.0548
    ## 
    ## ROI = 6:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.57079 0.0461 384  0.480111   0.6615
    ##  active       0.59722 0.0452 465  0.508313   0.6861
    ## 
    ## ROI = 7:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.33140 0.0461 384  0.240716   0.4221
    ##  active       0.34558 0.0452 465  0.256672   0.4345
    ## 
    ## ROI = 8:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.36644 0.0461 384  0.275755   0.4571
    ##  active       0.36738 0.0452 465  0.278472   0.4563
    ## 
    ## ROI = 9:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham        -0.00931 0.0461 384 -0.099992   0.0814
    ##  active      -0.04750 0.0452 465 -0.136409   0.0414
    ## 
    ## ROI = 10:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.22286 0.0461 384  0.132179   0.3135
    ##  active       0.30243 0.0452 465  0.213519   0.3913
    ## 
    ## ROI = 11:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham        -0.01935 0.0468 401 -0.111277   0.0726
    ##  active       0.05456 0.0459 484 -0.035615   0.1447
    ## 
    ## ROI = 12:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.04996 0.0468 401 -0.041960   0.1419
    ##  active       0.11378 0.0459 484  0.023605   0.2040
    ## 
    ## ROI = 13:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.63473 0.0461 384  0.544044   0.7254
    ##  active       0.64589 0.0452 465  0.556978   0.7348
    ## 
    ## ROI = 14:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.53435 0.0461 384  0.443670   0.6250
    ##  active       0.52399 0.0452 465  0.435080   0.6129
    ## 
    ## ROI = 15:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.49665 0.0461 384  0.405967   0.5873
    ##  active       0.48375 0.0452 465  0.394843   0.5727
    ## 
    ## ROI = 16:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.43102 0.0461 384  0.340333   0.5217
    ##  active       0.41997 0.0452 465  0.331059   0.5089
    ## 
    ## ROI = 17:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.24912 0.0468 401  0.157198   0.3410
    ##  active       0.26698 0.0459 484  0.176803   0.3571
    ## 
    ## ROI = 18:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.46448 0.0461 384  0.373800   0.5552
    ##  active       0.46345 0.0452 465  0.374546   0.5524
    ## 
    ## ROI = 19:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.25218 0.0461 384  0.161499   0.3429
    ##  active       0.26932 0.0452 465  0.180418   0.3582
    ## 
    ## ROI = 20:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.41025 0.0468 401  0.318329   0.5022
    ##  active       0.47185 0.0459 484  0.381678   0.5620
    ## 
    ## ROI = 21:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.09045 0.0461 384 -0.000234   0.1811
    ##  active       0.11920 0.0452 465  0.030293   0.2081
    ## 
    ## ROI = 22:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.46849 0.0461 384  0.377805   0.5592
    ##  active       0.48215 0.0452 465  0.393245   0.5711
    ## 
    ## ROI = 24:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.16825 0.0468 401  0.076331   0.2602
    ##  active       0.17339 0.0459 484  0.083219   0.2636
    ## 
    ## ROI = 25:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.07939 0.0468 401 -0.012533   0.1713
    ##  active       0.04714 0.0459 484 -0.043032   0.1373
    ## 
    ## ROI = 29:
    ##  Stimulation   emmean     SE  df  lower.CL upper.CL
    ##  sham         0.15691 0.0461 384  0.066226   0.2476
    ##  active       0.14843 0.0452 465  0.059526   0.2373
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.023811 0.0606 862  -0.393  0.6944
    ## 
    ## ROI = 2:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.031328 0.0606 862  -0.517  0.6053
    ## 
    ## ROI = 3:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.025055 0.0606 862  -0.413  0.6793
    ## 
    ## ROI = 4:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.006614 0.0606 862  -0.109  0.9131
    ## 
    ## ROI = 5:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.006803 0.0606 862  -0.112  0.9106
    ## 
    ## ROI = 6:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.026426 0.0606 862  -0.436  0.6629
    ## 
    ## ROI = 7:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.014180 0.0606 862  -0.234  0.8150
    ## 
    ## ROI = 8:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.000942 0.0606 862  -0.016  0.9876
    ## 
    ## ROI = 9:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.038193 0.0606 862   0.630  0.5287
    ## 
    ## ROI = 10:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.079564 0.0606 862  -1.313  0.1895
    ## 
    ## ROI = 11:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.073911 0.0616 885  -1.201  0.2302
    ## 
    ## ROI = 12:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.063813 0.0616 885  -1.037  0.3002
    ## 
    ## ROI = 13:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.011158 0.0606 862  -0.184  0.8539
    ## 
    ## ROI = 14:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.010366 0.0606 862   0.171  0.8642
    ## 
    ## ROI = 15:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.012900 0.0606 862   0.213  0.8315
    ## 
    ## ROI = 16:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.011050 0.0606 862   0.182  0.8553
    ## 
    ## ROI = 17:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.017854 0.0616 885  -0.290  0.7719
    ## 
    ## ROI = 18:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.001030 0.0606 862   0.017  0.9864
    ## 
    ## ROI = 19:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.017143 0.0606 862  -0.283  0.7773
    ## 
    ## ROI = 20:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.061599 0.0616 885  -1.001  0.3173
    ## 
    ## ROI = 21:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.028752 0.0606 862  -0.475  0.6353
    ## 
    ## ROI = 22:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.013664 0.0606 862  -0.226  0.8216
    ## 
    ## ROI = 24:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active -0.005137 0.0616 885  -0.083  0.9335
    ## 
    ## ROI = 25:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.032250 0.0616 885   0.524  0.6005
    ## 
    ## ROI = 29:
    ##  contrast       estimate     SE  df t.ratio p.value
    ##  sham - active  0.008476 0.0606 862   0.140  0.8888
    ## 
    ## Degrees-of-freedom method: kenward-roger

## Betas

``` r
LangRest = lmer(Lang.Rest ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(LangRest)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: Lang.Rest ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: 7571.8
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -2.9287 -0.5933 -0.0666  0.5203  4.4543 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance Std.Dev. Corr
    ##  Subject  (Intercept)       1.0824   1.0404       
    ##           Stimulationactive 0.7996   0.8942   0.25
    ##  Residual                   9.5302   3.0871       
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                         Estimate Std. Error t value
    ## (Intercept)              6.97365    0.59477  11.725
    ## Stimulationactive        0.32373    0.81363   0.398
    ## ROI2                     1.59491    0.79709   2.001
    ## ROI3                    -1.42881    0.79709  -1.793
    ## ROI4                     1.68634    0.79709   2.116
    ## ROI5                    -2.23231    0.79709  -2.801
    ## ROI6                     2.15491    0.79709   2.703
    ## ROI7                     1.17355    0.79709   1.472
    ## ROI8                     3.93380    0.79709   4.935
    ## ROI9                    -3.51399    0.79709  -4.409
    ## ROI10                    2.33445    0.79709   2.929
    ## ROI11                   -1.46382    0.80408  -1.820
    ## ROI12                   -2.97412    0.80408  -3.699
    ## ROI13                    0.01236    0.79709   0.016
    ## ROI14                   -0.58612    0.79709  -0.735
    ## ROI15                   -0.87136    0.79709  -1.093
    ## ROI16                   -2.88371    0.79709  -3.618
    ## ROI17                   -5.28689    0.80408  -6.575
    ## ROI18                    1.14503    0.79709   1.437
    ## ROI19                   -3.55151    0.79709  -4.456
    ## ROI20                    0.50815    0.80408   0.632
    ## ROI21                   -4.11879    0.79709  -5.167
    ## ROI22                   -1.88323    0.79709  -2.363
    ## ROI24                   -4.71342    0.80408  -5.862
    ## ROI25                   -2.20510    0.80408  -2.742
    ## ROI29                   -2.10712    0.79709  -2.644
    ## Stimulationactive:ROI2   0.11265    1.12725   0.100
    ## Stimulationactive:ROI3   0.24332    1.12725   0.216
    ## Stimulationactive:ROI4   0.19473    1.12725   0.173
    ## Stimulationactive:ROI5   0.19549    1.12725   0.173
    ## Stimulationactive:ROI6   0.38662    1.12725   0.343
    ## Stimulationactive:ROI7  -0.04599    1.12725  -0.041
    ## Stimulationactive:ROI8   0.24330    1.12725   0.216
    ## Stimulationactive:ROI9  -0.25432    1.12725  -0.226
    ## Stimulationactive:ROI10  0.73004    1.12725   0.648
    ## Stimulationactive:ROI11  0.61145    1.13710   0.538
    ## Stimulationactive:ROI12 -0.05349    1.13710  -0.047
    ## Stimulationactive:ROI13  0.44768    1.12725   0.397
    ## Stimulationactive:ROI14  0.43980    1.12725   0.390
    ## Stimulationactive:ROI15 -0.17723    1.12725  -0.157
    ## Stimulationactive:ROI16 -0.13657    1.12725  -0.121
    ## Stimulationactive:ROI17  0.38925    1.13710   0.342
    ## Stimulationactive:ROI18 -0.10919    1.12725  -0.097
    ## Stimulationactive:ROI19 -0.10125    1.12725  -0.090
    ## Stimulationactive:ROI20  0.17263    1.13710   0.152
    ## Stimulationactive:ROI21 -0.12817    1.12725  -0.114
    ## Stimulationactive:ROI22 -0.43296    1.12725  -0.384
    ## Stimulationactive:ROI24 -0.53951    1.13710  -0.474
    ## Stimulationactive:ROI25 -0.31186    1.13710  -0.274
    ## Stimulationactive:ROI29 -0.08029    1.12725  -0.071

``` r
plot_model(LangRest, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-3-1.png)

``` r
emmeans(LangRest, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          6.97 0.595 835    5.806     8.14
    ##  active        7.30 0.629 445    6.061     8.53
    ## 
    ## ROI = 2:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          8.57 0.595 835    7.401     9.74
    ##  active        9.00 0.629 445    7.769    10.24
    ## 
    ## ROI = 3:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          5.54 0.595 835    4.377     6.71
    ##  active        6.11 0.629 445    4.875     7.35
    ## 
    ## ROI = 4:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          8.66 0.595 835    7.493     9.83
    ##  active        9.18 0.629 445    7.942    10.41
    ## 
    ## ROI = 5:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          4.74 0.595 835    3.574     5.91
    ##  active        5.26 0.629 445    4.024     6.50
    ## 
    ## ROI = 6:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          9.13 0.595 835    7.961    10.30
    ##  active        9.84 0.629 445    8.603    11.08
    ## 
    ## ROI = 7:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          8.15 0.595 835    6.980     9.31
    ##  active        8.42 0.629 445    7.189     9.66
    ## 
    ## ROI = 8:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham         10.91 0.595 835    9.740    12.07
    ##  active       11.47 0.629 445   10.238    12.71
    ## 
    ## ROI = 9:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          3.46 0.595 835    2.292     4.63
    ##  active        3.53 0.629 445    2.293     4.77
    ## 
    ## ROI = 10:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          9.31 0.595 835    8.141    10.48
    ##  active       10.36 0.629 445    9.125    11.60
    ## 
    ## ROI = 11:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          5.51 0.604 859    4.324     6.70
    ##  active        6.45 0.638 463    5.191     7.70
    ## 
    ## ROI = 12:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          4.00 0.604 859    2.814     5.19
    ##  active        4.27 0.638 463    3.016     5.52
    ## 
    ## ROI = 13:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          6.99 0.595 835    5.819     8.15
    ##  active        7.76 0.629 445    6.521     8.99
    ## 
    ## ROI = 14:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          6.39 0.595 835    5.220     7.55
    ##  active        7.15 0.629 445    5.915     8.39
    ## 
    ## ROI = 15:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          6.10 0.595 835    4.935     7.27
    ##  active        6.25 0.629 445    5.012     7.49
    ## 
    ## ROI = 16:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          4.09 0.595 835    2.923     5.26
    ##  active        4.28 0.629 445    3.041     5.51
    ## 
    ## ROI = 17:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          1.69 0.604 859    0.501     2.87
    ##  active        2.40 0.638 463    1.146     3.65
    ## 
    ## ROI = 18:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          8.12 0.595 835    6.951     9.29
    ##  active        8.33 0.629 445    7.097     9.57
    ## 
    ## ROI = 19:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          3.42 0.595 835    2.255     4.59
    ##  active        3.64 0.629 445    2.408     4.88
    ## 
    ## ROI = 20:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          7.48 0.604 859    6.296     8.67
    ##  active        7.98 0.638 463    6.724     9.23
    ## 
    ## ROI = 21:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          2.85 0.595 835    1.687     4.02
    ##  active        3.05 0.629 445    1.814     4.29
    ## 
    ## ROI = 22:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          5.09 0.595 835    3.923     6.26
    ##  active        4.98 0.629 445    3.745     6.22
    ## 
    ## ROI = 24:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          2.26 0.604 859    1.074     3.45
    ##  active        2.04 0.638 463    0.791     3.30
    ## 
    ## ROI = 25:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          4.77 0.604 859    3.583     5.95
    ##  active        4.78 0.638 463    3.527     6.03
    ## 
    ## ROI = 29:
    ##  Stimulation emmean    SE  df lower.CL upper.CL
    ##  sham          4.87 0.595 835    3.699     6.03
    ##  active        5.11 0.629 445    3.874     6.35
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.3237 0.814 1205  -0.398  0.6908
    ## 
    ## ROI = 2:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.4364 0.814 1205  -0.536  0.5918
    ## 
    ## ROI = 3:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.5670 0.814 1205  -0.697  0.4860
    ## 
    ## ROI = 4:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.5185 0.814 1205  -0.637  0.5241
    ## 
    ## ROI = 5:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.5192 0.814 1205  -0.638  0.5235
    ## 
    ## ROI = 6:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.7104 0.814 1205  -0.873  0.3828
    ## 
    ## ROI = 7:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.2777 0.814 1205  -0.341  0.7329
    ## 
    ## ROI = 8:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.5670 0.814 1205  -0.697  0.4860
    ## 
    ## ROI = 9:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.0694 0.814 1205  -0.085  0.9320
    ## 
    ## ROI = 10:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -1.0538 0.814 1205  -1.295  0.1955
    ## 
    ## ROI = 11:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.9352 0.827 1217  -1.130  0.2585
    ## 
    ## ROI = 12:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.2702 0.827 1217  -0.327  0.7440
    ## 
    ## ROI = 13:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.7714 0.814 1205  -0.948  0.3433
    ## 
    ## ROI = 14:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.7635 0.814 1205  -0.938  0.3482
    ## 
    ## ROI = 15:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.1465 0.814 1205  -0.180  0.8571
    ## 
    ## ROI = 16:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.1872 0.814 1205  -0.230  0.8181
    ## 
    ## ROI = 17:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.7130 0.827 1217  -0.862  0.3889
    ## 
    ## ROI = 18:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.2145 0.814 1205  -0.264  0.7921
    ## 
    ## ROI = 19:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.2225 0.814 1205  -0.273  0.7846
    ## 
    ## ROI = 20:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.4964 0.827 1217  -0.600  0.5486
    ## 
    ## ROI = 21:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.1956 0.814 1205  -0.240  0.8101
    ## 
    ## ROI = 22:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active   0.1092 0.814 1205   0.134  0.8932
    ## 
    ## ROI = 24:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active   0.2158 0.827 1217   0.261  0.7943
    ## 
    ## ROI = 25:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.0119 0.827 1217  -0.014  0.9886
    ## 
    ## ROI = 29:
    ##  contrast      estimate    SE   df t.ratio p.value
    ##  sham - active  -0.2434 0.814 1205  -0.299  0.7648
    ## 
    ## Degrees-of-freedom method: kenward-roger

``` r
ControlTaskRest = lmer(ControlTask.Rest ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(ControlTaskRest)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: ControlTask.Rest ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: 5551.1
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -3.2791 -0.6628 -0.0365  0.5755  4.6998 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance Std.Dev. Corr 
    ##  Subject  (Intercept)       0.3803   0.6167        
    ##           Stimulationactive 0.6549   0.8092   -0.39
    ##  Residual                   2.2975   1.5158        
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                          Estimate Std. Error t value
    ## (Intercept)              2.874189   0.298767   9.620
    ## Stimulationactive        0.276185   0.418328   0.660
    ## ROI2                    -0.494260   0.391369  -1.263
    ## ROI3                    -2.168842   0.391369  -5.542
    ## ROI4                    -1.372784   0.391369  -3.508
    ## ROI5                    -3.192909   0.391369  -8.158
    ## ROI6                     0.650195   0.391369   1.661
    ## ROI7                    -0.399476   0.391369  -1.021
    ## ROI8                    -0.251398   0.391369  -0.642
    ## ROI9                    -3.116256   0.391369  -7.962
    ## ROI10                   -1.335294   0.391369  -3.412
    ## ROI11                   -2.950595   0.394837  -7.473
    ## ROI12                   -2.545729   0.394837  -6.448
    ## ROI13                    0.824126   0.391369   2.106
    ## ROI14                    0.332620   0.391369   0.850
    ## ROI15                    0.776234   0.391369   1.983
    ## ROI16                    0.001628   0.391369   0.004
    ## ROI17                   -1.101801   0.394837  -2.791
    ## ROI18                    0.231212   0.391369   0.591
    ## ROI19                   -1.114362   0.391369  -2.847
    ## ROI20                   -0.181004   0.394837  -0.458
    ## ROI21                   -2.240273   0.391369  -5.724
    ## ROI22                    0.286078   0.391369   0.731
    ## ROI24                   -1.744505   0.394837  -4.418
    ## ROI25                   -2.218851   0.394837  -5.620
    ## ROI29                   -1.870748   0.391369  -4.780
    ## Stimulationactive:ROI2   0.122113   0.553479   0.221
    ## Stimulationactive:ROI3  -0.080159   0.553479  -0.145
    ## Stimulationactive:ROI4  -0.159694   0.553479  -0.289
    ## Stimulationactive:ROI5  -0.081137   0.553479  -0.147
    ## Stimulationactive:ROI6   0.017785   0.553479   0.032
    ## Stimulationactive:ROI7  -0.045513   0.553479  -0.082
    ## Stimulationactive:ROI8  -0.293537   0.553479  -0.530
    ## Stimulationactive:ROI9  -0.169241   0.553479  -0.306
    ## Stimulationactive:ROI10  0.127962   0.553479   0.231
    ## Stimulationactive:ROI11  0.194055   0.558380   0.348
    ## Stimulationactive:ROI12  0.120232   0.558380   0.215
    ## Stimulationactive:ROI13 -0.101321   0.553479  -0.183
    ## Stimulationactive:ROI14  0.001426   0.553479   0.003
    ## Stimulationactive:ROI15 -0.218753   0.553479  -0.395
    ## Stimulationactive:ROI16 -0.107627   0.553479  -0.194
    ## Stimulationactive:ROI17 -0.062664   0.558380  -0.112
    ## Stimulationactive:ROI18 -0.124142   0.553479  -0.224
    ## Stimulationactive:ROI19 -0.003416   0.553479  -0.006
    ## Stimulationactive:ROI20 -0.008413   0.558380  -0.015
    ## Stimulationactive:ROI21 -0.066089   0.553479  -0.119
    ## Stimulationactive:ROI22 -0.181701   0.553479  -0.328
    ## Stimulationactive:ROI24 -0.426658   0.558380  -0.764
    ## Stimulationactive:ROI25 -0.428979   0.558380  -0.768
    ## Stimulationactive:ROI29 -0.342440   0.553479  -0.619

``` r
plot_model(ControlTaskRest, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-3-2.png)

``` r
emmeans(ControlTaskRest, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         2.8742 0.299 638   2.2875    3.461
    ##  active       3.1504 0.313 390   2.5349    3.766
    ## 
    ## ROI = 2:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         2.3799 0.299 638   1.7932    2.967
    ##  active       2.7782 0.313 390   2.1628    3.394
    ## 
    ## ROI = 3:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         0.7053 0.299 638   0.1187    1.292
    ##  active       0.9014 0.313 390   0.2859    1.517
    ## 
    ## ROI = 4:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         1.5014 0.299 638   0.9147    2.088
    ##  active       1.6179 0.313 390   1.0025    2.233
    ## 
    ## ROI = 5:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham        -0.3187 0.299 638  -0.9054    0.268
    ##  active      -0.1237 0.313 390  -0.7391    0.492
    ## 
    ## ROI = 6:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         3.5244 0.299 638   2.9377    4.111
    ##  active       3.8184 0.313 390   3.2029    4.434
    ## 
    ## ROI = 7:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         2.4747 0.299 638   1.8880    3.061
    ##  active       2.7054 0.313 390   2.0899    3.321
    ## 
    ## ROI = 8:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         2.6228 0.299 638   2.0361    3.209
    ##  active       2.6054 0.313 390   1.9900    3.221
    ## 
    ## ROI = 9:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham        -0.2421 0.299 638  -0.8288    0.345
    ##  active      -0.1351 0.313 390  -0.7506    0.480
    ## 
    ## ROI = 10:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         1.5389 0.299 638   0.9522    2.126
    ##  active       1.9430 0.313 390   1.3276    2.558
    ## 
    ## ROI = 11:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham        -0.0764 0.303 661  -0.6720    0.519
    ##  active       0.3938 0.317 407  -0.2301    1.018
    ## 
    ## ROI = 12:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         0.3285 0.303 661  -0.2671    0.924
    ##  active       0.7249 0.317 407   0.1010    1.349
    ## 
    ## ROI = 13:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         3.6983 0.299 638   3.1116    4.285
    ##  active       3.8732 0.313 390   3.2577    4.489
    ## 
    ## ROI = 14:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         3.2068 0.299 638   2.6201    3.793
    ##  active       3.4844 0.313 390   2.8690    4.100
    ## 
    ## ROI = 15:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         3.6504 0.299 638   3.0637    4.237
    ##  active       3.7079 0.313 390   3.0924    4.323
    ## 
    ## ROI = 16:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         2.8758 0.299 638   2.2891    3.463
    ##  active       3.0444 0.313 390   2.4289    3.660
    ## 
    ## ROI = 17:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         1.7724 0.303 661   1.1768    2.368
    ##  active       1.9859 0.317 407   1.3620    2.610
    ## 
    ## ROI = 18:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         3.1054 0.299 638   2.5187    3.692
    ##  active       3.2574 0.313 390   2.6420    3.873
    ## 
    ## ROI = 19:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         1.7598 0.299 638   1.1731    2.347
    ##  active       2.0326 0.313 390   1.4172    2.648
    ## 
    ## ROI = 20:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         2.6932 0.303 661   2.0976    3.289
    ##  active       2.9610 0.317 407   2.3371    3.585
    ## 
    ## ROI = 21:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         0.6339 0.299 638   0.0472    1.221
    ##  active       0.8440 0.313 390   0.2286    1.459
    ## 
    ## ROI = 22:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         3.1603 0.299 638   2.5736    3.747
    ##  active       3.2548 0.313 390   2.6393    3.870
    ## 
    ## ROI = 24:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         1.1297 0.303 661   0.5341    1.725
    ##  active       0.9792 0.317 407   0.3553    1.603
    ## 
    ## ROI = 25:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         0.6553 0.303 661   0.0598    1.251
    ##  active       0.5025 0.317 407  -0.1214    1.126
    ## 
    ## ROI = 29:
    ##  Stimulation  emmean    SE  df lower.CL upper.CL
    ##  sham         1.0034 0.299 638   0.4168    1.590
    ##  active       0.9372 0.313 390   0.3217    1.553
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2762 0.418 717  -0.660  0.5093
    ## 
    ## ROI = 2:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.3983 0.418 717  -0.952  0.3414
    ## 
    ## ROI = 3:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1960 0.418 717  -0.469  0.6395
    ## 
    ## ROI = 4:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1165 0.418 717  -0.278  0.7807
    ## 
    ## ROI = 5:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1950 0.418 717  -0.466  0.6412
    ## 
    ## ROI = 6:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2940 0.418 717  -0.703  0.4825
    ## 
    ## ROI = 7:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2307 0.418 717  -0.551  0.5815
    ## 
    ## ROI = 8:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0174 0.418 717   0.041  0.9669
    ## 
    ## ROI = 9:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1069 0.418 717  -0.256  0.7983
    ## 
    ## ROI = 10:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.4041 0.418 717  -0.966  0.3343
    ## 
    ## ROI = 11:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.4702 0.425 740  -1.107  0.2687
    ## 
    ## ROI = 12:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.3964 0.425 740  -0.933  0.3510
    ## 
    ## ROI = 13:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1749 0.418 717  -0.418  0.6761
    ## 
    ## ROI = 14:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2776 0.418 717  -0.664  0.5071
    ## 
    ## ROI = 15:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0574 0.418 717  -0.137  0.8908
    ## 
    ## ROI = 16:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1686 0.418 717  -0.403  0.6871
    ## 
    ## ROI = 17:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2135 0.425 740  -0.503  0.6154
    ## 
    ## ROI = 18:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1520 0.418 717  -0.363  0.7164
    ## 
    ## ROI = 19:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2728 0.418 717  -0.652  0.5146
    ## 
    ## ROI = 20:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2678 0.425 740  -0.630  0.5287
    ## 
    ## ROI = 21:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2101 0.418 717  -0.502  0.6157
    ## 
    ## ROI = 22:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0945 0.418 717  -0.226  0.8214
    ## 
    ## ROI = 24:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1505 0.425 740   0.354  0.7233
    ## 
    ## ROI = 25:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1528 0.425 740   0.360  0.7192
    ## 
    ## ROI = 29:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0663 0.418 717   0.158  0.8742
    ## 
    ## Degrees-of-freedom method: kenward-roger

``` r
LangControlTask = lmer(Lang.ControlTask ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(LangControlTask)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: Lang.ControlTask ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: 7107
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -3.7907 -0.5781 -0.0333  0.4959  5.2703 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance Std.Dev. Corr 
    ##  Subject  (Intercept)       1.356    1.164         
    ##           Stimulationactive 1.845    1.358    -0.60
    ##  Residual                   6.797    2.607         
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                          Estimate Std. Error t value
    ## (Intercept)              1.225271   0.521302   2.350
    ## Stimulationactive       -0.228639   0.717376  -0.319
    ## ROI2                     2.583428   0.673146   3.838
    ## ROI3                     2.908874   0.673146   4.321
    ## ROI4                     4.431908   0.673146   6.584
    ## ROI5                     4.153506   0.673146   6.170
    ## ROI6                     0.854524   0.673146   1.269
    ## ROI7                     1.972507   0.673146   2.930
    ## ROI8                     4.436594   0.673146   6.591
    ## ROI9                     2.718526   0.673146   4.039
    ## ROI10                    5.005035   0.673146   7.435
    ## ROI11                    4.443714   0.679120   6.543
    ## ROI12                    2.114529   0.679120   3.114
    ## ROI13                   -1.635895   0.673146  -2.430
    ## ROI14                   -1.251363   0.673146  -1.859
    ## ROI15                   -2.423826   0.673146  -3.601
    ## ROI16                   -2.886969   0.673146  -4.289
    ## ROI17                   -3.085731   0.679120  -4.544
    ## ROI18                    0.682608   0.673146   1.014
    ## ROI19                   -1.322785   0.673146  -1.965
    ## ROI20                    0.877624   0.679120   1.292
    ## ROI21                    0.361753   0.673146   0.537
    ## ROI22                   -2.455386   0.673146  -3.648
    ## ROI24                   -1.225164   0.679120  -1.804
    ## ROI25                    2.227756   0.679120   3.280
    ## ROI29                    1.634372   0.673146   2.428
    ## Stimulationactive:ROI2  -0.131574   0.951972  -0.138
    ## Stimulationactive:ROI3   0.403634   0.951972   0.424
    ## Stimulationactive:ROI4   0.514118   0.951972   0.540
    ## Stimulationactive:ROI5   0.357767   0.951972   0.376
    ## Stimulationactive:ROI6   0.351052   0.951972   0.369
    ## Stimulationactive:ROI7   0.045035   0.951972   0.047
    ## Stimulationactive:ROI8   0.830378   0.951972   0.872
    ## Stimulationactive:ROI9   0.084166   0.951972   0.088
    ## Stimulationactive:ROI10  0.474112   0.951972   0.498
    ## Stimulationactive:ROI11  0.208056   0.960403   0.217
    ## Stimulationactive:ROI12 -0.288507   0.960403  -0.300
    ## Stimulationactive:ROI13  0.650323   0.951972   0.683
    ## Stimulationactive:ROI14  0.436945   0.951972   0.459
    ## Stimulationactive:ROI15  0.260278   0.951972   0.273
    ## Stimulationactive:ROI16  0.078688   0.951972   0.083
    ## Stimulationactive:ROI17  0.517904   0.960403   0.539
    ## Stimulationactive:ROI18  0.139091   0.951972   0.146
    ## Stimulationactive:ROI19 -0.094417   0.951972  -0.099
    ## Stimulationactive:ROI20  0.176690   0.960403   0.184
    ## Stimulationactive:ROI21  0.004006   0.951972   0.004
    ## Stimulationactive:ROI22 -0.069563   0.951972  -0.073
    ## Stimulationactive:ROI24  0.315622   0.960403   0.329
    ## Stimulationactive:ROI25  0.554171   0.960403   0.577
    ## Stimulationactive:ROI29  0.604588   0.951972   0.635

``` r
plot_model(LangControlTask, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-3-3.png)

``` r
emmeans(LangControlTask, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.225271 0.521 543   0.2013    2.249
    ##  active       0.996632 0.520 561  -0.0242    2.017
    ## 
    ## ROI = 2:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         3.808699 0.521 543   2.7847    4.833
    ##  active       3.448486 0.520 561   2.4276    4.469
    ## 
    ## ROI = 3:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         4.134145 0.521 543   3.1101    5.158
    ##  active       4.309140 0.520 561   3.2883    5.330
    ## 
    ## ROI = 4:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         5.657178 0.521 543   4.6332    6.681
    ##  active       5.942658 0.520 561   4.9218    6.964
    ## 
    ## ROI = 5:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         5.378776 0.521 543   4.3548    6.403
    ##  active       5.507904 0.520 561   4.4870    6.529
    ## 
    ## ROI = 6:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         2.079795 0.521 543   1.0558    3.104
    ##  active       2.202208 0.520 561   1.1813    3.223
    ## 
    ## ROI = 7:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         3.197777 0.521 543   2.1738    4.222
    ##  active       3.014174 0.520 561   1.9933    4.035
    ## 
    ## ROI = 8:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         5.661865 0.521 543   4.6378    6.686
    ##  active       6.263604 0.520 561   5.2427    7.284
    ## 
    ## ROI = 9:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         3.943796 0.521 543   2.9198    4.968
    ##  active       3.799324 0.520 561   2.7785    4.820
    ## 
    ## ROI = 10:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         6.230305 0.521 543   5.2063    7.254
    ##  active       6.475779 0.520 561   5.4549    7.497
    ## 
    ## ROI = 11:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         5.668984 0.529 564   4.6299    6.708
    ##  active       5.648402 0.527 583   4.6124    6.684
    ## 
    ## ROI = 12:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         3.339799 0.529 564   2.3007    4.379
    ##  active       2.822654 0.527 583   1.7867    3.859
    ## 
    ## ROI = 13:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -0.410624 0.521 543  -1.4346    0.613
    ##  active       0.011060 0.520 561  -1.0098    1.032
    ## 
    ## ROI = 14:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -0.026092 0.521 543  -1.0501    0.998
    ##  active       0.182214 0.520 561  -0.8387    1.203
    ## 
    ## ROI = 15:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.198555 0.521 543  -2.2226   -0.175
    ##  active      -1.166916 0.520 561  -2.1878   -0.146
    ## 
    ## ROI = 16:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.661699 0.521 543  -2.6857   -0.638
    ##  active      -1.811650 0.520 561  -2.8325   -0.791
    ## 
    ## ROI = 17:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.860460 0.529 564  -2.8995   -0.821
    ##  active      -1.571195 0.527 583  -2.6071   -0.535
    ## 
    ## ROI = 18:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.907878 0.521 543   0.8839    2.932
    ##  active       1.818331 0.520 561   0.7975    2.839
    ## 
    ## ROI = 19:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -0.097514 0.521 543  -1.1215    0.927
    ##  active      -0.420570 0.520 561  -1.4414    0.600
    ## 
    ## ROI = 20:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         2.102895 0.529 564   1.0638    3.142
    ##  active       2.050946 0.527 583   1.0150    3.087
    ## 
    ## ROI = 21:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.587023 0.521 543   0.5630    2.611
    ##  active       1.362390 0.520 561   0.3415    2.383
    ## 
    ## ROI = 22:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.230116 0.521 543  -2.2541   -0.206
    ##  active      -1.528317 0.520 561  -2.5492   -0.507
    ## 
    ## ROI = 24:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         0.000106 0.529 564  -1.0390    1.039
    ##  active       0.087090 0.527 583  -0.9489    1.123
    ## 
    ## ROI = 25:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         3.453027 0.529 564   2.4140    4.492
    ##  active       3.778559 0.527 583   2.7426    4.815
    ## 
    ## ROI = 29:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         2.859643 0.521 543   1.8356    3.884
    ##  active       3.235592 0.520 561   2.2147    4.256
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2286 0.717 743   0.319  0.7500
    ## 
    ## ROI = 2:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.3602 0.717 743   0.502  0.6157
    ## 
    ## ROI = 3:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1750 0.717 743  -0.244  0.8073
    ## 
    ## ROI = 4:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2855 0.717 743  -0.398  0.6908
    ## 
    ## ROI = 5:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1291 0.717 743  -0.180  0.8572
    ## 
    ## ROI = 6:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1224 0.717 743  -0.171  0.8646
    ## 
    ## ROI = 7:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1836 0.717 743   0.256  0.7981
    ## 
    ## ROI = 8:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.6017 0.717 743  -0.839  0.4018
    ## 
    ## ROI = 9:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1445 0.717 743   0.201  0.8404
    ## 
    ## ROI = 10:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2455 0.717 743  -0.342  0.7323
    ## 
    ## ROI = 11:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0206 0.729 766   0.028  0.9775
    ## 
    ## ROI = 12:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.5171 0.729 766   0.710  0.4780
    ## 
    ## ROI = 13:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.4217 0.717 743  -0.588  0.5568
    ## 
    ## ROI = 14:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2083 0.717 743  -0.290  0.7716
    ## 
    ## ROI = 15:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0316 0.717 743  -0.044  0.9648
    ## 
    ## ROI = 16:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1500 0.717 743   0.209  0.8345
    ## 
    ## ROI = 17:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2893 0.729 766  -0.397  0.6914
    ## 
    ## ROI = 18:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0895 0.717 743   0.125  0.9007
    ## 
    ## ROI = 19:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.3231 0.717 743   0.450  0.6526
    ## 
    ## ROI = 20:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0519 0.729 766   0.071  0.9432
    ## 
    ## ROI = 21:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2246 0.717 743   0.313  0.7543
    ## 
    ## ROI = 22:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2982 0.717 743   0.416  0.6778
    ## 
    ## ROI = 24:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0870 0.729 766  -0.119  0.9050
    ## 
    ## ROI = 25:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.3255 0.729 766  -0.447  0.6551
    ## 
    ## ROI = 29:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.3759 0.717 743  -0.524  0.6004
    ## 
    ## Degrees-of-freedom method: kenward-roger

``` r
ControlTaskLang = lmer(ControlTask.Lang ~ Stimulation*ROI + (1 + Stimulation|Subject), df_TMS)
summary(ControlTaskLang)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: ControlTask.Lang ~ Stimulation * ROI + (1 + Stimulation | Subject)
    ##    Data: df_TMS
    ## 
    ## REML criterion at convergence: 7107
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -5.2703 -0.4959  0.0333  0.5781  3.7907 
    ## 
    ## Random effects:
    ##  Groups   Name              Variance Std.Dev. Corr 
    ##  Subject  (Intercept)       1.356    1.164         
    ##           Stimulationactive 1.845    1.358    -0.60
    ##  Residual                   6.797    2.607         
    ## Number of obs: 1488, groups:  Subject, 30
    ## 
    ## Fixed effects:
    ##                          Estimate Std. Error t value
    ## (Intercept)             -1.225271   0.521302  -2.350
    ## Stimulationactive        0.228639   0.717376   0.319
    ## ROI2                    -2.583428   0.673146  -3.838
    ## ROI3                    -2.908874   0.673146  -4.321
    ## ROI4                    -4.431908   0.673146  -6.584
    ## ROI5                    -4.153506   0.673146  -6.170
    ## ROI6                    -0.854524   0.673146  -1.269
    ## ROI7                    -1.972507   0.673146  -2.930
    ## ROI8                    -4.436594   0.673146  -6.591
    ## ROI9                    -2.718526   0.673146  -4.039
    ## ROI10                   -5.005035   0.673146  -7.435
    ## ROI11                   -4.443714   0.679120  -6.543
    ## ROI12                   -2.114529   0.679120  -3.114
    ## ROI13                    1.635895   0.673146   2.430
    ## ROI14                    1.251363   0.673146   1.859
    ## ROI15                    2.423826   0.673146   3.601
    ## ROI16                    2.886969   0.673146   4.289
    ## ROI17                    3.085731   0.679120   4.544
    ## ROI18                   -0.682608   0.673146  -1.014
    ## ROI19                    1.322785   0.673146   1.965
    ## ROI20                   -0.877624   0.679120  -1.292
    ## ROI21                   -0.361753   0.673146  -0.537
    ## ROI22                    2.455386   0.673146   3.648
    ## ROI24                    1.225164   0.679120   1.804
    ## ROI25                   -2.227756   0.679120  -3.280
    ## ROI29                   -1.634372   0.673146  -2.428
    ## Stimulationactive:ROI2   0.131574   0.951972   0.138
    ## Stimulationactive:ROI3  -0.403634   0.951972  -0.424
    ## Stimulationactive:ROI4  -0.514118   0.951972  -0.540
    ## Stimulationactive:ROI5  -0.357767   0.951972  -0.376
    ## Stimulationactive:ROI6  -0.351052   0.951972  -0.369
    ## Stimulationactive:ROI7  -0.045035   0.951972  -0.047
    ## Stimulationactive:ROI8  -0.830378   0.951972  -0.872
    ## Stimulationactive:ROI9  -0.084166   0.951972  -0.088
    ## Stimulationactive:ROI10 -0.474112   0.951972  -0.498
    ## Stimulationactive:ROI11 -0.208056   0.960403  -0.217
    ## Stimulationactive:ROI12  0.288507   0.960403   0.300
    ## Stimulationactive:ROI13 -0.650323   0.951972  -0.683
    ## Stimulationactive:ROI14 -0.436945   0.951972  -0.459
    ## Stimulationactive:ROI15 -0.260278   0.951972  -0.273
    ## Stimulationactive:ROI16 -0.078688   0.951972  -0.083
    ## Stimulationactive:ROI17 -0.517904   0.960403  -0.539
    ## Stimulationactive:ROI18 -0.139091   0.951972  -0.146
    ## Stimulationactive:ROI19  0.094417   0.951972   0.099
    ## Stimulationactive:ROI20 -0.176690   0.960403  -0.184
    ## Stimulationactive:ROI21 -0.004006   0.951972  -0.004
    ## Stimulationactive:ROI22  0.069563   0.951972   0.073
    ## Stimulationactive:ROI24 -0.315622   0.960403  -0.329
    ## Stimulationactive:ROI25 -0.554171   0.960403  -0.577
    ## Stimulationactive:ROI29 -0.604588   0.951972  -0.635

``` r
plot_model(ControlTaskLang, type = "pred", terms = c("ROI", "Stimulation"))
```

![](Localizer_indivROIs_estimates_files/figure-markdown_github/unnamed-chunk-3-4.png)

``` r
emmeans(ControlTaskLang, pairwise ~ Stimulation|ROI, adjust = "holm", type = "response")
```

    ## $emmeans
    ## ROI = 1:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.225271 0.521 543   -2.249  -0.2013
    ##  active      -0.996632 0.520 561   -2.017   0.0242
    ## 
    ## ROI = 2:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -3.808699 0.521 543   -4.833  -2.7847
    ##  active      -3.448486 0.520 561   -4.469  -2.4276
    ## 
    ## ROI = 3:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -4.134145 0.521 543   -5.158  -3.1101
    ##  active      -4.309140 0.520 561   -5.330  -3.2883
    ## 
    ## ROI = 4:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -5.657178 0.521 543   -6.681  -4.6332
    ##  active      -5.942658 0.520 561   -6.964  -4.9218
    ## 
    ## ROI = 5:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -5.378776 0.521 543   -6.403  -4.3548
    ##  active      -5.507904 0.520 561   -6.529  -4.4870
    ## 
    ## ROI = 6:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -2.079795 0.521 543   -3.104  -1.0558
    ##  active      -2.202208 0.520 561   -3.223  -1.1813
    ## 
    ## ROI = 7:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -3.197777 0.521 543   -4.222  -2.1738
    ##  active      -3.014174 0.520 561   -4.035  -1.9933
    ## 
    ## ROI = 8:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -5.661865 0.521 543   -6.686  -4.6378
    ##  active      -6.263604 0.520 561   -7.284  -5.2427
    ## 
    ## ROI = 9:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -3.943796 0.521 543   -4.968  -2.9198
    ##  active      -3.799324 0.520 561   -4.820  -2.7785
    ## 
    ## ROI = 10:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -6.230305 0.521 543   -7.254  -5.2063
    ##  active      -6.475779 0.520 561   -7.497  -5.4549
    ## 
    ## ROI = 11:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -5.668984 0.529 564   -6.708  -4.6299
    ##  active      -5.648402 0.527 583   -6.684  -4.6124
    ## 
    ## ROI = 12:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -3.339799 0.529 564   -4.379  -2.3007
    ##  active      -2.822654 0.527 583   -3.859  -1.7867
    ## 
    ## ROI = 13:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         0.410624 0.521 543   -0.613   1.4346
    ##  active      -0.011060 0.520 561   -1.032   1.0098
    ## 
    ## ROI = 14:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         0.026092 0.521 543   -0.998   1.0501
    ##  active      -0.182214 0.520 561   -1.203   0.8387
    ## 
    ## ROI = 15:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.198555 0.521 543    0.175   2.2226
    ##  active       1.166916 0.520 561    0.146   2.1878
    ## 
    ## ROI = 16:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.661699 0.521 543    0.638   2.6857
    ##  active       1.811650 0.520 561    0.791   2.8325
    ## 
    ## ROI = 17:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.860460 0.529 564    0.821   2.8995
    ##  active       1.571195 0.527 583    0.535   2.6071
    ## 
    ## ROI = 18:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.907878 0.521 543   -2.932  -0.8839
    ##  active      -1.818331 0.520 561   -2.839  -0.7975
    ## 
    ## ROI = 19:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         0.097514 0.521 543   -0.927   1.1215
    ##  active       0.420570 0.520 561   -0.600   1.4414
    ## 
    ## ROI = 20:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -2.102895 0.529 564   -3.142  -1.0638
    ##  active      -2.050946 0.527 583   -3.087  -1.0150
    ## 
    ## ROI = 21:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -1.587023 0.521 543   -2.611  -0.5630
    ##  active      -1.362390 0.520 561   -2.383  -0.3415
    ## 
    ## ROI = 22:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham         1.230116 0.521 543    0.206   2.2541
    ##  active       1.528317 0.520 561    0.507   2.5492
    ## 
    ## ROI = 24:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -0.000106 0.529 564   -1.039   1.0390
    ##  active      -0.087090 0.527 583   -1.123   0.9489
    ## 
    ## ROI = 25:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -3.453027 0.529 564   -4.492  -2.4140
    ##  active      -3.778559 0.527 583   -4.815  -2.7426
    ## 
    ## ROI = 29:
    ##  Stimulation    emmean    SE  df lower.CL upper.CL
    ##  sham        -2.859643 0.521 543   -3.884  -1.8356
    ##  active      -3.235592 0.520 561   -4.256  -2.2147
    ## 
    ## Degrees-of-freedom method: kenward-roger 
    ## Confidence level used: 0.95 
    ## 
    ## $contrasts
    ## ROI = 1:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2286 0.717 743  -0.319  0.7500
    ## 
    ## ROI = 2:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.3602 0.717 743  -0.502  0.6157
    ## 
    ## ROI = 3:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1750 0.717 743   0.244  0.8073
    ## 
    ## ROI = 4:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2855 0.717 743   0.398  0.6908
    ## 
    ## ROI = 5:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1291 0.717 743   0.180  0.8572
    ## 
    ## ROI = 6:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.1224 0.717 743   0.171  0.8646
    ## 
    ## ROI = 7:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1836 0.717 743  -0.256  0.7981
    ## 
    ## ROI = 8:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.6017 0.717 743   0.839  0.4018
    ## 
    ## ROI = 9:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1445 0.717 743  -0.201  0.8404
    ## 
    ## ROI = 10:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2455 0.717 743   0.342  0.7323
    ## 
    ## ROI = 11:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0206 0.729 766  -0.028  0.9775
    ## 
    ## ROI = 12:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.5171 0.729 766  -0.710  0.4780
    ## 
    ## ROI = 13:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.4217 0.717 743   0.588  0.5568
    ## 
    ## ROI = 14:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2083 0.717 743   0.290  0.7716
    ## 
    ## ROI = 15:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0316 0.717 743   0.044  0.9648
    ## 
    ## ROI = 16:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.1500 0.717 743  -0.209  0.8345
    ## 
    ## ROI = 17:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.2893 0.729 766   0.397  0.6914
    ## 
    ## ROI = 18:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0895 0.717 743  -0.125  0.9007
    ## 
    ## ROI = 19:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.3231 0.717 743  -0.450  0.6526
    ## 
    ## ROI = 20:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.0519 0.729 766  -0.071  0.9432
    ## 
    ## ROI = 21:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2246 0.717 743  -0.313  0.7543
    ## 
    ## ROI = 22:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active  -0.2982 0.717 743  -0.416  0.6778
    ## 
    ## ROI = 24:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.0870 0.729 766   0.119  0.9050
    ## 
    ## ROI = 25:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.3255 0.729 766   0.447  0.6551
    ## 
    ## ROI = 29:
    ##  contrast      estimate    SE  df t.ratio p.value
    ##  sham - active   0.3759 0.717 743   0.524  0.6004
    ## 
    ## Degrees-of-freedom method: kenward-roger

## Loops with individual models per task and ROI

### Loop for PSC WPM

``` r
# prepare df
df_PSC_WPM <- df_TMS[, c(1:2, 10, 14)]
df_PSC_WPM <- df_PSC_WPM %>% 
  pivot_wider(names_from = ROI, values_from = PSC_WPM, names_prefix = "ROI_")

# create data frame to store results
results_PSC_WPM <- data.frame()

# loop through the columns
for(var in names(df_PSC_WPM)[c(3:length(df_PSC_WPM))]){
        # dynamically generate formula
        fmla <- as.formula(paste(paste(var), "~ Stimulation"))

        # fit glm model
        fit <- lm(fmla, data = df_PSC_WPM)
        
        # get coefficents of fit
        cfit <- coef(summary(fit))

        #assign("v1", get("var"))
        LRT_model <- drop1(fit, test = "Chisq")
        
        # create temporary data frame
        df <- data.frame(var = var, 
                         intercept = cfit[1], 
                         beta_stimulation <- cfit[2],
                         #beta_age <- cfit[3],
                         #beta_edu <- cfit[4],
                         #beta_rms <- cfit[5],
                         #beta_network_age_int <- cfit[6],
                         std.error_intercept <- cfit[3],
                         std.error_stimulation <- cfit[4],
                         #std.error_age <- cfit[9],
                         #std.error_edu <- cfit[10],
                         #std.error_rms <- cfit[11],
                         #std.error_network_age_int <- cfit[12],
                         t.value_intercept <- cfit[5],
                         t.value_stimulation <- cfit[6],
                         #z.value_age <- cfit[15],
                         #z.value_edu <- cfit[16],
                         #z.value_rms <- cfit[17],
                         #z.value_network_age_int <- cfit[18],
                         p.value_intercept <- cfit[7],
                         p.value_stimulation <- cfit[8],
                         #p.value_age <- cfit[21],
                         #p.value_edu <- cfit[22],
                         #p.value_rms <- cfit[23],
                         #p.value_network_age_int <- cfit[24],
                         AIC = AIC(fit),
                         Deviance = deviance(fit),
                         #AIC_drop1_edu <- drop_int[2,2],
                         #AIC_drop1_rms <- drop_int[3,2],
                         #AIC_drop1_network_age_int <- drop_int[4,2],
                         LRT_drop1_stimulation <- LRT_model[2,3],
                         #LRT_drop1_rms <- drop_int[3,3],
                         #LRT_drop1_network_age_int <- drop_int[4,3],
                         p.value_drop1_stimulation <- LRT_model[2,5],
                         #p.value_drop1_rms <- drop_int[3,4],
                         #p.value_drop1_network_age_int <- drop_int[4,4],
                         #p.value_drop1_network_age_int_corrected <- (p.value_drop1_network_age_int < alpha_bonferroni),
                         stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results_PSC_WPM <- rbind(results_PSC_WPM, df)
}
results_PSC_WPM
```

    ##       var intercept beta_stimulation....cfit.2. std.error_intercept....cfit.3.
    ## 1   ROI_1 0.4779619                -0.007143903                     0.04035486
    ## 2  ROI_19 0.2234534                 0.003210555                     0.02401229
    ## 3  ROI_24 0.1636838                -0.013066040                     0.03371531
    ## 4  ROI_16 0.3191393                 0.007922754                     0.03701177
    ## 5  ROI_15 0.4011446                -0.001180895                     0.02806417
    ## 6   ROI_2 0.6434800                 0.011267570                     0.04488815
    ## 7   ROI_6 0.7431241                 0.031412280                     0.05826171
    ## 8  ROI_21 0.1936698                -0.011650299                     0.02370771
    ## 9  ROI_29 0.2618817                 0.005216080                     0.04093389
    ## 10 ROI_18 0.4931256                 0.003934520                     0.04275652
    ## 11  ROI_7 0.4694529                -0.008136943                     0.03484470
    ## 12 ROI_22 0.3502659                -0.005986182                     0.03347563
    ## 13  ROI_3 0.4480087                 0.016817213                     0.04769783
    ## 14 ROI_14 0.5243583                 0.046095553                     0.03935685
    ## 15 ROI_17 0.1203327                 0.063780283                     0.05527809
    ## 16  ROI_8 0.6959612                 0.012122833                     0.05726023
    ## 17  ROI_5 0.3427407                 0.030589880                     0.04132561
    ## 18 ROI_11 0.4058093                 0.037285842                     0.04594221
    ## 19  ROI_9 0.2452024                -0.001667489                     0.03235469
    ## 20 ROI_20 0.4798275                 0.033480838                     0.05448331
    ## 21  ROI_4 0.6506862                 0.006220037                     0.03901960
    ## 22 ROI_10 0.6546878                 0.057182850                     0.05978203
    ## 23 ROI_13 0.6164288                 0.045811667                     0.05583364
    ## 24 ROI_12 0.2823966                -0.010066496                     0.04135021
    ## 25 ROI_25 0.2771508                -0.020877009                     0.04243791
    ##    std.error_stimulation....cfit.4. t.value_intercept....cfit.5.
    ## 1                        0.05707039                    11.843974
    ## 2                        0.03395851                     9.305791
    ## 3                        0.04768065                     4.854881
    ## 4                        0.05234255                     8.622644
    ## 5                        0.03968874                    14.293833
    ## 6                        0.06348142                    14.335188
    ## 7                        0.08239450                    12.754931
    ## 8                        0.03352776                     8.169064
    ## 9                        0.05788927                     6.397675
    ## 10                       0.06046684                    11.533345
    ## 11                       0.04927785                    13.472719
    ## 12                       0.04734170                    10.463309
    ## 13                       0.06745492                     9.392643
    ## 14                       0.05565899                    13.323177
    ## 15                       0.07817502                     2.176861
    ## 16                       0.08097820                    12.154355
    ## 17                       0.05844323                     8.293664
    ## 18                       0.06497210                     8.833037
    ## 19                       0.04575644                     7.578572
    ## 20                       0.07705104                     8.806870
    ## 21                       0.05518204                    16.675883
    ## 22                       0.08454455                    10.951249
    ## 23                       0.07896069                    11.040456
    ## 24                       0.05847803                     6.829387
    ## 25                       0.06001627                     6.530736
    ##    t.value_stimulation....cfit.6.     p.value_intercept....cfit.7.
    ## 1                     -0.12517705 0.000000000000000040634276133788
    ## 2                      0.09454347 0.000000000000416567044557015330
    ## 3                     -0.27403231 0.000010048514893318414231086072
    ## 4                      0.15136356 0.000000000005591312028280110291
    ## 5                     -0.02975391 0.000000000000000000011592819150
    ## 6                      0.17749397 0.000000000000000000010167333950
    ## 7                      0.38124243 0.000000000000000001785809088604
    ## 8                     -0.34748212 0.000000000031913876470529643447
    ## 9                      0.09010445 0.000000029863086626667103808850
    ## 10                     0.06506905 0.000000000000000120805939503129
    ## 11                    -0.16512374 0.000000000000000000164092124141
    ## 12                    -0.12644629 0.000000000000005640719896815036
    ## 13                     0.24931042 0.000000000000300260839929535334
    ## 14                     0.82817806 0.000000000000000000268356039961
    ## 15                     0.81586524 0.033722714282465711399527918957
    ## 16                     0.14970491 0.000000000000000013846887265114
    ## 17                     0.52341185 0.000000000019755583472831619236
    ## 18                     0.57387463 0.000000000003360839994645554524
    ## 19                    -0.03644271 0.000000000312285633167433990847
    ## 20                     0.43452806 0.000000000003706035575254892090
    ## 21                     0.11271850 0.000000000000000000000008518768
    ## 22                     0.67636349 0.000000000000000961117275165970
    ## 23                     0.58018323 0.000000000000000697562769536190
    ## 24                    -0.17214150 0.000000006621248727795013168555
    ## 25                    -0.34785584 0.000000020565154927984744942326
    ##    p.value_stimulation....cfit.8.          AIC  Deviance
    ## 1                       0.9008163  -6.89484305 2.8336155
    ## 2                       0.9250033 -69.19234722 1.0032670
    ## 3                       0.7850681 -29.35243136 1.8460372
    ## 4                       0.8802142 -17.27194572 2.3835756
    ## 5                       0.9763655 -50.48099917 1.3704203
    ## 6                       0.8597388   5.88058951 3.5060054
    ## 7                       0.7044164  37.17315358 5.9063028
    ## 8                       0.7294870 -70.72422310 0.9779766
    ## 9                       0.9285147  -5.18525507 2.9155153
    ## 10                      0.9483428   0.04233115 3.1809281
    ## 11                      0.8694208 -24.51213290 2.1126265
    ## 12                      0.8998160 -29.32212021 1.9498754
    ## 13                      0.8040015  13.16604598 3.9586442
    ## 14                      0.4109646  -9.89985998 2.6951930
    ## 15                      0.4180340  28.00080414 4.9624033
    ## 16                      0.8815168  35.09250286 5.7049975
    ## 17                      0.6026818  -4.04238374 2.9715819
    ## 18                      0.5683505   6.54170999 3.4277558
    ## 19                      0.9710545 -33.40917379 1.8214774
    ## 20                      0.6655748  26.32087595 4.8207324
    ## 21                      0.9106430 -10.93258316 2.6492002
    ## 22                      0.5014983  40.26434986 6.2185701
    ## 23                      0.5640384  32.06492097 5.4242674
    ## 24                      0.8639469  -5.67388181 2.7767804
    ## 25                      0.7292526  -2.66200145 2.9247853
    ##    LRT_drop1_stimulation....LRT_model.2..3.
    ## 1                                 2.8343810
    ## 2                                 1.0034216
    ## 3                                 1.8485126
    ## 4                                 2.3845172
    ## 5                                 1.3704413
    ## 6                                 3.5079098
    ## 7                                 5.9211038
    ## 8                                 0.9800125
    ## 9                                 2.9159234
    ## 10                                3.1811603
    ## 11                                2.1136196
    ## 12                                1.9504130
    ## 13                                3.9628865
    ## 14                                2.7270650
    ## 15                                5.0213882
    ## 16                                5.7072019
    ## 17                                2.9856180
    ## 18                                3.4479142
    ## 19                                1.8215191
    ## 20                                4.8369865
    ## 21                                2.6497805
    ## 22                                6.2676183
    ## 23                                5.4557480
    ## 24                                2.7782497
    ## 25                                2.9311052
    ##    p.value_drop1_stimulation....LRT_model.2..5.
    ## 1                                     0.8986963
    ## 2                                     0.9233967
    ## 3                                     0.7804065
    ## 4                                     0.8776602
    ## 5                                     0.9758577
    ## 6                                     0.8567571
    ## 7                                     0.6983733
    ## 8                                     0.7239095
    ## 9                                     0.9269828
    ## 10                                    0.9472344
    ## 11                                    0.8666408
    ## 12                                    0.8976749
    ## 13                                    0.7998783
    ## 14                                    0.4009860
    ## 15                                    0.4077527
    ## 16                                    0.8789902
    ## 17                                    0.5949112
    ## 18                                    0.5597745
    ## 19                                    0.9704328
    ## 20                                    0.6585997
    ## 21                                    0.9087311
    ## 22                                    0.4923503
    ## 23                                    0.5556949
    ## 24                                    0.8609497
    ## 25                                    0.7234722

### Loop for PSC FPM

``` r
# prepare df
df_PSC_FPM <- df_TMS[, c(1:2, 11, 14)]
df_PSC_FPM <- df_PSC_FPM %>% 
  pivot_wider(names_from = ROI, values_from = PSC_FPM, names_prefix = "ROI_")

# create data frame to store results
results_PSC_FPM <- data.frame()

# loop through the columns
for(var in names(df_PSC_FPM)[c(3:length(df_PSC_FPM))]){
        # dynamically generate formula
        fmla <- as.formula(paste(paste(var), "~ Stimulation"))

        # fit glm model
        fit <- lm(fmla, data = df_PSC_FPM)
        
        # get coefficents of fit
        cfit <- coef(summary(fit))

        #assign("v1", get("var"))
        LRT_model <- drop1(fit, test = "Chisq")
        
        # create temporary data frame
        df <- data.frame(var = var, 
                         intercept = cfit[1], 
                         beta_stimulation <- cfit[2],
                         #beta_age <- cfit[3],
                         #beta_edu <- cfit[4],
                         #beta_rms <- cfit[5],
                         #beta_network_age_int <- cfit[6],
                         std.error_intercept <- cfit[3],
                         std.error_stimulation <- cfit[4],
                         #std.error_age <- cfit[9],
                         #std.error_edu <- cfit[10],
                         #std.error_rms <- cfit[11],
                         #std.error_network_age_int <- cfit[12],
                         t.value_intercept <- cfit[5],
                         t.value_stimulation <- cfit[6],
                         #z.value_age <- cfit[15],
                         #z.value_edu <- cfit[16],
                         #z.value_rms <- cfit[17],
                         #z.value_network_age_int <- cfit[18],
                         p.value_intercept <- cfit[7],
                         p.value_stimulation <- cfit[8],
                         #p.value_age <- cfit[21],
                         #p.value_edu <- cfit[22],
                         #p.value_rms <- cfit[23],
                         #p.value_network_age_int <- cfit[24],
                         AIC = AIC(fit),
                         Deviance = deviance(fit),
                         #AIC_drop1_edu <- drop_int[2,2],
                         #AIC_drop1_rms <- drop_int[3,2],
                         #AIC_drop1_network_age_int <- drop_int[4,2],
                         LRT_drop1_stimulation <- LRT_model[2,3],
                         #LRT_drop1_rms <- drop_int[3,3],
                         #LRT_drop1_network_age_int <- drop_int[4,3],
                         p.value_drop1_stimulation <- LRT_model[2,5],
                         #p.value_drop1_rms <- drop_int[3,4],
                         #p.value_drop1_network_age_int <- drop_int[4,4],
                         #p.value_drop1_network_age_int_corrected <- (p.value_drop1_network_age_int < alpha_bonferroni),
                         stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results_PSC_FPM <- rbind(results_PSC_FPM, df)
}

results_PSC_FPM
```

    ##       var intercept beta_stimulation....cfit.2. std.error_intercept....cfit.3.
    ## 1   ROI_1 0.4652298                0.0212566523                     0.04366280
    ## 2  ROI_19 0.2620000               -0.0144193871                     0.02835194
    ## 3  ROI_24 0.2023697               -0.0125043230                     0.03426403
    ## 4  ROI_16 0.3160713               -0.0024171155                     0.04222263
    ## 5  ROI_15 0.4496981                0.0083140280                     0.03275831
    ## 6   ROI_2 0.6448234               -0.0016060200                     0.04257066
    ## 7   ROI_6 0.7154461                0.0333197567                     0.05621818
    ## 8  ROI_21 0.2390041               -0.0025984760                     0.02608496
    ## 9  ROI_29 0.4391481                0.0033664673                     0.05019370
    ## 10 ROI_18 0.6357046               -0.0139856433                     0.04826107
    ## 11  ROI_7 0.5410354               -0.0018097967                     0.03856959
    ## 12 ROI_22 0.4194925               -0.0210067680                     0.03720346
    ## 13  ROI_3 0.4561134                0.0339893385                     0.05018094
    ## 14 ROI_14 0.4912108                0.0504735601                     0.03769467
    ## 15 ROI_17 0.1079213                0.0427113469                     0.06259737
    ## 16  ROI_8 0.7399595                0.0070036433                     0.05607778
    ## 17  ROI_5 0.3864419                0.0253265127                     0.03957819
    ## 18 ROI_11 0.3981578                0.0536110824                     0.04410670
    ## 19  ROI_9 0.3066896               -0.0006006603                     0.03012178
    ## 20 ROI_20 0.5925043                0.0349284948                     0.05517199
    ## 21  ROI_4 0.6551376                0.0006593733                     0.03839053
    ## 22 ROI_10 0.6573792                0.0614996267                     0.06049820
    ## 23 ROI_13 0.5707936                0.0498303300                     0.05311678
    ## 24 ROI_12 0.3048173                0.0116503359                     0.04146272
    ## 25 ROI_25 0.4292759               -0.0031583634                     0.04723129
    ##    std.error_stimulation....cfit.4. t.value_intercept....cfit.5.
    ## 1                        0.06174853                    10.655061
    ## 2                        0.04009569                     9.240993
    ## 3                        0.04845666                     5.906184
    ## 4                        0.05971182                     7.485827
    ## 5                        0.04632725                    13.727754
    ## 6                        0.06020400                    15.147134
    ## 7                        0.07950451                    12.726240
    ## 8                        0.03688970                     9.162527
    ## 9                        0.07098461                     8.749068
    ## 10                       0.06825147                    13.172203
    ## 11                       0.05454563                    14.027514
    ## 12                       0.05261363                    11.275631
    ## 13                       0.07096657                     9.089376
    ## 14                       0.05330832                    13.031307
    ## 15                       0.08852605                     1.724055
    ## 16                       0.07930596                    13.195235
    ## 17                       0.05597201                     9.764013
    ## 18                       0.06237629                     9.027150
    ## 19                       0.04259863                    10.181654
    ## 20                       0.07802497                    10.739224
    ## 21                       0.05429240                    17.065087
    ## 22                       0.08555737                    10.866096
    ## 23                       0.07511846                    10.746014
    ## 24                       0.05863714                     7.351598
    ## 25                       0.06679513                     9.088805
    ##    t.value_stimulation....cfit.6.    p.value_intercept....cfit.7.
    ## 1                      0.34424549 0.00000000000000280458430560602
    ## 2                     -0.35962433 0.00000000000053205636742643658
    ## 3                     -0.25805171 0.00000021591500822367200736310
    ## 4                     -0.04047968 0.00000000044714886672553371827
    ## 5                      0.17946301 0.00000000000000000007138910353
    ## 6                     -0.02667630 0.00000000000000000000080763587
    ## 7                      0.41909266 0.00000000000000000196730716716
    ## 8                     -0.07043907 0.00000000000071590744620066036
    ## 9                      0.04742532 0.00000000000344859710740974621
    ## 10                    -0.20491345 0.00000000000000000044222793482
    ## 11                    -0.03317950 0.00000000000000000002712657122
    ## 12                    -0.39926473 0.00000000000000030104617888039
    ## 13                     0.47894862 0.00000000000094456001654306393
    ## 14                     0.94682340 0.00000000000000000070671688019
    ## 15                     0.48247209 0.09021479410042138047387538791
    ## 16                     0.08831169 0.00000000000000000040970283393
    ## 17                     0.45248532 0.00000000000007464192569716598
    ## 18                     0.85947849 0.00000000000163007817356974637
    ## 19                    -0.01410046 0.00000000000001586155716220339
    ## 20                     0.44765789 0.00000000000000323705895725998
    ## 21                     0.01214485 0.00000000000000000000000279256
    ## 22                     0.71881157 0.00000000000000130625674511718
    ## 23                     0.66335661 0.00000000000000201638283763856
    ## 24                     0.19868526 0.00000000090792096490634615741
    ## 25                    -0.04728434 0.00000000000129624403587966692
    ##    p.value_stimulation....cfit.8.         AIC Deviance
    ## 1                       0.7319067   2.5593264 3.317206
    ## 2                       0.7204342 -49.2568156 1.398668
    ## 3                       0.7973130 -27.4797329 1.906615
    ## 4                       0.9678497  -1.4654946 3.101986
    ## 5                       0.8581997 -31.9214431 1.867206
    ## 6                       0.9788095  -0.4804356 3.153334
    ## 7                       0.6766965  32.8885609 5.499242
    ## 8                       0.9440865 -59.2572071 1.183939
    ## 9                       0.9623372  19.2864745 4.383768
    ## 10                      0.8383572  14.5747784 4.052688
    ## 11                      0.9736454 -12.3245774 2.588446
    ## 12                      0.6911645 -16.6520592 2.408329
    ## 13                      0.6337760  19.2559723 4.381540
    ## 14                      0.3476569 -15.0780113 2.472346
    ## 15                      0.6313512  42.4250138 6.363531
    ## 16                      0.9299332  32.5885080 5.471809
    ## 17                      0.6526074  -9.2268828 2.725593
    ## 18                      0.3937410   1.8120755 3.159331
    ## 19                      0.9887982 -41.9904351 1.578740
    ## 20                      0.6561271  27.7779388 4.943372
    ## 21                      0.9903517 -12.8829774 2.564468
    ## 22                      0.4751431  41.6933653 6.368455
    ## 23                      0.5097303  26.0788773 4.909222
    ## 24                      0.8432285  -5.3586921 2.791911
    ## 25                      0.9624548   9.7516745 3.622810
    ##    LRT_drop1_stimulation....LRT_model.2..3.
    ## 1                                  3.323984
    ## 2                                  1.401787
    ## 3                                  1.908882
    ## 4                                  3.102074
    ## 5                                  1.868243
    ## 6                                  3.153372
    ## 7                                  5.515895
    ## 8                                  1.184041
    ## 9                                  4.383938
    ## 10                                 4.055622
    ## 11                                 2.588496
    ## 12                                 2.414948
    ## 13                                 4.398870
    ## 14                                 2.510559
    ## 15                                 6.389983
    ## 16                                 5.472545
    ## 17                                 2.735215
    ## 18                                 3.201006
    ## 19                                 1.578745
    ## 20                                 4.961062
    ## 21                                 2.564475
    ## 22                                 6.425188
    ## 23                                 4.946468
    ## 24                                 2.793879
    ## 25                                 3.622955
    ##    p.value_drop1_stimulation....LRT_model.2..5.
    ## 1                                     0.7263749
    ## 2                                     0.7146871
    ## 3                                     0.7929041
    ## 4                                     0.9671593
    ## 5                                     0.8551859
    ## 6                                     0.9783542
    ## 7                                     0.6701552
    ## 8                                     0.9428870
    ## 9                                     0.9615285
    ## 10                                    0.8349332
    ## 11                                    0.9730793
    ## 12                                    0.6848810
    ## 13                                    0.6265023
    ## 14                                    0.3373979
    ## 15                                    0.6237780
    ## 16                                    0.9284315
    ## 17                                    0.6456488
    ## 18                                    0.3833025
    ## 19                                    0.9885575
    ## 20                                    0.6489834
    ## 21                                    0.9901444
    ## 22                                    0.4657085
    ## 23                                    0.5006786
    ## 24                                    0.8397866
    ## 25                                    0.9616200

### Loop for PSC tone

``` r
# prepare df
df_PSC_tone <- df_TMS[, c(1:2, 12, 14)]
df_PSC_tone <- df_PSC_tone %>% 
  pivot_wider(names_from = ROI, values_from = PSC_tone, names_prefix = "ROI_")

# create data frame to store results
results_PSC_tone <- data.frame()

# loop through the columns
for(var in names(df_PSC_tone)[c(3:length(df_PSC_tone))]){
        # dynamically generate formula
        fmla <- as.formula(paste(paste(var), "~ Stimulation"))

        # fit glm model
        fit <- lm(fmla, data = df_PSC_tone)
        
        # get coefficents of fit
        cfit <- coef(summary(fit))

        #assign("v1", get("var"))
        LRT_model <- drop1(fit, test = "Chisq")
        
        # create temporary data frame
        df <- data.frame(var = var, 
                         intercept = cfit[1], 
                         beta_stimulation <- cfit[2],
                         #beta_age <- cfit[3],
                         #beta_edu <- cfit[4],
                         #beta_rms <- cfit[5],
                         #beta_network_age_int <- cfit[6],
                         std.error_intercept <- cfit[3],
                         std.error_stimulation <- cfit[4],
                         #std.error_age <- cfit[9],
                         #std.error_edu <- cfit[10],
                         #std.error_rms <- cfit[11],
                         #std.error_network_age_int <- cfit[12],
                         t.value_intercept <- cfit[5],
                         t.value_stimulation <- cfit[6],
                         #z.value_age <- cfit[15],
                         #z.value_edu <- cfit[16],
                         #z.value_rms <- cfit[17],
                         #z.value_network_age_int <- cfit[18],
                         p.value_intercept <- cfit[7],
                         p.value_stimulation <- cfit[8],
                         #p.value_age <- cfit[21],
                         #p.value_edu <- cfit[22],
                         #p.value_rms <- cfit[23],
                         #p.value_network_age_int <- cfit[24],
                         AIC = AIC(fit),
                         Deviance = deviance(fit),
                         #AIC_drop1_edu <- drop_int[2,2],
                         #AIC_drop1_rms <- drop_int[3,2],
                         #AIC_drop1_network_age_int <- drop_int[4,2],
                         LRT_drop1_stimulation <- LRT_model[2,3],
                         #LRT_drop1_rms <- drop_int[3,3],
                         #LRT_drop1_network_age_int <- drop_int[4,3],
                         p.value_drop1_stimulation <- LRT_model[2,5],
                         #p.value_drop1_rms <- drop_int[3,4],
                         #p.value_drop1_network_age_int <- drop_int[4,4],
                         #p.value_drop1_network_age_int_corrected <- (p.value_drop1_network_age_int < alpha_bonferroni),
                         stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results_PSC_tone <- rbind(results_PSC_tone, df)
}
results_PSC_tone
```

    ##       var    intercept beta_stimulation....cfit.2.
    ## 1   ROI_1  0.383215118                0.0238110263
    ## 2  ROI_19  0.252182167                0.0171425447
    ## 3  ROI_24  0.172290950                0.0036669131
    ## 4  ROI_16  0.431016048               -0.0110502286
    ## 5  ROI_15  0.496649730               -0.0128997377
    ## 6   ROI_2  0.404061869                0.0313282909
    ## 7   ROI_6  0.570793966                0.0264260867
    ## 8  ROI_21  0.090448526                0.0287518088
    ## 9  ROI_29  0.156909018               -0.0084758021
    ## 10 ROI_18  0.464483163               -0.0010303361
    ## 11  ROI_7  0.331399228                0.0141802016
    ## 12 ROI_22  0.468488002                0.0136640043
    ## 13  ROI_3  0.136510839                0.0250549781
    ## 14 ROI_14  0.534352864               -0.0103660663
    ## 15 ROI_17  0.247223735                0.0192267454
    ## 16  ROI_8  0.366437221                0.0009419723
    ## 17  ROI_5 -0.040941492                0.0068031034
    ## 18 ROI_11 -0.019145312                0.0805615138
    ## 19  ROI_9 -0.009309551               -0.0381925394
    ## 20 ROI_20  0.415782927                0.0601834600
    ## 21  ROI_4  0.255826796                0.0066137496
    ## 22 ROI_10  0.222861545                0.0795643133
    ## 23 ROI_13  0.634726744                0.0111584183
    ## 24 ROI_12  0.054653672                0.0610399440
    ## 25 ROI_25  0.075044149               -0.0293370101
    ##    std.error_intercept....cfit.3. std.error_stimulation....cfit.4.
    ## 1                      0.04421693                       0.06253218
    ## 2                      0.03346516                       0.04732688
    ## 3                      0.03518806                       0.04976343
    ## 4                      0.04712456                       0.06664419
    ## 5                      0.04010263                       0.05671368
    ## 6                      0.04562741                       0.06452690
    ## 7                      0.06125135                       0.08662249
    ## 8                      0.02928369                       0.04141339
    ## 9                      0.04152440                       0.05872437
    ## 10                     0.05025758                       0.07107495
    ## 11                     0.04040541                       0.05714188
    ## 12                     0.03510221                       0.04964202
    ## 13                     0.05312321                       0.07512756
    ## 14                     0.04627984                       0.06544958
    ## 15                     0.05163384                       0.07302128
    ## 16                     0.04582940                       0.06481256
    ## 17                     0.03726491                       0.05270054
    ## 18                     0.04380058                       0.06194337
    ## 19                     0.03542065                       0.05009236
    ## 20                     0.05306000                       0.07503817
    ## 21                     0.04763865                       0.06737123
    ## 22                     0.05548586                       0.07846886
    ## 23                     0.05563498                       0.07867974
    ## 24                     0.04606534                       0.06514622
    ## 25                     0.05511679                       0.07794691
    ##    t.value_intercept....cfit.5. t.value_stimulation....cfit.6.
    ## 1                     8.6667056                     0.38078035
    ## 2                     7.5356637                     0.36221583
    ## 3                     4.8962898                     0.07368690
    ## 4                     9.1463152                    -0.16580934
    ## 5                    12.3844674                    -0.22745370
    ## 6                     8.8556836                     0.48550748
    ## 7                     9.3188795                     0.30507188
    ## 8                     3.0887000                     0.69426363
    ## 9                     3.7787183                    -0.14433193
    ## 10                    9.2420523                    -0.01449647
    ## 11                    8.2018522                     0.24815776
    ## 12                   13.3463964                     0.27525077
    ## 13                    2.5697024                     0.33349915
    ## 14                   11.5461252                    -0.15838247
    ## 15                    4.7880173                     0.26330331
    ## 16                    7.9956802                     0.01453379
    ## 17                   -1.0986606                     0.12908981
    ## 18                   -0.4371018                     1.30056719
    ## 19                   -0.2628284                    -0.76244242
    ## 20                    7.8360903                     0.80203798
    ## 21                    5.3701516                     0.09816875
    ## 22                    4.0165464                     1.01396036
    ## 23                   11.4087709                     0.14182073
    ## 24                    1.1864382                     0.93696829
    ## 25                    1.3615480                    -0.37637170
    ##    p.value_intercept....cfit.7. p.value_stimulation....cfit.8.        AIC
    ## 1   0.0000000000047240629965297                      0.7047574   4.072672
    ## 2   0.0000000003687005949944374                      0.7185072 -29.359683
    ## 3   0.0000086738870621033032379                      0.9415221 -24.392882
    ## 4   0.0000000000007612326854751                      0.8688836  11.715049
    ## 5   0.0000000000000000062829214                      0.8208708  -7.647226
    ## 6   0.0000000000022963203832624                      0.6291454   7.840765
    ## 7   0.0000000000003964961092850                      0.7614035  43.178050
    ## 8   0.0030836823782265381128920                      0.4902885 -45.376586
    ## 9   0.0003740667840643264237949                      0.8857387  -3.466508
    ## 10  0.0000000000005299301011634                      0.9884836  19.439101
    ## 11  0.0000000000281278550660868                      0.8048889  -6.744607
    ## 12  0.0000000000000000002485771                      0.7841018 -23.628570
    ## 13  0.0127708880917480876310277                      0.7399602  26.093410
    ## 14  0.0000000000000001154823402                      0.8747056   9.544522
    ## 15  0.0000127313036404741047213                      0.7932832  20.089691
    ## 16  0.0000000000622765415546234                      0.9884540   8.370836
    ## 17  0.2764559495946291023393826                      0.8977332 -16.453996
    ## 18  0.6637184919280376682593214                      0.1987335   1.004168
    ## 19  0.7936142399259030755231947                      0.4488857 -22.544873
    ## 20  0.0000000001439373266945827                      0.4259215  23.250221
    ## 21  0.0000014485722642483002580                      0.9221368  13.017081
    ## 22  0.0001720321530486204847452                      0.3148130  31.315132
    ## 23  0.0000000000000001876401646                      0.8877131  31.637198
    ## 24  0.2404585141232932488097873                      0.3527983   6.852162
    ## 25  0.1787936098560936604151550                      0.7080628  27.661822
    ##    Deviance LRT_drop1_stimulation....LRT_model.2..3.
    ## 1  3.401938                                 3.410443
    ## 2  1.948655                                 1.953063
    ## 3  2.010836                                 2.011031
    ## 4  3.864060                                 3.865891
    ## 5  2.798305                                 2.800801
    ## 6  3.622437                                 3.637159
    ## 7  6.528007                                 6.538482
    ## 8  1.492110                                 1.504510
    ## 9  3.000240                                 3.001318
    ## 10 4.394934                                 4.394950
    ## 11 2.840720                                 2.843736
    ## 12 2.143967                                 2.146768
    ## 13 4.910411                                 4.919827
    ## 14 3.726774                                 3.728385
    ## 15 4.329671                                 4.335032
    ## 16 3.654581                                 3.654594
    ## 17 2.416292                                 2.416986
    ## 18 3.115629                                 3.209736
    ## 19 2.183043                                 2.204923
    ## 20 4.572150                                 4.624670
    ## 21 3.948828                                 3.949484
    ## 22 5.356905                                 5.451862
    ## 23 5.385737                                 5.387604
    ## 24 3.446153                                 3.500178
    ## 25 4.933485                                 4.945965
    ##    p.value_drop1_stimulation....LRT_model.2..5.
    ## 1                                     0.6987205
    ## 2                                     0.7127242
    ## 3                                     0.9402232
    ## 4                                     0.8660925
    ## 5                                     0.8170891
    ## 6                                     0.6217959
    ## 7                                     0.7564372
    ## 8                                     0.4810145
    ## 9                                     0.8833008
    ## 10                                    0.9882362
    ## 11                                    0.8007836
    ## 12                                    0.7795812
    ## 13                                    0.7345811
    ## 14                                    0.8720362
    ## 15                                    0.7887909
    ## 16                                    0.9882059
    ## 17                                    0.8955481
    ## 18                                    0.1889292
    ## 19                                    0.4392005
    ## 20                                    0.4156994
    ## 21                                    0.9204691
    ## 22                                    0.3045300
    ## 23                                    0.8853167
    ## 24                                    0.3421899
    ## 25                                    0.7018742
