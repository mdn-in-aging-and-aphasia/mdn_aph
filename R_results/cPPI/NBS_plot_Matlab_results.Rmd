---
title: "NBS results from Matlab - Plots"
author: "Sandra Martin"
date: "11/10/2021"
output:
  md_document:
    variant: markdown_github
editor_options: 
  chunk_output_type: console
---


```{r setup, include=FALSE, echo=FALSE}
rm(list= ls()) # clear all 
knitr::opts_chunk$set()
options(scipen = 999)
```

# Load packages
```{r packages, message=FALSE, warning=TRUE}
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(RColorBrewer)
library(rstatix)
library(circlize)
library(ComplexHeatmap)
```


```{r Theme for plots, include=FALSE}
#source("https://gist.githubusercontent.com/benmarwick/2a1bb0133ff568cbe28d/raw/fb53bd97121f7f9ce947837ef1a4c65a73bffb3f/geom_flat_violin.R")

apatheme <- theme_bw()+
  theme(plot.title=element_text(family='serif',size=14,hjust = .5),
        panel.grid.major=element_blank(), panel.grid.minor=element_blank(),
        panel.border=element_blank(),axis.line=element_line(),
        text=element_text(family='sans',size=18))

today <- Sys.Date()
today <- format(today, format="%y%m%d")

palet <- rev(brewer.pal(name = "RdBu", n = 8))
palet.positive <- brewer.pal(name = "Reds", n = 8)

output_path = "/Users/sandramartin/ownCloud/MDN_APH/Results/R_results/cPPI/"
```

# Load data
```{r Load RData}
# 9 ROIs: Yeo 7 + semantic networks
load(here::here("cPPI/RData/ROI_labels_wholeNets_Yeo7.RData"))
load(here::here("cPPI/RData/active_cPPI_correlation_wholeNets_9ROIs_FisherTransform_confRMSD_NO_GSR.RData"))
load(here::here("cPPI/RData/sham_cPPI_correlation_wholeNets_9ROIs_FisherTransform_confRMSD_NO_GSR.RData"))

load(here::here("cPPI/RData/NBS_res_9ROIs_t1.96_sham_active_cPPIWithRMSD_NoGSR.RData"))

# 11 ROIs: Yeo 17
load("cPPI/RData/ROI_labels_wholeNets.RData")
load("cPPI/RData/active_cPPI_correlation_wholeNets_11ROIs_FisherTransform_confRMSD_NO_GSR.RData")
load("cPPI/RData/sham_cPPI_correlation_wholeNets_11ROIs_FisherTransform_confRMSD_NO_GSR.RData")
load("cPPI/RData/active_cPPI_pvalues_wholeNets_11ROIs_confRMSD_NO_GSR.RData")
load("cPPI/RData/sham_cPPI_pvalues_wholeNets_11ROIs_confRMSD_NO_GSR.RData")

load("cPPI/RData/NBS_res_11ROIs_t1.96_sham_active_cPPIWithRMSD_NoGSR.RData")
```

# Read in data
```{r Read in data}
# define path for individual cPPI matrices
path = here::here("cPPI/") 

# read table with NBS results
NBS_res <- read.table(paste0(path, "sham_active_Yeo7.txt"), sep = " ", fill = TRUE, col.names = ROI_labels[,1], row.names = ROI_labels[,1]) 

save(NBS_res, file = "cPPI/RData/NBS_res_9ROIs_t1.96_sham_active_cPPIWithRMSD_NoGSR.RData")
```

# Plots
## Chord diagram NBS results - 9 ROIs (Yeo 7) 
```{r}
#### Preparations for plot #### 
allCond <- sham

diag(allCond) <- NA
allCond[lower.tri(allCond)] <- NA

names = c("Limbic", "Visual", "Somatomotor", "Semantic", "Ventral Attention", "Frontoparietal", "Default", "Semantic Control", "Dorsal Attention")
colnames(allCond) <- names
rownames(allCond) <- names

# Make matrix into long df
allCond_long <- cor_gather(allCond)

# Create cond column
allCond_long$cond <- 'sham'

# Create df for NBS results of both cond
allCond_NBS <- NBS_res
diag(allCond_NBS) <- NA
allCond_NBS[lower.tri(allCond_NBS)] <- NA

colnames(allCond_NBS) <- names
rownames(allCond_NBS) <- names

# Make matrix into long df
allCond_NBS_long <- cor_gather(allCond_NBS)

# Create cond column
allCond_NBS_long$cond <- 'sham'

# Create df with combined network and cond values for thresholding
allCond_NBS_combined = data.frame(from = paste(allCond_NBS_long[[1]], allCond_NBS_long[[4]], sep = "|"),
                 to = paste(allCond_NBS_long[[2]], allCond_NBS_long[[4]], sep = "|"),
                 value = allCond_NBS_long[[3]], stringsAsFactors = FALSE)
allCond_NBS_combined <- filter(allCond_NBS_combined, value == 1)

# Assign colors for age groups and networks
all_cond = unique(allCond_long[[4]])
color_cond = structure(c("#410139"), names = all_cond)
color_networks = "#CFCFCF"

# Create df grouped by networks
df2 = data.frame(from = paste(allCond_long[[1]], allCond_long[[4]], sep = "|"),
                 to = paste(allCond_long[[2]], allCond_long[[4]], sep = "|"),
                 value = allCond_long[[3]], stringsAsFactors = FALSE)

combined = unique(data.frame(networks = c(allCond_long[[1]], allCond_long[[2]]), 
    cond = c(allCond_long[[4]], allCond_long[[4]]), stringsAsFactors = FALSE))
#combined <- combined[-c(2,4,8),]

combined = combined[order(combined$networks, combined$cond), ]
order = paste(combined$networks, combined$cond, sep = "|")

df2 <- df2[(df2$from %in% order), ]
df2 <- df2[(df2$to %in% order), ]

comb <- paste(df2$from, df2$to, sep = "__")
comb2 <- paste(allCond_NBS_combined$from, allCond_NBS_combined$to, sep = "__")
connections2keep <- which(comb %in% comb2)
df2 <- df2[connections2keep, ]

# Prepare colors and graphics for plot
grid.col = structure(color_cond[combined$cond], names = order)

col_fun = colorRamp2(c(0, 1.2), c("white", "#B72D2D"), transparency = 0.2)(df2[[3]])
col_fun[allCond_NBS_combined[[3]] < 1] <- "#FFFFFF00" # threshold color matrix according to p values matrix from cPPI calculation

par(family = "sans")

order = c("Visual|sham", "Somatomotor|sham", "Dorsal Attention|sham", "Ventral Attention|sham", "Limbic|sham", "Frontoparietal|sham",  "Default|sham", "Semantic|sham", "Semantic Control|sham")

col_fun2 = colorRamp2(c(0, 1.2), c("white", "#B72D2D"), transparency = 0.2) # color function for legend
lgd = Legend(col_fun = col_fun2, title = "r", legend_height = unit(3, "cm"))


df_add <- data.frame(from = c("Semantic|sham", "Semantic Control|sham"),
                     to = c("Semantic|sham", "Semantic Control|sham"),
                     value = c(0.7, 1.3))
df2 <- full_join(df2, df_add)
  
#### Plot ####
pdf(paste0(output_path, "chord_NBS_Sham_Active_Yeo7_", today, ".pdf")) 

circos.par(start.degree = 180, gap.degree = 1.5)
chordDiagram(df2, order = order, annotationTrack = "grid",
    grid.col = grid.col, col = col_fun, 
    preAllocateTracks = list(
        track.height = 0.15,
        track.margin = c(0.05, 0)
    )
)
for(net in unique(combined$networks)) {
    l = combined$networks == net
    sn = paste(combined$networks[l], combined$cond[l], sep = "|")
    highlight.sector(sn, track.index = 1, col = color_networks, facing = "bending.inside", text = net, cex = 1.5)
}
circos.clear()

legend("bottomright", pch = 15, col = color_cond,
        legend = names(color_cond), cex = 1, box.lty = 0)
 
draw(lgd, x = unit(2, "cm"), y = unit(1, "cm"), just = c("right", "bottom"))

dev.off()

```


## Chord diagram NBS results - 11 ROIs (Yeo 17) 
```{r}
#### Preparations for plot #### 
allCond <- sham

diag(allCond) <- NA
allCond[lower.tri(allCond)] <- NA

# Make matrix into long df
allCond_long <- cor_gather(allCond)

# Create cond column
allCond_long$cond <- 'sham'

# Create df for NBS results of both cond
allCond_NBS <- NBS_res
diag(allCond_NBS) <- NA
allCond_NBS[lower.tri(allCond_NBS)] <- NA

# Make matrix into long df
allCond_NBS_long <- cor_gather(allCond_NBS)

# Create cond column
allCond_NBS_long$cond <- 'sham'

# Create df with combined network and cond values for thresholding
allCond_NBS_combined = data.frame(from = paste(allCond_NBS_long[[1]], allCond_NBS_long[[4]], sep = "|"),
                 to = paste(allCond_NBS_long[[2]], allCond_NBS_long[[4]], sep = "|"),
                 value = allCond_NBS_long[[3]], stringsAsFactors = FALSE)
allCond_NBS_combined <- filter(allCond_NBS_combined, value == 1)

# Assign colors for age groups and networks
all_cond = unique(allCond_long[[4]])
color_cond = structure(c("#410139"), names = all_cond)
color_networks = "#CFCFCF"

# Create df grouped by networks
df2 = data.frame(from = paste(allCond_long[[1]], allCond_long[[4]], sep = "|"),
                 to = paste(allCond_long[[2]], allCond_long[[4]], sep = "|"),
                 value = allCond_long[[3]], stringsAsFactors = FALSE)

combined = unique(data.frame(networks = c(allCond_long[[1]], allCond_long[[2]]), 
    cond = c(allCond_long[[4]], allCond_long[[4]]), stringsAsFactors = FALSE))
combined <- combined[-c(7),]

combined = combined[order(combined$networks, combined$cond), ]
order = paste(combined$networks, combined$cond, sep = "|")

df2 <- df2[(df2$from %in% order), ]
df2 <- df2[(df2$to %in% order), ]

comb <- paste(df2$from, df2$to, sep = "__")
comb2 <- paste(allCond_NBS_combined$from, allCond_NBS_combined$to, sep = "__")
connections2keep <- which(comb %in% comb2)
df2 <- df2[connections2keep, ]

# Prepare colors and graphics for plot
grid.col = structure(color_cond[combined$cond], names = order)

col_fun = colorRamp2(c(0, 1.2), c("white", "#B72D2D"), transparency = 0.2)(df2[[3]])
col_fun[allCond_NBS_combined[[3]] < 1] <- "#FFFFFF00" # threshold color matrix according to p values matrix from cPPI calculation

par(family = "sans")

order = c("DefaultA|sham", "DefaultC|sham", "TempPar|sham", "DorsAttnA|sham", "DorsAttnB|sham", "SalVentAttnA|sham", "SalVentAttnB|sham", "ContA|sham", "ContB|sham", "ContC|sham")

#col_fun2 = colorRamp2(c(0, 1.2), c("white", "#B72D2D"), transparency = 0.2) # color function for legend
#lgd = Legend(col_fun = col_fun2, title = "r", legend_height = unit(3, "cm"))


#### Plot ####
pdf(paste0(output_path, "chord_NBS_Sham_Active_NoEmptySpaces_", today, ".pdf")) 

circos.par(start.degree = 180, gap.degree = 1.5)
chordDiagram(df2, order = order, annotationTrack = "grid",
    grid.col = grid.col, col = col_fun, 
    preAllocateTracks = list(
        track.height = 0.15,
        track.margin = c(0.05, 0)
    )
)
for(net in unique(combined$networks)) {
    l = combined$networks == net
    sn = paste(combined$networks[l], combined$cond[l], sep = "|")
    highlight.sector(sn, track.index = 1, col = color_networks, facing = "bending.inside", text = net, cex = 0.8)
}
circos.clear()

#legend("bottomright", pch = 15, col = color_age,
#        legend = names(color_age), cex = 1, box.lty = 0)
 
#draw(lgd, x = unit(2, "cm"), y = unit(1, "cm"), just = c("right", "bottom"))

dev.off()

```


## Heatmap NBS results - age group differences - 121 ROIs
```{r Heatmap plot OA}
ROI_labels <- ROI_labels_sorted

#### Prepare df for plot ####
diag(OA) <- NA
OA_long <- cor_gather(OA)
OA_long$idx <- seq.int(nrow(OA_long))
OA_long$var1 <- factor(OA_long$var1, levels = ROI_labels[,4])
OA_long$var2 <- factor(OA_long$var2, levels = ROI_labels[,4])

diag(YA) <- NA
YA_long <- cor_gather(YA)
YA_long$idx <- seq.int(nrow(YA_long))
YA_long$var1 <- factor(YA_long$var1, levels = ROI_labels[,4])
YA_long$var2 <- factor(YA_long$var2, levels = ROI_labels[,4])

diag(OA_NBS_res) <- NA
OA_NBS_res_long <- cor_gather(OA_NBS_res)
OA_NBS_res_long$idx <- seq.int(nrow(OA_NBS_res_long))
OA_NBS_res_long$var1 <- factor(OA_NBS_res_long$var1, levels = ROI_labels[,4])
OA_NBS_res_long$var2 <- factor(OA_NBS_res_long$var2, levels = ROI_labels[,4])

diag(YA_NBS_res) <- NA
YA_NBS_res_long <- cor_gather(YA_NBS_res)
YA_NBS_res_long$idx <- seq.int(nrow(YA_NBS_res_long))
YA_NBS_res_long$var1 <- factor(YA_NBS_res_long$var1, levels = ROI_labels[,4])
YA_NBS_res_long$var2 <- factor(YA_NBS_res_long$var2, levels = ROI_labels[,4])


plot_df <- YA_long
plot_df$age <- NA
plot_df$cor <- ifelse(YA_NBS_res_long$cor == 1, plot_df$cor, 
                      ifelse(OA_NBS_res_long$cor == 1, OA_long$cor, NA))
plot_df$age <- ifelse(YA_NBS_res_long$cor == 1, "YA", 
                      ifelse(OA_NBS_res_long$cor == 1, "OA", NA))
plot_df$cor <- ifelse(plot_df$cor < 0 & plot_df$age == "OA", YA_long$cor, plot_df$cor)
plot_df$age <- ifelse(plot_df$cor < 0 & plot_df$age == "OA", "YA", plot_df$age)


#### Plot ####

## Plot with white background for NA values
allSubs <- ggplot(plot_df, aes(var1, var2)) +
  geom_tile(aes(fill = cor)) +
  geom_tile(aes(color = age == "OA"), fill = NA, size = 0.5) + 
  scale_fill_gradientn(colours = palet, values = scales::rescale(c(-0.2, 0, 0.6)), limits = c(-0.2, 0.6), breaks = c(-0.2, 0, 0.2, 0.4, 0.6), oob = scales::squish, name = "r", na.value = "white") +
  scale_color_manual(values = c("#018571", "#52257a"), na.value = NA, name = "Age", labels = c("YA", "OA")) +   scale_x_discrete(labels = plot_df$idx) +
  scale_y_discrete(labels = plot_df$idx) + 
  apatheme +
  theme(
    #axis.text = element_blank(),
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank(),
    panel.background = element_rect(fill='lightgrey', colour='white'))
allSubs

ggsave(plot = allSubs, filename = paste0(output_path, "NBS_heatmap_allSubs_", today, ".pdf"), dpi = 300, width = 13, height = 12, device = "pdf")


## Plot with light grey background for NA values
na_subset = subset(plot_df, is.na(cor))

allSubs_grey <- ggplot(plot_df, aes(var1, var2)) +
  geom_tile(aes(fill = cor)) +
  geom_tile(aes(color = age == "OA"), fill = NA, size = 0.5) + 
  scale_fill_gradientn(colours = palet, values = scales::rescale(c(-0.2, 0, 0.6)), limits = c(-0.2, 0.6), breaks = c(-0.2, 0, 0.2, 0.4, 0.6), oob = scales::squish, name = "r", na.value = "white") +
  scale_color_manual(values = c("#018571", "#52257a"), na.value = NA, name = "Age", labels = c("YA", "OA")) +
  geom_tile(data = na_subset, aes(color = NA), linetype = 0, fill = "lightgrey", alpha = 0.5) +
  scale_x_discrete(labels = plot_df$idx) +
  scale_y_discrete(labels = plot_df$idx) + 
  apatheme +
  theme(
    #axis.text = element_blank(),
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank())
allSubs_grey

ggsave(plot = allSubs_grey, filename = paste0(output_path, "NBS_heatmap_allSubs_GREYbg_", today, ".pdf"), dpi = 300, width = 13, height = 12, device = "pdf")


## Plot with light grey background for NA values and fully green/violet tiles instead of correlation filling
allSubs_grey_fullfill <- ggplot(plot_df, aes(var1, var2)) +
  geom_tile(aes(color = age == "OA"), fill = ifelse(plot_df$age == "OA", "#52257a", ifelse(plot_df$age == "YA",  "#018571", NA)), size = 0.5) + 
  scale_color_manual(values = c("#018571", "#52257a"), na.value = NA, name = "Age", labels = c("YA", "OA")) +
  geom_tile(data = na_subset, aes(color = NA), linetype = 0, fill = "lightgrey", alpha = 0.5) +
  scale_x_discrete(labels = plot_df$idx) +
  scale_y_discrete(labels = plot_df$idx) + 
  apatheme +
  theme(
    #axis.text = element_blank(),
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank())

ggsave(plot = allSubs_grey_fullfill, filename = paste0(output_path, "NBS_heatmap_allSubs_GREYbg_FullFilling_", today, ".pdf"), dpi = 300, width = 13, height = 12, device = "pdf")


## Plot with green and violet color palet according to age group
OA_long$neg <- OA_long$cor*(-1)
YA_long$pos <- abs(YA_long$cor)

plot_df <- OA_long
plot_df$age <- NA
plot_df$neg <- ifelse(OA_NBS_res_long$cor == 1, plot_df$neg, 
                      ifelse(YA_NBS_res_long$cor == 1, YA_long$pos, NA))
plot_df$age <- ifelse(OA_NBS_res_long$cor == 1, "OA", 
                      ifelse(YA_NBS_res_long$cor == 1, "YA", NA))

palet_age <- colorRamp2(c(-0.1, 0, 0.6), c("#52257a", "white", "#018571"))(plot_df[[5]])


ggplot(plot_df, aes(var1, var2)) +
  geom_tile(aes(fill = neg)) +
  #geom_tile(aes(color = age == "OA"), fill = NA, size = 0.5) + 
  scale_fill_gradientn(colours = palet_age, values = scales::rescale(c(-0.1, 0, 0.6)), limits = c(-0.1, 0.6), breaks = c(-0.1, 0, 0.2, 0.4, 0.6), oob = scales::squish, name = "r", na.value = "white") +
  #scale_color_manual(values = c("#018571", "#52257a"), na.value = NA, name = "Age", labels = c("YA", "OA")) +
  scale_x_discrete(labels = plot_df$idx) +
  scale_y_discrete(labels = plot_df$idx) + 
  apatheme +
  theme(
    #axis.text = element_blank(),
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank())
```


## QC: Are all significant negative values for OA stronger decoupled (negative) in YA? (Yes)
```{r}
ya.neg <- YA_long[which(YA_long$cor < 0 & YA_NBS_res_long$cor == 0),]
oa.neg <- OA_long[which(OA_long$cor < 0 & OA_NBS_res_long$cor == 1),]

oa.neg$age <- "OA"
ya.neg$age <- "YA"

all_neg <- full_join(oa.neg, ya.neg)

doubles <- all_neg[duplicated(all_neg$idx)|duplicated(all_neg$idx, fromLast = TRUE),]
boolean <- as.data.frame(doubles$cor[doubles$age == "OA"] > doubles$cor[doubles$age == "YA"])
```

