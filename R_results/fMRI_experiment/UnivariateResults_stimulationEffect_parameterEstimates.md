# Load packages

``` r
library(here)
library(psych)
library(plyr)
library(tidyverse)
library(ggplot2)
library(ggpubr)
library(sjPlot)
```

# Load data

``` r
# df with estimates - only TMS sessions
load(here::here("fMRI_experiment/df_TMS_estimates_07_2022.RData"))

# behavioral df - only with TMS sessions
load(here::here("Behavioral/df_TMS_06_2022.RData"))
```

# Read in data

# Create df with difference scores for parameter estimates

``` r
# Calculate difference between parameter estimates
df_diff <- df_TMS_estimates[,-c(2, 36)]

df_diff <- df_diff %>% 
  pivot_wider(names_from = stim_type, values_from = c(2:34), names_sep = "__")
df_diff <- df_diff %>% 
  pivot_longer(cols = c(2:67), names_to = c("ROI", "stimulation"), names_sep = "__")

df_diff <- df_diff %>% 
  group_by(Subject, ROI) %>% 
  mutate(diff_score = value[stimulation == "active"] - value[stimulation == "sham"]) %>% 
  dplyr::select(-c(stimulation, value)) %>% 
  distinct()
df_diff$ROI <- as.factor(df_diff$ROI)

df_diff <- df_diff %>% 
  rename(sub = Subject)
df_diff$sub <- gsub("control", "", df_diff$sub)
```

# Calculate difference scores for behavior

``` r
# remove NAs
df_TMS <- df_TMS[!is.na(df_TMS$response_time),]

# relevel factors
df_TMS$stim_type <- factor(df_TMS$stim_type, levels = c("active", "sham"))
df_TMS$condition <- factor(df_TMS$condition, levels = c("WPM", "FPM", "tone"))

df_diff_TMS <- df_TMS
df_diff_TMS <- df_diff_TMS %>% 
  group_by(sub, stim_type, condition) %>% 
  summarise(meanRT = mean(response_time),
            meanAcc = mean(correct),
            age = max(age))
df_diff_TMS <- df_diff_TMS %>% 
  pivot_wider(names_from = stim_type, values_from = c(meanRT, meanAcc))
df_diff_TMS$RT_diff <- df_diff_TMS$meanRT_active - df_diff_TMS$meanRT_sham
df_diff_TMS$Acc_diff <- df_diff_TMS$meanAcc_active - df_diff_TMS$meanAcc_sham

# merge behavioral df with estimates df
df_diff_betas <- full_join(df_diff, df_diff_TMS)
```

# Relation bw PSC in pre-SMA and behavior

``` r
# restructure df
df_diff_betas2 <- df_diff_betas %>% 
  pivot_wider(names_from = ROI, values_from = diff_score)

# WPM
with(df_diff_betas2[df_diff_betas2$condition == "WPM", ], cor.test(preSMA_PSC_WPM, RT_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_WPM and RT_diff
    ## t = 0.25297, df = 28, p-value = 0.8021
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.3179874  0.4011209
    ## sample estimates:
    ##        cor 
    ## 0.04775242

``` r
with(df_diff_betas2[df_diff_betas2$condition == "WPM", ], cor.test(preSMA_PSC_WPM, Acc_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_WPM and Acc_diff
    ## t = -0.11037, df = 28, p-value = 0.9129
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.3782815  0.3419842
    ## sample estimates:
    ##         cor 
    ## -0.02085437

``` r
# FPM
with(df_diff_betas2[df_diff_betas2$condition == "FPM", ], cor.test(preSMA_PSC_FPM, RT_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_FPM and RT_diff
    ## t = 1.5057, df = 28, p-value = 0.1433
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.09605284  0.57705797
    ## sample estimates:
    ##       cor 
    ## 0.2736873

``` r
with(df_diff_betas2[df_diff_betas2$condition == "FPM", ], cor.test(preSMA_PSC_FPM, Acc_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_FPM and Acc_diff
    ## t = -0.56822, df = 28, p-value = 0.5744
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.4497400  0.2636399
    ## sample estimates:
    ##        cor 
    ## -0.1067706

``` r
ggscatter(df_diff_betas2[df_diff_betas2$condition == "FPM", ], x = "RT_diff", y = "preSMA_PSC_FPM", 
          add = "reg.line", conf.int = TRUE, 
          cor.coef = TRUE, cor.method = "pearson")
```

![](UnivariateResults_stimulationEffect_parameterEstimates_files/figure-markdown_github/unnamed-chunk-3-1.png)

``` r
# tone
with(df_diff_betas2[df_diff_betas2$condition == "tone", ], cor.test(preSMA_PSC_tone, RT_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_tone and RT_diff
    ## t = 0.13941, df = 28, p-value = 0.8901
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.3371306  0.3829728
    ## sample estimates:
    ##        cor 
    ## 0.02633748

``` r
with(df_diff_betas2[df_diff_betas2$condition == "tone", ], cor.test(preSMA_PSC_tone, Acc_diff)) # the lower PSC for active compared to sham, the better accuracy
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_tone and Acc_diff
    ## t = -2.0477, df = 28, p-value = 0.05007
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.6381913846 -0.0007258669
    ## sample estimates:
    ##        cor 
    ## -0.3609007

``` r
ggscatter(df_diff_betas2[df_diff_betas2$condition == "tone", ], x = "Acc_diff", y = "preSMA_PSC_tone", 
          add = "reg.line", conf.int = TRUE, cor.coef = TRUE, cor.method = "pearson") +
  labs(y = expression(Delta~"PSC pre-SMA"),
       x = expression(Delta~"Accuracy Tone judgement Active > Sham")) +
  apatheme
```

![](UnivariateResults_stimulationEffect_parameterEstimates_files/figure-markdown_github/unnamed-chunk-3-2.png)

``` r
# PSC for both semantic conditions
df_LANG <- df_diff_betas2[df_diff_betas2$condition == "WPM" | df_diff_betas2$condition == "FPM",]
df_LANG <- droplevels(df_LANG)
df_LANG <- df_LANG %>% 
  mutate(RT_diff_LANG = (RT_diff[condition == "WPM"]+RT_diff[condition == "FPM"])/2,
         Acc_diff_LANG = (Acc_diff[condition == "WPM"]+Acc_diff[condition == "FPM"])/2)
df_LANG <- df_LANG[,-c(2,4,5:9)]
df_LANG <- df_LANG %>% 
  distinct()
df_LANG <- df_LANG %>% 
  mutate(preSMA_PSC_Lang = (preSMA_PSC_WPM + preSMA_PSC_FPM)/2)

with(df_LANG, cor.test(preSMA_PSC_Lang, RT_diff_LANG))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_Lang and RT_diff_LANG
    ## t = 0.92907, df = 28, p-value = 0.3608
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.1997843  0.5019299
    ## sample estimates:
    ##       cor 
    ## 0.1729319

``` r
with(df_LANG, cor.test(preSMA_PSC_Lang, Acc_diff_LANG))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  preSMA_PSC_Lang and Acc_diff_LANG
    ## t = -0.52194, df = 28, p-value = 0.6058
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.4427717  0.2717175
    ## sample estimates:
    ##        cor 
    ## -0.0981608

# Other areas based on significant stimulation effects active \> sham

## FPM \> Rest

``` r
# restructure df
df_diff_betas2 <- df_diff_betas %>% 
  pivot_wider(names_from = ROI, values_from = diff_score)

## PSC FPM
### cluster 1
with(df_diff_betas2[df_diff_betas2$condition == "FPM", ], cor.test(PSC_FPM_FPM_act_sham_cluster1, RT_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_FPM_FPM_act_sham_cluster1 and RT_diff
    ## t = 1.7115, df = 28, p-value = 0.09805
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.05907454  0.60134225
    ## sample estimates:
    ##       cor 
    ## 0.3077443

``` r
with(df_diff_betas2[df_diff_betas2$condition == "FPM", ], cor.test(PSC_FPM_FPM_act_sham_cluster1, Acc_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_FPM_FPM_act_sham_cluster1 and Acc_diff
    ## t = -0.45414, df = 28, p-value = 0.6532
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.4324568  0.2834926
    ## sample estimates:
    ##        cor 
    ## -0.0855101

``` r
### cluster 2
with(df_diff_betas2[df_diff_betas2$condition == "FPM", ], cor.test(PSC_FPM_FPM_act_sham_cluster2, RT_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_FPM_FPM_act_sham_cluster2 and RT_diff
    ## t = 1.072, df = 28, p-value = 0.2929
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.1741665  0.5215235
    ## sample estimates:
    ##       cor 
    ## 0.1985619

``` r
with(df_diff_betas2[df_diff_betas2$condition == "FPM", ], cor.test(PSC_FPM_FPM_act_sham_cluster2, Acc_diff)) # sig effect 
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_FPM_FPM_act_sham_cluster2 and Acc_diff
    ## t = -2.1089, df = 28, p-value = 0.04403
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.64452513 -0.01148519
    ## sample estimates:
    ##        cor 
    ## -0.3702225

``` r
ggscatter(df_diff_betas2[df_diff_betas2$condition == "FPM", ], x = "Acc_diff", y = "PSC_FPM_FPM_act_sham_cluster2", 
          add = "reg.line", conf.int = TRUE, cor.coef = TRUE, cor.method = "pearson") +
  labs(y = expression(Delta~"PSC left SPL [-23 -71 46]"),
       x = expression(Delta~"Accuracy FPM Active > Sham")) +
  apatheme
```

![](UnivariateResults_stimulationEffect_parameterEstimates_files/figure-markdown_github/unnamed-chunk-4-1.png)

## WPM \> Rest

``` r
## PSC WPM
### cluster 1
with(df_diff_betas2[df_diff_betas2$condition == "WPM", ], cor.test(PSC_WPM_WPM_act_sham_cluster1, RT_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_WPM_WPM_act_sham_cluster1 and RT_diff
    ## t = 0.92803, df = 28, p-value = 0.3613
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.1999705  0.5017848
    ## sample estimates:
    ##       cor 
    ## 0.1727438

``` r
with(df_diff_betas2[df_diff_betas2$condition == "WPM", ], cor.test(PSC_WPM_WPM_act_sham_cluster1, Acc_diff))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_WPM_WPM_act_sham_cluster1 and Acc_diff
    ## t = 0.71983, df = 28, p-value = 0.4776
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2369838  0.4721352
    ## sample estimates:
    ##       cor 
    ## 0.1347939

## WPM+FPM

``` r
df_LANG <- df_diff_betas2[df_diff_betas2$condition == "WPM" | df_diff_betas2$condition == "FPM",]
df_LANG <- droplevels(df_LANG)
df_LANG <- df_LANG %>% 
  mutate(RT_diff_LANG = (RT_diff[condition == "WPM"]+RT_diff[condition == "FPM"])/2,
         Acc_diff_LANG = (Acc_diff[condition == "WPM"]+Acc_diff[condition == "FPM"])/2)
df_LANG <- df_LANG[,-c(2,4,5:9)]
df_LANG <- df_LANG %>% 
  distinct()
df_LANG <- df_LANG %>% 
  mutate(PSC_LANG_Lang_rest_cluster1 = (`PSC_FPM_WPM+FPM_act_sham_cluster1` + `PSC_WPM_WPM+FPM_act_sham_cluster1`)/2,
         PSC_LANG_Lang_rest_cluster2 = (`PSC_FPM_WPM+FPM_act_sham_cluster2` + `PSC_WPM_WPM+FPM_act_sham_cluster2`)/2,
         PSC_LANG_Lang_ControlTask_cluster1 = (`PSC_FPM_WPM+FPM_ControlTask_act_sham_cluster1` + `PSC_WPM_WPM+FPM_ControlTask_act_sham_cluster1`)/2)

# WPM+FPM > Rest
## PSC WPM+FPM
### cluster 1
with(df_LANG, cor.test(PSC_LANG_Lang_rest_cluster1, RT_diff_LANG))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_LANG_Lang_rest_cluster1 and RT_diff_LANG
    ## t = 0.85724, df = 28, p-value = 0.3986
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2125999  0.4918499
    ## sample estimates:
    ##       cor 
    ## 0.1599179

``` r
with(df_LANG, cor.test(PSC_LANG_Lang_rest_cluster1, Acc_diff_LANG)) 
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_LANG_Lang_rest_cluster1 and Acc_diff_LANG
    ## t = -0.71076, df = 28, p-value = 0.4831
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.4708141  0.2385865
    ## sample estimates:
    ##        cor 
    ## -0.1331256

``` r
### cluster 2
with(df_LANG, cor.test(PSC_LANG_Lang_rest_cluster2, RT_diff_LANG))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_LANG_Lang_rest_cluster2 and RT_diff_LANG
    ## t = 1.2647, df = 28, p-value = 0.2164
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.1394875  0.5469280
    ## sample estimates:
    ##       cor 
    ## 0.2324637

``` r
with(df_LANG, cor.test(PSC_LANG_Lang_rest_cluster2, Acc_diff_LANG)) # sig effect with accuracy
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_LANG_Lang_rest_cluster2 and Acc_diff_LANG
    ## t = -2.0771, df = 28, p-value = 0.04708
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.641252003 -0.005906673
    ## sample estimates:
    ##        cor 
    ## -0.3653983

``` r
ggscatter(df_LANG, x = "Acc_diff_LANG", y = "PSC_LANG_Lang_rest_cluster2", 
          add = "reg.line", conf.int = TRUE, cor.coef = TRUE, cor.method = "pearson") +
  labs(y = expression(Delta~"PSC left SPL [-23 -71 54]"),
       x = expression(Delta~"Accuracy Semantic judgement Active > Sham")) +
  apatheme
```

![](UnivariateResults_stimulationEffect_parameterEstimates_files/figure-markdown_github/unnamed-chunk-6-1.png)

``` r
# WPM+FPM > ControlTask
## PSC WPM+FPM
### cluster 1
with(df_LANG, cor.test(PSC_LANG_Lang_ControlTask_cluster1, RT_diff_LANG))
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_LANG_Lang_ControlTask_cluster1 and RT_diff_LANG
    ## t = 0.26381, df = 28, p-value = 0.7939
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.3161477  0.4028357
    ## sample estimates:
    ##        cor 
    ## 0.04979288

``` r
with(df_LANG, cor.test(PSC_LANG_Lang_ControlTask_cluster1, Acc_diff_LANG)) 
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  PSC_LANG_Lang_ControlTask_cluster1 and Acc_diff_LANG
    ## t = -1.0587, df = 28, p-value = 0.2988
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.5197270  0.1765552
    ## sample estimates:
    ##        cor 
    ## -0.1961934
