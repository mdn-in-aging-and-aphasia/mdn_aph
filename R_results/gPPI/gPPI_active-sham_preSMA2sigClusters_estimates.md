# Load packages

``` r
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(ggplot2)
library(lme4)
library(emmeans)
library(ggeffects)
library(ppcor)
library(ggpubr)
library(see)
```

# Load data

``` r
# behavioral df only with TMS sessions
load(here::here("Behavioral/df_TMS_06_2022.RData"))

# df with gPPI estimates
load(here::here("gPPI/df_gPPI_estimates_preSMA_09_22.RData"))
```

# Read in data

# Linear model to test effect of stimulation on connectivity with preSMA and different target ROIs

``` r
m1 <- glm(Lang.ControlTask ~ Stimulation * TargetROI, data = df_gPPI_estimates)
summary(m1) # no sig difference between active and sham
```

    ## 
    ## Call:
    ## glm(formula = Lang.ControlTask ~ Stimulation * TargetROI, data = df_gPPI_estimates)
    ## 
    ## Deviance Residuals: 
    ##      Min        1Q    Median        3Q       Max  
    ## -1.57283  -0.25103   0.01673   0.26090   1.22600  
    ## 
    ## Coefficients:
    ##                                                              Estimate
    ## (Intercept)                                                 -0.140585
    ## Stimulationsham                                              0.059729
    ## TargetROILSPL_FPM>Rest                                       0.009155
    ## TargetROILSPL_WPM+FPM>Rest                                   0.046862
    ## TargetROIRCuneus_WPM>Rest                                   -0.089970
    ## TargetROIRMiddleOccipitalGyrus_WPM+FPM>Rest                 -0.145195
    ## TargetROIRMTG_FPM>Rest                                       0.021253
    ## Stimulationsham:TargetROILSPL_FPM>Rest                      -0.054567
    ## Stimulationsham:TargetROILSPL_WPM+FPM>Rest                  -0.066607
    ## Stimulationsham:TargetROIRCuneus_WPM>Rest                    0.036887
    ## Stimulationsham:TargetROIRMiddleOccipitalGyrus_WPM+FPM>Rest -0.094286
    ## Stimulationsham:TargetROIRMTG_FPM>Rest                      -0.024689
    ##                                                             Std. Error t value
    ## (Intercept)                                                   0.080032  -1.757
    ## Stimulationsham                                               0.113182   0.528
    ## TargetROILSPL_FPM>Rest                                        0.112235   0.082
    ## TargetROILSPL_WPM+FPM>Rest                                    0.112235   0.418
    ## TargetROIRCuneus_WPM>Rest                                     0.112235  -0.802
    ## TargetROIRMiddleOccipitalGyrus_WPM+FPM>Rest                   0.112235  -1.294
    ## TargetROIRMTG_FPM>Rest                                        0.112235   0.189
    ## Stimulationsham:TargetROILSPL_FPM>Rest                        0.158724  -0.344
    ## Stimulationsham:TargetROILSPL_WPM+FPM>Rest                    0.158724  -0.420
    ## Stimulationsham:TargetROIRCuneus_WPM>Rest                     0.158724   0.232
    ## Stimulationsham:TargetROIRMiddleOccipitalGyrus_WPM+FPM>Rest   0.158724  -0.594
    ## Stimulationsham:TargetROIRMTG_FPM>Rest                        0.158724  -0.156
    ##                                                             Pr(>|t|)  
    ## (Intercept)                                                   0.0799 .
    ## Stimulationsham                                               0.5980  
    ## TargetROILSPL_FPM>Rest                                        0.9350  
    ## TargetROILSPL_WPM+FPM>Rest                                    0.6765  
    ## TargetROIRCuneus_WPM>Rest                                     0.4233  
    ## TargetROIRMiddleOccipitalGyrus_WPM+FPM>Rest                   0.1966  
    ## TargetROIRMTG_FPM>Rest                                        0.8499  
    ## Stimulationsham:TargetROILSPL_FPM>Rest                        0.7312  
    ## Stimulationsham:TargetROILSPL_WPM+FPM>Rest                    0.6750  
    ## Stimulationsham:TargetROIRCuneus_WPM>Rest                     0.8164  
    ## Stimulationsham:TargetROIRMiddleOccipitalGyrus_WPM+FPM>Rest   0.5529  
    ## Stimulationsham:TargetROIRMTG_FPM>Rest                        0.8765  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## (Dispersion parameter for gaussian family taken to be 0.1857479)
    ## 
    ##     Null deviance: 66.383  on 357  degrees of freedom
    ## Residual deviance: 64.269  on 346  degrees of freedom
    ## AIC: 427.11
    ## 
    ## Number of Fisher Scoring iterations: 2

``` r
allROIS <- ggplot(df_gPPI_estimates, aes(x = Stimulation, y = Lang.Rest, fill = Stimulation)) +
  geom_violindot(trim = F) +
  facet_wrap(~ TargetROI)
allROIS
```

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-1-1.png)

``` r
#ggsave(plot = allROIS, filename = paste0(output_path, "Plots/gPPI/PPI_preSMA_allROIs_act_vs_sham_Lang.Rest_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")

## Test role of age
m_Lang.Rest <- lm(Lang.Rest ~ Stimulation * age, data = df_gPPI_estimates)
summary(m_Lang.Rest)
```

    ## 
    ## Call:
    ## lm(formula = Lang.Rest ~ Stimulation * age, data = df_gPPI_estimates)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -1.03176 -0.24764 -0.05202  0.19884  2.06205 
    ## 
    ## Coefficients:
    ##                      Estimate Std. Error t value Pr(>|t|)  
    ## (Intercept)         -0.050220   0.228922  -0.219   0.8265  
    ## Stimulationsham      0.721392   0.323744   2.228   0.0265 *
    ## age                  0.005228   0.003689   1.417   0.1573  
    ## Stimulationsham:age -0.011374   0.005217  -2.180   0.0299 *
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.377 on 354 degrees of freedom
    ## Multiple R-squared:  0.01409,    Adjusted R-squared:  0.005737 
    ## F-statistic: 1.687 on 3 and 354 DF,  p-value: 0.1695

``` r
Stim_age <- plot(ggpredict(m_Lang.Rest, terms = c("age", "Stimulation")))
Stim_age
```

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-1-2.png)

``` r
#ggsave(plot = Stim_age, filename = paste0(output_path, "Plots/gPPI/PPI_preSMA_Interaction_Stim_Age_Lang.Rest_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")

m_Lang.ControlTask <- lm(Lang.ControlTask ~ Stimulation * age, data = df_gPPI_estimates)
summary(m_Lang.ControlTask)
```

    ## 
    ## Call:
    ## lm(formula = Lang.ControlTask ~ Stimulation * age, data = df_gPPI_estimates)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -1.74873 -0.24106 -0.00656  0.27843  0.99232 
    ## 
    ## Coefficients:
    ##                      Estimate Std. Error t value Pr(>|t|)   
    ## (Intercept)          0.501659   0.259209   1.935  0.05374 . 
    ## Stimulationsham     -0.188082   0.366577  -0.513  0.60822   
    ## age                 -0.010858   0.004177  -2.600  0.00972 **
    ## Stimulationsham:age  0.003471   0.005907   0.588  0.55721   
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.4269 on 354 degrees of freedom
    ## Multiple R-squared:  0.02803,    Adjusted R-squared:  0.01979 
    ## F-statistic: 3.403 on 3 and 354 DF,  p-value: 0.01791

``` r
plot_age <- plot(ggpredict(m_Lang.ControlTask, terms = c("age", "Stimulation")))
plot_age
```

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-1-3.png)

# Calculate differences for behavior

``` r
# remove NAs
df_TMS <- df_TMS[!is.na(df_TMS$response_time),]

# relevel factors
df_TMS$stim_type <- factor(df_TMS$stim_type, levels = c("active", "sham"))
df_TMS$condition <- factor(df_TMS$condition, levels = c("WPM", "FPM", "tone"))

# calculate means and differences for active - sham
df_diff_TMS <- df_TMS
df_diff_TMS <- df_diff_TMS %>% 
  group_by(sub, stim_type, condition) %>% 
  summarise(meanRT = mean(response_time),
            meanAcc = mean(correct),
            age = max(age))
df_diff_TMS <- df_diff_TMS %>% 
  pivot_wider(names_from = stim_type, values_from = c(meanRT, meanAcc))
df_diff_TMS$RT_diff <- df_diff_TMS$meanRT_active - df_diff_TMS$meanRT_sham
df_diff_TMS$Acc_diff <- df_diff_TMS$meanAcc_active - df_diff_TMS$meanAcc_sham
```

# Semantic judgment \> Tone judgment

## Calculate difference score df with estimates

``` r
# Calculate difference between parameter estimates
df_diff_gPPI <- df_gPPI_estimates[,-c(5,7)]

df_diff_gPPI <- df_diff_gPPI %>% 
  pivot_wider(names_from = Stimulation, values_from = Lang.ControlTask, names_prefix = "beta_")
df_diff_gPPI$Diff_score_beta <- df_diff_gPPI$beta_active - df_diff_gPPI$beta_sham
#df_diff_gPPI <- df_diff_gPPI %>% 
#  pivot_longer(cols = 3:4, names_to = "Stimulation", values_to = "Estimate_Lang_ControlTask")

#df_diff_gPPI <- df_diff_gPPI %>% 
#  rename(sub = Subject)
```

## Correlate difference score in connectivity with age - no sig correlation

``` r
age <- df_TMS %>% 
  group_by(sub) %>% 
  summarise(age = mean(age))

#df_diff_gPPI <- full_join(age, df_diff_gPPI)

cor_age <- df_diff_gPPI %>% 
  group_by(TargetROI) %>% 
  summarize(cor=cor(age, Diff_score_beta))

df_lingual <- df_diff_gPPI %>% 
  filter(TargetROI == "LLingualGyrus_WPM+FPM>ControlTask")
cor.test(df_lingual$age, df_lingual$Diff_score_beta)
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  df_lingual$age and df_lingual$Diff_score_beta
    ## t = -1.7437, df = 27, p-value = 0.09259
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.61315346  0.05474725
    ## sample estimates:
    ##       cor 
    ## -0.318142

## Difference in RT with difference in parameter estimates

``` r
# merge
df_behav_gPPI <- full_join(df_diff_gPPI, df_diff_TMS)
df_behav_gPPI <- df_behav_gPPI[, -c(3, 5:6, 9:12)]

# filter
## WPM
df_WPM <- df_behav_gPPI[df_behav_gPPI$condition == "WPM",]
df_WPM <- df_WPM %>% 
  pivot_wider(values_from = RT_diff, names_from = condition, names_prefix = "DiffRT_") %>% 
  pivot_wider(values_from = Diff_score_beta, names_from = TargetROI)

## FPM
df_FPM <- df_behav_gPPI[df_behav_gPPI$condition == "FPM",]
df_FPM <- df_FPM %>% 
  pivot_wider(values_from = RT_diff, names_from = condition, names_prefix = "DiffRT_") %>% 
  pivot_wider(values_from = Diff_score_beta, names_from = TargetROI)

## Language
df_lang <- df_behav_gPPI[df_behav_gPPI$condition == "FPM" | df_behav_gPPI$condition == "WPM",]
df_lang <- df_lang %>% 
  group_by(sub, TargetROI, Diff_score_beta, age) %>% 
  summarize(RT_diff = mean(RT_diff), Acc_diff = mean(Acc_diff))
df_lang$condition <- "language"
df_lang <- df_lang %>% 
  pivot_wider(values_from = RT_diff, names_from = condition, names_prefix = "DiffRT_") %>% 
  pivot_wider(values_from = Diff_score_beta, names_from = TargetROI)

## Tone
df_tone <- df_behav_gPPI[df_behav_gPPI$condition == "tone",]
df_tone <- df_tone %>% 
  pivot_wider(values_from = RT_diff, names_from = condition, names_prefix = "DiffRT_") %>% 
  pivot_wider(values_from = Diff_score_beta, names_from = TargetROI)


# cor_RT <- df_behav_gPPI_diffRT %>% 
#   group_by(ROI, condition) %>% 
#   summarise(cor = cor.test(Diff_score_beta, RT_diff))
```

### Correlation for RT

``` r
# WPM
rho <- pval <- res_WPM <- NULL
for(i in 5:length(df_WPM)){
   rho[i]  <- cor.test(df_WPM$DiffRT_WPM, df_WPM[[i]], method = "pearson")$estimate
   pval[i] <- cor.test(df_WPM$DiffRT_WPM, df_WPM[[i]], method = "pearson")$p.value
}

res_WPM <- cbind(rho,pval)
res_WPM <- res_WPM[-c(1:4),]
res_WPM <- as.data.frame(res_WPM)
res_WPM$ROI <- paste(colnames(df_WPM)[4],"vs",colnames(df_WPM)[5:length(df_WPM)])

p_holm <- p.adjust(res_WPM$pval, method = "holm")
res_WPM <- cbind(res_WPM, p_holm)


# FPM
rho <- pval <- res_FPM <- NULL
for(i in 5:length(df_FPM)){
   rho[i]  <- cor.test(df_FPM$DiffRT_FPM, df_FPM[[i]], method = "pearson")$estimate
   pval[i] <- cor.test(df_FPM$DiffRT_FPM, df_FPM[[i]], method = "pearson")$p.value
}

res_FPM <- cbind(rho,pval)
res_FPM <- res_FPM[-c(1:4),]
res_FPM <- as.data.frame(res_FPM)
res_FPM$ROI <- paste(colnames(df_FPM)[4],"vs",colnames(df_FPM)[5:length(df_FPM)])

p_holm <- p.adjust(res_FPM$pval, method = "holm")
res_FPM <- cbind(res_FPM, p_holm)

cor_plot <- ggscatter(df_FPM, x = "DiffRT_FPM",  y = "LSPL_FPM>Rest",
                      add = "reg.line", conf.int = TRUE, cor.coef = F, cor.method = "pearson") +
  stat_cor(method = "pearson", label.x = 50, label.y = 1.6) +
  labs(y = expression(Delta~"Connectivity pre-SMA - left SPL Active > Sham"),
       x = expression(Delta~"RT FPM Active > Sham")) +
  apatheme +
  theme(text=element_text(family='sans',size=14))
cor_plot
```

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-6-1.png)

``` r
#ggsave(plot = cor_plot, filename = paste0(output_path, "Plots/gPPI/PPI_preSMA_SPL_FPM_RT_", today, ".pdf"), dpi = 300, width = 6, height = 4, device = "pdf")


# LANG
rho <- pval <- res_lang <- NULL
for(i in 5:length(df_lang)){
   rho[i]  <- cor.test(df_lang$DiffRT_language, df_lang[[i]], method = "pearson")$estimate
   pval[i] <- cor.test(df_lang$DiffRT_language, df_lang[[i]], method = "pearson")$p.value
}

res_lang <- cbind(rho,pval)
res_lang <- res_lang[-c(1:4),]
res_lang <- as.data.frame(res_lang)
res_lang$ROI <- paste(colnames(df_lang)[4],"vs",colnames(df_lang)[5:length(df_lang)])

p_holm <- p.adjust(res_lang$pval, method = "holm")
res_lang <- cbind(res_lang, p_holm)
```

### LM for RT

``` r
# WPM
lm_WPM <- lm(DiffRT_WPM ~ age * (`LLingualGyrus_WPM+FPM>ControlTask` + `RCuneus_WPM>Rest` + `LSPL_WPM+FPM>Rest` + `LSPL_FPM>Rest` + `RMiddleOccipitalGyrus_WPM+FPM>Rest` + `RMTG_FPM>Rest`), data = df_WPM)
summary(lm_WPM)
```

    ## 
    ## Call:
    ## lm(formula = DiffRT_WPM ~ age * (`LLingualGyrus_WPM+FPM>ControlTask` + 
    ##     `RCuneus_WPM>Rest` + `LSPL_WPM+FPM>Rest` + `LSPL_FPM>Rest` + 
    ##     `RMiddleOccipitalGyrus_WPM+FPM>Rest` + `RMTG_FPM>Rest`), 
    ##     data = df_WPM)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -137.086  -45.898    0.401   37.855  183.612 
    ## 
    ## Coefficients:
    ##                                          Estimate Std. Error t value Pr(>|t|)
    ## (Intercept)                              -254.156    292.332  -0.869    0.398
    ## age                                         3.641      4.589   0.793    0.440
    ## `LLingualGyrus_WPM+FPM>ControlTask`       765.031    737.786   1.037    0.316
    ## `RCuneus_WPM>Rest`                       -171.018    589.602  -0.290    0.776
    ## `LSPL_WPM+FPM>Rest`                      -135.009   1021.037  -0.132    0.897
    ## `LSPL_FPM>Rest`                          -618.200    841.434  -0.735    0.474
    ## `RMiddleOccipitalGyrus_WPM+FPM>Rest`      654.204    592.579   1.104    0.287
    ## `RMTG_FPM>Rest`                           417.184    692.359   0.603    0.556
    ## age:`LLingualGyrus_WPM+FPM>ControlTask`   -11.829     11.743  -1.007    0.330
    ## age:`RCuneus_WPM>Rest`                      2.951      9.582   0.308    0.762
    ## age:`LSPL_WPM+FPM>Rest`                     3.234     16.016   0.202    0.843
    ## age:`LSPL_FPM>Rest`                         8.237     13.329   0.618    0.546
    ## age:`RMiddleOccipitalGyrus_WPM+FPM>Rest`   -9.105      9.736  -0.935    0.365
    ## age:`RMTG_FPM>Rest`                        -7.604     11.680  -0.651    0.525
    ## 
    ## Residual standard error: 89.59 on 15 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.3721, Adjusted R-squared:  -0.172 
    ## F-statistic: 0.6839 on 13 and 15 DF,  p-value: 0.7513

``` r
plot(ggpredict(lm_WPM))
```

    ## $age

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-1.png)

    ## 
    ## $`LLingualGyrus_WPM+FPM>ControlTask`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-2.png)

    ## 
    ## $`RCuneus_WPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-3.png)

    ## 
    ## $`LSPL_WPM+FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-4.png)

    ## 
    ## $`LSPL_FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-5.png)

    ## 
    ## $`RMiddleOccipitalGyrus_WPM+FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-6.png)

    ## 
    ## $`RMTG_FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-7.png)

``` r
# FPM
lm_FPM <- lm(DiffRT_FPM ~ age + `LLingualGyrus_WPM+FPM>ControlTask` + `RCuneus_WPM>Rest` + `LSPL_WPM+FPM>Rest` + `LSPL_FPM>Rest` + `RMiddleOccipitalGyrus_WPM+FPM>Rest` + `RMTG_FPM>Rest`, data = df_FPM)
summary(lm_FPM)
```

    ## 
    ## Call:
    ## lm(formula = DiffRT_FPM ~ age + `LLingualGyrus_WPM+FPM>ControlTask` + 
    ##     `RCuneus_WPM>Rest` + `LSPL_WPM+FPM>Rest` + `LSPL_FPM>Rest` + 
    ##     `RMiddleOccipitalGyrus_WPM+FPM>Rest` + `RMTG_FPM>Rest`, data = df_FPM)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -192.864  -41.556    7.589   59.353  166.661 
    ## 
    ## Coefficients:
    ##                                       Estimate Std. Error t value Pr(>|t|)
    ## (Intercept)                            9.80138  172.89708   0.057    0.955
    ## age                                   -0.01183    2.77802  -0.004    0.997
    ## `LLingualGyrus_WPM+FPM>ControlTask`   36.45329   53.64110   0.680    0.504
    ## `RCuneus_WPM>Rest`                     4.34506   41.92091   0.104    0.918
    ## `LSPL_WPM+FPM>Rest`                   12.45987   71.32340   0.175    0.863
    ## `LSPL_FPM>Rest`                      -95.31738   77.32136  -1.233    0.231
    ## `RMiddleOccipitalGyrus_WPM+FPM>Rest`  -0.92451   52.07568  -0.018    0.986
    ## `RMTG_FPM>Rest`                      -15.72778   53.76481  -0.293    0.773
    ## 
    ## Residual standard error: 91.98 on 21 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.2759, Adjusted R-squared:  0.03451 
    ## F-statistic: 1.143 on 7 and 21 DF,  p-value: 0.3751

``` r
plot(ggpredict(lm_WPM))
```

    ## $age

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-8.png)

    ## 
    ## $`LLingualGyrus_WPM+FPM>ControlTask`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-9.png)

    ## 
    ## $`RCuneus_WPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-10.png)

    ## 
    ## $`LSPL_WPM+FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-11.png)

    ## 
    ## $`LSPL_FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-12.png)

    ## 
    ## $`RMiddleOccipitalGyrus_WPM+FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-13.png)

    ## 
    ## $`RMTG_FPM>Rest`

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-14.png)

``` r
lm_FPM2 <- lm(DiffRT_FPM ~ age * `LSPL_FPM>Rest`, data = df_FPM)
summary(lm_FPM2)
```

    ## 
    ## Call:
    ## lm(formula = DiffRT_FPM ~ age * `LSPL_FPM>Rest`, data = df_FPM)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -177.08  -54.81    5.52   58.64  174.62 
    ## 
    ## Coefficients:
    ##                     Estimate Std. Error t value Pr(>|t|)
    ## (Intercept)           74.807    128.824   0.581    0.566
    ## age                   -1.156      2.072  -0.558    0.582
    ## `LSPL_FPM>Rest`     -147.430    317.240  -0.465    0.646
    ## age:`LSPL_FPM>Rest`    1.078      5.121   0.211    0.835
    ## 
    ## Residual standard error: 85.98 on 26 degrees of freedom
    ## Multiple R-squared:  0.2503, Adjusted R-squared:  0.1639 
    ## F-statistic: 2.894 on 3 and 26 DF,  p-value: 0.05431

``` r
# LANG
lm_LANG <- lm(DiffRT_language ~ age + `LLingualGyrus_WPM+FPM>ControlTask` + `RCuneus_WPM>Rest` + `LSPL_WPM+FPM>Rest` + `LSPL_FPM>Rest` + `RMiddleOccipitalGyrus_WPM+FPM>Rest` + `RMTG_FPM>Rest`, data = df_lang)
summary(lm_LANG)
```

    ## 
    ## Call:
    ## lm(formula = DiffRT_language ~ age + `LLingualGyrus_WPM+FPM>ControlTask` + 
    ##     `RCuneus_WPM>Rest` + `LSPL_WPM+FPM>Rest` + `LSPL_FPM>Rest` + 
    ##     `RMiddleOccipitalGyrus_WPM+FPM>Rest` + `RMTG_FPM>Rest`, data = df_lang)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -186.46  -29.93    1.55   46.44  116.23 
    ## 
    ## Coefficients:
    ##                                      Estimate Std. Error t value Pr(>|t|)  
    ## (Intercept)                          -108.151    154.297  -0.701   0.4910  
    ## age                                     1.793      2.479   0.723   0.4775  
    ## `LLingualGyrus_WPM+FPM>ControlTask`    39.708     47.870   0.829   0.4162  
    ## `RCuneus_WPM>Rest`                     -7.063     37.411  -0.189   0.8521  
    ## `LSPL_WPM+FPM>Rest`                    62.583     63.650   0.983   0.3367  
    ## `LSPL_FPM>Rest`                      -122.482     69.003  -1.775   0.0904 .
    ## `RMiddleOccipitalGyrus_WPM+FPM>Rest`   16.496     46.473   0.355   0.7262  
    ## `RMTG_FPM>Rest`                       -29.055     47.981  -0.606   0.5513  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 82.09 on 21 degrees of freedom
    ##   (1 observation deleted due to missingness)
    ## Multiple R-squared:  0.231,  Adjusted R-squared:  -0.02533 
    ## F-statistic: 0.9012 on 7 and 21 DF,  p-value: 0.5238

``` r
plot(ggpredict(lm_LANG, terms = "LSPL_FPM>Rest"))
```

![](gPPI_active-sham_preSMA2sigClusters_estimates_files/figure-markdown_github/unnamed-chunk-7-15.png)

### Correlation for Accuracy

``` r
# WPM
rho <- pval <- res_WPM <- NULL
for(i in 5:length(df_WPM)){
   rho[i]  <- cor.test(df_WPM$Acc_diff, df_WPM[[i]], method = "pearson")$estimate
   pval[i] <- cor.test(df_WPM$Acc_diff, df_WPM[[i]], method = "pearson")$p.value
}

res_WPM <- cbind(rho,pval)
res_WPM <- res_WPM[-c(1:4),]
res_WPM <- as.data.frame(res_WPM)
res_WPM$ROI <- paste(colnames(df_WPM)[3],"vs",colnames(df_WPM)[5:length(df_WPM)])

p_holm <- p.adjust(res_WPM$pval, method = "holm")
res_WPM <- cbind(res_WPM, p_holm)


# FPM
rho <- pval <- res_FPM <- NULL
for(i in 5:length(df_FPM)){
   rho[i]  <- cor.test(df_FPM$Acc_diff, df_FPM[[i]], method = "pearson")$estimate
   pval[i] <- cor.test(df_FPM$Acc_diff, df_FPM[[i]], method = "pearson")$p.value
}

res_FPM <- cbind(rho,pval)
res_FPM <- res_FPM[-c(1:4),]
res_FPM <- as.data.frame(res_FPM)
res_FPM$ROI <- paste(colnames(df_FPM)[3],"vs",colnames(df_FPM)[5:length(df_FPM)])

p_holm <- p.adjust(res_FPM$pval, method = "holm")
res_FPM <- cbind(res_FPM, p_holm)


# LANG
rho <- pval <- res_lang <- NULL
for(i in 5:length(df_lang)){
   rho[i]  <- cor.test(df_lang$Acc_diff, df_lang[[i]], method = "pearson")$estimate
   pval[i] <- cor.test(df_lang$Acc_diff, df_lang[[i]], method = "pearson")$p.value
}

res_lang <- cbind(rho,pval)
res_lang <- res_lang[-c(1:4),]
res_lang <- as.data.frame(res_lang)
res_lang$ROI <- paste(colnames(df_lang)[4],"vs",colnames(df_lang)[5:length(df_lang)])

p_holm <- p.adjust(res_lang$pval, method = "holm")
res_lang <- cbind(res_lang, p_holm)
```
