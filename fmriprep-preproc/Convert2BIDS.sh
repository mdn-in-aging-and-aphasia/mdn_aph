#!/bin/bash

# Sandra Martin, 09/20
# Modified 09/2019 for control group for MDN_LANG (BIDS version 1.2.1)
# Modified for MDN_APH 02/2020, and then 09/20 (BIDS version 1.4.0)

usage() { echo "Usage: $0 [-g <control||patient>] [-s <session number 1, 2 or 3>] [-p <participant number>] [-a <scan number for anatomy if applicable>] [-f <scan number for flair if applicable>] [-l <scan numbers for localizer if applicable>] [-t <scan numbers for task>] [-m <scan numbers for fieldmaps>]" ; }

loc=()
task=()
fmap=()

while getopts ":g:s:p:a:f:l:t:m:" opt; do
    case $opt in
	g)
	 g=${OPTARG}
	 ((g == control || g == patient)) || usage
	 echo "group = ${g}"
	 ;;
        s)
         s=${OPTARG}
         ((s == 1 || s == 2 || s == 3)) || usage
	 echo "session = ${s}"
         ;;
        p)
         p=${OPTARG}
	 echo "participant = ${p}"
         ;;
        a)
         a=${OPTARG}
	 echo "anatomy = ${a}"
         ;;
        f)
         f=${OPTARG}
	 echo "flair = ${f}"
         ;;
        l)
         loc+=("$OPTARG")
	 ;;
        t)
         task+=("$OPTARG")
	 ;;
	m)
	 fmap+=("$OPTARG")
	 ;;
        *)
         usage
         ;;
    esac
done
shift $((OPTIND-1))

# Throw an error and report usage again when group, session or participant are missing
if [ -z "${g}" ] || [ -z "${s}" ] || [ -z "${p}" ]; then
    echo "You are missing relevant information, either group, session number or participant number. Check usage:"
    usage
fi

# If fmap array is not empty, write out information
if [ ! ${#fmap[@]} -eq 0 ]; then
	fmap1=${fmap[0]}
	fmap2=${fmap[1]}
	echo "field map1 =" $fmap1 "and field map2 =" $fmap2
fi

# If localizer array is not empty, write out information
if [ ! ${#loc[@]} -eq 0 ]; then
	loc_run1=${loc[0]}
	loc_run2=${loc[1]}
	echo "localizer run1 =" $loc_run1 "and run2 =" $loc_run2
fi

# If task array is not empty, write out information
if [ ! ${#task[@]} -eq 0 ]; then
	task_run1=${task[0]}
	task_run2=${task[1]}
	echo "semantic task run1 =" $task_run1 "and run2 =" $task_run2
fi

#Convert Dicom images to Nifti using dcm2niix
#This will result in a packaged .tar.gz for the Niftis as well as a .json-file with metadata

source_folder="/data/p_02221/MDN_APH/sourcedata/"
output_folder="/data/p_02221/MDN_APH/nifti/"

if [ "$s" == 1 ]; then
   if [ "$g" == control ]; then

	# Anatomy – controls
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$a" -o "$output_folder"sub-control"$p"/ses-1/anat -f "sub-control"$p"_ses-1_T1w" "$source_folder""sub-control"$p"/ses-1/anat"

	# Functional scans – controls
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$loc_run1" -o "$output_folder"sub-control"$p"/ses-1/func -f "sub-control"$p"_ses-1_task-Localizer_run-1_bold" "$source_folder""sub-control"$p"/ses-1/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$loc_run2" -o "$output_folder"sub-control"$p"/ses-1/func -f "sub-control"$p"_ses-1_task-Localizer_run-2_bold" "$source_folder""sub-control"$p"/ses-1/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$task_run1" -o "$output_folder"sub-control"$p"/ses-1/func -f "sub-control"$p"_ses-1_task-semanticmatching_run-1_bold" "$source_folder""sub-control"$p"/ses-1/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$task_run2" -o "$output_folder"sub-control"$p"/ses-1/func -f "sub-control"$p"_ses-1_task-semanticmatching_run-2_bold" "$source_folder""sub-control"$p"/ses-1/func"

	# Fieldmaps – controls
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$fmap1" -o "$output_folder"sub-control"$p"/ses-1/fmap -f "sub-control"$p"_ses-1_dir-AP_epi" "$source_folder""sub-control"$p"/ses-1/fmap"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$fmap2" -o "$output_folder"sub-control"$p"/ses-1/fmap -f "sub-control"$p"_ses-1_dir-PA_epi" "$source_folder""sub-control"$p"/ses-1/fmap"


   elif [ "$g" == patient ]; then

	# Anatomy – patients
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$a" -o "$output_folder"sub-patient"$p"/ses-1/anat -f "sub-patient"$p"_ses-1_T1w" "$source_folder""sub-patient"$p"/ses-1/anat"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$f" -o "$output_folder"sub-patient"$p"/ses-1/anat -f "sub-patient"$p"_ses-1_FLAIR" "$source_folder""sub-patient"$p"/ses-1/anat"

	# Functional scans – patients
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$loc_run1" -o "$output_folder"sub-patient"$p"/ses-1/func -f "sub-patient"$p"_ses-1_task-Localizer_run-1_bold" "$source_folder""sub-patient"$p"/ses-1/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$loc_run2" -o "$output_folder"sub-patient"$p"/ses-1/func -f "sub-patient"$p"_ses-1_task-Localizer_run-2_bold" "$source_folder""sub-patient"$p"/ses-1/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$task_run1" -o "$output_folder"sub-patient"$p"/ses-1/func -f "sub-patient"$p"_ses-1_task-semanticmatching_run-1_bold" "$source_folder""sub-patient"$p"/ses-1/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$task_run2" -o "$output_folder"sub-patient"$p"/ses-1/func -f "sub-patient"$p"_ses-1_task-semanticmatching_run-2_bold" "$source_folder""sub-patient"$p"/ses-1/func"

	# Fieldmaps – patients
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$fmap1" -o "$output_folder"sub-patient"$p"/ses-1/fmap -f "sub-patient"$p"_ses-1_dir-AP_epi" "$source_folder""sub-patient"$p"/ses-1/fmap"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z y -n "$fmap2" -o "$output_folder"sub-patient"$p"/ses-1/fmap -f "sub-patient"$p"_ses-1_dir-PA_epi" "$source_folder""sub-patient"$p"/ses-1/fmap"

   fi

elif [ "$s" == 2 ] || [ "$s" == 3 ]; then
   if [ "$g" == control ]; then

	# Functional scans – controls
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$task_run1" -o "$output_folder"sub-control"$p"/ses-"$s"/func -f "sub-control"$p"_ses-"$s"_task-semanticmatching_run-1_bold" "$source_folder""sub-control"$p"/ses-"$s"/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$task_run2" -o "$output_folder"sub-control"$p"/ses-"$s"/func -f "sub-control"$p"_ses-"$s"_task-semanticmatching_run-2_bold" "$source_folder""sub-control"$p"/ses-"$s"/func"

	# Fieldmaps – controls
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$fmap1" -o "$output_folder"sub-control"$p"/ses-"$s"/fmap -f "sub-control"$p"_ses-"$s"_dir-AP_epi" "$source_folder""sub-control"$p"/ses-"$s"/fmap"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$fmap2" -o "$output_folder"sub-control"$p"/ses-"$s"/fmap -f "sub-control"$p"_ses-"$s"_dir-PA_epi" "$source_folder""sub-control"$p"/ses-"$s"/fmap"

   elif [ "$g" == patient ]; then

	# Functional scans – patients
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$task_run1" -o "$output_folder"sub-patient"$p"/ses-"$s"/func -f "sub-patient"$p"_ses-"$s"_task-semanticmatching_run-1_bold" "$source_folder""sub-patient"$p"/ses-"$s"/func"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$task_run2" -o "$output_folder"sub-patient"$p"/ses-"$s"/func -f "sub-patient"$p"_ses-"$s"_task-semanticmatching_run-2_bold" "$source_folder""sub-patient"$p"/ses-"$s"/func"

	# Fieldmaps patients
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$fmap1" -o "$output_folder"sub-patient"$p"/ses-"$s"/fmap -f "sub-patient"$p"_ses-"$s"_dir-AP_epi" "$source_folder""sub-patient"$p"/ses-"$s"/fmap"
	"/afs/cbs.mpg.de/software/scripts/dcm2niix" -b y -z n -n "$fmap2" -o "$output_folder"sub-patient"$p"/ses-"$s"/fmap -f "sub-patient"$p"_ses-"$s"_dir-PA_epi" "$source_folder""sub-patient"$p"/ses-"$s"/fmap"

   fi
fi
