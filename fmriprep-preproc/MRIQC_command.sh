#!/bin/bash

# This script runs MRIQC on the given participants. The mriqc_config.sh file contains the data path variables.

source MRIQC_config.sh

#for p in {001..030}
for p in 012 020
do
(singularity run -B /data/pt_02221/MDN_APH,/data/p_02221/MDN_APH /data/pt_02221/MDN_APH/derivatives/MRIQC/mriqc-22.0.1.simg "$data_path" "$output_dir/"sub-control"$p" -w "$working_dir/"sub-control"$p" participant --participant-label control"$p" --fd_thres 0.9 --no-sub &)
done
