#!/bin/bash

# This script runs MRIQC on the given participants. The mriqc_config.sh file contains the data path variables.

source MRIQC_config.sh

singularity run -B /data/pt_02221/MDN_APH,/data/p_02221/MDN_APH /data/pt_02221/MDN_APH/derivatives/MRIQC/mriqc-22.0.1.simg "$data_path" "$output_dir" -w "$working_dir" group --fd_thres 0.9 --no-sub
