# -*- coding: utf-8 -*-
"""
Created 09/20
Adapted 04/22: also add info to json for fieldmap

@author: martin
"""
# This script adds information of 'TaskName' to json files of functional scans

import os
import json
import argparse

# Initialize parser 
parser = argparse.ArgumentParser(description = "Add the information 'task name' to the functional json files so that fmriprep will run.") 
  
# Adding optional argument 
parser.add_argument("-p", "--Participant", nargs = "+", help = "Provide participant number with leading zeros", required = True) 
parser.add_argument("-g", "--Group", nargs = "+", help = "Specify participant group: 'patient' or 'control'", choices = ["patient", "control"], required = True)
parser.add_argument("-s", "--Session", nargs = "+", help = "Specify session number(s) (1, 2, or 3)", choices = ["1", "2", "3"], required = True) 
 
# Read arguments from command line 
args = parser.parse_args() 

base_dir = "/data/p_02221/MDN_APH/nifti/"

group = args.Group
print("group: %s" %group)

participants = args.Participant
print("participant: %s" %participants)

runs = ['1', '2']

session = args.Session
print("session: %s" %session)

if session == ['1']:
    if group == ['control']:
        print("Processing control")
        a = 'sub-control{0}'     
        
        for vp in participants:
            func_dir = os.path.join(base_dir, a.format(vp), 'ses-1/func')   
            os.chdir(func_dir)
            
            for run in runs:
                    
                entry = {'TaskName': 'semanticmatching'}
                
                with open('sub-control%s_ses-1_task-semanticmatching_run-%s_bold.json' %(vp, run)) as f:
                    data = json.load(f)
                
                data.update(entry)
                
                with open('sub-control%s_ses-1_task-semanticmatching_run-%s_bold.json' %(vp, run), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
        
                entry2 = {'TaskName': 'Localizer'}
                
                with open('sub-control%s_ses-1_task-Localizer_run-%s_bold.json' %(vp, run)) as f:
                    data = json.load(f)
                
                data.update(entry2)
                
                with open('sub-control%s_ses-1_task-Localizer_run-%s_bold.json' %(vp, run), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                    
            ## fieldmap        
            fmap_dir = os.path.join(base_dir, a.format(vp), 'ses-1/fmap')   
            os.chdir(fmap_dir)
                    
            entry_fm = {'IntendedFor' : ["ses-1/func/sub-control%s_ses-1_task-Localizer_run-1_bold.nii.gz" %vp, 
                                    "ses-1/func/sub-control%s_ses-1_task-Localizer_run-2_bold.nii.gz" %vp,
                                    "ses-1/func/sub-control%s_ses-1_task-semanticmatching_run-1_bold.nii.gz" %vp,
                                    "ses-1/func/sub-control%s_ses-1_task-semanticmatching_run-2_bold.nii.gz" %vp]}

            with open('sub-control%s_ses-1_dir-AP_epi.json' %vp) as f:
                data = json.load(f)
            
            data.update(entry_fm)
            
            with open('sub-control%s_ses-1_dir-AP_epi.json' %vp, 'w') as f: 
                json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                
            with open('sub-control%s_ses-1_dir-PA_epi.json' %vp) as f:
                data = json.load(f)
            
            data.update(entry_fm)
            
            with open('sub-control%s_ses-1_dir-PA_epi.json' %vp, 'w') as f: 
                json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
    
                    
    elif group == ['patient']:
        a = 'sub-patient{0}'     
    
        for vp in participants:
            func_dir = os.path.join(base_dir, a.format(vp), 'ses-1/func')   
            os.chdir(func_dir)
            
            for run in runs:
                    
                entry = {'TaskName': 'semanticmatching'}
                
                with open('sub-patient%s_ses-1_task-semanticmatching_run-%s_bold.json' %(vp, run)) as f:
                    data = json.load(f)
                
                data.update(entry)
                
                with open('sub-patient%s_ses-1_task-semanticmatching_run-%s_bold.json' %(vp, run), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
        
                entry2 = {'TaskName': 'Localizer'}
                
                with open('sub-patient%s_ses-1_task-Localizer_run-%s_bold.json' %(vp, run)) as f:
                    data = json.load(f)
                
                data.update(entry2)
                
                with open('sub-patient%s_ses-1_task-Localizer_run-%s_bold.json' %(vp, run), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                    
                    
            ## fieldmap        
            fmap_dir = os.path.join(base_dir, a.format(vp), 'ses-1/fmap')   
            os.chdir(fmap_dir)
                    
            entry_fm = {'IntendedFor' : ["ses-1/func/sub-patient%s_ses-1_task-Localizer_run-1_bold.nii.gz" %vp, 
                                    "ses-1/func/sub-patient%s_ses-1_task-Localizer_run-2_bold.nii.gz" %vp,
                                    "ses-1/func/sub-patient%s_ses-1_task-semanticmatching_run-1_bold.nii.gz" %vp,
                                    "ses-1/func/sub-patient%s_ses-1_task-semanticmatching_run-2_bold.nii.gz" %vp]}

            with open('sub-patient%s_ses-1_dir-AP_epi.json' %vp) as f:
                data = json.load(f)
            
            data.update(entry_fm)
            
            with open('sub-patient%s_ses-1_dir-AP_epi.json' %vp, 'w') as f: 
                json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                
            with open('sub-patient%s_ses-1_dir-PA_epi.json' %vp) as f:
                data = json.load(f)
            
            data.update(entry_fm)
            
            with open('sub-patient%s_ses-1_dir-PA_epi.json' %vp, 'w') as f: 
                json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                
                    
elif session == ['2'] or session == ['3']:
    
    if group == ['control']:
        print("Processing control")
        a = 'sub-control{0}'
        b = "ses-{0}"

        for vp in participants:
            for s in session:
                func_dir = os.path.join(base_dir, a.format(vp), b.format(s), 'func')   
                os.chdir(func_dir)
                
                for run in runs:
                        
                    entry = {'TaskName': 'semanticmatching'}
                    
                    with open('sub-control%s_ses-%s_task-semanticmatching_run-%s_bold.json' %(vp, s, run)) as f:
                        data = json.load(f)
                    
                    data.update(entry)
                    
                    with open('sub-control%s_ses-%s_task-semanticmatching_run-%s_bold.json' %(vp, s, run), 'w') as f: 
                        json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                     
                ## fieldmap        
                fmap_dir = os.path.join(base_dir, a.format(vp), b.format(s), 'fmap')   
                os.chdir(fmap_dir)
                        
                entry_fm = {'IntendedFor' : ["ses-%s/func/sub-control%s_ses-%s_task-semanticmatching_run-1_bold.nii.gz" %(s, vp, s),
                                        "ses-%s/func/sub-control%s_ses-%s_task-semanticmatching_run-2_bold.nii.gz" %(s, vp, s)]}
    
                with open('sub-control%s_ses-%s_dir-AP_epi.json' %(vp, s)) as f:
                    data = json.load(f)
                
                data.update(entry_fm)
                
                with open('sub-control%s_ses-%s_dir-AP_epi.json' %(vp, s), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                    
                with open('sub-control%s_ses-%s_dir-PA_epi.json' %(vp, s)) as f:
                    data = json.load(f)
                
                data.update(entry_fm)
                
                with open('sub-control%s_ses-%s_dir-PA_epi.json' %(vp, s), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
    
    elif group == ['patient']:
        a = 'sub-patient{0}'
        b = 'ses-{0}'

        for vp in participants:
            for s in session:
                func_dir = os.path.join(base_dir, a.format(vp), b.format(s), 'func')   
                os.chdir(func_dir)
                
                for run in runs:
                        
                    entry = {'TaskName': 'semanticmatching'}
                    
                    with open('sub-patient%s_ses-%s_task-semanticmatching_run-%s_bold.json' %(vp, s, run)) as f:
                        data = json.load(f)
                    
                    data.update(entry)
                    
                    with open('sub-patient%s_ses-%s_task-semanticmatching_run-%s_bold.json' %(vp, s, run), 'w') as f: 
                        json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                        
                        
                ## fieldmap        
                fmap_dir = os.path.join(base_dir, a.format(vp), b.format(s), 'fmap')   
                os.chdir(fmap_dir)
                        
                entry_fm = {'IntendedFor' : ["ses-%s/func/sub-patient%s_ses-%s_task-semanticmatching_run-1_bold.nii.gz" %(s, vp, s),
                                        "ses-%s/func/sub-patient%s_ses-%s_task-semanticmatching_run-2_bold.nii.gz" %(s, vp, s)]}
    
                with open('sub-patient%s_ses-%s_dir-AP_epi.json' %(vp, s)) as f:
                    data = json.load(f)
                
                data.update(entry_fm)
                
                with open('sub-patient%s_ses-%s_dir-AP_epi.json' %(vp, s), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
                    
                with open('sub-patient%s_ses-%s_dir-PA_epi.json' %(vp, s)) as f:
                    data = json.load(f)
                
                data.update(entry_fm)
                
                with open('sub-patient%s_ses-%s_dir-PA_epi.json' %(vp, s), 'w') as f: 
                    json.dump(data, f, sort_keys = True, indent = 4, separators=(',', ':'))
    
               