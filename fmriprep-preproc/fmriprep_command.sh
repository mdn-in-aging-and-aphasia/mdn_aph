#!/bin/bash

# singularity image of fmriprep contains version: 1.5.8 (02/2020)
# Updated and recalculated all participants with first stable fmriprep version 20.2.0 (09/2020)
# Switched to version 20.2.3 after there was a fix for the resampling error to standard space in version 20.2.0 (from first checks, it appears my data were not affected but to make sure, I switched to 20.2.3 on July 28 2021)
# This script runs fmriprep on the given participants. The fmriprep_config.sh file contains the data path variables.

usage() { echo "Usage: $0 [-g <control||patient>] [-p <participant number>]" ; }

while getopts ":g:s:p:a:f:l:t:m:" opt; do
    case $opt in
	g)
	 g=${OPTARG}
	 ((g == control || g == patient)) || usage
	 echo "group = ${g}"
	 ;;
        p)
	 vp+=("$OPTARG")
         #p=${OPTARG}
	 echo "participant = ${p}"
         ;;
        *)
         usage
         ;;
    esac
done
shift $((OPTIND-1))

# Throw an error and report usage again when group, session or participant are missing
if [ -z "${g}" ] || [ -z "${vp}" ]; then
    echo "You are missing relevant information, either group or participant number. Check usage:"
    usage
fi

source fmriprep_config.sh

if [ "$g" == control ]; then
	for p in "${vp[@]}"; do
		(singularity run -B /data/pt_02221/MDN_APH,/data/p_02221/MDN_APH /data/pt_02221/MDN_APH/derivatives/fmriprep-preproc/fmriprep-20.2.3.simg "$data_path" "$output_dir/"sub-control"$p" --fs-license-file /data/pt_02221/MDN_APH/derivatives/fmriprep-preproc/license.txt -w "$working_dir/"sub-control"$p" participant --participant-label control"$p" --output-spaces T1w MNI152NLin6Asym &)
		wait
		echo "All participants are in the pipeline"
	done

elif [ "$g" == patient ]; then
	for p in "${vp[@]}"; do
		(singularity run -B /data/pt_02221/MDN_APH,/data/p_02221/MDN_APH /data/pt_02221/MDN_APH/derivatives/fmriprep-preproc/fmriprep-20.2.3.simg "$data_path" "$output_dir/"sub-patient"$p" --fs-license-file /data/pt_02221/MDN_APH/derivatives/fmriprep-preproc/license.txt -w "$working_dir/"sub-patient"$p" participant --participant-label patient"$p" --output-spaces T1w MNI152NLin6Asym --fs-no-reconall &)
		wait
		echo "All participants are in the pipeline"
	done
fi
	
