#!/bin/bash

# Sandra Martin, 09/20

usage() { echo "Usage: $0 [-g <control||patient>] [-s <session number 1, 2 or 3>] [-p <participant number>]" ; }

vp=()
sess=()

while getopts ":g:s:p:" opt; do
    case $opt in
	g)
	 g=${OPTARG}
	 ((g == control || g == patient)) || usage
	 echo "group = ${g}"
	 ;;
        s)
         sess+=("$OPTARG")
         #((s == 1 || s == 2 || s == 3)) || usage
	 #echo "session = ${s}"
         ;;
        p)
         vp+=("$OPTARG")
	 #echo "participant = ${p}"
         ;;
        *)
         usage
         ;;
    esac
done
shift $((OPTIND-1))

# Throw an error and report usage again when group, session or participant are missing
if [ -z "${g}" ] || [ -z "${sess}" ] || [ -z "${vp}" ]; then
    echo "You are missing relevant information, either group, session number or participant number. Check usage:"
    usage
fi

for p in "${vp[@]}"; do

anat_MNI=/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-"$g""$p"/fmriprep/sub-"$g""$p"/ses-1/anat/sub-"$g""$p"_ses-1_desc-preproc_T1w.nii.gz

for s in "${sess[@]}"; do
for y in 1 2; do

loc=/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-"$g""$p"/fmriprep/sub-"$g""$p"/ses-"$s"/func/sub-"$g""$p"_ses-"$s"_task-Localizer_run-"$y"_space-MNI152NLin6Asym_desc-preproc_bold.nii.gz

task_MNI=/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-"$g""$p"/fmriprep/sub-"$g""$p"/ses-"$s"/func/sub-"$g""$p"_ses-"$s"_task-semanticmatching_run-"$y"_space-MNI152NLin6Asym_desc-preproc_bold.nii.gz

subject_MNI=/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-"$g""$p"/fmriprep/sub-"$g""$p"/ses-"$s"/func/sub-"$g""$p"_ses-"$s"_task-semanticmatching_run-"$y"_space-T1w_desc-preproc_bold.nii.gz

if [ "$s" == 1 ]; then
	for x in "$loc" "$task_MNI" "$subject_MNI" "$anat_MNI"; do
		if [ -f "$x" ]; then	
			gzip -df "$x"
			echo "unpacking $x now."
		else
			echo "file $x does not exist."
		fi
	done
elif [ "$s" == 2 ] || [ "$s" == 3 ]; then
	for x in "$task_MNI"; do
		if [ -f "$x" ]; then	
			gzip -df "$x"
			echo "unpacking $x now."
		else
			echo "file $x does not exist."
		fi
	done
fi

done
done
done
