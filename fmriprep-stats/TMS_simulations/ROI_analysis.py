import os
import numpy as np
import simnibs
import pandas as pd

# Define paths
base_dir = "/data/p_02221/MDN_APH/derivatives/TMS_simulation"

# Define list of subjects
subs = os.listdir(base_dir)

# Load file with indiv stimulation intensities and coordinates in subject space
stim_dir = "/data/p_02221/MDN_APH/derivatives/TMS_simulation/intensity.csv"
stim_intensity = pd.read_csv(stim_dir, sep=",")

head_msh = "{0}_TMS_1-0001_MagVenture_MCF-B65_nii_scalar.msh"

average_fields = pd.DataFrame()

def calculate_efield(base_dir, sub):
    sub_dir = os.path.join(base_dir, sub)

    ## Load simulation result
    # Read the simulation result
    head_mesh = simnibs.read_msh(os.path.join(sub_dir, 'efield_sim', head_msh.format(sub)))

    # Crop the mesh so we only have gray matter volume elements (tag 2 in the mesh)
    gray_matter = head_mesh.crop_mesh(2)


    ## Define the ROI
    # find coordinates in table
    sub_coords = stim_intensity.loc[stim_intensity.Subject == sub, 'coords_subSpace_preSMA'].iloc[0]
    sub_coords = [float(s) for s in sub_coords.split(',')]
    # define sphere radius
    r = 10.

    # Electric fields are defined in the center of the elements
    # get element centers
    elm_centers = gray_matter.elements_baricenters()[:]
    # determine the elements in the ROI
    roi = np.linalg.norm(elm_centers - sub_coords, axis=1) < r
    # get the element volumes, we will use those for averaging
    elm_vols = gray_matter.elements_volumes_and_areas()[:]

    ## Plot the ROI
    #gray_matter.add_element_field(roi, 'roi')
    #gray_matter.view(visible_fields='roi').show()

    ## Get field and calculate the mean
    # get the field of interest
    field_name = 'magnE'
    field = gray_matter.field[field_name][:]

    # Calculate the mean
    mean_magnE = np.average(field[roi], weights=elm_vols[roi])
    print('mean ', field_name, ' in pre-SMA ROI: ', mean_magnE)
    return mean_magnE

for sub in subs:
    if "sub-control" in sub:
        print("This is sub ", sub)
        x = calculate_efield(base_dir, sub)

        temp = pd.DataFrame(
            {
                "subject": sub,
                "mean_field": [x]
            }
        )
    average_fields = pd.concat([average_fields, temp])
    average_fields = average_fields.sort_values(by="subject")
    average_fields.to_csv(os.path.join(base_dir, "average_efields_sphere_10mm.txt"), index = False, sep = "\t")