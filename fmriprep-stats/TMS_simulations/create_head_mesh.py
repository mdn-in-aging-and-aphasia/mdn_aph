import os
import subprocess
import concurrent.futures
# from simnibs import sim_struct, run_simnibs

# Define paths
base_dir = "/data/p_02221/MDN_APH/derivatives/TMS_simulation"
charm_dir = "/data/u_martin_software/SimNIBS/bin/charm"


# Function to create head mesh
def create_head_mesh(id, anatomy):
    cmd = ["bash", charm_dir, id, anatomy, "--forceqform", "--forcerun"]
    subprocess.run(cmd)


# Wrapper function to run create_head_mesh in parallel
def create_head_mesh_wrapper(sub):
    os.chdir(os.path.join(base_dir, sub))
    anat_file = anatomy_form.format(sub)
    anat_file = os.path.abspath(anat_file)

    # run head mesh calculation
    create_head_mesh(sub, anat_file)


# Define list of subjects
subs = os.listdir(base_dir)

anatomy_form = "{0}_ses-1_desc-preproc_T1w.nii"

# # Loop over subjects to start
# for sub in subs:
#     if "sub-control" in sub:
#         os.chdir(os.path.join(base_dir, sub))
#         anat_file = anatomy_form.format(sub)
#         anat_file = os.path.abspath(anat_file)
#
#         # run head mesh calculation
#         create_head_mesh(sub, anat_file)

with concurrent.futures.ProcessPoolExecutor() as executor:
    futures = []
    for sub in subs:
        if "sub-control" in sub:
            if sub == "sub-control001" or sub == "sub-control023" or sub == "sub-control019":
                continue
            future = executor.submit(create_head_mesh_wrapper, sub)
            futures.append(future)

    for future in concurrent.futures.as_completed(futures):
        future.result()
