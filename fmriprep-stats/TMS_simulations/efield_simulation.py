import os
import concurrent.futures
import fnmatch
import pandas as pd
from simnibs import sim_struct, run_simnibs, localite

# Define paths
base_dir = "/data/p_02221/MDN_APH/derivatives/TMS_simulation"

# Define coil model
TMS_coil = "/data/u_martin_software/SimNIBS/resources/coil_models/Drakaki_BrainStim_2022/MagVenture_MCF-B65.nii.gz"
setup_factor = 1.43

# Load file with indiv stimulation intensities
stim_dir = "/data/p_02221/MDN_APH/derivatives/TMS_simulation/intensity.csv"
stim_intensity = pd.read_csv(stim_dir, sep=",")

def initialize_session(base_dir, sub):
    sub_dir = os.path.join(base_dir, sub)
    s = sim_struct.SESSION()

    # extract data from instrument marker xml file
    for file in os.listdir(sub_dir):
        if fnmatch.fnmatch(file, "InstrumentMarker*.xml"):
            tms_list = localite().read(os.path.join(base_dir, sub, file))  # read all TriggerMarkers from file and return as TMSLIST()
            s.add_tmslist(tms_list)
            tms_list.pos[0].didt  # <- stimulation intensity is filled with data from .xml if available or defaults to 1 A/µs.
            tms_list.pos[0].name  # <- name is filled with data from .xml if available or defaults to ''.

    # calculate dI/dT value based on stimulation intensity and set-up factor
    ## stim intensity is in %, didt in A/us
    MSO = stim_intensity.loc[stim_intensity.Subject == sub, "iTBS_intensity"].iloc[0]
    didt = MSO * setup_factor
    print("Field strength in A/us is", didt)
    didt_s = didt*1e6
    s.poslists[0].pos[0].didt = didt_s

    # define path for mesh
    mesh_dir = os.path.join(sub_dir, mesh_form.format(sub))
    s.subpath = mesh_dir

    # define output folder
    output_dir = os.path.join(sub_dir, "efield_sim")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    s.pathfem = output_dir

    # define name of coil
    # Select coil
    tms_list.fnamecoil = TMS_coil

    # open results directly in gmesh? (default = yes)
    s.open_in_gmsh = False

    # map results on surface
    s.map_to_surf = True

    # map results to fsaverage
    s.map_to_fsavg = True

    # map results to volume
    #s.map_to_vol = True

    print("This is subject", sub)
    return s


# Define list of subjects
subs = os.listdir(base_dir)

# Define file formats
anatomy_form = "{0}_ses-1_desc-preproc_T1w.nii"
mesh_form = "m2m_{0}"

# Loop over subjects to start
# for sub in subs:
#     if "sub-control" in sub:
#         sub_dir = os.path.join(base_dir, sub)
#         s = sim_struct.SESSION()
#         # os.chdir(os.path.join(base_dir, sub))
#
#         # extract data from instrument marker xml file
#         for file in os.listdir(sub_dir):
#             if fnmatch.fnmatch(file, "InstrumentMarker*.xml"):
#                 tms_list = localite().read(file)  # read all TriggerMarkers from file and return as TMSLIST()
#                 s.add_tmslist(tms_list)
#                 tms_list.pos[0].didt  # <- stimulation intensity is filled with data from .xml if available or defaults to 1 A/µs.
#                 tms_list.pos[0].name  # <- name is filled with data from .xml if available or defaults to ''.
#
#         # define path for mesh
#         mesh_dir = os.path.join(sub_dir, mesh_form.format(sub))
#         s.subpath = mesh_dir
#
#         # define output folder
#         output_dir = os.path.join(sub_dir, "efield_sim")
#         if not os.path.exists(output_dir):
#             os.makedirs(output_dir)
#         s.pathfem = output_dir
#
#         # define name of coil
#         # Select coil
#         tms_list.fnamecoil = TMS_coil
#
#         # open results directly in gmesh? (default = yes)
#         s.open_in_gmsh = False
#
#         # run simulation
#         run_simnibs(s)

# Create a thread pool
with concurrent.futures.ThreadPoolExecutor() as executor:
    # Submit the initialize_session and run_simnibs functions to the thread pool,
    # along with the base_dir and mesh_form arguments, and the sub argument for each iteration of the loop
    # futures = [executor.submit(initialize_session, base_dir, sub, mesh_form) for sub in subs if "sub-control" in sub]

    futures = []
    for sub in subs:
        if "sub-control" in sub:
            # if sub == "sub-control001" or sub == "sub-control030":
            #     continue
            future = executor.submit(initialize_session, base_dir, sub)
            futures.append(future)

    # Iterate over the completed futures in the order they complete
    for future in concurrent.futures.as_completed(futures):
        # Extract the session object and sub directory returned by the initialize_session function
        s = future.result()
        run_simnibs(s)