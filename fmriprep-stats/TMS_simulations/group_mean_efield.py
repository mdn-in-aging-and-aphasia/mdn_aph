import os
import numpy as np
import simnibs

# Define paths
base_dir = "/data/p_02221/MDN_APH/derivatives/TMS_simulation"

# Define list of subjects
subs = os.listdir(base_dir)

## Load simulation results
results_folder = "efield_sim/fsavg_overlays"
fsavg_msh_name = "{0}_TMS_1-0001_MagVenture_MCF-B65_nii_scalar_fsavg.msh"
field_name = 'E_magn'

fields = []
for sub in subs:
    if "sub-control" in sub:
        # read mesh with results transformed to fsaverage space
        results_fsavg = simnibs.read_msh(
            os.path.join(base_dir, sub, results_folder, fsavg_msh_name.format(sub))
        )
        # save the field in each subject
        fields.append(results_fsavg.field[field_name].value)

## Calculate and plot averages
# Calculate
fields = np.vstack(fields)
avg_field = np.mean(fields, axis=0)
std_field = np.std(fields, axis=0)

# Plot
results_fsavg.nodedata = [] # cleanup fields
results_fsavg.add_node_field(avg_field, 'E_magn_avg') # add average field
results_fsavg.add_node_field(std_field, 'E_magn_std') # add std field

# show surface with the fields
results_fsavg.view(visible_fields='E_magn_avg').show()
simnibs.write_freesurfer_surface(z, "/data/p_02221/MDN_APH/derivatives/TMS_simulation/group_avg.gii", True)

## Calculate average in an ROI defined using an atlas
# load atlas and define a region
# atlas = simnibs.get_atlas('HCP_MMP1')
# region_name = 'lh.4'
# roi = atlas[region_name]
# # visualize region
# results_fsavg.add_node_field(roi, region_name)
# results_fsavg.view(visible_fields=region_name).show()
#
# # calculate mean field using a weighted mean
# node_areas = results_fsavg.nodes_areas()
# avg_field_roi = np.average(avg_field[roi], weights=node_areas[roi])
# print(f'Average {field_name} in {region_name}: ', avg_field_roi)
# results_fsavg.add_node_field(roi, region_name)