#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to extract time series for pre-defined networks in order to run cPPI on these whole-network time series
@author: martin
"""

import numpy as np
import pandas as pd
import os
import json
import re
#import glob
import time
#import nibabel as nib

from nilearn import image
from nilearn import plotting
from nilearn.maskers import NiftiMapsMasker
#from nilearn.input_data import NiftiLabelsMasker # to parcellate given a template atlas
#from nilearn.input_data import NiftiSpheresMasker # to extract signal from given coordinates
#from nilearn.image import load_img, math_img

# function for natural sorting of acompcor components
def natural_key(string_):
    """See https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]  


base_dir_gt = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects'
base_dir_fmriprep = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output'
base_dir_networks = '/data/p_02221/Masks_ROIs/Yeo_2011_resampled'

subjects = ['control001','control002','control003','control004','control005','control006','control007','control008',
           'control009','control010','control011','control012','control013','control014','control015','control016',
           'control017','control018','control019','control020','control021','control022','control023','control024',
           'control025','control026','control027','control028','control029','control030']

runs = ['1', '2']
sessions = ['2', '3']

nifti_fn = 'sub-{0}_ses-{1}_task-semanticmatching_run-{2}_space-MNI152NLin6Asym_desc-preproc_bold.nii'
gm_fn = 'sub-{0}_ses-1_space-MNI152NLin6Asym_label-GM_probseg.nii.gz'
gm_mask_fn = 'sub-{0}_space-MNI152NLin6Asym_label-GM_thr02.nii'
timeSeries_fn = 'sub-{0}_run-{1}_gm_dTS.txt'
timeSeries_network_fn = 'sub-{0}_ses-{1}_run-{2}_gm_dTS.txt'
subject = 'sub-{0}'
session = 'ses-{0}'
confounds = 'sub-{0}_ses-{1}_task-semanticmatching_run-{2}_desc-confounds_timeseries.tsv'
json_file = 'sub-{0}_ses-{1}_task-semanticmatching_run-{2}_desc-confounds_timeseries.json'
final_confounds = 'sub-{0}_ses-{1}_task-semanticmatching_run-{2}_extracted_confounds_06.2023.csv'
timestr = time.strftime('%Y%m%d')

# creating/opening a logfile
f = open(os.path.join(base_dir_gt, "error_" + timestr + "_wholeNets.log"), "a")


###----------------- Create list of network maps for NiftiMapsMasker ----------------########
maps = []
for network in os.listdir(base_dir_networks):
    if network.endswith(".nii"):
        map = os.path.join(base_dir_networks, network)
        maps.append(map)
        maps = sorted(maps)

## Read confounds file and create df with relevant confounds. Currently these confounds are included:
## 24 realignment parameters (6 RP + temporal derivatives + squared terms), global signal, first 5 aCompCor components for WM and CSF, respectively (all recommended by Mascali et al., 2021), FD censored volumes (FD > 0.9), cosine regressors

for sub in subjects:
    for sess in sessions:
        for run in runs:
            print('This is participant %s and session %s and run %s' % (sub, sess, run))
            # Read confound file for each participant
            # confounds_dir = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), session.format(sess), 'func')
            # confound_file = pd.read_csv(os.path.join(confounds_dir, confounds.format(sub, sess, run)), sep = '\t', header = 0)
            #
            # ##----------------- Extract confound components ------------------------------
            #
            # # 24 realignment parameters
            # motion_conf = ['trans_x', 'trans_x_derivative1', 'trans_x_derivative1_power2', 'trans_x_power2', 'trans_y', 'trans_y_derivative1', 'trans_y_derivative1_power2', 'trans_y_power2', 'trans_z', 'trans_z_derivative1', 'trans_z_power2', 'trans_z_derivative1_power2', 'rot_x', 'rot_x_derivative1', 'rot_x_power2', 'rot_x_derivative1_power2', 'rot_y', 'rot_y_derivative1', 'rot_y_derivative1_power2', 'rot_y_power2', 'rot_z', 'rot_z_derivative1', 'rot_z_power2', 'rot_z_derivative1_power2']
            #
            # # Global signal
            # #gs_conf = ['global_signal']
            #
            # # aCompCor (first 5 components for CSF and WM each)
            # with open(os.path.join(confounds_dir,json_file.format(sub, sess, run)), 'r') as f:
            #
            #     data = json.load(f)
            #     comp_WM = []
            #     comp_CSF = []
            #     n_comps_total, n_comps_dropped, n_comps_retained = 0, 0, 0
            #
            #     for key, dat in data.items():
            #         try:
            #             if dat['Method'] == 'aCompCor' and dat['Mask'] == 'CSF':
            #                 n_comps_total += 1
            #
            #                 if dat['Retained']:
            #                     comp_CSF.append(key)
            #                     n_comps_retained +=1
            #                 else:
            #                     n_comps_dropped +=1
            #             elif dat['Method'] == 'aCompCor' and dat['Mask'] == 'WM':
            #                 n_comps_total += 1
            #
            #                 if dat['Retained']:
            #                     comp_WM.append(key)
            #                     n_comps_retained +=1
            #                 else:
            #                     n_comps_dropped +=1
            #
            #         except KeyError:
            #     # just ignore the items that don't have 'Method' or Mask 'key'
            #             pass
            #
            #     acompcor_WM_sorted = sorted(comp_WM, key = natural_key)
            #     acompcor_CSF_sorted = sorted(comp_CSF, key = natural_key)
            #     acompcor_WM = acompcor_WM_sorted[:5]
            #     acompcor_CSF = acompcor_CSF_sorted[:5]
            #
            #
            # # cosine regressors (instead of high-pass filtering, recommended by fMRIPrep)
            # cosine_conf = [col for col in confound_file if col.startswith('cosine')]
            #
            # # framewise displacement
            # fd = confound_file[confound_file.framewise_displacement >= 0.7] # current threshold is 0.7
            #
            # if not fd.empty:
            # #fd_values = fd[['framewise_displacement']]
            #
            #     # extract indices of rows/volumes with FD greater than threshold
            #     fd_idx = fd.index.values
            #
            #     # create array with amount of zero columns according to number of FD indices
            #     for i in range(len(fd_idx)):
            #         arr = np.zeros((421, i+1), dtype = int)
            #
            #     # loop over zero columns in array arr and fill cells with a 1 according to index in fd_idx
            #     for col in range(arr.shape[1]):
            #         arr[fd_idx[col],[col]] = 1
            #
            #     # make df from this array so we can concatenate it with other df for confounds
            #     fd_df = pd.DataFrame(arr)
            #
            #     # subset confounds df to extract relevant confounds for previously created lists
            #     confounds_final = confound_file[motion_conf + acompcor_CSF + acompcor_WM + cosine_conf]
            #
            #     # reset indices for both df since they can't be merged properly otherwise
            #     confounds_final.reset_index(drop=True, inplace=True)
            #     fd_df.reset_index(drop=True, inplace=True)
            #
            #     # concatenate confound df with df for fd
            #     confounds_df = pd.concat([confounds_final, fd_df], axis=1)
            #
            # else:
            #     # subset confounds df to extract relevant confounds for previously created lists
            #     confounds_final = confound_file[motion_conf + acompcor_CSF + acompcor_WM + cosine_conf]
            #
            #     # just rename df since there is no merge with FD regressors
            #     confounds_df = confounds_final
            #
            # # save df to file for later checks
            # # make output dir
            # outdir_confounds = os.path.join(base_dir_gt, subject.format(sub), session.format(sess), 'cPPI')
            # if not os.path.exists(outdir_confounds):
            #     os.makedirs(outdir_confounds)
            #
            # output_confounds_fn = os.path.join(outdir_confounds, final_confounds.format(sub, sess, run))
            # confounds_df.to_csv(output_confounds_fn, index = False)


    ###----------------- Loop over subjects with NiftiMapsMasker ----------------########
        
            # load pre-configured confounds df
            confounds_df = pd.read_csv(os.path.join(base_dir_gt, subject.format(sub), session.format(sess), 'cPPI', final_confounds.format(sub, sess, run)), sep = ',', header = 0, engine = 'python')
            #confounds_df = np.nan_to_num(confounds_df)

            '''----------------- Parcellation with confound removal ------------------------------'''
            # load nifti file
            nifti = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), session.format(sess),
                                 'func', nifti_fn.format(sub, sess, run))

            # load gm mask
            gm_mask = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub),
                                   'ses-1/anat', gm_mask_fn.format(sub))

            # apply GM mask to functional image
            first_func_img = image.index_img(nifti, 0)
            resampledGM_mask = image.resample_to_img(gm_mask, first_func_img, interpolation = 'nearest')
            # plotting.plot_roi(resampledGM_mask, first_func_img)
            # plotting.show()

            # denoise the data using the confound df as well as detrending
            # first, transform confounds df into a matrix
            confounds_matrix = confounds_df.to_numpy()
            confounds_matrix = np.nan_to_num(confounds_matrix)


            masker = NiftiMapsMasker(
                maps_img = maps,
                mask_img = resampledGM_mask,
                detrend= True,
                standardize = "zscore_sample",
                resampling_target = 'data',
                low_pass=0.1,
                high_pass=0.01,
                t_r=2,
            ).fit()

            try:
                time_series_raw = masker.transform(nifti, confounds=confounds_matrix)

                # check if there are columns in the array that are only zeros meaning that there was no signal in this ROI
                # If true, delete these columns from array
                try:
                    col = np.where(~time_series_raw.any(axis=0))[0]
                    time_series_raw = np.delete(time_series_raw, col, axis = 1)
                except:
                    pass

                # prepare output folders
                output_folder = os.path.join(base_dir_gt, subject.format(sub), session.format(sess), 'cPPI', 'parcellated', 'wholeNetworks_Yeo7_NO_GSR_new')
                if not os.path.exists(output_folder):
                    os.makedirs(output_folder)

                # save timeseries of components in file and add network names to header
                names = [_ for _ in maps]
                # names = [n.removeprefix("/data/p_02221/Masks_ROIs/Yeo_2011_resampled/*_*_").removesuffix(".nii") for n in names]
                names = [re.sub(r"^/data/p_02221/Masks_ROIs/Yeo_2011_resampled/resampled_\w+_", '', n) for n in names]
                names = [n.removesuffix(".nii") for n in names]
                df = pd.DataFrame(time_series_raw, columns=names)
                timeSeries = os.path.join(output_folder, timeSeries_network_fn.format(sub, sess, run))
                df.to_csv(timeSeries, index=False, header=True, sep=" ")
                # np.savetxt(timeSeries, time_series_raw, header = maps, fmt='%.4f')


    #            # Write ratio and number of voxels per ROI to table
    #            atlas_img = nib.load(atlas_fn[0])
    #            roi_indices, roi_counts = np.unique(atlas_img.get_fdata(), return_counts=True)
    #
    #            # transform to atlas
    #            masker_count = NiftiLabelsMasker(atlas_img, resampling_target='data')
    #            voxel_ratios = masker_count.fit_transform(resampledGM_mask)
    #
    #            # multiple by the number of voxels in each ROI
    #            n_voxels = voxel_ratios[0,:] * roi_counts[1:]
    #            #voxels.append((sub, run, IC, voxel_ratios, n_voxels))
    #
    #            voxels = pd.DataFrame()
    #            for r in range(len(roi_indices[1:])):
    #                voxels.loc[r, ['sub']] = sub
    #                voxels.loc[r, ['run']] = run
    #                voxels.loc[r, ['IC']] = IC
    #                voxels.loc[r, ['voxel ratio']] = voxel_ratios[0, r]
    #                voxels.loc[r, ['n voxels']] = n_voxels[r]
    #
    #            total_count = total_count.append(voxels, ignore_index = False)

            except Exception as ex:
                #import logging
                print(ex)

                # writing in the file
                f.write(sub + ' ' + sess + ' ' + run + ' ' + str(ex) + '\n')

            finally:
                pass
            

# closing the file
f.close()            
            
# write df to file
#total_count.to_csv(os.path.join(base_dir_ICA, 'N_voxels_allComponents_allSub3.txt'))

