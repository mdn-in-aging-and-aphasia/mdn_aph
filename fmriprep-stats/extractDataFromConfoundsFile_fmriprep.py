# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 18:22:08 2019

@author: martin
"""
import os
import shutil
import pandas as pd
import numpy as np
import argparse
import time
pd.options.mode.chained_assignment = None  # default='warn'

# Initialize parser 
parser = argparse.ArgumentParser(description = "Extract data from confounds file for SPM") 
  
# Adding optional argument 
parser.add_argument("-p", "--Participant", nargs = "+", help = "Provide participant number with leading zeros", required = True) 
parser.add_argument("-g", "--Group", nargs = "+", help = "Specify participant group: 'patient' or 'control'", choices = ["patient", "control"], required = True)
parser.add_argument("-s", "--Session", nargs = "+", help = "Specify session number(s) (1, 2, or 3)", choices = ["1", "2", "3"], required = True) 
 
# Read arguments from command line 
args = parser.parse_args() 

base_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/'

group = args.Group
print('group: %s' %group)

participants = args.Participant
print('participant: %s' %participants)

runs = ('1', '2')

sessions = args.Session
print("session: %s" %sessions)

timestr = time.strftime('%Y%m%d')
# Open file for participants with motion
motion_outliers = open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', 'motion_outliers_' + timestr + '.log'), 'a')

for session in sessions:
    if session == '1':
        tasks = ['semanticmatching'] # ['Localizer', 'semanticmatching']
        if group == ['control']:
            print("Processing control")
            a = 'sub-control{0}'
            b = 'sub-control{0}_ses-1_task-{1}_run-{2}_desc-confounds_timeseries.tsv'
            c = 'motion_task-{0}_run-{1}.txt'
            d = 'fd_task-{0}_run-{1}.txt'
            e = 'outliers_index_task-{0}_run-{1}.txt'
            h = 'sub-control{0}'
            l = 'outliers_index_task-Localizer_run-{0}.txt'
            m = 'outliers_index_task-semanticmatching_run-{0}.txt'
            n = 'fd_regressors_task-Localizer_run-{0}.txt'
            p = 'fd_regressors_task-semanticmatching_run-{0}.txt'
            o = 'multiple_regressors_task-Localizer_run-{0}.txt'
            q = 'multiple_regressors_task-semanticmatching_run-{0}.txt'
            
        elif group == ['patient']:
            print("Processing patient")
            a = 'sub-patient{0}'
            b = 'sub-patient{0}_ses-1_task-{1}_run-{2}_desc-confounds_timeseries.tsv'
            c = 'motion_task-{0}_run-{1}.txt'
            d = 'fd_task-{0}_run-{1}.txt'
            e = 'outliers_index_task-{0}_run-{1}.txt'
            h = 'sub-patient{0}'
            l = 'outliers_index_task-Localizer_run-{0}.txt'
            m = 'outliers_index_task-semanticmatching_run-{0}.txt'
            n = 'fd_regressors_task-Localizer_run-{0}.txt'
            p = 'fd_regressors_task-semanticmatching_run-{0}.txt'
            o = 'multiple_regressors_task-Localizer_run-{0}.txt'
            q = 'multiple_regressors_task-semanticmatching_run-{0}.txt'
            
        for vp in participants:
            for task in tasks:
                for run in runs:
                    print("processing session %s of participant %s and task %s" %(session, vp, task))
                    confound_dir = os.path.join(base_dir, h.format(vp), 'fmriprep', a.format(vp), 'ses-1/func')
                    os.chdir(confound_dir)
    
                    ## This part extracts the 6 motion regressors from the confounds.tsv file written by fmriprep
                    # Read confound file for second echo for each participant
                    confound_file = pd.read_csv(b.format(vp, task, run), sep = '\t', header = 0)   
                    # Extraxt six motion regressors from data frame
                    z = confound_file[['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z']]                        
                    # Write data frame to text file, don't write index and header   
                    f = os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           c.format(task, run))
                    z.to_csv(f, index = False, header = False, sep = '\t')
                    
                    # Qualitycheck: detect motion greater than 1.5 voxel in any direction
                    # Multiply rotation parameters with average head diameter since they are given as radians        
                    z.rot_x = z.rot_x * 50
                    z.rot_y = z.rot_y * 50
                    z.rot_z = z.rot_z * 50
            
                    # Perform a boolean search for values that are smaller and bigger than a given voxel size
                    r = z[(z < -3.75) | (z > 3.75)]
                    t = np.isnan(r).all() 
                    if t.isin([False]).any() == True:
                        print('group', group, 'participant', vp, ', task', task, ', session 1, run', run, ', has motion greater than 1.5 voxel.')
                        motion_outliers.write(''.join(group) + ' ' + vp + ' session 1, task ' + task + ', run ' + run + ' has motion greater than 1.5 voxel in any direction.' + '\n')
                        
                    r = z[(z < -2.5) | (z > 2.5)]
                    t = np.isnan(r).all() 
                    if t.isin([False]).any() == True:
                        print('group', group, 'participant', vp, ', task', task, ', session 1, run', run, ', has motion greater than 1 voxel.')
                        motion_outliers.write(''.join(group) + ' ' + vp + ' session 1, task ' + task + ', run ' + run + ' has motion greater than 1 voxel in any direction.' + '\n')
                    
                    # check if there are values in FD greater than 1 voxel size
                    fd_column = confound_file[['framewise_displacement']]
                    fd_outlier = fd_column[fd_column >= 2.5]
                    fd_out = np.isnan(fd_outlier).all() 
                    if fd_out.isin([False]).any() == True:
                        print('group', ''.join(group), 'participant', vp, ', task', task, ', session 1, run', run, ', has FD greater than 1 voxel.')
                        motion_outliers.write(''.join(group) + ' ' + vp + ' session 1, task ' + task + ', run ' + run + ' has FD greater than 1 voxel in any direction.' + '\n')
                    
            
                    ## This part extracts the index numbers from the framewise_displacement column that exceed the value of 0.7
                    w = confound_file
                    x = w[w.framewise_displacement >= 0.7]
                    if not x.empty:
                        fd = x[['framewise_displacement']]            
                        with open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                               d.format(task, run)), 'w') as f:
                              f.write(fd.to_string(header=False))      
                        with open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                               d.format(task, run))) as f:
                            with open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                                   e.format(task, run)), 'w') as i:
                                for line in f:
                                      if line.strip():
                                          i.write("\t".join(line.split()[:1]) + "\n")
                        
                        if task == 'Localizer':
                            path = os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion')
                            os.chdir(path)
                            
                            with open(l.format(run)) as f:
                                content = f.readlines()
                            
                            content = [y.strip() for y in content]
                            
                            content = list(map(int, content))
                            
                            f = open('fd_regressors_task-Localizer_run-%s.txt' %run, 'w')   
                            firstline = content[0]
                            arr = np.zeros((133, 1), dtype = int)
                            np.put(arr, [firstline - 1], [1])
                            np.savetxt('fd_regressors_task-Localizer_run-%s.txt' %run, arr, fmt='%i')
                            
                            def appendAsColumn(arr):
                                fileContent = np.loadtxt('fd_regressors_task-Localizer_run-%s.txt' %run, dtype = int, ndmin = 2)
                                fileContent = np.hstack((fileContent, arr.astype(int)))
                                np.savetxt('fd_regressors_task-Localizer_run-%s.txt' %run, fileContent, fmt='%i')
                            
                            for line in content[1:]:
                                arr = np.zeros((133, 1), dtype = int)
                                np.put(arr, [line - 1], [1])    
                                appendAsColumn(arr) 
                                
                            motion_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           c.format(task, run)), header = None, sep = '\t')
                            fd_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion',
                                            n.format(run)), header = None, sep = ' ')
                            
                            multiple_regressors = pd.concat([motion_df, fd_df], axis = 1)
                            multiple_regressors.to_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           o.format(run)), index = False, header = False, sep = '\t')
                            
                            shutil.copy(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           o.format(run)), os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects',
                                                   a.format(vp), 'ses-1/1st_level_Localizer'))
                        
                        else:
                        #elif task == "semanticmatching":
                            path = os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion')
                            os.chdir(path)
                            
                            with open(m.format(run)) as f:
                                content = f.readlines()
                        
                            content = [y.strip() for y in content]
                            
                            content = list(map(int, content))
                            
                            f = open('fd_regressors_task-semanticmatching_run-%s.txt' %run, 'w')   
                            firstline = content[0]
                            arr = np.zeros((421, 1), dtype = int)
                            np.put(arr, [firstline - 1], [1])
                            np.savetxt('fd_regressors_task-semanticmatching_run-%s.txt' %run, arr, fmt='%i')
                            
                            def appendAsColumn(arr):
                                fileContent = np.loadtxt('fd_regressors_task-semanticmatching_run-%s.txt' %run, dtype = int, ndmin = 2)
                                fileContent = np.hstack((fileContent, arr.astype(int)))
                                np.savetxt('fd_regressors_task-semanticmatching_run-%s.txt' %run, fileContent, fmt='%i')
                            
                            for line in content[1:]:
                                arr = np.zeros((421, 1), dtype = int)
                                np.put(arr, [line - 1], [1])    
                                appendAsColumn(arr)
                                
                            motion_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           c.format(task, run)), header = None, sep = '\t')
                            fd_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion',
                                            p.format(run)), header = None, sep = ' ')
                            multiple_regressors = pd.concat([motion_df, fd_df], axis = 1)
                            multiple_regressors.to_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           q.format(run)), index = False, header = False, sep = '\t')
                            
# 05/22: repeated preprocessing with FD included and recalculating stats except for TMS folder 
                            destinations = [os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects',
                                                   a.format(vp), 'ses-1/1st_level_semanticmatching')] 
                            #os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/1st_level_TMS'), 
                            
                            for dest in destinations:
                                shutil.copy(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           q.format(run)), dest)
                    
                    else:
                        if task == 'Localizer':
                            motion_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           c.format(task, run)), header = None, sep = '\t')
                            multiple_regressors = motion_df
                            multiple_regressors.to_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           o.format(run)), index = False, header = False, sep = '\t')
                            
                            destinations = [os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects',
                                                   a.format(vp), 'ses-1/1st_level_Localizer')]
                            
                            for dest in destinations:
                                shutil.copy(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           o.format(run)), dest)   
                        else: 
                            motion_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           c.format(task, run)), header = None, sep = '\t')
                            multiple_regressors = motion_df
                            multiple_regressors.to_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           q.format(run)), index = False, header = False, sep = '\t')
                            
# 05/22: repeated preprocessing with FD included and recalculating stats except for TMS folder 
                            destinations = [os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects',
                                                   a.format(vp), 'ses-1/1st_level_semanticmatching')] 
                            #os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/1st_level_TMS'), 
                            
                            for dest in destinations:
                                shutil.copy(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), 'ses-1/motion', 
                                           q.format(run)), dest)            
                                                     
    elif session == '2' or session == '3':
        tasks = ['semanticmatching']
        if group == ['control']:
            print("Processing control")
            a = 'sub-control{0}'
            b = 'sub-control{0}_ses-{1}_task-{2}_run-{3}_desc-confounds_timeseries.tsv'
            c = 'motion_task-{0}_run-{1}.txt'
            d = 'fd_task-{0}_run-{1}.txt'
            e = 'outliers_index_task-{0}_run-{1}.txt'
            h = 'sub-control{0}'
            i = 'ses-{0}'
            m = 'outliers_index_task-semanticmatching_run-{0}.txt'
            p = 'fd_regressors_task-semanticmatching_run-{0}.txt'
            q = 'multiple_regressors_task-semanticmatching_run-{0}.txt'
            
        elif group == ['patient']:
            print("Processing patient")
            a = 'sub-patient{0}'
            b = 'sub-patient{0}_ses-{1}_task-{2}_run-{3}_desc-confounds_timeseries.tsv'
            c = 'motion_task-{0}_run-{1}.txt'
            d = 'fd_task-{0}_run-{1}.txt'
            e = 'outliers_index_task-{0}_run-{1}.txt'
            h = 'sub-patient{0}'
            i = 'ses-{0}'
            m = 'outliers_index_task-semanticmatching_run-{0}.txt'
            p = 'fd_regressors_task-semanticmatching_run-{0}.txt'
            q = 'multiple_regressors_task-semanticmatching_run-{0}.txt'
            
        for vp in participants:
            for s in session:
                for task in tasks:
                    for run in runs:
                        print("processing session %s of participant %s and task %s" %(session, vp, task))
                        confound_dir = os.path.join(base_dir, h.format(vp), 'fmriprep', a.format(vp), i.format(s), 'func')
                        os.chdir(confound_dir)
        
                        ## This part extracts the 6 motion regressors from the confounds.tsv file written by fmriprep
                        # Read confound file for second echo for each participant
                        confound_file = pd.read_csv(b.format(vp, s, task, run), sep = '\t', header = 0)   
                        # Extraxt six motion regressors from data frame
                        z = confound_file[['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z']]                        
                        # Write data frame to text file, don't write index and header    
                        f = os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           c.format(task, run))
                        z.to_csv(f, index = False, header = False, sep = '\t')
                        
                        # Qualitycheck: detect motion greater than 1.5 voxel in any direction
                        # Multiply rotation parameters with average head diameter since they are given as radians        
                        z.rot_x = z.rot_x * 50
                        z.rot_y = z.rot_y * 50
                        z.rot_z = z.rot_z * 50
                
                        # Perform a boolean search for values that are smaller and bigger than a given voxel size
                        r = z[(z < -3.75) | (z > 3.75)]
                        t = np.isnan(r).all() 
                        if t.isin([False]).any() == True:
                            print('group ', group, ' participant ', vp, ' , task', task, ' ,session ', session, ' , run ', run, ', has motion greater than 1.5 voxel.')
                            motion_outliers.write(''.join(group) + ' ' + vp + ' session ' + session + ', task' + task + ', run ' + run + ' has motion greater than 1.5 voxel in any direction.' + '\n')
                            
                        r = z[(z < -2.5) | (z > 2.5)]
                        t = np.isnan(r).all() 
                        if t.isin([False]).any() == True:
                            print('group ', group, ' participant ', vp, ' , task', task, ' ,session ', session, ' , run ', run, ', has motion greater than 1 voxel.')
                            motion_outliers.write(''.join(group) + ' ' + vp + ' session ' + session + ', task' + task + ', run ' + run + ' has motion greater than 1 voxel in any direction.' + '\n')
               
                        ## This part extracts the index numbers from the framewise_displacement column that exceed the value of 0.7
                        w = confound_file
                        x = w[w.framewise_displacement >= 0.7]
                        if not x.empty:
                            fd = x[['framewise_displacement']]
                              
                            with open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                                   d.format(task, run)), 'w') as f:
                                  f.write(fd.to_string(header=False))
                                
                            with open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                                   d.format(task, run))) as f:
                                with open(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                                       e.format(task, run)), 'w') as k:
                                    for line in f:
                                          if line.strip():
                                              k.write("\t".join(line.split()[:1]) + "\n")
                                              
                            path = os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion')
                            os.chdir(path)
                            
                            with open(m.format(run)) as f:
                                content = f.readlines()
                        
                            content = [y.strip() for y in content]
                            
                            content = list(map(int, content))
                            
                            f = open('fd_regressors_task-semanticmatching_run-%s.txt' %run, 'w')   
                            firstline = content[0]
                            arr = np.zeros((421, 1), dtype = int)
                            np.put(arr, [firstline - 1], [1])
                            np.savetxt('fd_regressors_task-semanticmatching_run-%s.txt' %run, arr, fmt='%i')
                            
                            def appendAsColumn(arr):
                                fileContent = np.loadtxt('fd_regressors_task-semanticmatching_run-%s.txt' %run, dtype = int, ndmin = 2)
                                fileContent = np.hstack((fileContent, arr.astype(int)))
                                np.savetxt('fd_regressors_task-semanticmatching_run-%s.txt' %run, fileContent, fmt='%i')
                            
                            for line in content[1:]:
                                arr = np.zeros((421, 1), dtype = int)
                                np.put(arr, [line - 1], [1])    
                                appendAsColumn(arr)
                                
                            motion_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           c.format(task, run)), header = None, sep = '\t')
                            fd_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion',
                                            p.format(run)), header = None, sep = ' ')
                            multiple_regressors = pd.concat([motion_df, fd_df], axis = 1)
                            multiple_regressors.to_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           q.format(run)), index = False, header = False, sep = '\t')
                            
                            destinations = [os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects',
                                                   a.format(vp), i.format(s), '1st_level_semanticmatching')]
                            
                            for dest in destinations:
                                shutil.copy(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           q.format(run)), dest)
                        
                        else:
                            motion_df = pd.read_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           c.format(task, run)), header = None, sep = '\t')
                            multiple_regressors = motion_df
                            multiple_regressors.to_csv(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           q.format(run)), index = False, header = False, sep = '\t')
                            
                            destinations = [os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects',
                                                   a.format(vp), i.format(s), '1st_level_semanticmatching')]
                            
                            for dest in destinations:
                                shutil.copy(os.path.join('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects', a.format(vp), i.format(s), 'motion', 
                                           q.format(run)), dest)
                                
motion_outliers.close()