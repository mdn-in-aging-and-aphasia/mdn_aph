#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to write out specific motion time series from larger confound file
@author: martin
"""

import pandas as pd
import os
import numpy as np
from statistics import mean

center_function = lambda x: x - x.mean()

base_dir_gt = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects'
base_dir_fmriprep = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output'
base_dir_networks = '/data/p_02221/Masks_ROIs/Yeo_2011_resampled'

subjects = ['control001','control002','control003','control004','control005','control006','control007','control008',
           'control009','control010','control011','control012','control013','control014','control015','control016',
           'control017','control018','control019','control020','control021','control022','control023','control024',
           'control025','control026','control027','control028','control029','control030']

runs = ['1', '2']
sessions = ['2', '3']

subject = 'sub-{0}'
session = 'ses-{0}'
confounds = 'sub-{0}_ses-{1}_task-semanticmatching_run-{2}_desc-confounds_timeseries.tsv'
output_fn = 'sub-{0}_ses-{1}_task-semanticmatching_run-{2}_conf_RMSD.csv'

res = []

for sub in subjects:
    for sess in sessions:
        for run in runs:
            print('This is participant %s and session %s and run %s' %(sub, sess, run))

            confounds_dir = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub),
                                         session.format(sess), 'func')
            confound_file = pd.read_csv(os.path.join(confounds_dir, confounds.format(sub, sess, run)), sep='\t',
                                        header=0)

            fd = confound_file.framewise_displacement
            mean_fd = np.nanmean(fd)

            rmsd = confound_file.rmsd
            mean_rmsd = np.nanmean(rmsd)

            res.append((sub, sess, mean_fd, mean_rmsd))

            outdir_confounds = os.path.join(base_dir_gt, subject.format(sub), session.format(sess), 'cPPI')
            output_confounds_fn = os.path.join(outdir_confounds, output_fn.format(sub, sess, run))
            rmsd.to_csv(output_confounds_fn, index=False)
    
cols = ['sub', 'session', 'meanFD', 'meanRMSD']
result = pd.DataFrame(res, columns = cols)    
results = result.groupby(['sub', 'session']).mean()
results['meanRMSD_centered'] = center_function(results['meanRMSD'])

#results.to_csv('/data/pt_02004/MDN_LANG/meanFD_meanRMSD.txt', index = True)


