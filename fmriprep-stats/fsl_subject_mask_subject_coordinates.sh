#!/bin/bash

# This script uses FSL to transform coordinates and a ROI mask from standard into subject space.

usage() { echo "Usage: $0 [-g <control||patient>] [-p <participant number>]" ; }

while getopts ":g:s:p:a:f:l:t:m:" opt; do
    case $opt in
	g)
	 g=${OPTARG}
	 ((g == control || g == patient)) || usage
	 echo "group = ${g}"
	 ;;
        p)
         p=${OPTARG}
	 echo "participant = ${p}"
         ;;
        *)
         usage
         ;;
    esac
done
shift $((OPTIND-1))

# Throw an error and report usage again when group or participant are missing
if [ -z "${g}" ] || [ -z "${p}" ]; then
    echo "You are missing relevant information, either group or participant number. Check usage:"
    usage
fi

ref_brain="/data/p_02221/Template_brains/MNI2009cNLinAsymm/mni_icbm152_t1_tal_nlin_asym_09c.nii"
preSMA_mask="/data/p_02221/Masks_ROIs/bilateral_preSMA_mask_thr0.3.nii"
#"/data/p_02221/Masks_ROIs/bilateral_preSMA_mask_unthresh.nii"
coord_rightM1="/data/p_02221/Scripts/fmriprep-stats/coord_rightM1.txt"

if [ "$g" == control ]; then

	anatomy_subject_space="/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-control"$p"/fmriprep/sub-control"$p"/ses-1/anat/sub-control"$p"_ses-1_desc-preproc_T1w.nii"
	diff2standard="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$p"/ses-1/1st_level_TMS/diff2standard.mat"
	standard2diff="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$p"/ses-1/1st_level_TMS/standard2diff.mat"
	preSMA_subject_space="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$p"/ses-1/1st_level_TMS/sub-control"$p"_bilateral_preSMA_mask_subject_space.nii.gz"
	coord_rightM1_subject_space="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$p"/ses-1/1st_level_TMS/sub-control"$p"_coord_rightM1_subject_space.txt"

	FSL flirt -in $anatomy_subject_space -ref $ref_brain -omat $diff2standard

	FSL convert_xfm -omat $standard2diff -inverse $diff2standard

	FSL flirt -in $preSMA_mask -ref $anatomy_subject_space -applyxfm -init $standard2diff -datatype float -out $preSMA_subject_space

	FSL img2imgcoord -src $ref_brain -dest $anatomy_subject_space -xfm $standard2diff -mm $coord_rightM1 > $coord_rightM1_subject_space

	gzip -d $preSMA_subject_space

elif [ "$g" == patient ]; then

	anatomy_subject_space="/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-patient"$p"/fmriprep/sub-patient"$p"/ses-1/anat/sub-patient"$p"_ses-1_desc-preproc_T1w.nii"
	diff2standard="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-patient"$p"/ses-1/1st_level_TMS/diff2standard.mat"
	standard2diff="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-patient"$p"/ses-1/1st_level_TMS/standard2diff.mat"
	preSMA_subject_space="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-patient"$p"/ses-1/1st_level_TMS/sub-patient"$p"_bilateral_preSMA_mask_subject_space.nii.gz"
	coord_rightM1_subject_space="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-patient"$p"/ses-1/1st_level_TMS/sub-patient"$p"_coord_rightM1_subject_space.txt"

	FSL flirt -in $anatomy_subject_space -ref $ref_brain -omat $diff2standard

	FSL convert_xfm -omat $standard2diff -inverse $diff2standard

	FSL flirt -in $preSMA_mask -ref $anatomy_subject_space -applyxfm -init $standard2diff -datatype float -out $preSMA_subject_space

	FSL img2imgcoord -src $ref_brain -dest $anatomy_subject_space -xfm $standard2diff -mm $coord_rightM1 > $coord_rightM1_subject_space

	gzip -d $preSMA_subject_space
fi
