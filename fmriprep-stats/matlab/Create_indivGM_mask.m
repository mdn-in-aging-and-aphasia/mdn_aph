function Create_indivGM_mask(group, sub)

%% This script creates an individual GM mask in MNI space based on
% probability threshold > 0.2

%% Specify paths & folders
%  Data folder
data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/';
spm_path = '/data/p_02221/spm12';
addpath(spm_path);
      
%% Initialise SPM defaults
spm('defaults', 'FMRI');
spm_jobman('initcfg');
  
%% Start looping over subjects
for i = 1:numel(sub)
    if strcmp('control', group)
        %% Display which participant is currently processed
        X = ['Calculating GM mask for control participant ' sub{i}];
        disp(X);

        %% Get zipped GM mask
        % Run-1
        GM_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/anat/']);
        curr_GM_folder = GM_folder.folder;
        nifti_file = dir([curr_GM_folder '/sub-control' sub{i} '_ses-1_space-MNI152NLin6Asym_label-GM_probseg.nii.gz']);
        GM_mask_gz = [nifti_file.folder '/' nifti_file.name];
        GM_mask_probSeg = gunzip(GM_mask_gz);

        %% Define output directory
        output_dir = [data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/anat/'];
        output_name = ['sub-control' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02'];

    elseif strcmp('patient', group)
        %% Display which participant is currently processed
        X = ['Calculating GM mask for patient participant ' sub{i}];
        disp(X);

        %% Get zipped GM mask
        % Run-1
        GM_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/anat/']);
        curr_GM_folder = GM_folder.folder;
        nifti_file = dir([curr_GM_folder '/sub-patient' sub{i} '_ses-1_space-MNI152NLin6Asym_label-GM_probseg.nii.gz']);
        GM_mask_gz = [nifti_file.folder '/' nifti_file.name];
        GM_mask_probSeg = gunzip(GM_mask_gz);

        %% Define output directory
        output_dir = [data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/anat/'];
        output_name = ['sub-patient' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02'];

    end

    %% Set up batch
    clear matlabbatch            
    matlabbatch{1}.spm.util.imcalc.input = GM_mask_probSeg;
    matlabbatch{1}.spm.util.imcalc.output = output_name;
    matlabbatch{1}.spm.util.imcalc.outdir = {output_dir};
    matlabbatch{1}.spm.util.imcalc.expression = 'i1>0.2';
    matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
    matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
    matlabbatch{1}.spm.util.imcalc.options.mask = 0;
    matlabbatch{1}.spm.util.imcalc.options.interp = 1;
    matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
    spm_jobman('run', matlabbatch)
                
end