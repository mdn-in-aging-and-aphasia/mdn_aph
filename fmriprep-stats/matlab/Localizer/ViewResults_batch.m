%% created by Sandra Martin, 2022
clear

%% Add paths
addpath(genpath('/data/p_02221/spm12'));
% Initialise SPM defaults
spm('defaults', 'FMRI');
spm_jobman('initcfg');

%% Settings
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';
sub_folders = dir([root_dir 'subjects/sub-control*']);

localizer_dir = 'ses-1/1st_level_Localizer';

% which contrast(s) to display
cons = 4; % 4 = IntactSpeech > DegradedSpeech
%con_names = {'IntactSpeech_DegradedSpeech'};

p_threshold = 0.001;

for isub = 1:numel(sub_folders) 
    
    curr_folder = sub_folders(isub).name
    curr_folder_path = [root_dir 'subjects/' curr_folder]; 

    curr_firstLevel_dir = [curr_folder_path '/' localizer_dir];

    curr_SPM = dir([curr_firstLevel_dir '/SPM.mat']);
    curr_SPM_path = [curr_SPM.folder '/' curr_SPM.name];
        
    matlabbatch{1}.spm.stats.results.spmmat = {curr_SPM_path};
    matlabbatch{1}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{1}.spm.stats.results.conspec.contrasts = cons;
    matlabbatch{1}.spm.stats.results.conspec.threshdesc = 'none';
    matlabbatch{1}.spm.stats.results.conspec.thresh = p_threshold;
    matlabbatch{1}.spm.stats.results.conspec.extent = 0;
    matlabbatch{1}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{1}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{1}.spm.stats.results.units = 1;
    matlabbatch{1}.spm.stats.results.export = cell(1, 0);
    
    spm_jobman('run',matlabbatch);
    
    input('')
end
