#!/bin/bash

root_dir="/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Localizer_new/"
output_folder=$root_dir"IndivActivations/IntactSpeech_DegradedSpeech/Indexed_ROIs/"
ROI_dir=$root_dir"IndivActivations/IntactSpeech_DegradedSpeech/"
ROI_dirs=("$ROI_dir"*0.05*/)
total=${#ROI_dirs[@]}
i=0

# print out number of ROIs
echo total number of ROIs: $total

# Create ROIs with different index for subsequent merge
# loop over ROIs
#for roi in "${ROI_dirs[@]}"; do
#    i=$(( i+1 ))
#    echo index "$i"
#    echo "- processing $roi"
#	# loop over subjects	
#	for sub in {001..030}; do
#		mask_img="$roi"top10pct_above0/sub-control"$sub".nii
#		FSL fslmaths "$mask_img" -mul "$i" "$output_folder"sub-control"$sub"/sub-control"$sub"_index"$i".nii
#	done
#done

# Merge ROIs as one nii image
# loop over subjects
for sub in {001..030}; do
    echo "$sub"
    FSL fslmaths "$output_folder"sub-control"$sub"/sub-control"$sub"_index1.nii $(echo "-add "$output_folder"sub-control"$sub"/sub-control"$sub"_index"{2..48}".nii.gz") "$output_folder"sub-control"$sub"_combined_ROIs.nii
done
