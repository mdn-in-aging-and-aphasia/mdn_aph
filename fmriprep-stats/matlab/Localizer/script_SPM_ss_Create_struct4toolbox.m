%% written by Sandra Martin
% 05/2022
clear
close 

%% Add paths
% for spm_ss toolbox
addpath /data/p_02221/Scripts/fmriprep-stats/matlab/Localizer/spm_ss
% for SPM12
addpath(genpath('/data/p_02221/spm12'));
% Initialise SPM defaults
spm('defaults', 'FMRI');
spm_jobman('initcfg');

%% Settings
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';
sub_folders = dir([root_dir 'subjects/sub-control*']);

localizer_dir = 'ses-1/1st_level_Localizer';
first_level_dir = 'ses-1/1st_level_semanticmatching';

% output directory
outdir = [root_dir 'group_statistics/Localizer_spmSS_Localizer_thr_0.05FDR_minOverlap_60%_Lang_Rest'];
if ~exist(outdir)
    mkdir(outdir)
end

spmfiles_loc = {};
spmfiles_semMatch = {};
% ROI = {};

for isub = 1:numel(sub_folders)
    curr_folder = sub_folders(isub).name
    curr_folder_path = [root_dir 'subjects/' curr_folder]; 

    % get SPM file for localizer
    curr_dir = [curr_folder_path '/' localizer_dir];
    curr_SPM = [curr_dir '/SPM.mat'];
    spmfiles_loc{isub} = curr_SPM;
    
    % get SPM file for semantic matching
    curr_dir = [curr_folder_path '/' first_level_dir];
    curr_SPM = [curr_dir '/SPM.mat'];
    spmfiles_semMatch{isub} = curr_SPM;
    
%     % get ROI image file
%     curr_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Localizer_new/IndivActivations/IntactSpeech_DegradedSpeech/Indexed_ROIs/';
%     curr_ROIfile = [curr_dir sub_folders(isub).name '_combined_ROIs.nii.gz'];
%     ROI{isub} = curr_ROIfile;
end

%% Define structure for spm_ss functions
ss=struct(...
    'swd', outdir, ...         % output directory
    'EffectOfInterest_spm', {spmfiles_semMatch}, ...
    'Localizer_spm', {spmfiles_loc}, ...
    'EffectOfInterest_contrasts',{{'Control_task - Rest - All Sessions','Control_task - WPM+FPM - All Sessions', 'WPM+FPM - Control_task - All Sessions', ...
    'Language - Rest - All Sessions', 'WPM - Rest - All Sessions', 'FPM - Rest - All Sessions', 'WPM - FPM - All Sessions', 'WPM - Control_task - All Sessions', ...
    'FPM - Control_task - All Sessions'}}, ...
    'Localizer_contrasts',{{'Intact sound - Degraded sound - All Sessions'}}, ...       % localizer contrast (note: if these contrasts are not orthogonal the toolbox will automatically partition theses contrasts by sessions and perform cross-validation) 
    'EffectOfInterest_contrasts_filenameextension', '.nii', ...
    'Localizer_thr_type','FDR',...     % cell array of multiple comparisons correction types for each first-level localizer mask ('FDR','FWE','none','percentile-whole-brain','percentile-ROI-level','Nvoxels-whole-brain','Nvoxels-ROI-level','automatic') (default 'FDR')
    'Localizer_thr_p', .05, ...     % vector of false positive thresholds for each first-level localizer mask (default .05) note:if ss.Localizer_thr_type='automatic' the optimal FDR-corrected threshold level (maximizing the expected results sensitivity) is used (and this field is disregarded) 
    'type', 'GcSS', ...     % can be 'GcSS' (for automatically defined ROIs), 'mROI' (for manually defined ROIs), or 'voxel' (for voxel-based analyses)
    'smooth', 5, ...        % (FWHM mm)
    'overlap_thr_vox', 0.1, ...     %(for ss.type=='GcSS') voxel-level minimal proportion of subjects overlap when constructing ROI parcellation (default .10) 
    'overlap_thr_roi', 0.6, ...     %(for ss.type=='GcSS'|ss.type=='mROI') roi-level minimal proportion of subjects overlap when reporting ROI results (default .5) 
    'ExplicitMasking', '', ...
    'model',1 , ...     % can be 1 (one-sample t-test), 2 (two-sample t-test), or 3 (multiple regression)
    'estimation', 'ReML', ...       % Between-subjects model estimation type ('ReML','OLS') (Restricted Maximum Likelihood estimation vs. Ordinary Least Squares estimation; default 'ReML')
    'ask','missing');               % can be 'none' (any missing information is assumed to take default values), 'missing' (any missing information will be asked to the user), 'all' (it will ask for confirmation on each parameter)

%% Prepare design
ss=spm_ss_design(ss);               % see help spm_ss_design for additional information

%% Estimate design
ss=spm_ss_estimate(ss);
