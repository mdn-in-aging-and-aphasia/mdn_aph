function smooth(group, sub, session)

%% Specify paths & folders
%  Data folder
data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/';
spm_path = '/data/p_02221/spm12';
addpath(spm_path);

run = {'1','2'};

sessionnum = str2double(session);
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');

%% 
for i = 1:numel(sub)
for s = sessionnum
for x = 1:numel(run)
 
    if s == 1
       %contains(session, first_session)
        if strcmp('control', group)

        % Display which participant and which run is currently processed
        X = ['This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
            disp(X);

         %% Get volumes for Localizer – Controls
        epi_mainfolder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func']);
        curr_epi_subfolder = epi_mainfolder.folder;
        nifti_files = dir([curr_epi_subfolder '/sub-control' sub{i} '_ses-1_task-Localizer_run-' run{x} '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols = {};
            for iVol = 1:numel(func_vols)
                Vols{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols = Vols';

        clear matlabbatch
        matlabbatch{1}.spm.spatial.smooth.data = Vols;
        matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        % Run the batch
        spm_jobman('run', matlabbatch)

        %% Get volumes for semantic matching task – Controls – subject space
        epi_mainfolder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func']);
        curr_epi_subfolder = epi_mainfolder.folder;
        nifti_files = dir([curr_epi_subfolder '/sub-control' sub{i} '_ses-1_task-semanticmatching_run-' run{x} '_space-T1w_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols = {};
            for iVol = 1:numel(func_vols)
                Vols{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols = Vols';

        clear matlabbatch
        matlabbatch{1}.spm.spatial.smooth.data = Vols;
        matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        % Run the batch
        spm_jobman('run', matlabbatch)

        %% Get volumes for semantic matching task – Controls – MNI space
        epi_mainfolder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func']);
        curr_epi_subfolder = epi_mainfolder.folder;
        nifti_files = dir([curr_epi_subfolder '/sub-control' sub{i} '_ses-1_task-semanticmatching_run-' run{x} '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols = {};
            for iVol = 1:numel(func_vols)
                Vols{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols = Vols';

        clear matlabbatch
        matlabbatch{1}.spm.spatial.smooth.data = Vols;
        matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        % Run the batch
        spm_jobman('run', matlabbatch)

        elseif strcmp('patient', group)

            % Display which participant and which run is currently processed
            X = ['This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
            disp(X);

            %% Get volumes for Localizer – Patients
            epi_mainfolder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func']);
            curr_epi_subfolder = epi_mainfolder.folder;
            nifti_files = dir([curr_epi_subfolder '/sub-patient' sub{i} '_ses-1_task-Localizer_run-' run{x} '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
            func_images_path = [nifti_files.folder '/' nifti_files.name];
                func_vols = spm_vol(func_images_path);
                Vols = {};
                for iVol = 1:numel(func_vols)
                    Vols{iVol} = [func_images_path ',' num2str(iVol)];
                end
                Vols = Vols';

            clear matlabbatch
            matlabbatch{1}.spm.spatial.smooth.data = Vols;
            matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
            matlabbatch{1}.spm.spatial.smooth.dtype = 0;
            matlabbatch{1}.spm.spatial.smooth.im = 0;
            matlabbatch{1}.spm.spatial.smooth.prefix = 's';

            % Run the batch
            spm_jobman('run', matlabbatch)


            %% Get volumes for semantic matching task – patients – subject space
            epi_mainfolder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func']);
            curr_epi_subfolder = epi_mainfolder.folder;
            nifti_files = dir([curr_epi_subfolder '/sub-patient' sub{i} '_ses-1_task-semanticmatching_run-' run{x} '_space-T1w_desc-preproc_bold.nii']);
            func_images_path = [nifti_files.folder '/' nifti_files.name];
                func_vols = spm_vol(func_images_path);
                Vols = {};
                for iVol = 1:numel(func_vols)
                    Vols{iVol} = [func_images_path ',' num2str(iVol)];
                end
                Vols = Vols';

            clear matlabbatch
            matlabbatch{1}.spm.spatial.smooth.data = Vols;
            matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
            matlabbatch{1}.spm.spatial.smooth.dtype = 0;
            matlabbatch{1}.spm.spatial.smooth.im = 0;
            matlabbatch{1}.spm.spatial.smooth.prefix = 's';

            % Run batch
            spm_jobman('run', matlabbatch)

            %% Get volumes for semantic matching task – patients – MNI space
            epi_mainfolder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func']);
            curr_epi_subfolder = epi_mainfolder.folder;
            nifti_files = dir([curr_epi_subfolder '/sub-patient' sub{i} '_ses-1_task-semanticmatching_run-' run{x} '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
            func_images_path = [nifti_files.folder '/' nifti_files.name];
                func_vols = spm_vol(func_images_path);
                Vols = {};
                for iVol = 1:numel(func_vols)
                    Vols{iVol} = [func_images_path ',' num2str(iVol)];
                end
                Vols = Vols';

            clear matlabbatch
            matlabbatch{1}.spm.spatial.smooth.data = Vols;
            matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
            matlabbatch{1}.spm.spatial.smooth.dtype = 0;
            matlabbatch{1}.spm.spatial.smooth.im = 0;
            matlabbatch{1}.spm.spatial.smooth.prefix = 's';

            % Run batch
            spm_jobman('run', matlabbatch)
            end
    
    elseif s == 2 | s == 3
       if strcmp('control', group)

        % Display which participant and which run is currently processed
        X = ['This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
            disp(X);

        %% Get volumes for semantic matching task – Controls – subject space
%         epi_mainfolder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-' num2str(s) '/func']);
%         curr_epi_subfolder = epi_mainfolder.folder;
%         nifti_files = dir([curr_epi_subfolder '/sub-control' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-' run{x} '_space-T1w_desc-preproc_bold.nii']);
%         func_images_path = [nifti_files.folder '/' nifti_files.name];
%             func_vols = spm_vol(func_images_path);
%             Vols = {};
%             for iVol = 1:numel(func_vols)
%                 Vols{iVol} = [func_images_path ',' num2str(iVol)];
%             end
%             Vols = Vols';
% 
%         clear matlabbatch
%         matlabbatch{1}.spm.spatial.smooth.data = Vols;
%         matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
%         matlabbatch{1}.spm.spatial.smooth.dtype = 0;
%         matlabbatch{1}.spm.spatial.smooth.im = 0;
%         matlabbatch{1}.spm.spatial.smooth.prefix = 's';
% 
%         % Run the batch
%         spm_jobman('run', matlabbatch)

        %% Get volumes for semantic matching task – Controls – MNI space
        epi_mainfolder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-' num2str(s) '/func']);
        curr_epi_subfolder = epi_mainfolder.folder;
        nifti_files = dir([curr_epi_subfolder '/sub-control' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-' run{x} '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols = {};
            for iVol = 1:numel(func_vols)
                Vols{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols = Vols';

        clear matlabbatch
        matlabbatch{1}.spm.spatial.smooth.data = Vols;
        matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        % Run the batch
        spm_jobman('run', matlabbatch)

        elseif strcmp('patient', group)

            % Display which participant and which run is currently processed
            X = ['This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
            disp(X);

        %% Get volumes for semantic matching task – patients – subject space
%         epi_mainfolder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-' num2str(s) '/func']);
%         curr_epi_subfolder = epi_mainfolder.folder;
%         nifti_files = dir([curr_epi_subfolder '/sub-patient' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-' run{x} '_space-T1w_desc-preproc_bold.nii']);
%         func_images_path = [nifti_files.folder '/' nifti_files.name];
%             func_vols = spm_vol(func_images_path);
%             Vols = {};
%             for iVol = 1:numel(func_vols)
%                 Vols{iVol} = [func_images_path ',' num2str(iVol)];
%             end
%             Vols = Vols';
% 
%         clear matlabbatch
%         matlabbatch{1}.spm.spatial.smooth.data = Vols;
%         matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
%         matlabbatch{1}.spm.spatial.smooth.dtype = 0;
%         matlabbatch{1}.spm.spatial.smooth.im = 0;
%         matlabbatch{1}.spm.spatial.smooth.prefix = 's';
% 
%         % Run batch
%         spm_jobman('run', matlabbatch)

        %% Get volumes for semantic matching task – patients – MNI space
        epi_mainfolder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-' num2str(s) '/func']);
        curr_epi_subfolder = epi_mainfolder.folder;
        nifti_files = dir([curr_epi_subfolder '/sub-patient' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-' run{x} '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols = {};
            for iVol = 1:numel(func_vols)
                Vols{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols = Vols';

        clear matlabbatch
        matlabbatch{1}.spm.spatial.smooth.data = Vols;
        matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        % Run batch
        spm_jobman('run', matlabbatch)
        end 
    end
end
end
end