function prepare_logfiles_for_matlab(group, sub, session)

%% Prepare logfiles from Presentation for SPM and Matlab
% This function gets the logfiles of all participants for session * and imports them with the
% importPresentationLog function found on the internet (renamed to ..._orig). The output of this
% function is saved as sub*_run*_log_a/b.mat in the subject logfile directory and
% can be accessed to get names, onsets and durations for SPM.

%% Define important paths
data_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/';

run = {'1', '2'};
sessionnum = str2double(session);

for i = 1:numel(sub)
for s = sessionnum
for x = 1:numel(run)
    
    if s == 1
        if strcmp('control', group)

            % Display which participant and which run is currently processed
            X = ['This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

            % define the folders where the logfiles can be found
            filename_logfolder = [data_dir 'sub-control' sub{i} '/ses-1/logfiles/'];

            %% get the files for Localizer
            fileName_path = dir([filename_logfolder strip(sub{i},'left','0') '-MDN_APH_Localizer' run{x} '.log']);
            fileName = [fileName_path.folder '/' fileName_path.name];

            % import using importPresentationLog_orig function
            [a,b] = importPresentationLog_orig(fileName);

            disp (['save logfiles ' sub{i} ]);

            % save in each subject's folder
            save([data_dir 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_task-Localizer_run-' run{x} '_log_a.mat'],'a');
            save([data_dir 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_task-Localizer_run-' run{x} '_log_b.mat'],'b');


            %% get the files for semantic matching task
            fileName_path = dir([filename_logfolder strip(sub{i},'left','0') '-MDN_APH_controls_run' run{x} '.log']);
            fileName = [fileName_path.folder '/' fileName_path.name];

            % import using importPresentationLog_orig function
            [a,b] = importPresentationLog_orig(fileName);

            disp (['save logfiles ' sub{i} ]);

            % save in each subjects' folder
            save([data_dir 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_task-semanticmatching_run-' run{x} '_log_a.mat'],'a');
            save([data_dir 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat'],'b');

        elseif strcmp('patient', group)

            % Display which participant and which run is currently processed
            X = ['This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
            disp(X);

            % define the folders where the logfiles can be found
            filename_logfolder = [data_dir 'sub-patient' sub{i} '/ses-1/logfiles/'];

            %% get the files for Localizer
            fileName_path = dir([filename_logfolder strip(sub{i},'left','0') '-MDN_APH_Localizer' run{x} '.log']);
            fileName = [fileName_path.folder '/' fileName_path.name];

            % import using importPresentationLog_orig function
            [a,b] = importPresentationLog_orig(fileName);

            disp (['save logfiles ' sub{i} ]);

            % save in each subjects' folder
            save([data_dir 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_task-Localizer_run-' run{x} '_log_a.mat'],'a');
            save([data_dir 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_task-Localizer_run-' run{x} '_log_b.mat'],'b');


            %% get the files for semantic matching task
            fileName_path = dir([filename_logfolder strip(sub{i},'left','0') '-MDN_APH_patients_run' run{x} '.log']);
            fileName = [fileName_path.folder '/' fileName_path.name];

            % import using importPresentationLog_orig function
            [a,b] = importPresentationLog_orig(fileName);

            disp (['save logfiles ' sub{i} ]);

            % save in each subjects' folder
            save([data_dir 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_task-semanticmatching_run-' run{x} '_log_a.mat'],'a');
            save([data_dir 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat'],'b');
        end
     elseif s == 2 | s == 3
        if strcmp('control', group)

           % Display which participant and which run is currently processed
           X = ['This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

           % define the folders where the logfiles can be found
           filename_logfolder = [data_dir 'sub-control' sub{i} '/ses-' num2str(s) '/logfiles/'];

           %% get the files for semantic matching task
           fileName_path = dir([filename_logfolder strip(sub{i},'left','0') '-MDN_APH_controls_run' run{x} '.log']);
           fileName = [fileName_path.folder '/' fileName_path.name];

           % import using importPresentationLog_orig function
           [a,b] = importPresentationLog_orig(fileName);

           disp (['save logfiles ' sub{i} ]);

           % save in each subjects' folder
           save([data_dir 'sub-control' sub{i} '/ses-' num2str(s) '/logfiles/sub-control' sub{i} '_task-semanticmatching_run-' run{x} '_log_a.mat'],'a');
           save([data_dir 'sub-control' sub{i} '/ses-' num2str(s) '/logfiles/sub-control' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat'],'b');

        elseif strcmp('patient', group)

            % Display which participant and which run is currently processed
            X = ['This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
            disp(X);
            
            % define the folders where the logfiles can be found
            filename_logfolder = [data_dir 'sub-patient' sub{i} '/ses-' num2str(s) '/logfiles/'];

            %% get the files for semantic matching task
            fileName_path = dir([filename_logfolder strip(sub{i},'left','0') '-MDN_APH_patients_run' run{x} '.log']);
            fileName = [fileName_path.folder '/' fileName_path.name];

            % import using importPresentationLog_orig function
            [a,b] = importPresentationLog_orig(fileName);

            disp (['save logfiles ' sub{i} ]);

            % save in each subjects' folder
            save([data_dir 'sub-patient' sub{i} '/ses-' num2str(s) '/logfiles/sub-patient' sub{i} '_task-semanticmatching_run-' run{x} '_log_a.mat'],'a');
            save([data_dir 'sub-patient' sub{i} '/ses-' num2str(s) '/logfiles/sub-patient' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat'],'b');

        end
    end
end
end
end
