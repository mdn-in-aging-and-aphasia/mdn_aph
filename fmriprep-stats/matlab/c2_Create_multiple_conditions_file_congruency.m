function Create_multiple_conditions_file(group, sub, session)

%% This function creates a mat-File for multiple conditions 1st level design
% This script reads the VP*_run_*_log_b.mat that was created in the "prepare logfiles for matlab" function
% and gets names, onsets and durations for SPM.
% 10/2020
% adapted 05/2022 to include onsets and RT for congruent and incongruent
% trials

%% Define important paths
data_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/';
spm_path = '/data/p_02221/spm12';
addpath(spm_path);

run = {'1','2'};
sessionnum = str2double(session);
% sub = erase(sub,"00");

%% Start looping over subjects and runs
for i = 1:numel(sub)
for s = sessionnum
for x = 1:numel(run)
    
    if s == 1
        if strcmp('control', group)
            %% Display which participant and which run is currently processed
            X = ['This is Localizer. This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

            % Define the folders where the logfiles can be found
            logfolder = [data_dir 'sub-control' sub{i} '/ses-1/logfiles/'];

            % Load b struct, in which the information about the onset times is stored
            matFile = dir([logfolder 'sub-control' sub{i} '_task-Localizer_run-' run{x} '_log_b.mat']);
            mat = [matFile.folder '/' matFile.name];
            load(mat);

            % find row of first pulse which is pulse before 'Wait_for_Pulse' stimulus
            firstPulse_idx = find(strcmp(b.event_type, 'Pulse'));
            firstPulse_idx = firstPulse_idx(1,1);
            % get time of first pulse
            firstPulse_time = b.time(firstPulse_idx);
            disp(firstPulse_idx);

            %% Find indexes, get event times and calculate stimulus onsets for different conditions

            % find rows where intact sound was presented
            % get times of events
            % calculate picture onsets with respect to first pulse
            sound_intact = find(strcmp(b.event_type, 'Sound') & contains(b.code, 'intact'));
            sound_intact_time = b.time(sound_intact);
            sound_intact_onsets = (sound_intact_time - firstPulse_time)/10000; 

            % find rows where degraded sound was presented
            % get times of events
            % calculate picture onsets with respect to first pulse
            sound_degraded = find(strcmp(b.event_type, 'Sound') & contains(b.code, 'degraded'));
            sound_degraded_time = b.time(sound_degraded);
            sound_degraded_onsets = (sound_degraded_time - firstPulse_time)/10000; 


            %% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
            onsets = {sound_intact_onsets, sound_degraded_onsets};
            names = {'sound_intact', 'sound_degraded'};
            durations = {18, 18};       


            %% Save names,onsets,durations in onsets_explicit_stimduration.mat and the parametric modulation of RT as pmod.mat     
            save([logfolder 'sub-control' sub{i} '_session-1_task-Localizer_run-' run{x} '_1st_level_block_design' '.mat'],'names', 'durations', 'onsets');
            
            %% Display which task, participant and which run is currently processed
            X = ['This is Semantic matching. This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

            % Load b struct, in which the information about the onset times is stored
            matFile = dir([logfolder 'sub-control' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat']);
            mat = [matFile.folder '/' matFile.name];
            load(mat);

            % load the txt file with reaction times and accuracy and convert to struct
            txtfile = dir([logfolder 'MDN_APH_control-' strip(sub{i},'left','0') '_session1_run' run{x} '*.txt']);
            txt = [txtfile.folder '/' txtfile.name];
            fileID = fopen(txt, 'r');
            ownlog = textscan(fileID, '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %d', 'HeaderLines', 9);
            names = {'block_number', 'stimulus_audio', 'time_audio','length_audio','stimulus_picture','time_picture','response_time',...
                'relative_response_time','button_response','accuracy','condition','correctness','category','ISI','time_ISI'};
            ownlog_struct = cell2struct(ownlog, names,2);
            % ownlog_struct.response_time = ownlog_struct.response_time * 10;
            % ownlog_struct.response_time = num2cell(ownlog_struct.response_time);

            % find row of first pulse which is pulse before 'Wait_for_Pulse' stimulus
            firstPulse_idx = find(strcmp(b.event_type, 'Pulse'));
            firstPulse_idx = firstPulse_idx(1,1);
            % get time of first pulse
            firstPulse_time = b.time(firstPulse_idx);
            disp(firstPulse_idx);


            %% Compare both logfiles and add relevant information into b
            % loop through b & ownlog_struct and check if b.code has same
            % stimulus name as ownlog.stimulus_picture --> write relevant information in b

            for jLog = 1:numel(b.code)
                %for iOwnlog = 1:numel(ownlog_struct.stimulus_picture)
                for iOwnlog = 1:numel(ownlog_struct.stimulus_audio)
                    if strcmp(b.code{jLog}, ownlog_struct.stimulus_audio{iOwnlog})
                        b.accuracy{jLog} = ownlog_struct.accuracy{iOwnlog};
                        b.response_time{jLog} = ownlog_struct.response_time{iOwnlog};
                        b.condition{jLog} = ownlog_struct.condition{iOwnlog};
                        b.correctness{jLog} = ownlog_struct.correctness{iOwnlog};
                        b.category{jLog} = ownlog_struct.category{iOwnlog};
                    end
                end
            end

            % transpose the new cell arrays
            b.response_time = b.response_time';
            b.accuracy = b.accuracy';
            b.condition = b.condition';
            b.correctness = b.correctness';
            b.category = b.category';

            %% Find indices, get event times and calculate stimulus onsets for different conditions               
            b.trial2 = compose('%d', b.trial);

            % find rows where stimuli are presented
            stimulus_idx = find(strcmp(b.event_type, 'Sound')); % this will be the index for ALL trials (correct & incorrect)
            stimulus_pics_idx = find(strcmp(b.event_type, 'Picture'));

            % get times of events
            stimulus_times = b.time(stimulus_idx); % all trials

            % get reaction times
            %stimuli_idx = find(strcmp('WPM', b.condition) | strcmp('tone', b.condition) | strcmp('FPM', b.condition));
            %stimulus_rt = b.response_time(stimuli_idx) % RT for all stimuli
            rel_rt = b.response_time(stimulus_idx);
            stim_trials = b.trial(stimulus_idx);
            stim_trials = compose('%d', stim_trials);
            pics_idx = find(ismember(b.trial2, stim_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            pics_time = b.time(pics_idx);
            length = (pics_time - stimulus_times) ./10; % length in ms
            stimulus_rt = str2double(rel_rt) + length;

            % find ITI rows to indicate ITI length for erroneous trials
            ISI_idx = find(strcmp(b.code, 'ISI'));
            ISI_dur = b.duration(ISI_idx);
            ISI_times = b.time(ISI_idx);

            % calculate sound-onsets w.r.t. first pulse
            stimulus_onsets = (stimulus_times - firstPulse_time)/10000;% all trials
            ISI_onsets = (ISI_times - firstPulse_time)/10000; % ISIs

            % Only correct trials                
            corr_idx  = find(strcmp(b.accuracy, 'correct'));
            corr_times = b.time(corr_idx); % only correct trials
            corr_rel_rt = b.response_time(corr_idx);
            corr_trials = b.trial(corr_idx);
            corr_trials = compose('%d', corr_trials);
            corr_pics_idx = find(ismember(b.trial2, corr_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            corr_pics_time = b.time(corr_pics_idx);
            corr_length = (corr_pics_time - corr_times) ./10; % length in ms
            corr_rt = str2double(corr_rel_rt) + corr_length;
%             corr_rt = b.response_time(corr_idx); % Rt only for correct trials
%             corr_rt = (cellfun(@str2num, corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            corr_rt = corr_rt ./1000;
            
            corr_onsets = (corr_times - firstPulse_time)/10000;
            corr_dur = repmat(3.5, numel(corr_onsets), 1);

            % extract conditions and respective onset times and create different
            % columns(separate for all vs only correct trials so it can be analyzed
            % either way)
            % put all erroneous trials in error regressor
            % due to different information I need to find index for conditions first.
            % This index is on picture, however, we want the sound. I thus search for
            % sound with same trial number in b.trial2 and extract this index. Then I
            % can calculate the right onset time.

            % all trials
            WPM_idx = find(strcmp('WPM', b.condition)); % word-picture matching condition
            WPM_times = b.time(WPM_idx);
            WPM_rel_rt = b.response_time(WPM_idx);
            WPM_trials = b.trial(WPM_idx); 
            WPM_trials = compose('%d', WPM_trials);
            WPM_idx = find(ismember(b.trial2, WPM_trials) & strcmp('Sound', b.event_type));
            WPM_pics_idx = find(ismember(b.trial2, WPM_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            WPM_pics_time = b.time(WPM_pics_idx);
            WPM_length = (WPM_pics_time - WPM_times) ./10; % length in ms
            WPM_rt = str2double(WPM_rel_rt) + WPM_length;
            WPM_rt = WPM_rt ./1000;
            WPM_onsets = (WPM_times - firstPulse_time)/10000;
            
            tone_idx = find(strcmp('tone', b.condition)); % word-picture matching condition
            tone_times = b.time(tone_idx);
            tone_rel_rt = b.response_time(tone_idx);
            tone_trials = b.trial(tone_idx); 
            tone_trials = compose('%d', tone_trials);
            tone_idx = find(ismember(b.trial2, tone_trials) & strcmp('Sound', b.event_type));
            tone_pics_idx = find(ismember(b.trial2, tone_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            tone_pics_time = b.time(tone_pics_idx);
            tone_length = (tone_pics_time - tone_times) ./10; % length in ms
            tone_rt = str2double(tone_rel_rt) + tone_length;
            tone_rt = tone_rt ./1000;
            tone_onsets = (tone_times - firstPulse_time)/10000;

            FPM_idx = find(strcmp('FPM', b.condition)); % word-picture matching condition
            FPM_times = b.time(FPM_idx);
            FPM_rel_rt = b.response_time(FPM_idx);
            FPM_trials = b.trial(FPM_idx); 
            FPM_trials = compose('%d', FPM_trials);
            FPM_idx = find(ismember(b.trial2, FPM_trials) & strcmp('Sound', b.event_type));
            FPM_pics_idx = find(ismember(b.trial2, FPM_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            FPM_pics_time = b.time(FPM_pics_idx);
            FPM_length = (FPM_pics_time - FPM_times) ./10; % length in ms
            FPM_rt = str2double(FPM_rel_rt) + FPM_length;
            FPM_rt = FPM_rt ./1000;
            FPM_onsets = (FPM_times - firstPulse_time)/10000;              

            % only correct
            WPM_corr_idx = find(strcmp('WPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            WPM_corr_times = b.time(WPM_corr_idx);
            WPM_corr_rel_rt = b.response_time(WPM_corr_idx);
            WPM_corr_trials = b.trial(WPM_corr_idx); 
            WPM_corr_trials = compose('%d', WPM_corr_trials);
            WPM_corr_idx = find(ismember(b.trial2, WPM_corr_trials) & strcmp('Sound', b.event_type));
            WPM_corr_pics_idx = find(ismember(b.trial2, WPM_corr_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            WPM_corr_pics_time = b.time(WPM_corr_pics_idx);
            WPM_corr_length = (WPM_corr_pics_time - WPM_corr_times) ./10; % length in ms
            WPM_corr_rt = str2double(WPM_corr_rel_rt) + WPM_corr_length;
            WPM_corr_rt = WPM_corr_rt ./1000;
            WPM_corr_onsets = (WPM_corr_times - firstPulse_time)/10000;
            
            tone_corr_idx = find(strcmp('tone', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            tone_corr_times = b.time(tone_corr_idx);
            tone_corr_rel_rt = b.response_time(tone_corr_idx);
            tone_corr_trials = b.trial(tone_corr_idx); 
            tone_corr_trials = compose('%d', tone_corr_trials);
            tone_corr_idx = find(ismember(b.trial2, tone_corr_trials) & strcmp('Sound', b.event_type));
            tone_corr_pics_idx = find(ismember(b.trial2, tone_corr_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            tone_corr_pics_time = b.time(tone_corr_pics_idx);
            tone_corr_length = (tone_corr_pics_time - tone_corr_times) ./10; % length in ms
            tone_corr_rt = str2double(tone_corr_rel_rt) + tone_corr_length;
            tone_corr_rt = tone_corr_rt ./1000;
            tone_corr_onsets = (tone_corr_times - firstPulse_time)/10000;

            FPM_corr_idx = find(strcmp('FPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            FPM_corr_times = b.time(FPM_corr_idx);
            FPM_corr_rel_rt = b.response_time(FPM_corr_idx);
            FPM_corr_trials = b.trial(FPM_corr_idx); 
            FPM_corr_trials = compose('%d', FPM_corr_trials);
            FPM_corr_idx = find(ismember(b.trial2, FPM_corr_trials) & strcmp('Sound', b.event_type));
            FPM_corr_pics_idx = find(ismember(b.trial2, FPM_corr_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            FPM_corr_pics_time = b.time(FPM_corr_pics_idx);
            FPM_corr_length = (FPM_corr_pics_time - FPM_corr_times) ./10; % length in ms
            FPM_corr_rt = str2double(FPM_corr_rel_rt) + FPM_corr_length;
            FPM_corr_rt = FPM_corr_rt ./1000;
            FPM_corr_onsets = (FPM_corr_times - firstPulse_time)/10000;
            
              
%             congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy)); % congruent items – correct trials
%             congruent_corr_rt = b.response_time(congruent_corr_idx);
%             congruent_corr_rt = (cellfun(@str2num, congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
%             congruent_corr_times = b.time(congruent_corr_idx);
%             congruent_corr_onsets = (congruent_corr_times - firstPulse_time)/10000;
%             congruent_corr_dur = repmat(3.5, numel(congruent_corr_onsets), 1);
%             
%             incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy)); % incongruent items – correct trials
%             incongruent_corr_rt = b.response_time(incongruent_corr_idx);
%             incongruent_corr_rt = (cellfun(@str2num, incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
%             incongruent_corr_times = b.time(incongruent_corr_idx);
%             incongruent_corr_onsets = (incongruent_corr_times - firstPulse_time)/10000;
%             incongruent_corr_dur = repmat(3.5, numel(incongruent_corr_onsets), 1);
            
            WPM_congruent_idx = find(strcmp('correct', b.correctness) & strcmp('WPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            WPM_congruent_times = b.time(WPM_congruent_idx);
            WPM_congruent_rel_rt = b.response_time(WPM_congruent_idx);
            WPM_congruent_trials = b.trial(WPM_congruent_idx); 
            WPM_congruent_trials = compose('%d', WPM_congruent_trials);
            WPM_congruent_idx = find(ismember(b.trial2, WPM_congruent_trials) & strcmp('Sound', b.event_type));
            WPM_congruent_pics_idx = find(ismember(b.trial2, WPM_congruent_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            WPM_congruent_pics_time = b.time(WPM_congruent_pics_idx);
            WPM_congruent_length = (WPM_congruent_pics_time - WPM_congruent_times) ./10; % length in ms
            WPM_congruent_rt = str2double(WPM_congruent_rel_rt) + WPM_congruent_length;
            WPM_congruent_rt = WPM_congruent_rt ./1000;
            WPM_congruent_onsets = (WPM_congruent_times - firstPulse_time)/10000;

            WPM_incongruent_idx = find(strcmp('incorrect', b.correctness) & strcmp('WPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            WPM_incongruent_times = b.time(WPM_incongruent_idx);
            WPM_incongruent_rel_rt = b.response_time(WPM_incongruent_idx);
            WPM_incongruent_trials = b.trial(WPM_incongruent_idx); 
            WPM_incongruent_trials = compose('%d', WPM_incongruent_trials);
            WPM_incongruent_idx = find(ismember(b.trial2, WPM_incongruent_trials) & strcmp('Sound', b.event_type));
            WPM_incongruent_pics_idx = find(ismember(b.trial2, WPM_incongruent_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            WPM_incongruent_pics_time = b.time(WPM_incongruent_pics_idx);
            WPM_incongruent_length = (WPM_incongruent_pics_time - WPM_incongruent_times) ./10; % length in ms
            WPM_incongruent_rt = str2double(WPM_incongruent_rel_rt) + WPM_incongruent_length;
            WPM_incongruent_rt = WPM_incongruent_rt ./1000;
            WPM_incongruent_onsets = (WPM_incongruent_times - firstPulse_time)/10000;

            
            FPM_congruent_idx = find(strcmp('correct', b.correctness) & strcmp('FPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            FPM_congruent_times = b.time(FPM_congruent_idx);
            FPM_congruent_rel_rt = b.response_time(FPM_congruent_idx);
            FPM_congruent_trials = b.trial(FPM_congruent_idx); 
            FPM_congruent_trials = compose('%d', FPM_congruent_trials);
            FPM_congruent_idx = find(ismember(b.trial2, FPM_congruent_trials) & strcmp('Sound', b.event_type));
            FPM_congruent_pics_idx = find(ismember(b.trial2, FPM_congruent_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            FPM_congruent_pics_time = b.time(FPM_congruent_pics_idx);
            FPM_congruent_length = (FPM_congruent_pics_time - FPM_congruent_times) ./10; % length in ms
            FPM_congruent_rt = str2double(FPM_congruent_rel_rt) + FPM_congruent_length;
            FPM_congruent_rt = FPM_congruent_rt ./1000;
            FPM_congruent_onsets = (FPM_congruent_times - firstPulse_time)/10000;

            FPM_incongruent_idx = find(strcmp('incorrect', b.correctness) & strcmp('FPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            FPM_incongruent_times = b.time(FPM_incongruent_idx);
            FPM_incongruent_rel_rt = b.response_time(FPM_incongruent_idx);
            FPM_incongruent_trials = b.trial(FPM_incongruent_idx); 
            FPM_incongruent_trials = compose('%d', FPM_incongruent_trials);
            FPM_incongruent_idx = find(ismember(b.trial2, FPM_incongruent_trials) & strcmp('Sound', b.event_type));
            FPM_incongruent_pics_idx = find(ismember(b.trial2, FPM_incongruent_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            FPM_incongruent_pics_time = b.time(FPM_incongruent_pics_idx);
            FPM_incongruent_length = (FPM_incongruent_pics_time - FPM_incongruent_times) ./10; % length in ms
            FPM_incongruent_rt = str2double(FPM_incongruent_rel_rt) + FPM_incongruent_length;
            FPM_incongruent_rt = FPM_incongruent_rt ./1000;
            FPM_incongruent_onsets = (FPM_incongruent_times - firstPulse_time)/10000;

            
            tone_congruent_idx = find(strcmp('correct', b.correctness) & strcmp('tone', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            tone_congruent_times = b.time(tone_congruent_idx);
            tone_congruent_rel_rt = b.response_time(tone_congruent_idx);
            tone_congruent_trials = b.trial(tone_congruent_idx); 
            tone_congruent_trials = compose('%d', tone_congruent_trials);
            tone_congruent_idx = find(ismember(b.trial2, tone_congruent_trials) & strcmp('Sound', b.event_type));
            tone_congruent_pics_idx = find(ismember(b.trial2, tone_congruent_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            tone_congruent_pics_time = b.time(tone_congruent_pics_idx);
            tone_congruent_length = (tone_congruent_pics_time - tone_congruent_times) ./10; % length in ms
            tone_congruent_rt = str2double(tone_congruent_rel_rt) + tone_congruent_length;
            tone_congruent_rt = tone_congruent_rt ./1000;
            tone_congruent_onsets = (tone_congruent_times - firstPulse_time)/10000;

            tone_incongruent_idx = find(strcmp('incorrect', b.correctness) & strcmp('tone', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            tone_incongruent_times = b.time(tone_incongruent_idx);
            tone_incongruent_rel_rt = b.response_time(tone_incongruent_idx);
            tone_incongruent_trials = b.trial(tone_incongruent_idx); 
            tone_incongruent_trials = compose('%d', tone_incongruent_trials);
            tone_incongruent_idx = find(ismember(b.trial2, tone_incongruent_trials) & strcmp('Sound', b.event_type));
            tone_incongruent_pics_idx = find(ismember(b.trial2, tone_incongruent_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            tone_incongruent_pics_time = b.time(tone_incongruent_pics_idx);
            tone_incongruent_length = (tone_incongruent_pics_time - tone_incongruent_times) ./10; % length in ms
            tone_incongruent_rt = str2double(tone_incongruent_rel_rt) + tone_incongruent_length;
            tone_incongruent_rt = tone_incongruent_rt ./1000;
            tone_incongruent_onsets = (tone_incongruent_times - firstPulse_time)/10000;
            
            errors_idx = find(strcmp('incorrect', b.accuracy) | strcmp('NaN', b.accuracy)); % all trials that were answered incorrectly – includes NaNs
            errors_times = b.time(errors_idx);
            errors_rel_rt = b.response_time(errors_idx);
            errors_trials = b.trial(errors_idx); 
            errors_trials = compose('%d', errors_trials);
            errors_idx = find(ismember(b.trial2, errors_trials) & strcmp('Sound', b.event_type));
            errors_pics_idx = find(ismember(b.trial2, errors_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            errors_pics_time = b.time(errors_pics_idx);
            errors_length = (errors_pics_time - errors_times) ./10; % length in ms
            errors_rt = str2double(errors_rel_rt) + errors_length;
            errors_rt = errors_rt ./1000;
            if any(isnan(errors_rt), 'all')
                nan_idx = find(isnan(errors_rt))
                errors_rt(nan_idx) = 3.5;
            end
            
            errors_onsets = (errors_times - firstPulse_time)/10000;
            errors_dur = repmat(3.5, numel(errors_onsets), 1);
            
            %% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
            if ~isempty(errors_onsets)
                onsets = {WPM_congruent_onsets, WPM_incongruent_onsets, FPM_congruent_onsets, FPM_incongruent_onsets, ...
                    tone_congruent_onsets, tone_incongruent_onsets, errors_onsets};
                names = {'WPM_congruent_corr', 'WPM_incongruent_corr', 'FPM_congruent_corr', 'FPM_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr', 'Errors'};
                durations = {WPM_congruent_rt, WPM_incongruent_rt, FPM_congruent_rt, FPM_incongruent_rt, ...
                    tone_congruent_rt, tone_incongruent_rt, errors_rt};
            else
                onsets = {WPM_congruent_onsets, WPM_incongruent_onsets, FPM_congruent_onsets, FPM_incongruent_onsets, ...
                    tone_congruent_onsets, tone_incongruent_onsets};
                names = {'WPM_congruent_corr', 'WPM_incongruent_corr', 'FPM_congruent_corr', 'FPM_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr'};
                durations = {WPM_congruent_rt, WPM_incongruent_rt, FPM_congruent_rt, FPM_incongruent_rt, ...
                    tone_congruent_rt, tone_incongruent_rt};
            end;
            

%             onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, errors_onsets};
%             names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'Errors'};
%             durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, errors_rt};
            %durations = {WPM_corr_dur, CPM_corr_dur, FPM_corr_dur, errors_dur};

            %% Save names,onsets,durations in mat file and the parametric modulation of RT as pmod.mat     
            save([logfolder 'sub-control' sub{i} '_session-1_task-semanticmatching_run-' run{x} '_1st_level_event_related_congruency_incl_length_audio' '.mat'],'names', 'durations', 'onsets');

        elseif strcmp('patient', group)
            
            % Display which participant and which run is currently processed
            X = ['This is Localizer. This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

            % Define the folders where the logfiles can be found
            logfolder = [data_dir 'sub-patient' sub{i} '/ses-1/logfiles/'];

            % Load b struct, in which the information about the onset times is stored
            matFile = dir([logfolder 'sub-patient' sub{i} '_task-Localizer_run-' run{x} '_log_b.mat']);
            mat = [matFile.folder '/' matFile.name];
            load(mat);

            % find row of first pulse which is pulse before 'Wait_for_Pulse' stimulus
            firstPulse_idx = find(strcmp(b.event_type, 'Pulse'));
            firstPulse_idx = firstPulse_idx(1,1);
            % get time of first pulse
            firstPulse_time = b.time(firstPulse_idx);
            disp(firstPulse_idx);

            %% Find indexes, get event times and calculate stimulus onsets for different conditions

            % find rows where intact sound was presented
            % get times of events
            % calculate picture onsets with respect to first pulse
            sound_intact = find(strcmp(b.event_type, 'Sound') & contains(b.code, 'intact'));
            sound_intact_time = b.time(sound_intact);
            sound_intact_onsets = (sound_intact_time - firstPulse_time)/10000; 

            % find rows where degraded sound was presented
            % get times of events
            % calculate picture onsets with respect to first pulse
            sound_degraded = find(strcmp(b.event_type, 'Sound') & contains(b.code, 'degraded'));
            sound_degraded_time = b.time(sound_degraded);
            sound_degraded_onsets = (sound_degraded_time - firstPulse_time)/10000; 


            %% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
            onsets = {sound_intact_onsets, sound_degraded_onsets};
            names = {'sound_intact', 'sound_degraded'};
            durations = {18, 18};       

            %% Save names,onsets,durations in onsets_explicit_stimduration.mat and the parametric modulation of RT as pmod.mat     
            save([logfolder 'sub-patient' sub{i} '_session-1_task-Localizer_run-' run{x} '_1st_level_block_design' '.mat'],'names', 'durations', 'onsets');         

            %% Display which participant and which run is currently processed
            X = ['This is Semantic matching. This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

            % Load b struct, in which the information about the onset times is stored
            matFile = dir([logfolder 'sub-patient' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat']);
            mat = [matFile.folder '/' matFile.name];
            load(mat);

            % load the txt file with reaction times and accuracy and convert to struct
            txtfile = dir([logfolder 'MDN_APH_patient-' strip(sub{i},'left','0') '_session1_run' run{x} '*.txt']);
            txt = [txtfile.folder '/' txtfile.name];
            fileID = fopen(txt, 'r');
            ownlog = textscan(fileID, '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %d', 'HeaderLines', 9);
            names = {'block_number', 'stimulus_audio', 'time_audio','length_audio','stimulus_picture','time_picture','response_time',...
                'relative_response_time','button_response','accuracy','condition','correctness','category','ISI','time_ISI'};
            ownlog_struct = cell2struct(ownlog, names,2);
            % ownlog_struct.response_time = ownlog_struct.response_time * 10;
            % ownlog_struct.response_time = num2cell(ownlog_struct.response_time);

            % find row of first pulse which is pulse before 'Wait_for_Pulse' stimulus
            firstPulse_idx = find(strcmp(b.event_type, 'Pulse'));
            firstPulse_idx = firstPulse_idx(1,1);
            % get time of first pulse
            firstPulse_time = b.time(firstPulse_idx);
            disp(firstPulse_idx);


            %% Compare both logfiles and add relevant information into b
            % loop through b & ownlog_struct and check if b.code has same
            % stimulus name as ownlog.stimulus_picture --> write relevant information in b

            for jLog = 1:numel(b.code)
                %for iOwnlog = 1:numel(ownlog_struct.stimulus_picture)
                for iOwnlog = 1:numel(ownlog_struct.stimulus_audio)
                    if strcmp(b.code{jLog}, ownlog_struct.stimulus_audio{iOwnlog})
                        b.accuracy{jLog} = ownlog_struct.accuracy{iOwnlog};
                        b.response_time{jLog} = ownlog_struct.response_time{iOwnlog};
                        b.condition{jLog} = ownlog_struct.condition{iOwnlog};
                        b.correctness{jLog} = ownlog_struct.correctness{iOwnlog};
                        b.category{jLog} = ownlog_struct.category{iOwnlog};
                    end
                end
            end

            % transpose the new cell arrays
            b.response_time = b.response_time';
            b.accuracy = b.accuracy';
            b.condition = b.condition';
            b.correctness = b.correctness';
            b.category = b.category';

            %% Find indices, get event times and calculate stimulus onsets for different conditions               
            b.trial2 = compose('%d', b.trial);

            % find rows where Sounds are presented
            stimulus_idx = find(strcmp(b.event_type, 'Sound')); % this will be the index for ALL trials (correct & incorrect)

            % get reaction times
            %stimuli_idx = find(strcmp('WPM', b.condition) | strcmp('tone', b.condition) | strcmp('FPM', b.condition));
%             stimulus_rt = b.response_time(stimuli_idx) % RT for all stimuli
            rel_rt = b.response_time(stimulus_idx);
            stim_trials = b.trial(stimulus_idx);
            stim_trials = compose('%d', stim_trials);
            pics_idx = find(ismember(b.trial2, stim_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            pics_time = b.time(pics_idx);
            length = (pics_time - stimulus_times) ./10; % length in ms
            stimulus_rt = str2double(rel_rt) + length;

            % find ITI rows to indicate ITI length for erroneous trials
            ISI_idx = find(strcmp(b.code, 'ISI'));
            ISI_dur = b.duration(ISI_idx);

            % get times of events
            stimulus_times = b.time(stimulus_idx); % all trials
            ISI_times = b.time(ISI_idx);

            % calculate sound-onsets w.r.t. first pulse
            stimulus_onsets = (stimulus_times - firstPulse_time)/10000;% all trials
            ISI_onsets = (ISI_times - firstPulse_time)/10000; % ISIs

            % Only correct trials                
            corr_idx  = find(strcmp(b.accuracy, 'correct')); % find rows of correctly answered trials (find(contains) did not work on this format, neither did strmatch)
            corr_rt = b.response_time(corr_idx); % Rt only for correct trials
            corr_rt = (cellfun(@str2num, corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            corr_trials = b.trial(corr_idx);
            corr_trials = compose('%d', corr_trials);
            corr_idx2 = find(ismember(b.trial2, corr_trials) & strcmp('Sound', b.event_type));
            corr_times = b.time(corr_idx2); % only correct trials
            corr_onsets = (corr_times - firstPulse_time)/10000;
            corr_dur = repmat(3.5, numel(corr_onsets), 1);

            % extract conditions and respective onset times and create different
            % columns(separate for all vs only correct trials so it can be analyzed
            % either way)
            % put all erroneous trials in error regressor
            % due to different information I need to find index for conditions first.
            % This index is on picture, however, we want the sound. I thus search for
            % sound with same trial number in b.trial2 and extract this index. Then I
            % can calculate the right onset time.
            
            % all trials
            WPM_idx = find(strcmp('WPM', b.condition)); % word-picture matching condition
            WPM_rt = b.response_time(WPM_idx);
            %WPM_rt = (cellfun(@str2num, WPM_rt))/1000; % convert cell array of str to array of doubles for later calculations
            WPM_trials = b.trial(WPM_idx); 
            WPM_trials = compose('%d', WPM_trials);
            WPM_idx = find(ismember(b.trial2, WPM_trials) & strcmp('Sound', b.event_type));
            WPM_times = b.time(WPM_idx);
            WPM_onsets = (WPM_times - firstPulse_time)/10000;

            tone_idx = find(strcmp('tone', b.condition)); % control condition
            tone_rt = b.response_time(tone_idx);
            tone_rt = (cellfun(@str2num, tone_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_trials = b.trial(tone_idx); 
            tone_trials = compose('%d', tone_trials);
            tone_idx = find(ismember(b.trial2, tone_trials) & strcmp('Sound', b.event_type));
            tone_times = b.time(tone_idx);
            tone_onsets = (tone_times - firstPulse_time)/10000;

            FPM_idx = find(strcmp('FPM', b.condition)); % feature-picture matching condition
            FPM_rt = b.response_time(FPM_idx);
            FPM_rt = (cellfun(@str2num, FPM_rt))/1000; % convert cell array of str to array of doubles for later calculations
            FPM_trials = b.trial(FPM_idx); 
            FPM_trials = compose('%d', FPM_trials);
            FPM_idx = find(ismember(b.trial2, FPM_trials) & strcmp('Sound', b.event_type));
            FPM_times = b.time(FPM_idx);
            FPM_onsets = (FPM_times - firstPulse_time)/10000;               


            % only correct
            WPM_corr_idx = find(strcmp('WPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            WPM_corr_rt = b.response_time(WPM_corr_idx);
            WPM_corr_rt = (cellfun(@str2num, WPM_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            WPM_corr_trials = b.trial(WPM_corr_idx); 
            WPM_corr_trials2 = compose('%d', WPM_corr_trials);
            WPM_corr_idx = find(ismember(b.trial2, WPM_corr_trials2) & strcmp('Sound', b.event_type));
            WPM_corr_times = b.time(WPM_corr_idx);
            WPM_corr_onsets = (WPM_corr_times - firstPulse_time)/10000;
            WPM_corr_dur = repmat(3.5, numel(WPM_corr_onsets), 1);

            tone_corr_idx = find(strcmp('tone', b.condition) & strcmp('correct', b.accuracy)); % control condition – correct trials
            tone_corr_rt = b.response_time(tone_corr_idx);
            tone_corr_rt = (cellfun(@str2num, tone_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_corr_trials = b.trial(tone_corr_idx); 
            tone_corr_trials = compose('%d', tone_corr_trials);
            tone_corr_idx = find(ismember(b.trial2, tone_corr_trials) & strcmp('Sound', b.event_type));
            tone_corr_times = b.time(tone_corr_idx);
            tone_corr_onsets = (tone_corr_times - firstPulse_time)/10000;
            tone_corr_dur = repmat(3.5, numel(tone_corr_onsets), 1);

            FPM_corr_idx = find(strcmp('FPM', b.condition) & strcmp('correct', b.accuracy)); % feature-picture matching condition – correct trials
            FPM_corr_rt = b.response_time(FPM_corr_idx);
            FPM_corr_rt = (cellfun(@str2num, FPM_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            FPM_corr_trials = b.trial(FPM_corr_idx); 
            FPM_corr_trials = compose('%d', FPM_corr_trials);
            FPM_corr_idx = find(ismember(b.trial2, FPM_corr_trials) & strcmp('Sound', b.event_type));
            FPM_corr_times = b.time(FPM_corr_idx);
            FPM_corr_onsets = (FPM_corr_times - firstPulse_time)/10000;
            FPM_corr_dur = repmat(3.5, numel(FPM_corr_onsets), 1);


            congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy)); % congruent items – correct trials
            congruent_corr_rt = b.response_time(congruent_corr_idx);
            congruent_corr_rt = (cellfun(@str2num, congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            congruent_corr_times = b.time(congruent_corr_idx);
            congruent_corr_onsets = (congruent_corr_times - firstPulse_time)/10000;
            congruent_corr_dur = repmat(3.5, numel(congruent_corr_onsets), 1);
            
            incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy)); % incongruent items – correct trials
            incongruent_corr_rt = b.response_time(incongruent_corr_idx);
            incongruent_corr_rt = (cellfun(@str2num, incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            incongruent_corr_times = b.time(incongruent_corr_idx);
            incongruent_corr_onsets = (incongruent_corr_times - firstPulse_time)/10000;
            incongruent_corr_dur = repmat(3.5, numel(incongruent_corr_onsets), 1);
            
            LANG_congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy) & (strcmp('FPM', b.condition) | strcmp('WPM', b.condition))); % language tasks, congruent items – correct trials
            LANG_congruent_corr_rt = b.response_time(LANG_congruent_corr_idx);
            LANG_congruent_corr_rt = (cellfun(@str2num, LANG_congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            LANG_congruent_corr_times = b.time(LANG_congruent_corr_idx);
            LANG_congruent_corr_onsets = (LANG_congruent_corr_times - firstPulse_time)/10000;
            LANG_congruent_corr_dur = repmat(3.5, numel(LANG_congruent_corr_onsets), 1);
            
            LANG_incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy) & (strcmp('FPM', b.condition) | strcmp('WPM', b.condition))); % language tasks, incongruent items – correct trials
            LANG_incongruent_corr_rt = b.response_time(LANG_incongruent_corr_idx);
            LANG_incongruent_corr_rt = (cellfun(@str2num, LANG_incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            LANG_incongruent_corr_times = b.time(LANG_incongruent_corr_idx);
            LANG_incongruent_corr_onsets = (LANG_incongruent_corr_times - firstPulse_time)/10000;
            LANG_incongruent_corr_dur = repmat(3.5, numel(LANG_incongruent_corr_onsets), 1);
            
            tone_congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy) & strcmp('tone', b.condition)); % control task, congruent items – correct trials
            tone_congruent_corr_rt = b.response_time(tone_congruent_corr_idx);
            tone_congruent_corr_rt = (cellfun(@str2num, tone_congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_congruent_corr_times = b.time(tone_congruent_corr_idx);
            tone_congruent_corr_onsets = (tone_congruent_corr_times - firstPulse_time)/10000;
            tone_congruent_corr_dur = repmat(3.5, numel(tone_congruent_corr_onsets), 1);
            
            tone_incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy) & strcmp('tone', b.condition)); % control task, incongruent items – correct trials
            tone_incongruent_corr_rt = b.response_time(tone_incongruent_corr_idx);
            tone_incongruent_corr_rt = (cellfun(@str2num, tone_incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_incongruent_corr_times = b.time(tone_incongruent_corr_idx);
            tone_incongruent_corr_onsets = (tone_incongruent_corr_times - firstPulse_time)/10000;
            tone_incongruent_corr_dur = repmat(3.5, numel(tone_incongruent_corr_onsets), 1);
            
            errors_idx = find(strcmp('incorrect', b.accuracy) | strcmp('NaN', b.accuracy)); % all trials that were answered incorrectly – includes NaNs
            errors_rt = b.response_time(errors_idx);
            errors_rt = (cellfun(@str2num, errors_rt))/1000; % convert cell array of str to array of doubles for later calculations
            errors_trials = b.trial(errors_idx); 
            errors_trials = compose('%d', errors_trials);
            errors_idx = find(ismember(b.trial2, errors_trials) & strcmp('Sound', b.event_type));
            errors_times = b.time(errors_idx);
            errors_onsets = (errors_times - firstPulse_time)/10000;
            errors_dur = repmat(3.5, numel(errors_onsets), 1);
            
            %% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
            if ~isempty(errors_onsets)
                onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, congruent_corr_onsets, incongruent_corr_onsets, LANG_congruent_corr_onsets, ...
                    LANG_incongruent_corr_onsets, tone_congruent_corr_onsets, tone_incongruent_corr_onsets, errors_onsets};
                names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'congruent_corr', 'incongruent_corr', 'LANG_congruent_corr', 'LANG_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr', 'Errors'};
                durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, congruent_corr_rt, incongruent_corr_rt, LANG_congruent_corr_rt, LANG_incongruent_corr_rt, ...
                    tone_congruent_corr_rt, tone_incongruent_corr_rt, errors_rt};
            else
                onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, congruent_corr_onsets, incongruent_corr_onsets, LANG_congruent_corr_onsets, ...
                    LANG_incongruent_corr_onsets, tone_congruent_corr_onsets, tone_incongruent_corr_onsets};
                names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'congruent_corr', 'incongruent_corr', 'LANG_congruent_corr', 'LANG_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr'};
                durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, congruent_corr_rt, incongruent_corr_rt, LANG_congruent_corr_rt, LANG_incongruent_corr_rt, ...
                    tone_congruent_corr_rt, tone_incongruent_corr_rt};
            end;

            %% Save names,onsets,durations in mat file and the parametric modulation of RT as pmod.mat     
            save([logfolder 'sub-patient' sub{i} '_session-1_task-semanticmatching_run-' run{x} '_1st_level_event_related' '.mat'],'names', 'durations', 'onsets');

        end
     elseif s == 2 | s == 3
        if strcmp('control', group)
            %% Display which task, participant and which run is currently processed
            X = ['This is Semantic matching. This is control participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);

            % Define the folders where the logfiles can be found
            logfolder = [data_dir 'sub-control' sub{i} '/ses-' num2str(s) '/logfiles/'];    
                
            % Load b struct, in which the information about the onset times is stored
            matFile = dir([logfolder 'sub-control' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat']);
            mat = [matFile.folder '/' matFile.name];
            load(mat);

            % load the txt file with reaction times and accuracy and convert to struct
            txtfile = dir([logfolder 'MDN_APH_control-' strip(sub{i},'left','0') '_session' num2str(s) '_run' run{x} '*.txt']);
            txt = [txtfile.folder '/' txtfile.name];
            fileID = fopen(txt, 'r');
            ownlog = textscan(fileID, '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %d', 'HeaderLines', 9);
            names = {'block_number', 'stimulus_audio', 'time_audio','length_audio','stimulus_picture','time_picture','response_time',...
                'relative_response_time','button_response','accuracy','condition','correctness','category','ISI','time_ISI'};
            ownlog_struct = cell2struct(ownlog, names,2);
            % ownlog_struct.response_time = ownlog_struct.response_time * 10;
            % ownlog_struct.response_time = num2cell(ownlog_struct.response_time);

            % find row of first pulse which is pulse before 'Wait_for_Pulse' stimulus
            firstPulse_idx = find(strcmp(b.event_type, 'Pulse'));
            firstPulse_idx = firstPulse_idx(1,1);
            % get time of first pulse
            firstPulse_time = b.time(firstPulse_idx);
            disp(firstPulse_idx);


            %% Compare both logfiles and add relevant information into b
            % loop through b & ownlog_struct and check if b.code has same
            % stimulus name as ownlog.stimulus_picture --> write relevant information in b

            for jLog = 1:numel(b.code)
                %for iOwnlog = 1:numel(ownlog_struct.stimulus_picture)
                for iOwnlog = 1:numel(ownlog_struct.stimulus_audio)
                    if strcmp(b.code{jLog}, ownlog_struct.stimulus_audio{iOwnlog})
                        b.accuracy{jLog} = ownlog_struct.accuracy{iOwnlog};
                        b.response_time{jLog} = ownlog_struct.response_time{iOwnlog};
                        b.condition{jLog} = ownlog_struct.condition{iOwnlog};
                        b.correctness{jLog} = ownlog_struct.correctness{iOwnlog};
                        b.category{jLog} = ownlog_struct.category{iOwnlog};
                    end
                end
            end

            % transpose the new cell arrays
            b.response_time = b.response_time';
            b.accuracy = b.accuracy';
            b.condition = b.condition';
            b.correctness = b.correctness';
            b.category = b.category';

            %% Find indices, get event times and calculate stimulus onsets for different conditions               
            b.trial2 = compose('%d', b.trial);

            % find rows where Sounds are presented
            stimulus_idx = find(strcmp(b.event_type, 'Sound')); % this will be the index for ALL trials (correct & incorrect)

            % get reaction times
            %stimuli_idx = find(strcmp('WPM', b.condition) | strcmp('tone', b.condition) | strcmp('FPM', b.condition));
%             stimulus_rt = b.response_time(stimuli_idx) % RT for all stimuli
            rel_rt = b.response_time(stimulus_idx);
            stim_trials = b.trial(stimulus_idx);
            stim_trials = compose('%d', stim_trials);
            pics_idx = find(ismember(b.trial2, stim_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            pics_time = b.time(pics_idx);
            length = (pics_time - stimulus_times) ./10; % length in ms
            stimulus_rt = str2double(rel_rt) + length;

            % find ITI rows to indicate ITI length for erroneous trials
            ISI_idx = find(strcmp(b.code, 'ISI'));
            ISI_dur = b.duration(ISI_idx);

            % get times of events
            stimulus_times = b.time(stimulus_idx); % all trials
            ISI_times = b.time(ISI_idx);

            % calculate sound-onsets w.r.t. first pulse
            stimulus_onsets = (stimulus_times - firstPulse_time)/10000;% all trials
            ISI_onsets = (ISI_times - firstPulse_time)/10000; % ISIs

            % Only correct trials                
            corr_idx  = find(strcmp(b.accuracy, 'correct')); % find rows of correctly answered trials (find(contains) did not work on this format, neither did strmatch)
            corr_rt = b.response_time(corr_idx); % Rt only for correct trials
            corr_rt = (cellfun(@str2num, corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            corr_trials = b.trial(corr_idx);
            corr_trials = compose('%d', corr_trials);
            corr_idx2 = find(ismember(b.trial2, corr_trials) & strcmp('Sound', b.event_type));
            corr_times = b.time(corr_idx2); % only correct trials
            corr_onsets = (corr_times - firstPulse_time)/10000;
            corr_dur = repmat(3.5, numel(corr_onsets), 1);

            % extract conditions and respective onset times and create different
            % columns(separate for all vs only correct trials so it can be analyzed
            % either way)
            % put all erroneous trials in error regressor
            % due to different information I need to find index for conditions first.
            % This index is on picture, however, we want the sound. I thus search for
            % sound with same trial number in b.trial2 and extract this index. Then I
            % can calculate the right onset time.

            % all trials
            WPM_idx = find(strcmp('WPM', b.condition)); % word-picture matching condition
            WPM_rt = b.response_time(WPM_idx);
            %WPM_rt = (cellfun(@str2num, WPM_rt))/1000; % convert cell array of str to array of doubles for later calculations
            WPM_trials = b.trial(WPM_idx); 
            WPM_trials = compose('%d', WPM_trials);
            WPM_idx = find(ismember(b.trial2, WPM_trials) & strcmp('Sound', b.event_type));
            WPM_times = b.time(WPM_idx);
            WPM_onsets = (WPM_times - firstPulse_time)/10000;

            tone_idx = find(strcmp('tone', b.condition)); % control condition
            tone_rt = b.response_time(tone_idx);
            tone_rt = (cellfun(@str2num, tone_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_trials = b.trial(tone_idx); 
            tone_trials = compose('%d', tone_trials);
            tone_idx = find(ismember(b.trial2, tone_trials) & strcmp('Sound', b.event_type));
            tone_times = b.time(tone_idx);
            tone_onsets = (tone_times - firstPulse_time)/10000;

            FPM_idx = find(strcmp('FPM', b.condition)); % feature-picture matching condition
            FPM_rt = b.response_time(FPM_idx);
            FPM_rt = (cellfun(@str2num, FPM_rt))/1000; % convert cell array of str to array of doubles for later calculations
            FPM_trials = b.trial(FPM_idx); 
            FPM_trials = compose('%d', FPM_trials);
            FPM_idx = find(ismember(b.trial2, FPM_trials) & strcmp('Sound', b.event_type));
            FPM_times = b.time(FPM_idx);
            FPM_onsets = (FPM_times - firstPulse_time)/10000;               

            % only correct
            WPM_corr_idx = find(strcmp('WPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            WPM_corr_rt = b.response_time(WPM_corr_idx);
            WPM_corr_rt = (cellfun(@str2num, WPM_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            WPM_corr_trials = b.trial(WPM_corr_idx); 
            WPM_corr_trials2 = compose('%d', WPM_corr_trials);
            WPM_corr_idx = find(ismember(b.trial2, WPM_corr_trials2) & strcmp('Sound', b.event_type));
            WPM_corr_times = b.time(WPM_corr_idx);
            WPM_corr_onsets = (WPM_corr_times - firstPulse_time)/10000;
            WPM_corr_dur = repmat(3.5, numel(WPM_corr_onsets), 1);

            tone_corr_idx = find(strcmp('tone', b.condition) & strcmp('correct', b.accuracy)); % control condition – correct trials
            tone_corr_rt = b.response_time(tone_corr_idx);
            tone_corr_rt = (cellfun(@str2num, tone_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_corr_trials = b.trial(tone_corr_idx); 
            tone_corr_trials = compose('%d', tone_corr_trials);
            tone_corr_idx = find(ismember(b.trial2, tone_corr_trials) & strcmp('Sound', b.event_type));
            tone_corr_times = b.time(tone_corr_idx);
            tone_corr_onsets = (tone_corr_times - firstPulse_time)/10000;
            tone_corr_dur = repmat(3.5, numel(tone_corr_onsets), 1);

            FPM_corr_idx = find(strcmp('FPM', b.condition) & strcmp('correct', b.accuracy)); % feature-picture matching condition – correct trials
            FPM_corr_rt = b.response_time(FPM_corr_idx);
            FPM_corr_rt = (cellfun(@str2num, FPM_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            FPM_corr_trials = b.trial(FPM_corr_idx); 
            FPM_corr_trials = compose('%d', FPM_corr_trials);
            FPM_corr_idx = find(ismember(b.trial2, FPM_corr_trials) & strcmp('Sound', b.event_type));
            FPM_corr_times = b.time(FPM_corr_idx);
            FPM_corr_onsets = (FPM_corr_times - firstPulse_time)/10000;
            FPM_corr_dur = repmat(3.5, numel(FPM_corr_onsets), 1);


            congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy)); % congruent items – correct trials
            congruent_corr_rt = b.response_time(congruent_corr_idx);
            congruent_corr_rt = (cellfun(@str2num, congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            congruent_corr_times = b.time(congruent_corr_idx);
            congruent_corr_onsets = (congruent_corr_times - firstPulse_time)/10000;
            congruent_corr_dur = repmat(3.5, numel(congruent_corr_onsets), 1);
            
            incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy)); % incongruent items – correct trials
            incongruent_corr_rt = b.response_time(incongruent_corr_idx);
            incongruent_corr_rt = (cellfun(@str2num, incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            incongruent_corr_times = b.time(incongruent_corr_idx);
            incongruent_corr_onsets = (incongruent_corr_times - firstPulse_time)/10000;
            incongruent_corr_dur = repmat(3.5, numel(incongruent_corr_onsets), 1);
            
            LANG_congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy) & (strcmp('FPM', b.condition) | strcmp('WPM', b.condition))); % language tasks, congruent items – correct trials
            LANG_congruent_corr_rt = b.response_time(LANG_congruent_corr_idx);
            LANG_congruent_corr_rt = (cellfun(@str2num, LANG_congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            LANG_congruent_corr_times = b.time(LANG_congruent_corr_idx);
            LANG_congruent_corr_onsets = (LANG_congruent_corr_times - firstPulse_time)/10000;
            LANG_congruent_corr_dur = repmat(3.5, numel(LANG_congruent_corr_onsets), 1);
            
            LANG_incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy) & (strcmp('FPM', b.condition) | strcmp('WPM', b.condition))); % language tasks, incongruent items – correct trials
            LANG_incongruent_corr_rt = b.response_time(LANG_incongruent_corr_idx);
            LANG_incongruent_corr_rt = (cellfun(@str2num, LANG_incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            LANG_incongruent_corr_times = b.time(LANG_incongruent_corr_idx);
            LANG_incongruent_corr_onsets = (LANG_incongruent_corr_times - firstPulse_time)/10000;
            LANG_incongruent_corr_dur = repmat(3.5, numel(LANG_incongruent_corr_onsets), 1);
            
            tone_congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy) & strcmp('tone', b.condition)); % control task, congruent items – correct trials
            tone_congruent_corr_rt = b.response_time(tone_congruent_corr_idx);
            tone_congruent_corr_rt = (cellfun(@str2num, tone_congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_congruent_corr_times = b.time(tone_congruent_corr_idx);
            tone_congruent_corr_onsets = (tone_congruent_corr_times - firstPulse_time)/10000;
            tone_congruent_corr_dur = repmat(3.5, numel(tone_congruent_corr_onsets), 1);
            
            tone_incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy) & strcmp('tone', b.condition)); % control task, incongruent items – correct trials
            tone_incongruent_corr_rt = b.response_time(tone_incongruent_corr_idx);
            tone_incongruent_corr_rt = (cellfun(@str2num, tone_incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_incongruent_corr_times = b.time(tone_incongruent_corr_idx);
            tone_incongruent_corr_onsets = (tone_incongruent_corr_times - firstPulse_time)/10000;
            tone_incongruent_corr_dur = repmat(3.5, numel(tone_incongruent_corr_onsets), 1);
            
            errors_idx = find(strcmp('incorrect', b.accuracy) | strcmp('NaN', b.accuracy)); % all trials that were answered incorrectly – includes NaNs
            errors_rt = b.response_time(errors_idx);
            errors_rt = (cellfun(@str2num, errors_rt))/1000; % convert cell array of str to array of doubles for later calculations
            errors_trials = b.trial(errors_idx); 
            errors_trials = compose('%d', errors_trials);
            errors_idx = find(ismember(b.trial2, errors_trials) & strcmp('Sound', b.event_type));
            errors_times = b.time(errors_idx);
            errors_onsets = (errors_times - firstPulse_time)/10000;
            errors_dur = repmat(3.5, numel(errors_onsets), 1);
            
            %% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
            if ~isempty(errors_onsets)
                onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, congruent_corr_onsets, incongruent_corr_onsets, LANG_congruent_corr_onsets, ...
                    LANG_incongruent_corr_onsets, tone_congruent_corr_onsets, tone_incongruent_corr_onsets, errors_onsets};
                names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'congruent_corr', 'incongruent_corr', 'LANG_congruent_corr', 'LANG_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr', 'Errors'};
                durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, congruent_corr_rt, incongruent_corr_rt, LANG_congruent_corr_rt, LANG_incongruent_corr_rt, ...
                    tone_congruent_corr_rt, tone_incongruent_corr_rt, errors_rt};
            else
                onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, congruent_corr_onsets, incongruent_corr_onsets, LANG_congruent_corr_onsets, ...
                    LANG_incongruent_corr_onsets, tone_congruent_corr_onsets, tone_incongruent_corr_onsets};
                names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'congruent_corr', 'incongruent_corr', 'LANG_congruent_corr', 'LANG_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr'};
                durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, congruent_corr_rt, incongruent_corr_rt, LANG_congruent_corr_rt, LANG_incongruent_corr_rt, ...
                    tone_congruent_corr_rt, tone_incongruent_corr_rt};
            end;
            %durations = {WPM_corr_dur, CPM_corr_dur, FPM_corr_dur, errors_dur};

            %% Save names,onsets,durations in mat file and the parametric modulation of RT as pmod.mat     
            save([logfolder 'sub-control' sub{i} '_session-' num2str(s) '_task-semanticmatching_run-' run{x} '_1st_level_event_related' '.mat'],'names', 'durations', 'onsets');

        elseif strcmp('patient', group)
            
            %% Display which participant and which run is currently processed
            X = ['This is Semantic matching. This is patient participant ' sub{i} ' and session ' num2str(s) ' and run ' run{x}];
                disp(X);
                
            % Define the folders where the logfiles can be found
            logfolder = [data_dir 'sub-patient' sub{i} '/ses-' num2str(s) '/logfiles/'];

            % Load b struct, in which the information about the onset times is stored
            matFile = dir([logfolder 'sub-patient' sub{i} '_task-semanticmatching_run-' run{x} '_log_b.mat']);
            mat = [matFile.folder '/' matFile.name];
            load(mat);

            % load the txt file with reaction times and accuracy and convert to struct
            txtfile = dir([logfolder 'MDN_APH_patient-' strip(sub{i},'left','0') '_session' num2str(s) '_run' run{x} '*.txt']);
            txt = [txtfile.folder '/' txtfile.name];
            fileID = fopen(txt, 'r');
            ownlog = textscan(fileID, '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %d', 'HeaderLines', 9);
            names = {'block_number', 'stimulus_audio', 'time_audio','length_audio','stimulus_picture','time_picture','response_time',...
                'relative_response_time','button_response','accuracy','condition','correctness','category','ISI','time_ISI'};
            ownlog_struct = cell2struct(ownlog, names,2);
            % ownlog_struct.response_time = ownlog_struct.response_time * 10;
            % ownlog_struct.response_time = num2cell(ownlog_struct.response_time);

            % find row of first pulse which is pulse before 'Wait_for_Pulse' stimulus
            firstPulse_idx = find(strcmp(b.event_type, 'Pulse'));
            firstPulse_idx = firstPulse_idx(1,1);
            % get time of first pulse
            firstPulse_time = b.time(firstPulse_idx);
            disp(firstPulse_idx);


            %% Compare both logfiles and add relevant information into b
            % loop through b & ownlog_struct and check if b.code has same
            % stimulus name as ownlog.stimulus_picture --> write relevant information in b

            for jLog = 1:numel(b.code)
                %for iOwnlog = 1:numel(ownlog_struct.stimulus_picture)
                for iOwnlog = 1:numel(ownlog_struct.stimulus_audio)
                    if strcmp(b.code{jLog}, ownlog_struct.stimulus_audio{iOwnlog})
                        b.accuracy{jLog} = ownlog_struct.accuracy{iOwnlog};
                        b.response_time{jLog} = ownlog_struct.response_time{iOwnlog};
                        b.condition{jLog} = ownlog_struct.condition{iOwnlog};
                        b.correctness{jLog} = ownlog_struct.correctness{iOwnlog};
                        b.category{jLog} = ownlog_struct.category{iOwnlog};
                    end
                end
            end

            % transpose the new cell arrays
            b.response_time = b.response_time';
            b.accuracy = b.accuracy';
            b.condition = b.condition';
            b.correctness = b.correctness';
            b.category = b.category';

            %% Find indices, get event times and calculate stimulus onsets for different conditions               
            b.trial2 = compose('%d', b.trial);

            % find rows where Sounds are presented
            stimulus_idx = find(strcmp(b.event_type, 'Sound')); % this will be the index for ALL trials (correct & incorrect)

            % get reaction times
            %stimuli_idx = find(strcmp('WPM', b.condition) | strcmp('tone', b.condition) | strcmp('FPM', b.condition));
%             stimulus_rt = b.response_time(stimuli_idx) % RT for all stimuli
            rel_rt = b.response_time(stimulus_idx);
            stim_trials = b.trial(stimulus_idx);
            stim_trials = compose('%d', stim_trials);
            pics_idx = find(ismember(b.trial2, stim_trials) & strcmp('Picture', b.event_type) & ~strcmp('fixation_green', b.code));
            pics_time = b.time(pics_idx);
            length = (pics_time - stimulus_times) ./10; % length in ms
            stimulus_rt = str2double(rel_rt) + length;

            % find ITI rows to indicate ITI length for erroneous trials
            ISI_idx = find(strcmp(b.code, 'ISI'));
            ISI_dur = b.duration(ISI_idx);

            % get times of events
            stimulus_times = b.time(stimulus_idx); % all trials
            ISI_times = b.time(ISI_idx);

            % calculate sound-onsets w.r.t. first pulse
            stimulus_onsets = (stimulus_times - firstPulse_time)/10000;% all trials
            ISI_onsets = (ISI_times - firstPulse_time)/10000; % ISIs

            % Only correct trials                
            corr_idx  = find(strcmp(b.accuracy, 'correct')); % find rows of correctly answered trials (find(contains) did not work on this format, neither did strmatch)
            corr_rt = b.response_time(corr_idx); % Rt only for correct trials
            corr_rt = (cellfun(@str2num, corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            corr_trials = b.trial(corr_idx);
            corr_trials = compose('%d', corr_trials);
            corr_idx2 = find(ismember(b.trial2, corr_trials) & strcmp('Sound', b.event_type));
            corr_times = b.time(corr_idx2); % only correct trials
            corr_onsets = (corr_times - firstPulse_time)/10000;
            corr_dur = repmat(3.5, numel(corr_onsets), 1);

            % extract conditions and respective onset times and create different
            % columns(separate for all vs only correct trials so it can be analyzed
            % either way)
            % put all erroneous trials in error regressor
            % due to different information I need to find index for conditions first.
            % This index is on picture, however, we want the sound. I thus search for
            % sound with same trial number in b.trial2 and extract this index. Then I
            % can calculate the right onset time.
            
            % all trials
            WPM_idx = find(strcmp('WPM', b.condition)); % word-picture matching condition
            WPM_rt = b.response_time(WPM_idx);
            %WPM_rt = (cellfun(@str2num, WPM_rt))/1000; % convert cell array of str to array of doubles for later calculations
            WPM_trials = b.trial(WPM_idx); 
            WPM_trials = compose('%d', WPM_trials);
            WPM_idx = find(ismember(b.trial2, WPM_trials) & strcmp('Sound', b.event_type));
            WPM_times = b.time(WPM_idx);
            WPM_onsets = (WPM_times - firstPulse_time)/10000;

            tone_idx = find(strcmp('tone', b.condition)); % control condition
            tone_rt = b.response_time(tone_idx);
            tone_rt = (cellfun(@str2num, tone_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_trials = b.trial(tone_idx); 
            tone_trials = compose('%d', tone_trials);
            tone_idx = find(ismember(b.trial2, tone_trials) & strcmp('Sound', b.event_type));
            tone_times = b.time(tone_idx);
            tone_onsets = (tone_times - firstPulse_time)/10000;

            FPM_idx = find(strcmp('FPM', b.condition)); % feature-picture matching condition
            FPM_rt = b.response_time(FPM_idx);
            FPM_rt = (cellfun(@str2num, FPM_rt))/1000; % convert cell array of str to array of doubles for later calculations
            FPM_trials = b.trial(FPM_idx); 
            FPM_trials = compose('%d', FPM_trials);
            FPM_idx = find(ismember(b.trial2, FPM_trials) & strcmp('Sound', b.event_type));
            FPM_times = b.time(FPM_idx);
            FPM_onsets = (FPM_times - firstPulse_time)/10000;               


            % only correct
            WPM_corr_idx = find(strcmp('WPM', b.condition) & strcmp('correct', b.accuracy)); % word-picture matching condition – correct trials
            WPM_corr_rt = b.response_time(WPM_corr_idx);
            WPM_corr_rt = (cellfun(@str2num, WPM_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            WPM_corr_trials = b.trial(WPM_corr_idx); 
            WPM_corr_trials2 = compose('%d', WPM_corr_trials);
            WPM_corr_idx = find(ismember(b.trial2, WPM_corr_trials2) & strcmp('Sound', b.event_type));
            WPM_corr_times = b.time(WPM_corr_idx);
            WPM_corr_onsets = (WPM_corr_times - firstPulse_time)/10000;
            WPM_corr_dur = repmat(3.5, numel(WPM_corr_onsets), 1);

            tone_corr_idx = find(strcmp('tone', b.condition) & strcmp('correct', b.accuracy)); % control condition – correct trials
            tone_corr_rt = b.response_time(tone_corr_idx);
            tone_corr_rt = (cellfun(@str2num, tone_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_corr_trials = b.trial(tone_corr_idx); 
            tone_corr_trials = compose('%d', tone_corr_trials);
            tone_corr_idx = find(ismember(b.trial2, tone_corr_trials) & strcmp('Sound', b.event_type));
            tone_corr_times = b.time(tone_corr_idx);
            tone_corr_onsets = (tone_corr_times - firstPulse_time)/10000;
            tone_corr_dur = repmat(3.5, numel(tone_corr_onsets), 1);

            FPM_corr_idx = find(strcmp('FPM', b.condition) & strcmp('correct', b.accuracy)); % feature-picture matching condition – correct trials
            FPM_corr_rt = b.response_time(FPM_corr_idx);
            FPM_corr_rt = (cellfun(@str2num, FPM_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            FPM_corr_trials = b.trial(FPM_corr_idx); 
            FPM_corr_trials = compose('%d', FPM_corr_trials);
            FPM_corr_idx = find(ismember(b.trial2, FPM_corr_trials) & strcmp('Sound', b.event_type));
            FPM_corr_times = b.time(FPM_corr_idx);
            FPM_corr_onsets = (FPM_corr_times - firstPulse_time)/10000;
            FPM_corr_dur = repmat(3.5, numel(FPM_corr_onsets), 1);


            congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy)); % congruent items – correct trials
            congruent_corr_rt = b.response_time(congruent_corr_idx);
            congruent_corr_rt = (cellfun(@str2num, congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            congruent_corr_times = b.time(congruent_corr_idx);
            congruent_corr_onsets = (congruent_corr_times - firstPulse_time)/10000;
            congruent_corr_dur = repmat(3.5, numel(congruent_corr_onsets), 1);
            
            incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy)); % incongruent items – correct trials
            incongruent_corr_rt = b.response_time(incongruent_corr_idx);
            incongruent_corr_rt = (cellfun(@str2num, incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            incongruent_corr_times = b.time(incongruent_corr_idx);
            incongruent_corr_onsets = (incongruent_corr_times - firstPulse_time)/10000;
            incongruent_corr_dur = repmat(3.5, numel(incongruent_corr_onsets), 1);
            
            LANG_congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy) & (strcmp('FPM', b.condition) | strcmp('WPM', b.condition))); % language tasks, congruent items – correct trials
            LANG_congruent_corr_rt = b.response_time(LANG_congruent_corr_idx);
            LANG_congruent_corr_rt = (cellfun(@str2num, LANG_congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            LANG_congruent_corr_times = b.time(LANG_congruent_corr_idx);
            LANG_congruent_corr_onsets = (LANG_congruent_corr_times - firstPulse_time)/10000;
            LANG_congruent_corr_dur = repmat(3.5, numel(LANG_congruent_corr_onsets), 1);
            
            LANG_incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy) & (strcmp('FPM', b.condition) | strcmp('WPM', b.condition))); % language tasks, incongruent items – correct trials
            LANG_incongruent_corr_rt = b.response_time(LANG_incongruent_corr_idx);
            LANG_incongruent_corr_rt = (cellfun(@str2num, LANG_incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            LANG_incongruent_corr_times = b.time(LANG_incongruent_corr_idx);
            LANG_incongruent_corr_onsets = (LANG_incongruent_corr_times - firstPulse_time)/10000;
            LANG_incongruent_corr_dur = repmat(3.5, numel(LANG_incongruent_corr_onsets), 1);
            
            tone_congruent_corr_idx = find(strcmp('correct', b.correctness) & strcmp('correct', b.accuracy) & strcmp('tone', b.condition)); % control task, congruent items – correct trials
            tone_congruent_corr_rt = b.response_time(tone_congruent_corr_idx);
            tone_congruent_corr_rt = (cellfun(@str2num, tone_congruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_congruent_corr_times = b.time(tone_congruent_corr_idx);
            tone_congruent_corr_onsets = (tone_congruent_corr_times - firstPulse_time)/10000;
            tone_congruent_corr_dur = repmat(3.5, numel(tone_congruent_corr_onsets), 1);
            
            tone_incongruent_corr_idx = find(strcmp('incorrect', b.correctness) & strcmp('correct', b.accuracy) & strcmp('tone', b.condition)); % control task, incongruent items – correct trials
            tone_incongruent_corr_rt = b.response_time(tone_incongruent_corr_idx);
            tone_incongruent_corr_rt = (cellfun(@str2num, tone_incongruent_corr_rt))/1000; % convert cell array of str to array of doubles for later calculations
            tone_incongruent_corr_times = b.time(tone_incongruent_corr_idx);
            tone_incongruent_corr_onsets = (tone_incongruent_corr_times - firstPulse_time)/10000;
            tone_incongruent_corr_dur = repmat(3.5, numel(tone_incongruent_corr_onsets), 1);
            
            errors_idx = find(strcmp('incorrect', b.accuracy) | strcmp('NaN', b.accuracy)); % all trials that were answered incorrectly – includes NaNs
            errors_rt = b.response_time(errors_idx);
            errors_rt = (cellfun(@str2num, errors_rt))/1000; % convert cell array of str to array of doubles for later calculations
            errors_trials = b.trial(errors_idx); 
            errors_trials = compose('%d', errors_trials);
            errors_idx = find(ismember(b.trial2, errors_trials) & strcmp('Sound', b.event_type));
            errors_times = b.time(errors_idx);
            errors_onsets = (errors_times - firstPulse_time)/10000;
            errors_dur = repmat(3.5, numel(errors_onsets), 1);
            
            %% Write onsets, names and durations of conditions in vector for SPM (Have to be named 'onsets', 'names', 'durations' for SPM)
            if ~isempty(errors_onsets)
                onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, congruent_corr_onsets, incongruent_corr_onsets, LANG_congruent_corr_onsets, ...
                    LANG_incongruent_corr_onsets, tone_congruent_corr_onsets, tone_incongruent_corr_onsets, errors_onsets};
                names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'congruent_corr', 'incongruent_corr', 'LANG_congruent_corr', 'LANG_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr', 'Errors'};
                durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, congruent_corr_rt, incongruent_corr_rt, LANG_congruent_corr_rt, LANG_incongruent_corr_rt, ...
                    tone_congruent_corr_rt, tone_incongruent_corr_rt, errors_rt};
            else
                onsets = {WPM_corr_onsets, FPM_corr_onsets, tone_corr_onsets, congruent_corr_onsets, incongruent_corr_onsets, LANG_congruent_corr_onsets, ...
                    LANG_incongruent_corr_onsets, tone_congruent_corr_onsets, tone_incongruent_corr_onsets};
                names = {'WPM_corr', 'FPM_corr', 'tone_corr', 'congruent_corr', 'incongruent_corr', 'LANG_congruent_corr', 'LANG_incongruent_corr', ...
                    'tone_congruent_corr', 'tone_incongruent_corr'};
                durations = {WPM_corr_rt, FPM_corr_rt, tone_corr_rt, congruent_corr_rt, incongruent_corr_rt, LANG_congruent_corr_rt, LANG_incongruent_corr_rt, ...
                    tone_congruent_corr_rt, tone_incongruent_corr_rt};
            end;
            %durations = {WPM_corr_dur, CPM_corr_dur, FPM_corr_dur, errors_dur};

            %% Save names,onsets,durations in mat file and the parametric modulation of RT as pmod.mat     
            save([logfolder 'sub-patient' sub{i} '_session-' num2str(s) '_task-semanticmatching_run-' run{x} '_1st_level_event_related' '.mat'],'names', 'durations', 'onsets');
            
        end
    end   
end
end
end               
