% This script makes the below-described mat-files for input into cPPI_master.m in cPPI toolbox: roi timeseries, (confound) task regressors, or confound timeseries, depending on what's not commented out

%  Path and name of .mat file containing the ROI time courses. The time 
%  courses should be stored as a cell structure containing N cells, where 
%  N = number of subjects. Each cell must contain an L x M matrix, where L 
%  is the total number of time points (concenated across sessions) and M is 
%  the number of ROIs. So, for example, if there are 5 sessions each with 
%  200 volumes, L = 1000. The cell structure should be called roi_ts. 
%  If not, simply uncomment the line underneath the load command and change 
%  the right hand side of the '=' to refer to the name you have assigned to 
%  the cell structure.
%load /path/to/roi/time/series/my_roi_timeseries.mat
%roi_ts = timecourse_cell ;  % rename input here if required

clear

%% Paths
% define main folder
subjects_folder = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects';
group_folder = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI';

sub = {'control001','control002','control003','control004','control005','control006','control007','control008', ...
            'control009','control010','control011','control012','control013','control014','control015','control016', ...
            'control017','control018','control019','control020','control021','control022','control023','control024', ...
            'control025','control026','control027','control028','control029','control030'};
      
sessions = {'2', '3'}; % define for which session you are creating cell structure

parcellation_atlas = dir('/data/p_02221/Masks_ROIs/Yeo_2011_resampled/*nii');

%% Create cells with one timeseries per network for cPPI
roi_ts={}; %create empty cell for timeseries of all subjects

for isess = 1:numel(sessions)
    for isub = 1:numel(sub) % loop over subjects
        all_networks = table();

        current_sub = sub{isub};
        if strcmp(current_sub, 'control006') & sessions{isess} == '2' % define subjects that only have one task run
            run = {'2'};
        else
            run = {'1', '2'};
        end

        % Display which participant and IC is currently processed
        X = ['This is participant ' sub{isub}];
        disp(X);
        
        allSes_confTCs = [];
        for irun = 1:numel(run) % loop over runs
            % load txt file with timeseries for ROIs
            roi_file = dir([subjects_folder filesep 'sub-' sub{isub} filesep 'ses-' sessions{isess} filesep 'cPPI' filesep 'parcellated' filesep ...
                    'wholeNetworks_Yeo7_NO_GSR' filesep 'sub-' sub{isub} '_ses-' sessions{isess} '_run-' run{irun} '_gm_dTS.txt']);
            roi_fn = [roi_file.folder filesep roi_file.name];
            roi_table = readtable(roi_fn, 'Delimiter', 'space', 'VariableNamingRule', 'preserve');
            % concatenate timeseries for both runs in one table
            allSes_confTCs = [allSes_confTCs; roi_table];
        end

        % add timeseries of one subject to cell structure
        roi_ts{isub} = allSes_confTCs;
    end
    
    ts_fn = [group_folder filesep 'ROI_timeseries_cPPI' filesep 'roi_timeseries_wholeNetworks_Yeo7_NO_GSR_ses-' sessions{isess} '.mat']; % save file in group folder
    save(ts_fn, 'roi_ts')
end

%% Create cell with confounds per subject for cPPI
conf_ts={}; %create empty cell for timeseries of all subjects

for isess = 1:numel(sessions)
    for isub = 1:numel(sub) % loop over subjects
        current_sub = sub{isub};
        if strcmp(current_sub, 'control006') & sessions{isess} == '2' % define subjects that only have one task run
            run = {'2'};
        else
            run = {'1', '2'};
        end
    
        % Display which participant and IC is currently processed
        X = ['This is participant ' sub{isub}];
        disp(X);
        
        allSes_confTCs = [];
        for irun = 1:numel(run) % loop over runs
            % load txt file with timeseries for ROIs
            conf_file = dir([subjects_folder filesep 'sub-' sub{isub} filesep 'ses-' sessions{isess} filesep 'cPPI' filesep...
                'sub-' sub{isub} '_ses-' sessions{isess} '_task-semanticmatching_run-' run{irun} '_conf_RMSD.csv']);
            conf_fn = [conf_file.folder filesep conf_file.name];
            conf_table = readtable(conf_fn);
    
            null = table(0, 'VariableNames', {'rmsd'});
            conf_table = [null; conf_table];
            % concatenate timeseries for both runs in one table
            allSes_confTCs = [allSes_confTCs; conf_table];
        end

        % add timeseries of one subject to cell structure
        conf_ts{isub} = allSes_confTCs;
    end
    confound_fn = [group_folder filesep 'ROI_timeseries_cPPI' filesep 'RMSD_confound_timeseries_ses-' sessions{isess} '.mat']; % save file in group folder
    save(confound_fn, 'conf_ts')
end