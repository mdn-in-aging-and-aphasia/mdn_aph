% ==========================================================================
%  OVERVIEW
% ==========================================================================
% 
%  This is a master script for generating cPPI correlation matrices from
%  input ROI time series. A number of inputs/parameters need to be defined 
%  at the beginning. The script then calls a series of functions to generate 
%  the cPPI functional connectivity matrices. See also cPPI_manual.pdf.
%  
%  Note that wherever directory paths are specified, they 
%  should always end with a '/'. 
% 
%  Note that this script calls on functions in SPM (SPM5 of later) and the 
%  Matlab Statistics toolbox, so these must be installed for the script to work.
% 
%  When using this toolbox, please cite:
% 
%  Fornito, A., Harrison, B., Zalesky, A., Simons, J. S. (2012). Competitive 
%  and cooperative dynamics of large-scale brain functional networks 
%  supporting recollection. Proceedings of the National Academy of Sciences, 
%  USA, 109(31): 12788-12793.
%  
%  Alex Fornito, Monash University, August 2014.
% 
%  alex.fornito@monash.edu
% __________________________________________________________________________
%  
%  Fixes: 
%  August 2012 - adjusted to allow session lengths to vary across subjects.
%
%  August 2014 - included new option to deconvolve time courses after they
%  have been corrected for confound signals (see 'correct' option below).
% __________________________________________________________________________
% 
%      This program is free software: you can redistribute it and/or modify
%      it under the terms of the GNU General Public License as published by
%      the Free Software Foundation, either version 3 of the License, or
%      (at your option) any later version.
%  
%      This program is distributed in the hope that it will be useful,
%      but WITHOUT ANY WARRANTY; without even the implied warranty of
%      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%      GNU General Public License for more details.
%  
%      You should have received a copy of the GNU General Public License
%      along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%      This software comes with absolutely no warranty. Use at own risk!
% 
% ==========================================================================
% 
% 
% ==========================================================================
%  INPUTS 
% ==========================================================================
% 
clear all
% 
%--------------------------------------------------------------------------
%
% % Which session is being processed
% @Katie: This needs to be adapted manually for session 2 and 3 since the
% time series are in separate mat files
session = '2'; % '3'
%
% --------------------------------------------------------------------------
% 
%  Path to directory where SPM is installed (should be SPM5 or later)
addpath /data/p_02221/spm12/

% --------------------------------------------------------------------------
% 
%  Path where the cPPI toolbox is 
%  CAVE: use this version of the toolbox since some scripts had to be
%  adapted for serveral runs per participant and uneven number of runs
%  across participants
addpath /data/p_02221/Scripts/fmriprep-stats/matlab/cPPI/cPPI

%--------------------------------------------------------------------------

% Base directory where all subject's subdirectories are. e.g.,
% basedir/sub1/, basedir/sub2/, etc.
basedir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/';

%--------------------------------------------------------------------------

% Keyword common to all subjects' directory names that can be used to identify them in
% basedir. If there is no common keyword, and there are only subject's data 
% directories in basedir (and nothing else), use: 
% dir([basedir,'*']); dir(1:2)=[]; to generate list of subject IDs.  

keyword='sub-control';
subs = dir([basedir,'*',keyword, '*']);

% participants with only one session
subs_one_sess = {'sub-control006'};

% Parcellation atlas
parcellation_atlas = 'wholeNetworks_Yeo7_confRMSD_NO_GSR';  

%--------------------------------------------------------------------------

% Name of sub-directory in which the SPM.mat results file is located. This 
% directory is located inside each subject's directory; i.e., the location
% of the SPM file will be given by basedir/sub1/resdir/SPM.mat
resdir = ['ses-' session '/1st_level_semanticmatching/'];

%--------------------------------------------------------------------------

%  Path and name of .mat file containing the ROI time courses. The time 
%  courses should be stored as a cell structure containing N cells, where 
%  N = number of subjects. Each cell must contain an L x M matrix, where L 
%  is the total number of time points (concenated across sessions) and M is 
%  the number of ROIs. So, for example, if there are 5 sessions each with 
%  200 volumes, L = 1000. The cell structure should be called roi_ts. 
%  If not, simply uncomment the line underneath the load command and change 
%  the right hand side of the '=' to refer to the name you have assigned to 
%  the cell structure.

ts = ['/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI/ROI_timeseries_cPPI/roi_timeseries_wholeNetworks_Yeo7_NO_GSR_ses-' session '.mat'];
load(ts)

%--------------------------------------------------------------------------

% Choose the specific ROIs to process, as defined by their column index in
% roi_ts{i}
rois=1:size(roi_ts{1},2); % processes all rois

% --------------------------------------------------------------------------
% 
%  A cell structure containing various confound time series (e.g.,
%  motion parameters). ROI time courses will be corrected for these
%  prior to deconvolution. Should be organized in the same way as roi_ts. 
%  That is,a unique cell for each subject containing an L x K matrix, where 
%  K = number of confounds. If no confounds are specified, use conf={};
motion_ts = ['/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI/ROI_timeseries_cPPI/RMSD_confound_timeseries_ses-' session '.mat'];
load(motion_ts)
conf=conf_ts;

%--------------------------------------------------------------------------

%  A cell structure containing additional time series to be partialled in 
%  computing the PPI partial correlations (e.g., additional task regressors).
%  Should be organized in the same way as roi_ts. If
%  none are specified, use part={}. The cell structure should be called part. 
%  It is a good idea to include all other task regressors in the design matrix
%  here. If you have a different variable name, simply uncomment the line 
%  underneath the load command and change the right hand side of the '=' to 
%  refer to the name you have assigned.
% load /path/to/confound/time/series/my_confound_timecourses.mat
part = {};  % rename input here if required

% ==========================================================================
%  OUTPUTS 
% ==========================================================================

%  Directory where the PPI mat files will be output to. The script will 
%  automatically create a directory for each subject in out_dir. The PPI mat
%  file for each region and session will be output into these 
%  subject-specific directories
out_dir = '/data/p_02221/Scripts4Katie/cPPI_output/subjects/';

%--------------------------------------------------------------------------

% Filename suffix of PPI output. The script will automatically add  
% the session number at the end
out_suff = strcat('PPI_SemMatching_', parcellation_atlas, 'WPM_FPM_ses-', session);

% ==========================================================================
%  PARAMETERS
% ==========================================================================
% 
%  Uu is a contrast matrix required by the cPPI_ppi_PEB.m function. It is a 
%  Kx3 matrix, where K=number of regressors involved in the contrast (i.e.. 
%  not in the design matrix; just the contrast used to generate the PPI term).
% 
%  The first column indexes the number of the regressor as stored in 
%  SPM.Sess.U (i.e., is it listed first, second, third, etc.). 
% 
%  The second column identifies where the name of the regressors is and 
%  should usually be 1. 
%  
%  The third column is the contrast weight. So for 
%  example, if 
%  Uu=[2,1,1;3,1,-1]; 
%  we are running the contrast [2nd regressor > 3rd regressor].
% 
% To take another example:
% Uu=[5, 1, 1;
%     6, 1, 1;
%     7, 1, -1;
%     8, 1, -1];  %contrasts mean of regressors 5+6 > mean of 7+8.
Uu = [2, 1, -1; % FPM > WPM (2; 1) / FPM > tone (2; 3)
      1, 1, 1];
  
% --------------------------------------------------------------------------
%
% This option allows you to deconvolve either corrected or uncorrected
% regional time courses prior to computing the PPI term. The correction is 
% with respect to the signals stored in conf (via linear regression). In 
% the standard SPM implementation, the corrected signal is stored in PPI.Y 
% but the uncorrected signal is used for deconvolution. Confound signals
% are then entered into the design matrix when estimating the first-level
% model.
%
% Since there is no first-level model estimation in cPPI
% analysis, the 'correct' option allows you to correct regional signals prior to 
% deconvolution. Signals will be corrected for time
% courses specified in conf, as well as SPM-computed session-specific
% constant terms.
%
% An alternative would be to correct regional time courses for nuisance
% signals using some other software, and then feed these corrected signals
% into roi_ts.
% 
% Set to 1 to used corrected signal; 0 to deconvolve uncorrected signal.
correct = 1;

% --------------------------------------------------------------------------
% 
%  The types of ppi correlation matrices to compute. 
%   - 'orig' computes the partial correlation coefficient between
%     roi_i and roi_j based on the original regression
%     formulation. That is, ppi_i and ppi_j are correlated while
%     partialling for brain_i, brain_j, psy and other confounds, where 
%     ppi_i and ppi_j represent the PPI terms for roi_i and roi_j, 
%     respectively; brain_i and brain_j represent the original BOLD time
%     courses for roi_i and roi_j, respectively; and psy represents the task
%     regressor of interest.
%                  
%   - 'p_brain' computes the correlation as above, but
%      additionally partials the main effects of neural activity
%      of other ROIs indexed with pinds (defined below).
cor_types = {'orig', 'p_brain'};

%--------------------------------------------------------------------------
 
%  The indices of other ROIs to partial out if 'p_brain' is selected as
%  a cor_type. Allows only specific ROIs to be partialled out whe computing
%  cPPI correlations between rois i and j. Set pinds = 1:length(rois) to
%  partial all other M-2 ROIs.
pinds = 1:length(rois);

%--------------------------------------------------------------------------

%  Output directory for correlation matrices. The matrices for the entire
%  sample will be saved in a single .mat file. Separate .mat files will be
%  generate for each combination of ts_types and cor_types.
cordir = '/data/p_02221/Scripts4Katie/cPPI_output/group/';

%--------------------------------------------------------------------------

%  The prefix for the output mat for correlation matrix data. This file will
%  contain the cPPI correlation matrices, corresponding p-value matrices, as
%  well as several different types of time courses. See the help of
%  cPPI_gen_cormats.m and cPPI_prep_ppi_ts.m for more information about what
%  these outputs represent.
% 
%  One separate .mat file will be output for each type of cor_types
%  specified above.
cor_out = strcat('cPPI_output_SemMatching_', parcellation_atlas, '_ses-', session, '_WPM_FPM');

% ==========================================================================
%  Begin computation
% ==========================================================================
% 
% --------------------------------------------------------------------------
%  Generate ppi variables
% -------------------------------------------------------------------------- 

sess={};
for sub=1:length(subs)
    tic;
    load([basedir,subs(sub).name,'/',resdir,'SPM']);  % Load SPM.mat

    output_dir = ([out_dir subs(sub).name filesep 'ses-' session filesep 'cPPI' filesep parcellation_atlas filesep]);
    if ~exist(output_dir)
        mkdir(output_dir)
    end
    %system(['mkdir ',out_dir,subs(i).name]);  % make output directory

    X = ['This is participant ' subs(sub).name];
    disp(X);

    % Get volume indices for each session
    for s=1:length(SPM.nscan)
        sess{sub,s} = SPM.Sess(s).row' ; 
    end

    for j=1:length(rois) 

        % Extract relevant confound time series
         if ~isempty(conf)
             Cnf=conf{sub};
         else 
             Cnf={};
         end

%% adapted by Sandra so that script doesn't throw an error for subjects who have only one session        
        if any(strcmp(subs(sub).name, subs_one_sess)) & session == '2'
            k = 1;
%%            
            % Prep roi time series. Frequency filters data and stores other
            % confound signals
            xY=[];
            xY=cPPI_prep_roi_ts(SPM, roi_ts{sub}(:,j), Cnf, k); 

            % Generate ppi mat file
            PPI=[];
            PPI=cPPI_peb_ppi(SPM,'ppi', xY, Uu, correct);

              % define output name
            n = length(num2str(length(rois)));     % get length of zero-padding for file name  

            out_name=[output_dir,'/',out_suff,'_',...
                [sprintf(['%0',num2str(n),'d'],rois(j))],'_',num2str(k)];   % file name  

            save(out_name, 'PPI');
        else
            for k=1:size(sess,2)         

                % Prep roi time series. Frequency filters data and stores other
                % confound signals
                xY=[];
                xY=cPPI_prep_roi_ts(SPM, roi_ts{sub}(:,j), Cnf, k); 

                % Generate ppi mat file
                PPI=[];
                PPI=cPPI_peb_ppi(SPM,'ppi', xY, Uu, correct);

                % define output name
                n = length(num2str(length(rois)));     % get length of zero-padding for file name  

                out_name=[output_dir,'/',out_suff,'_',...
                    [sprintf(['%0',num2str(n),'d'],rois(j))],'_',num2str(k)];   % file name  

                save(out_name, 'PPI');
            end
        end
    end
    fprintf('PPI mats created for subject %d of %d \n', sub, length(subs)); toc;
    %sess = {};
end

% --------------------------------------------------------------------------
%  Prepare time series 
% -------------------------------------------------------------------------- 
% 
%  Format PPI-relevant time courses into cell structures.
[ppi_ts_ppi ppi_ts_brain ppi_ts_psy] = cPPI_prep_ppi_ts(out_dir, subs, out_suff, sess, subs_one_sess, parcellation_atlas, session);

fprintf('Time series prepared \n');

% --------------------------------------------------------------------------
%  Build cPPI correlation matrices
% -------------------------------------------------------------------------- 
%    
%  Build correlation matrices
for j=1:length(cor_types)
    
    tic;    
    [ppi_cor ppi_cor_p ppi_cor_sep ppi_cor_sep_p] =...
         cPPI_gen_ppi_cormats(ppi_ts_ppi,ppi_ts_brain,ppi_ts_psy,sess,part,cor_types{j},pinds);
        
    save([cordir,cor_out,'_',cor_types{j}],'ppi*','sess');
        
    fprintf([cor_types{j},' correlation matrices computed \n']); 
    toc;
              
end

fprintf('Processing complete! \n');
        