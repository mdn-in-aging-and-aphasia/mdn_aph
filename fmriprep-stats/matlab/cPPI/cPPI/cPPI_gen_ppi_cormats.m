function [cor cor_p cor_sep cor_sep_p] = cPPI_gen_ppi_cormats(ppi,brain,psy,sess,part,cortype,pinds)

% [cor cor_p cor_sep cor_sep_p] = cPPI_gen_ppi_cormats(ppi,brain,psy,sess,part,cortype,pinds)
% 
% This function will compute correlation matrices for a cPPI analysis. The
% correlation values are computed using a partial correlation analog of the
% original PPI regression model introduced in Friston et al. (1997)
% NeuroImage. Here, the correlation between ppi interaction terms for roi1
% and roi2 are computed while partialling out the main effects of activity
% in roi1 and roi2, and the main task regressor. There is also an option 
% ('p_brain') to partial out the effects of all other ROIs in the network. 
% See below.
%
% ------
% INPUTS
% ------
%
% ppi       - the ppi time courses. Should be a structure containing N cells,
%             where N = number of subjects. Each cell should contain
%             L rows, where L=the total length of the time series, 
%             concatenated across sessions, and M columns, where M = number of ROIs.
% brain     - the ROI time courses included as covariates of no interest.
%             These are the other ROI times courses (i.e., not ROIs i and j)
%             to partial out. Data should be stored in the same format as ppi.
% psy       - the task regressor (stimulus function) time courses used in 
%             generating the PPI term. An N x 1 cell structure, where each
%             cell contains a vector of length L representing the task
%             regressors concatenated across sessions.
% sess      - An N x S cell structure where N = number of subjects and
%             S = number of sessions. Each cell contains a vector of
%             indices for the volumes comprising that session, as 
%             listed in SPM.Sess(S).row. Is generated in cPPI_master.m.
% part      - time courses of additional confounds to partial out. Typically
%             includes other task regressors not used in the PPI contrast. 
%             Should be stored in the same format as ppi. If none, set to
%             part={}.
% cortype   - the type of correlation value to compute.
%                 
%                 * 'orig' computes the correlation coefficient between
%                 roi1 and roi2 based on the original regression
%                 formulation. That is, ppi_i and ppi_j are correlated while
%                 partialling for brain_i, brain_j, psy and other confounds.
%                 
%                 * 'p_brain' computes the correlation as above, but
%                 additionally partials the main effects of neural activity
%                 other ROIs indexed with pinds; i.e., brain_k ... brain_M.
%
% pinds     - indices of ROIs to partial. Default = all other ROIs.
%
% -------
% OUTPUTS
% -------
%
% cor             - a cell structure of N cells each conatining an 
%                   L x L x N matrix of cPPI values. 
%                   If length(sess)>1, the correlations are
%                   computed using the full ROI time courses,
%                   concatenated across sessions.
% cor_p           - matrix p-values for each correlation, in same format as cor.
%                   Note: p-vals will only be valid if the original time
%                   series have been pre-whitened.
% cor_sep         - a structure containing M cells, each of which contains an 
%                   L x L x length(sess) matrix. Only output if
%                   length(sess)>1.
% cor_sep_p       - p-values for each correlation value in each session, in
%                   same format as cor_sep.
%                   Note: p-vals will only be valid if the original time
%                   series have been pre-whitened.
%
%
% When using this function, please cite:
%
% Fornito, A., Harrison, B., Zalesky, A., Simons, J. S. (2012). Competitive 
% and cooperative dynamics of large-scale brain functional networks 
% supporting recollection. Proceedings of the National Academy of Sciences, 
% USA, 109(31): 12788-12793.
%
% Alex Fornito, Monash University, August 2013.
%
% alex.fornito@monash.edu
%__________________________________________________________________________
% 
% Fixes: 
% August 2012 - adjusted to allow session lengths to vary across subjects.
%__________________________________________________________________________
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%==========================================================================

%--------------------------------------------------------------------------
% Set default for pinds to partial all other ROIs
%--------------------------------------------------------------------------
% if nargin<7 
%     pinds=1:size(ppi{1},2);
% end

%--------------------------------------------------------------------------
% Compute correlation matrices
%--------------------------------------------------------------------------

% ADAPTED by Sandra to account for different number of ROIs in participants
% (now updated in loop)
% number of rois
N = size(ppi{1},2);

for i=1:length(ppi)
    
%     % Define n of ROIs based on subject
%     if i == 4 | i == 18 | i == 38
%         N = 49;
% %     elseif i == 47
% %         rois = 1:80;
%     else
%         N = 50; % processes all rois
%     end
      pinds = N;

    
    tic;
    % Initialize matrices
    C=ones(N,N);
    P=ones(N,N);
    Cs=ones(N,N,size(sess,2));
    Ps=ones(N,N,size(sess,2));
            
    for roi1=1:N
        
        for roi2=roi1+1:N

           % Build base data
           %----------------------------------------------------------
              
           % Time courses to be correlated
           X=[];
           X=[ppi{i}(:,roi1),ppi{i}(:,roi2)];
               
           % Create the confound matrix: add main effects of ROI
           % activity and regressor of interest
           Y=[];
           Y=[brain{i}(:,roi1), brain{i}(:,roi2), psy{i}];
              
           % add additional confounds if specified
           if ~isempty(part)
               Cmat=part{i};        
               Y=[Y,Cmat];
           end
             
                % Compute correlations
                %----------------------------------------------------------

                switch cortype
                            
                    case {'orig'} % Based on standard PPI implementation
                 
                        [Rval Pval]=partialcorr(X,Y);  
                        
                        C(roi1,roi2)=Rval(2,1); % session concatenated correlations
                        C(roi2,roi1)=Rval(2,1); % make symmetric
                        
                        P(roi1,roi2)=Pval(2,1); % P-values
                        P(roi2,roi1)=Pval(2,1); % make symmetric
                        
%% adapted by Sandra: check if participant has two sessions or only one session
                        if ~isempty(sess{i,2})
                            for s=1:size(sess,2) % For each session
                          
                                [Rval Pval]=partialcorr(X(sess{i,s}(:),:),Y(sess{i,s}(:),:)); 

                                Cs(roi1,roi2,s)=Rval(2,1); % session separated correlations
                                Cs(roi2,roi1,s)=Rval(2,1); % make symmetric

                                Ps(roi1,roi2,s)=Pval(2,1); % P-vals
                                Ps(roi2,roi1,s)=Pval(2,1); % make symmetric
                            end
                        else
                            s = 1;
                            
                            [Rval Pval]=partialcorr(X(sess{i,s}(:),:),Y(sess{i,s}(:),:)); 

                            Cs(roi1,roi2,s)=Rval(2,1); % session separated correlations
                            Cs(roi2,roi1,s)=Rval(2,1); % make symmetric
                            
                            Ps(roi1,roi2,s)=Pval(2,1); % P-vals
                            Ps(roi2,roi1,s)=Pval(2,1); % make symmetric
                         end
                                
                    case {'p_brain'} % Partial for all other roi brain signals
                                 
                        % prepare additional brain signals to partial
                        Z=[];
                        Z=brain{i};
           
                        % Add other ROI time courses to confound matrix
                        pinds2=pinds;
                            
                        % do not partial rois to be correlated
                        i1 = find(pinds2==roi1);
                        i2 = find(pinds2==roi2);
                        del = [i1 i2];
                        
                        if ~isempty(del)
                            pinds2(del)=[]; % delete rois being correlated
                        end
                        
                        Zn=Z(:,pinds2);

                        Y=[Y Zn];
                    
                        [Rval Pval]=partialcorr(X,Y);
                        
                        C(roi1,roi2)=Rval(2,1); % session concatenated correlations
                        C(roi2,roi1)=Rval(2,1); % make symmetric
                        
                        P(roi1,roi2)=Pval(2,1); % P-vals
                        P(roi2,roi1)=Pval(2,1); % make symmetric
                        
                        %% adapted by Sandra: check if participant has two sessions or only one session
                        if ~isempty(sess{i,2})
                            for s=1:size(sess,2) % For each session
                          
                                [Rval Pval]=partialcorr(X(sess{i,s}(:),:),Y(sess{i,s}(:),:)); 
                                
                                Cs(roi1,roi2,s)=Rval(2,1); % session separated correlations
                                Cs(roi2,roi1,s)=Rval(2,1); % make symmetric

                                Ps(roi1,roi2,s)=Pval(2,1); % P-vals
                                Ps(roi2,roi1,s)=Pval(2,1); % make symmetric
                            end
                        else
                            s = 1;
                            
                            [Rval Pval]=partialcorr(X(sess{i,s}(:),:),Y(sess{i,s}(:),:)); 

                            Cs(roi1,roi2,s)=Rval(2,1); % session separated correlations
                            Cs(roi2,roi1,s)=Rval(2,1); % make symmetric
                            
                            Ps(roi1,roi2,s)=Pval(2,1); % P-vals
                            Ps(roi2,roi1,s)=Pval(2,1); % make symmetric
                         end

                end
        end
    end
    
    % Store outputs
    % ADAPTED by Sandra to account for different number of ROIs across
    % participants
%     cor(:,:,i)=C;
%     cor_p(:,:,i)=P;
    cor{i} = C;
    cor_p{i} = P;
    
    if size(sess,2)>1
        cor_sep{i}=Cs;
        cor_sep_p{i}=Ps;
    else
        cor_sep=[];
        cor_sep_p=[];
    end
    
    fprintf('Correlation matrices for subject %d of %d computed \n',i,length(ppi)); toc;
end
    