function [ts_ppi ts_brain ts_psy] = cPPI_prep_ppi_ts(indir,subs,keyword,sess, subs_one_sess, parcellation_atlas, session)

% [ts_ppi ts_brain ts_psy] = cPPI_prep_ppi_ts(indir,subs,keyword,sess)
%
% This function will collate the output from cPPI_peb_ppi for all subjects
% into a single mat file.
%
% ------
% INPUTS
% ------
%
% indir         - The base directory where each subject's subdirectories
%                 are. Needs to end with '/'.
% subs          - The IDs of the subjects to be processed. Should be stored
%                 as structured array, where each subject's name can be
%                 found with subs(1).name, subs(2).name, etc. as obtained
%                 when using the dir command.
% keyword       - A keyword common to all the ROI PPI mat files
% sess          - An N x S cell structure where N = number of subjects and
%                 S = number of sessions. Each cell contains a vector of
%                 indices for the volumes comprising that session, as 
%                 listed in SPM.Sess(S).row. Is generated in cPPI_master.m.
%                  
% -------
% OUTPUTS
% -------
%
% ts_ppi        - the PPI time course
% ts_brain      - the ROI activity time course
% ts_psy        - the task time regressor (i.e., the stimulus function)
%                 
% Each of the ts_ outputs will be a cell structure of length N, where N = the
% number of subjects. Each cell will contain a matrix with the number of rows
% corresponding to the total length of the time series, concatenated across
% sessions (unless deconvolved time courses are selected, in which the time 
% series will be longer) and each ROI is represented as a distinct column. Thus, 
% each column within each cell will contain the full time course concatenated across 
% sessions for each ROI. Each cell in ts_psy will only contain one column,
% representing the task regressor of interest.
%
% Alex Fornito, Monash University, August 2013.
%
% alex.fornito@monash.edu
%__________________________________________________________________________
% 
% Fixes: 
% August 2012 - adjusted to allow session lengths to vary across subjects.
%__________________________________________________________________________
%
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%==========================================================================

cwd=pwd;
%nsess=size(sess,2);

%--------------------------------------------------------------------------
% Extract and store data
%--------------------------------------------------------------------------

for i=1:length(subs)
%% adapted by Sandra so that script doesn't throw an error for subjects with only one session
% provide cell array subs_one_sess with names for those subjects in cPPI_master script 
    if any(strcmp(subs(i).name, subs_one_sess)) & session == '2'
        nsess = 1;
    else
        nsess=size(sess,2);
    end
%%    
    cd([indir,subs(i).name,filesep,'ses-',session,filesep,'cPPI',filesep,parcellation_atlas]);
    rois=dir(['*',keyword,'*']);
    
    cnt=1;
    for j=1:length(rois)/nsess
        
        for k=1:nsess
        
            load(rois(cnt).name);

            ts_ppi{i}(sess{i,k}(:),j) = PPI.ppi ;
            ts_brain{i}(sess{i,k}(:),j) = PPI.Y;
            ts_psy{i}(sess{i,k}(:),1) = PPI.P;
             
            cnt=cnt+1;   
        end     
    end
    
cd(cwd);
end

    