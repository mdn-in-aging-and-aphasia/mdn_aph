function xY = cPPI_prep_roi_ts(SPM, y, Conf, Sess)

%
% function xY = cPPI_prep_roi_ts(SPM, y, Conf, Sess, iG)
%
% This function will prepare a VOI structure to be used with cPPI_peb_ppi.m. 
% It tries to emulate, where possible, spm_routines in generating VOI
% time series. Specifically, it uses routines from spm_regions.m to whiten
% and filter the raw time series using the low-frequency confounds contained in
% SPM.xX.K and the whitening matrix contained in SPM.xX.W. It also defines 
% the session-specific confound matrix, xY.X0,used by cPPI_peb_ppi.m to 
% correct the VOI time series, and which is stored in PPI.Y.
%
% -------
% INPUTS:
% -------
%
% SPM:      The SPM.mat file. The design AND contrasts have to have been
%           estimated
%
% y:        The VOI time series. It should be a vector of length N*S, where 
%           N = the number of volumes per session, and S = the number of sessions 
%
% Conf:     User-specified nuisance confounds. Should be matrix with N*S
%           rows and K columns, where K = number of confounds. These should
%           be different to the confounds indexed in iG (see below).
% 
% Sess:     A number indicating which session is being analysed. (e.g., 1,
%           2, 3, ...etc.). Default = 1.
%
% iG:       A vector of indices identifying columns in the design matrix
%           that represent nuisance confounds. SPM.xX.iG is generally used 
%           for this purpose, but the field is left blank by default by SPM. 
%           If nuisance confounds have been included in the design matrix,
%           this argument provides a means for including them in xY.X0 (see
%           below). Alternatively, if SPM.xX.iG has already been specified
%           manually, leave this field blank. This should generally be
%           entered manually after model specification, but before model
%           estimation. Otherwise, its probably easier to include confounds
%           in Conf.
%
% --------
% OUTPUTS:
% --------
%
% xY: the VOI structure required by cPPI_peb_ppi.m. The subfields are:
%   
%   .u:    the whitened, filtered time series
%   .X0:   the confound matrix, containing:
%           1 - low-frequency confounds stored in SPM.xX.K
%           2 - session-specific constant terms stored in SPM.xX.xKXs.X(:,SPM.xX.iB)
%           3 - user-specified nuisance variables stored in SPM.xX.xKXs.X(:,SPM.xX.iG)
%               and/or contained in Conf.
%
% NB: xY.u is not corrected for fields 2 and 3 in xY.X0 by this function; 
% it is only corrected for SPM.xX.K, and whitened using SPM.xX.W. These
% fields are however used for correction by cPPI_peb_ppi.m.
% 
% Alex Fornito, University of Melbourne, May 2010.
%
% ------------------------------------------------------------------------

% Set Sess and iG default
if nargin<4
    Sess=1;
    iG=[];
end

% Set iG default
if nargin<5
    iG=[];
end

%-Get raw data, whiten and filter 
%-----------------------------------------------------------------------

% This step extracts the raw data from using the VOI definition, which
% is provided by xSPM. In this case, we supply the VOI time series, so this
% step is not required here.

% y        = spm_get_data(SPM.xY.VY,xSPM.XYZ(:,Q));

% This step filters and whitens the data
% SPM.xX.K contains session-specific low-frequency confounds used in
% high-pass filtering
% SPM.xX.W containg the whitening matrix used to whiten the data
%y        = spm_filter(SPM.xX.K,y); 

% original:
% y        = spm_filter(SPM.xX.K,SPM.xX.W*y);

%% Sandra:
if istable(y)
    y = table2array(y);
    y = double(y); %% Katie added this bc y is a single after conversion from table, and the elseif doesn't catch it yet
    y = spm_filter(SPM.xX.K,SPM.xX.W*y); 
elseif isa(y, 'single')
    y = double(y);
    y = spm_filter(SPM.xX.K,SPM.xX.W*y); 
else
    y = spm_filter(SPM.xX.K,SPM.xX.W*y); 
end

%%
%-Computation
%=======================================================================

% remove null space of contrast
%-----------------------------------------------------------------------
%
% This step adjusts the VOI time course for other contrasts in the design
% matrix if specified. It is commented out here for simplicity.
%
% if xY.Ic
% 
% 	%-Parameter estimates: beta = xX.pKX*xX.K*y
% 	%---------------------------------------------------------------
% 	beta  = spm_get_data(SPM.Vbeta,xSPM.XYZ(:,Q));
% 
% 	%-subtract Y0 = XO*beta,  Y = Yc + Y0 + e
% 	%---------------------------------------------------------------
% 	y     = y - spm_FcUtil('Y0',SPM.xCon(xY.Ic),SPM.xX.xKXs,beta);
% 
% end


%%% Define confound matrix.
%-----------------------------------------------------------------------

% This step defines the confound matrix. This matrix is used by cPPI_peb_ppi.m
% to correct the VOI time series. 

% extract confounds
% SPM.xX.xKXs.X is the whitened, filtered design matrix
% SPM.xX.iB indexes design matrix columns listing session-specific constant terms (block effects)
% SPM.xX.iG indexes design matrix columns listing user-specified confounds (nuisance variables). 

xY.X0     = SPM.xX.xKXs.X(:,[SPM.xX.iB SPM.xX.iG]);

% If xX.iG field is empty, but user has defined iG indices for design
% matrix confounds, add them.
if isempty(SPM.xX.iG) && ~isempty(iG)
    xY.X0     = [xY.X0 SPM.xX.xKXs.X(:,iG)];
end

% extract session-specific rows from data and confounds
% NB: In the following, Sess has been changed from spm_regions.m, where
% xY.Sess was used instead. This is simply a number to indicate which
% session is being analysed.

try
	i     = SPM.Sess(Sess).row;
	y     = y(i,:);
	xY.X0 = xY.X0(i,:);
end

% and add session-specific filter confounds

try
	xY.X0 = [xY.X0 SPM.xX.K(Sess).X0];
end

%=======================================================================
try
	xY.X0 = [xY.X0 SPM.xX.K(Sess).KH]; % Compatibility check
end
%=======================================================================

% Remove null space of X0
%--------------------------------------------------------------------------
xY.X0   = xY.X0(:,~~any(xY.X0));


% Add user-specified confounds
%--------------------------------------------------------------------------

if ~isempty(Conf)
    xY.X0 = [xY.X0 table2array(Conf(i,:))];
end

%--------------------------------------------------------------------------
% Save VOI time course
xY.u    = y;

% Save Sess index (required by cPPI_peb_ppi.m)
xY.Sess = Sess;

