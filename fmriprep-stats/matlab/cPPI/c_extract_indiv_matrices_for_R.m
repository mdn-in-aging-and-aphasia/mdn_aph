%% This script load cPPI correlation matrices output and saves (averaged) matrices in mat and txt files so they can be analyzed in NBS and R
% written by Sandra Maritn, 07/2023

clear

%% Set-up
data_path = '/data/p_02221/Scripts4Katie/cPPI_output/group/';

sub = {'control001','control002','control003','control004','control005','control006','control007','control008', ...
            'control009','control010','control011','control012','control013','control014','control015','control016', ...
            'control017','control018','control019','control020','control021','control022','control023','control024', ...
            'control025','control026','control027','control028','control029','control030'};

sessions = {'2','3'};


% Table with stimulation order
stim_order_tbl = readtable("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/stim_order.txt", 'ReadVariableNames', true, 'Delimiter', 'tab', 'Format', ...
    '%s%s%s%s');

% Create empty arrays
active_array = zeros(9,9,30);
sham_array = zeros(9,9,30);
active_array_p = zeros(9,9,30);
sham_array_p = zeros(9,9,30);

%% Loop over subjects
 for isess = 1:numel(sessions)
    parcel_out = 'wholeNetworks_Yeo7_confRMSD';

    % load 3D matrix containing all subjects static connectivity matrices 
    % size: nodes x nodes x subjects
    load([data_path 'cPPI_output_SemMatching_wholeNetworks_Yeo7_confRMSD_NO_GSR_ses-' sessions{isess} '_WPM_FPM_p_brain.mat']);
    FC = ppi_cor;
    pvalues = ppi_cor_p;

    for isub = 1:numel(sub)
        disp(sub{isub}); % display actual subject working on 
        sub_name = sub{isub};

        FCi = FC{isub};
        pvaluei = pvalues{isub};
    
        % Determine stimulation order to find out whether second or third session was active or sham, respectively
        if contains(sub_name, stim_order_tbl.sub_number(isub))
            stim_order = stim_order_tbl.order(isub);
        end
           
        if strcmp(stim_order, 'SA') && sessions{isess} == '2'
            sham_array(:,:,isub) = FCi;
            sham_array_p(:,:,isub) = pvaluei;
        elseif strcmp(stim_order, 'SA') && sessions{isess} == '3'
            active_array(:,:,isub) = FCi;
            active_array_p(:,:,isub) = pvaluei;
        elseif strcmp(stim_order, 'AS') && sessions{isess} == '2'
            active_array(:,:,isub) = FCi;
            active_array_p(:,:,isub) = pvaluei;
        elseif strcmp(stim_order, 'AS') && sessions{isess} == '3'
            sham_array(:,:,isub) = FCi;
            sham_array_p(:,:,isub) = pvaluei;
        end
            
    end
 end

%% Create mat file with individual matrices for NBS
sham = atanh(sham_array); % Fisher-z-transform matrices
active = atanh(active_array); % Fisher-z-transform matrices
Y = cat(3, active, sham); % Concatenate into one 3D array
save([data_path 'NBS/cPPI_matrices_wholeNets_Yeo7_confRMSD_no_GSR_all_subs_active_sham_WPM_FPM.mat'],'Y');

%% Extract session-specific average matrices for plotting in R
% sham_avg = squeeze(mean(sham(:,:,:),3, 'omitnan')); % create mean matrix
% output_name = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI/NBS/wholeNets_Yeo7_shamArray_FisherTrans_confRMSD.txt';
% writematrix(sham_avg, output_name);
% 
% active_avg = squeeze(mean(active(:,:,:),3, 'omitnan')); % create mean matrix
% output_name = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI/NBS/wholeNets_Yeo7_activeArray_FisherTrans_confRMSD.txt';
% writematrix(active_avg, output_name);
% 
% 
% % p values
% sham_p_avg = squeeze(mean(sham_p(:,:,:),3, 'omitnan')); % create mean matrix
% output_name = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI/NBS/wholeNets_Yeo_shamArray_FisherTrans_confRMSD_pvalues.txt';
% writematrix(sham_p_avg, output_name);
% 
% % active = cat(3,active_array{:}); % concatenate all cPPI matrices in one table
% active_p_avg = squeeze(mean(active_p(:,:,:),3, 'omitnan')); % create mean matrix
% output_name = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/cPPI/NBS/wholeNets_Yeo_activeArray_FisherTrans_confRMSD_pvalues.txt';
% writematrix(active_p_avg, output_name);