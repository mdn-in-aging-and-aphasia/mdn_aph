function firstLevel_semantic_matching(group, sub, session, space)

% This script performs first level analyses on single subject level

%% Specify paths & folders
%  Data folder
data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/';
stats_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/';
spm_path = '/data/p_02221/spm12';
addpath(spm_path);

sessionnum = str2double(session);
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');
  
% Estimating the GLM can take some time, particularly if you have a lot of betas. If you just want to specify your
% design matrix so that you can assess it for singularities, turn this to 0.
% If you wish to do it later, estimating the GLM through the GUI is very quick.
ESTIMATE_GLM = 1;
  
%% Start looping over subjects
for i = 1:numel(sub)
for s = sessionnum
    
    if s == 1
        if strcmp('control', group)
            if strcmp('subject', space)
                %% Display which participant is currently processed
                X = ['This is semantic matching in subject space. This is control participant ' sub{i} ' and session ' num2str(s)];
                    disp(X);

                %% Get smoothed epis for each run
                % Run-1
                epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func/']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-1_task-semanticmatching_run-1_space-T1w_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_1 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_1 = Vols_1';

                % Run-2    
                epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-1_task-semanticmatching_run-2_space-T1w_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_2 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_2 = Vols_2';   

                %% Get movement parameters
                movement_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-1/1st_level_TMS/multiple_regressors_task-semanticmatching_run-1.txt']);
                movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

                % Run-2
                movement_folder_2 = dir([stats_path 'sub-control' sub{i} '/ses-1/1st_level_TMS/multiple_regressors_task-semanticmatching_run-2.txt']);
                movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

                %% Get multiple conditions mat-file
                % Run-1
                multCond_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_session-1_task-semanticmatching_run-1_1st_level_event_related.mat']);
                multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];
                multCond_mat_1 = load(multCond_run_1);

                % Run-2
                multCond_folder_2 = dir([stats_path 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_session-1_task-semanticmatching_run-2_1st_level_event_related.mat']);
                multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
                multCond_mat_2 = load(multCond_run_2);
                
                %% Define output directory
                output_dir = [stats_path 'sub-control' sub{i} '/ses-1/1st_level_TMS'];
                if ~exist(output_dir)
                    mkdir(output_dir)
                end

            elseif strcmp('standard', space)
                %% Display which participant is currently processed
                X = ['This is semantic matching in standard space. This is control participant ' sub{i} ' and session ' num2str(s)];
                    disp(X);

                %% Get smoothed epis for each run
                % Run-1
                epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func/']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-1_task-semanticmatching_run-1_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_1 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_1 = Vols_1';

                % Run-2    
                epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-1_task-semanticmatching_run-2_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_2 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_2 = Vols_2';   

                %% Get movement parameters
                movement_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-1/motion/multiple_regressors_task-semanticmatching_run-1.txt']);
                movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

                % Run-2
                movement_folder_2 = dir([stats_path 'sub-control' sub{i} '/ses-1/motion/multiple_regressors_task-semanticmatching_run-2.txt']);
                movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

                %% Get multiple conditions mat-file
                % Run-1
                multCond_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_session-1_task-semanticmatching_run-1_1st_level_event_related_incl_length_audio.mat']);
                multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];
                multCond_mat_1 = load(multCond_run_1);
                
                % Run-2
                multCond_folder_2 = dir([stats_path 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_session-1_task-semanticmatching_run-2_1st_level_event_related_incl_length_audio.mat']);
                multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
                multCond_mat_2 = load(multCond_run_2);

%                 %% Get GM mask
%                 GM_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/anat/sub-control' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02.nii']);                            
%                 GM_mask = [GM_folder.folder '/' GM_folder.name];
                
                %% Define output directory
                output_dir = [stats_path 'sub-control' sub{i} '/ses-1/1st_level_semanticmatching_allTrialsEqualDurations'];
                if ~exist(output_dir)
                    mkdir(output_dir)
                end
            end
        elseif strcmp('patient', group)
            if strcmp('subject', space)
                %% Display which participant is currently processed
                X = ['This is semantic matching in subject space. This is patient participant ' sub{i} ' and session ' num2str(s)];
                    disp(X);

                %% Get smoothed epis for each run
                % Run-1
                epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func/']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-1_task-semanticmatching_run-1_space-T1w_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_1 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_1 = Vols_1';

                % Run-2    
                epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-1_task-semanticmatching_run-2_space-T1w_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_2 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_2 = Vols_2';   

                %% Get movement parameters
                movement_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-1/1st_level_TMS/multiple_regressors_task-semanticmatching_run-1.txt']);
                movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

                % Run-2
                movement_folder_2 = dir([stats_path 'sub-patient' sub{i} '/ses-1/1st_level_TMS/multiple_regressors_task-semanticmatching_run-2.txt']);
                movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

                %% Get multiple conditions mat-file
                % Run-1
                multCond_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_session-1_task-semanticmatching_run-1_1st_level_event_related.mat']);
                multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];
                multCond_mat_1 = load(multCond_run_1);

                % Run-2
                multCond_folder_2 = dir([stats_path 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_session-1_task-semanticmatching_run-2_1st_level_event_related.mat']);
                multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
                multCond_mat_2 = load(multCond_run_2);

                %% Define output directory
                output_dir = [stats_path 'sub-patient' sub{i} '/ses-1/1st_level_TMS'];

            elseif strcmp('standard', space)
                %% Display which participant is currently processed
                X = ['This is semantic matching in standard space. This is patient participant ' sub{i} ' and session ' num2str(s)];
                    disp(X);

                %% Get smoothed epis for each run
                % Run-1
                epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func/']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-1_task-semanticmatching_run-1_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_1 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_1 = Vols_1';

                % Run-2    
                epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-1_task-semanticmatching_run-2_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_2 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_2 = Vols_2';   

                %% Get movement parameters
                movement_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-1/1st_level_semanticmatching/multiple_regressors_task-semanticmatching_run-1.txt']);
                movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

                % Run-2
                movement_folder_2 = dir([stats_path 'sub-patient' sub{i} '/ses-1/1st_level_semanticmatching/multiple_regressors_task-semanticmatching_run-2.txt']);
                movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

                %% Get multiple conditions mat-file
                % Run-1
                multCond_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_session-1_task-semanticmatching_run-1_1st_level_event_related.mat']);
                multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];
                multCond_mat_1 = load(multCond_run_1);

                % Run-2
                multCond_folder_2 = dir([stats_path 'sub-patient' sub{i} '/ses-1/logfiles/sub-patient' sub{i} '_session-1_task-semanticmatching_run-2_1st_level_event_related.mat']);
                multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
                multCond_mat_2 = load(multCond_run_2);

                %% Get GM mask
                GM_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/anat/sub-patient' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02.nii']);                            
                GM_mask = [GM_folder.folder '/' GM_folder.name];
                
                %% Define output directory
                output_dir = [stats_path 'sub-patient' sub{i} '/ses-1/1st_level_semanticmatching'];
            end
        end
    elseif s == 2 | s == 3
        if strcmp('control', group)
            if strcmp('standard', space)
                %% Display which participant is currently processed
                X = ['This is semantic matching in standard space. This is control participant ' sub{i} ' and session ' num2str(s)];
                    disp(X);

                %% Get smoothed epis for each run
                % Run-1
                epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-' num2str(s) '/func/']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-1_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_1 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_1 = Vols_1';

                % Run-2    
                epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-' num2str(s) '/func']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-2_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_2 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_2 = Vols_2';   

                %% Get movement parameters
                movement_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-' num2str(s) '/motion/multiple_regressors_task-semanticmatching_run-1.txt']);
                movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

                % Run-2
                movement_folder_2 = dir([stats_path 'sub-control' sub{i} '/ses-' num2str(s) '/motion/multiple_regressors_task-semanticmatching_run-2.txt']);
                movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

                %% Get multiple conditions mat-file
                % Run-1
                multCond_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-' num2str(s) '/logfiles/sub-control' sub{i} '_session-' num2str(s) '_task-semanticmatching_run-1_1st_level_event_related_incl_length_audio.mat']);
                multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];
                multCond_mat_1 = load(multCond_run_1);

                % Run-2
                multCond_folder_2 = dir([stats_path 'sub-control' sub{i} '/ses-' num2str(s) '/logfiles/sub-control' sub{i} '_session-' num2str(s) '_task-semanticmatching_run-2_1st_level_event_related_incl_length_audio.mat']);
                multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
                multCond_mat_2 = load(multCond_run_2);

%                 %% Get GM mask
%                 GM_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/anat/sub-control' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02.nii']);                            
%                 GM_mask = [GM_folder.folder '/' GM_folder.name];
                
                %% Define output directory
                output_dir = [stats_path 'sub-control' sub{i} '/ses-' num2str(s) '/1st_level_semanticmatching_allTrialsEqualDurations'];
                if ~exist(output_dir)
                    mkdir(output_dir)
                end
            end
        elseif strcmp('patient', group)
            if strcmp('standard', space)
                %% Display which participant is currently processed
                X = ['This is semantic matching in standard space. This is patient participant ' sub{i} ' and session ' num2str(s)];
                    disp(X);

                %% Get smoothed epis for each run
                % Run-1
                epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-' num2str(s) '/func/']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-1_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_1 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_1 = Vols_1';

                % Run-2    
                epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-' num2str(s) '/func']);
                curr_epi_folder = epi_folder.folder;
                nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-' num2str(s) '_task-semanticmatching_run-2_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
                func_images_path = [nifti_files.folder '/' nifti_files.name];
                    func_vols = spm_vol(func_images_path);
                    Vols_2 = {};
                    for iVol = 1:numel(func_vols)
                        Vols_2{iVol} = [func_images_path ',' num2str(iVol)];
                    end
                    Vols_2 = Vols_2';   

                %% Get movement parameters
                movement_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-' num2str(s) '/1st_level_semanticmatching/multiple_regressors_task-semanticmatching_run-1.txt']);
                movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

                % Run-2
                movement_folder_2 = dir([stats_path 'sub-patient' sub{i} '/ses-' num2str(s) '/1st_level_semanticmatching/multiple_regressors_task-semanticmatching_run-2.txt']);
                movement_run_2 = [movement_folder_2.folder '/' movement_folder_2.name];

                %% Get multiple conditions mat-file
                % Run-1
                multCond_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-' num2str(s) '/logfiles/sub-patient' sub{i} '_session-' num2str(s) '_task-semanticmatching_run-1_1st_level_event_related.mat']);
                multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];
                multCond_mat_1 = load(multCond_run_1);

                % Run-2
                multCond_folder_2 = dir([stats_path 'sub-patient' sub{i} '/ses-' num2str(s) '/logfiles/sub-patient' sub{i} '_session-' num2str(s) '_task-semanticmatching_run-2_1st_level_event_related.mat']);
                multCond_run_2 = [multCond_folder_2.folder '/' multCond_folder_2.name];
                multCond_mat_2 = load(multCond_run_2);

                %% Get GM mask
                GM_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/anat/sub-patient' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02.nii']);                            
                GM_mask = [GM_folder.folder '/' GM_folder.name];
                
                %% Define output directory
                output_dir = [stats_path 'sub-patient' sub{i} '/ses-' num2str(s) '/1st_level_semanticmatching'];
            end
        end
    end
                
                %% Set up batch
                % Specify model
                clear matlabbatch
                matlabbatch{1}.spm.stats.fmri_spec.dir = {output_dir};
                matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
                matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
                matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 60;
                matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 30;

                % Run-1
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = Vols_1;
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {multCond_run_1};
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {movement_run_1};
                matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;

                % Run-2
                matlabbatch{1}.spm.stats.fmri_spec.sess(2).scans = Vols_2;
                matlabbatch{1}.spm.stats.fmri_spec.sess(2).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
                matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi = {multCond_run_2};
                matlabbatch{1}.spm.stats.fmri_spec.sess(2).regress = struct('name', {}, 'val', {});
                matlabbatch{1}.spm.stats.fmri_spec.sess(2).multi_reg = {movement_run_2};
                matlabbatch{1}.spm.stats.fmri_spec.sess(2).hpf = 128;

                matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
                matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 1]; % set to 1 1 if I want time and dispersion derivatives
                matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
                matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
                matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
                matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
                matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';

                %% Navigate to output directory, specify and estimate GLM
                 cd(output_dir);
                 spm_jobman('run', matlabbatch)

                if ESTIMATE_GLM == 1
                    load SPM;
                    spm_spm(SPM);
                end

                %% Contrast setup: define all contrasts and run them
                clear matlabbatch
                spmmat = dir([output_dir '/SPM.mat']);
                
                % First, check if any errors were made since the number of
                % parameters might differ between sessions
                if any(strcmp(multCond_mat_1.names, 'Errors')) & any(strcmp(multCond_mat_2.names, 'Errors'))
                    errors = 'allRunsErrors'
                elseif any(strcmp(multCond_mat_1.names, 'Errors'))
                    errors = 'ErrorsRun1'
                elseif any(strcmp(multCond_mat_2.names, 'Errors'))
                    errors = 'ErrorsRun2'
                elseif ~any(strcmp(multCond_mat_1.names, 'Errors')) & ~any(strcmp(multCond_mat_2.names, 'Errors'))
                    errors = 'noErrors'
                end
                
                switch errors
                    case ('allRunsErrors') % both sessions have errors
                        matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllStimuli - Rest';
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 0 0 1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Language - Rest';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'WPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1];
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'FPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Control_task - Rest';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [0 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'WPM+FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [1 0 0 1 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'Control_task - WPM+FPM';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [-1 0 0 -1 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.name = 'FPM - WPM';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.weights = [-1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.name = 'WPM - FPM';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.weights = [1 0 0 -1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.name = 'WPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.weights = [1 0 0 0 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.name = 'Control_task - WPM';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.weights = [-1 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.name = 'FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.weights = [0 0 0 1 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.name = 'Control_task - FPM';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.weights = [0 0 0 -1 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.name = 'EOI';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0 
                                                                                0 0 0 0 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.name = 'F-test LANG';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.name = 'AllStimuli - Errors';
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.weights = [1 0 0 1 0 0 1 0 0 -3];
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.name = 'Errors - Tasks';
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.weights = [-1 0 0 -1 0 0 -1 0 0 3];
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.name = 'Errors - Language';
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.weights = [-1 0 0 -1 0 0 0 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.name = 'Language - Errors';
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.weights = [1 0 0 1 0 0 0 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.sessrep = 'repl';
                        
                        matlabbatch{1}.spm.stats.con.delete = 1;
                        spm_jobman('run', matlabbatch)
                        
                                               
                    case ('ErrorsRun1')
                        disp('Only session 1 had errors') % only session 1 has errors
                        matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllStimuli - Rest';
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 0 0 1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Language - Rest';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'WPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1];
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'FPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Control_task - Rest';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [0 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'WPM+FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [1 0 0 1 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'Control_task - WPM+FPM';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [-1 0 0 -1 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.name = 'FPM - WPM';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.weights = [-1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.name = 'WPM - FPM';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.weights = [1 0 0 -1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.name = 'WPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.weights = [1 0 0 0 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.name = 'Control_task - WPM';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.weights = [-1 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.name = 'FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.weights = [0 0 0 1 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.name = 'Control_task - FPM';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.weights = [0 0 0 -1 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.name = 'EOI';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0 
                                                                                0 0 0 0 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.name = 'F-test LANG';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.name = 'AllStimuli - Errors';
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.weights = [1 0 0 1 0 0 1 0 0 -3];
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.sessrep = 'none';
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.name = 'Errors - Tasks';
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.weights = [-1 0 0 -1 0 0 -1 0 0 3];
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.sessrep = 'none';
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.name = 'Errors - Language';
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.weights = [-1 0 0 -1 0 0 0 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.sessrep = 'none';
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.name = 'Language - Errors';
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.weights = [1 0 0 1 0 0 0 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.sessrep = 'none';
                        
                        matlabbatch{1}.spm.stats.con.delete = 1;
                        spm_jobman('run', matlabbatch)
                        
                        
                    case ('ErrorsRun2')
                        disp('Only session 2 had errors') % only session 2 has errors
                        
                        Here, we first need to find out how many
                        regressors session 1 has in total so that we can
                        supply the correct number of 0s before we specify
                        the error contrasts only for session 2
                        n_regressors = load(movement_run_1);
                        n_cols_regressors = size(n_regressors, 2);
                        
                        matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllStimuli - Rest';
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 0 0 1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Language - Rest';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'WPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1];
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'FPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Control_task - Rest';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [0 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'WPM+FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [1 0 0 1 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'Control_task - WPM+FPM';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [-1 0 0 -1 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.name = 'FPM - WPM';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.weights = [-1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.name = 'WPM - FPM';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.weights = [1 0 0 -1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.name = 'WPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.weights = [1 0 0 0 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.name = 'Control_task - WPM';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.weights = [-1 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.name = 'FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.weights = [0 0 0 1 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.name = 'Control_task - FPM';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.weights = [0 0 0 -1 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.name = 'EOI';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0 
                                                                                0 0 0 0 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.name = 'F-test LANG';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.name = 'AllStimuli - Errors';
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.weights = [0 0 0 0 0 0 0 0 0 0 0 0 repelem(0,n_cols_regressors) 1 0 0 1 0 0 1 0 0 -3];
                        matlabbatch{1}.spm.stats.con.consess{16}.tcon.sessrep = 'none';
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.name = 'Errors - Tasks';
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.weights = [0 0 0 0 0 0 0 0 0 0 0 0 repelem(0,n_cols_regressors) -1 0 0 -1 0 0 -1 0 0 3];
                        matlabbatch{1}.spm.stats.con.consess{17}.tcon.sessrep = 'none';
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.name = 'Errors - Language';
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.weights = [0 0 0 0 0 0 0 0 0 0 0 0 repelem(0,n_cols_regressors) -1 0 0 -1 0 0 0 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{18}.tcon.sessrep = 'none';
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.name = 'Language - Errors';
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.weights = [0 0 0 0 0 0 0 0 0 0 0 0 repelem(0,n_cols_regressors) 1 0 0 1 0 0 0 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{19}.tcon.sessrep = 'none';
                        
                        matlabbatch{1}.spm.stats.con.delete = 1;
                        spm_jobman('run', matlabbatch)
                                            
                    case ('noErrors')
                        disp('This participant had no Errors');
                        
                        matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllStimuli - Rest';
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 0 0 1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Language - Rest';
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'WPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [1];
                        matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'FPM - Rest';
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Control_task - Rest';
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [0 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.name = 'WPM+FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.weights = [1 0 0 1 0 0 -2];
                        matlabbatch{1}.spm.stats.con.consess{6}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.name = 'Control_task - WPM+FPM';
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.weights = [-1 0 0 -1 0 0 2];
                        matlabbatch{1}.spm.stats.con.consess{7}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.name = 'FPM - WPM';
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.weights = [-1 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{8}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.name = 'WPM - FPM';
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.weights = [1 0 0 -1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{9}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.name = 'WPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.weights = [1 0 0 0 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{10}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.name = 'Control_task - WPM';
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.weights = [-1 0 0 0 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{11}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.name = 'FPM - Control_task';
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.weights = [0 0 0 1 0 0 -1];
                        matlabbatch{1}.spm.stats.con.consess{12}.tcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.name = 'Control_task - FPM';
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.weights = [0 0 0 -1 0 0 1];
                        matlabbatch{1}.spm.stats.con.consess{13}.tcon.sessrep = 'repl';

                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.name = 'EOI';
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0 
                                                                                0 0 0 0 0 0 1 0 0];
                        matlabbatch{1}.spm.stats.con.consess{14}.fcon.sessrep = 'repl';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.name = 'F-test LANG';
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.weights = [1 0 0 0 0 0 0 0 0
                                                                                0 0 0 1 0 0 0 0 0];
                        matlabbatch{1}.spm.stats.con.consess{15}.fcon.sessrep = 'repl';
                        
                        matlabbatch{1}.spm.stats.con.delete = 1;
                        spm_jobman('run', matlabbatch)
                end              
end
end