function firstLevel_localizer(group, sub, run)
%%  1st level batch script: Segmentation, Coregistration, Normalisation & Smoothing
% This script performs first level analyses on single subject level

%% Specify paths & folders
%  Data folder
data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/';
stats_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/';
spm_path = '/data/p_02221/spm12';
addpath(spm_path);
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');
  
% Estimating the GLM can take some time, particularly if you have a lot of betas. If you just want to specify your
% design matrix so that you can assess it for singularities, turn this to 0.
% If you wish to do it later, estimating the GLM through the GUI is very quick.
ESTIMATE_GLM = 1;
  
%% Start looping over subject
for i = 1:numel(sub) 
    if strcmp('control', group)
        % Display which participant and which run is currently processed
        X = ['This is control participant ' sub{i} ' and run ' run];
            disp(X);

        %% Get smoothed epis for run
        % Run-1
        epi_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/func/']);
        curr_epi_folder = epi_folder.folder;
        nifti_files = dir([curr_epi_folder '/ssub-control' sub{i} '_ses-1_task-Localizer_run-' run '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols_1 = {};
            for iVol = 1:numel(func_vols)
                Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols_1 = Vols_1';

        %% Get movement parameters
        movement_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-1/1st_level_Localizer/multiple_regressors_task-Localizer_run-' run '.txt']);
        movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

        %% Get multiple conditions mat-file
        % Run-1
        multCond_folder_1 = dir([stats_path 'sub-control' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_session-1_task-Localizer_run-' run '_1st_level_block_design.mat']);
        multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];

        %% Get GM mask
        GM_folder = dir([data_path 'sub-control' sub{i} '/fmriprep/sub-control' sub{i} '/ses-1/anat/sub-control' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02.nii']);                            
        GM_mask = [GM_folder.folder '/' GM_folder.name];
                
        %% Define output directory
        output_dir = [stats_path 'sub-control' sub{i} '/ses-1/1st_level_Localizer'];
        if ~exist(output_dir)
                    mkdir(output_dir)
        end
        
        
    elseif strcmp('patient', group)
        
        % Display which participant and which run is currently processed
        X = ['This is patient participant ' sub{i} ' and run ' run];
            disp(X);

        %% Get smoothed epis for each run
        % Run-1
        epi_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/func/']);
        curr_epi_folder = epi_folder.folder;
        nifti_files = dir([curr_epi_folder '/ssub-patient' sub{i} '_ses-1_task-Localizer_run-' run '_space-MNI152NLin6Asym_desc-preproc_bold.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols_1 = {};
            for iVol = 1:numel(func_vols)
                Vols_1{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols_1 = Vols_1';

        %% Get movement parameters
        movement_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-1/1st_level_Localizer/multiple_regressors_task-Localizer_run-' run '.txt']);
        movement_run_1 = [movement_folder_1.folder '/' movement_folder_1.name];

        %% Get multiple conditions mat-file
        % Run-1
        multCond_folder_1 = dir([stats_path 'sub-patient' sub{i} '/ses-1/logfiles/sub-control' sub{i} '_session-1_task-Localizer_run-' run '_1st_level_block_design.mat']);
        multCond_run_1 = [multCond_folder_1.folder '/' multCond_folder_1.name];

        %% Get GM mask
        GM_folder = dir([data_path 'sub-patient' sub{i} '/fmriprep/sub-patient' sub{i} '/ses-1/anat/sub-patient' sub{i} '_space-MNI152NLin6Asym_label-GM_thr02.nii']);                            
        GM_mask = [GM_folder.folder '/' GM_folder.name];
        
        %% Define output directory
        output_dir = [stats_path 'sub-patient' sub{i} '/ses-1/1st_level_Localizer'];
        if ~exist(output_dir)
                    mkdir(output_dir)
        end
    end
    
    %% Set up batch
    % Specify model
    clear matlabbatch
    matlabbatch{1}.spm.stats.fmri_spec.dir = {output_dir};
    matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 60;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 30;

    % Run-1
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = Vols_1;
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {multCond_run_1};
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {movement_run_1};
    matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;

    matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [1 1]; % set to 1 1 if I want time and dispersion derivatives
    matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
    matlabbatch{1}.spm.stats.fmri_spec.mask = {GM_mask};
    matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';

    %% Navigate to output directory, specify and estimate GLM
     cd(output_dir);
     spm_jobman('run', matlabbatch)

    if ESTIMATE_GLM == 1
        load SPM;
        spm_spm(SPM);
    end

    %% Contrast setup: define all contrasts and run them
    clear matlabbatch
    spmmat = dir([output_dir '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllStimuli - Rest';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 0 0 1];
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Intact sound - Rest';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [1];
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.name = 'Degraded sound - Rest';
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.weights = [0 0 0 1];
    matlabbatch{1}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{4}.tcon.name = 'Intact sound - Degraded sound';
    matlabbatch{1}.spm.stats.con.consess{4}.tcon.weights = [1 0 0 -1];
    matlabbatch{1}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{5}.tcon.name = 'Degraded sound - Intact sound';
    matlabbatch{1}.spm.stats.con.consess{5}.tcon.weights = [-1 0 0 1];
    matlabbatch{1}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.consess{6}.fcon.name = 'EOI';
    matlabbatch{1}.spm.stats.con.consess{6}.fcon.weights = [1 0 0 0 0 0
                                                            0 0 0 1 0 0];
    matlabbatch{1}.spm.stats.con.consess{6}.fcon.sessrep = 'repl';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    %spm_jobman('interactive',matlabbatch);

end