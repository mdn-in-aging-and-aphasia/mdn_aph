#!/bin/bash
set -x

#mkdir -p /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active_vs_Sham_NoDerivatives/{Overall_effect_masked,MainEffectStim_masked,WPM_act_sham_masked,FPM_act_sham_masked,WPM+FPM_act_sham_masked,ControlTask_act_sham_masked,FPM-WPM_act_sham_masked,WPM+FPM-ControlTask_act_sham_masked,Control-WPM_act_sham_masked,Control-FPM_act_sham_masked}

#mkdir -p /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/{Cong-Incong_masked,WPMCong-WPMIncong_masked,FPMCong-FPMIncong_masked,toneCong-toneIncong_WPM,LangCong-LangIncong_masked}


#for x in {001..030}; do
#for con in {0001..0013}; do

#mv /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/1st_level_semanticmatching_acrossSessions/con_"$con".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/1st_level_semanticmatching_acrossSessions/con_"$con"_sub-control"$x".nii

#mv /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_"$con".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_"$con"_sub-control"$x".nii

#done
#done

# copy contrast images for group stats 1st session
for x in {001..030}; do

cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0001_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/Cong-Incong_masked
#cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0002_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/WPM+FPM-rest
cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0003_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/WPMCong-WPMIncong_masked
#cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0004_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/FPM-rest
cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0005_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/FPMCong-FPMIncong_masked
#cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0006_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/WPM+FPM-ControlTask
cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0007_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/toneCong-toneIncong_WPM
cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0009_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/LangCong-LangIncong_masked
#cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0012_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/FPM-ControlTask
#cp /data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/sub-control"$x"/ses-1/1st_level_semanticmatching_congruency/con_0016_sub-control"$x".nii /data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask/AllTasks-Errors

done











