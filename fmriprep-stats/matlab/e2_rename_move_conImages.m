%% This script reads table with session order for sham and active sessions and renames con images for contrasts of interest accordingly
% written by Sandra Martin, 04/2022

clear

%% Specify paths & folders
%  Data folder
stats_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/';

stim_order_tbl = readtable("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/stim_order.txt", 'ReadVariableNames', true, 'Delimiter', 'tab', 'Format', ...
    '%s%s%s%s');

group_folder = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active_Sham_T-Images/';


sub = {'001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020', ...
    '021','022','023','024','025','026','027','028','029','030'};


for i = 1:numel(sub)
    
    % Determine stimulation order to find out whether second or third
    % session was active or sham, respectively
    if strcmp(stim_order_tbl.sub_number{str2double(sub{i})}, sub{i})
        stim_order = stim_order_tbl.order{str2double(sub{i})};
    end
    
    %% participants with order sham – active
    if strcmp(stim_order, 'SA')
                  
%         curr_con = ['con_' num2str(con, '%04.f')];
%         con_img = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching/' curr_con '.nii'];
        
%         % All Tasks
%         curr_con = 'con_0001';
%         con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching/' curr_con '.nii'];
%         copyfile(con_img_sham, [group_folder 'MainEffectStim_paired_ttest_masked/' curr_con '_sub-control' sub{i} '_sham.nii']);
%         con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching/' curr_con '.nii'];
%         copyfile(con_img_active, [group_folder 'MainEffectStim_paired_ttest_masked/' curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM+FPM
        curr_con = 'con_0002';
        dest = [group_folder 'WPM+FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM
        curr_con = 'con_0003';
        dest = [group_folder 'WPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % FPM
        curr_con = 'con_0004';
        dest = [group_folder 'FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % Control Task
        curr_con = 'con_0005';
        dest = [group_folder 'ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM+FPM - Control Task
        curr_con = 'con_0006';
        dest = [group_folder 'WPM+FPM-ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % Control Task - WPM+FPM
        curr_con = 'con_0007';
        dest = [group_folder 'ControlTask-WPM+FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % FPM-WPM
        curr_con = 'con_0008';
        dest = [group_folder 'FPM-WPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM-FPM
        curr_con = 'con_0009';
        dest = [group_folder 'WPM-FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM - Control Task
        curr_con = 'con_0010';
        dest = [group_folder 'WPM-ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
         % Control Task - WPM
        curr_con = 'con_0011';
        dest = [group_folder 'ControlTask-WPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % FPM - Control Task
        curr_con = 'con_0012';
        dest = [group_folder 'FPM-ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
         % Control Task - FPM
        curr_con = 'con_0013';
        dest = [group_folder 'ControlTask-FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % Ftest LANG
        curr_con = 'ess_0015';
        dest = [group_folder 'Ftest_LANG_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        
    %% participants with order: active – sham    
    elseif strcmp(stim_order, 'AS')
          
        % WPM+FPM
        curr_con = 'con_0002';
        dest = [group_folder 'WPM+FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM
        curr_con = 'con_0003';
        dest = [group_folder 'WPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % FPM
        curr_con = 'con_0004';
        dest = [group_folder 'FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % Control Task
        curr_con = 'con_0005';
        dest = [group_folder 'ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM+FPM - Control Task
        curr_con = 'con_0006';
        dest = [group_folder 'WPM+FPM-ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % Control Task - WPM+FPM
        curr_con = 'con_0007';
        dest = [group_folder 'ControlTask-WPM+FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % FPM-WPM
        curr_con = 'con_0008';
        dest = [group_folder 'FPM-WPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM-FPM
        curr_con = 'con_0009';
        dest = [group_folder 'WPM-FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % WPM - Control Task
        curr_con = 'con_0010';
        dest = [group_folder 'WPM-ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
         % Control Task - WPM
        curr_con = 'con_0011';
        dest = [group_folder 'ControlTask-WPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % FPM - Control Task
        curr_con = 'con_0012';
        dest = [group_folder 'FPM-ControlTask_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
         % Control Task - FPM
        curr_con = 'con_0013';
        dest = [group_folder 'ControlTask-FPM_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
        
        % F-test LANG
        curr_con = 'ess_0015';
        dest = [group_folder 'Ftest_LANG_act_sham_conImages'];
        if ~exist(dest)
            mkdir(dest)
        end
        con_img_sham = [stats_path 'sub-control' sub{i} '/ses-3/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_sham, [dest filesep curr_con '_sub-control' sub{i} '_sham.nii']);
        con_img_active = [stats_path 'sub-control' sub{i} '/ses-2/1st_level_semanticmatching_allTrialsEqualDurations/' curr_con '.nii'];
        copyfile(con_img_active, [dest filesep curr_con '_sub-control' sub{i} '_active.nii']);
         
    end
end