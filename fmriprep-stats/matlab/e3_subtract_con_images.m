%% This script uses Image Calculator to subtract con-images

clear

%% Specify paths & folders
%  Data folder
group_folder = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/';
spm_path = '/data/p_02221/spm12';
addpath(spm_path);
      
%% Initialise SPM defaults
spm('defaults', 'FMRI');
spm_jobman('initcfg');

sub = {'001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020', ...
    '021','022','023','024','025','026','027','028','029','030'};
% sub = {'001','003','006','007','010','011','014','017','020', ...
%     '023','024','026','027','028','030'};
% sub = {'002','004','005','008','009','012','013','015','016','018','019', ...
%     '021','022','025','029'};

stats_folder = [group_folder 'Control_group_Active_Sham_T-Images/'];
dirs = dir(stats_folder);

for folder = 3:length(dirs)
    currDir = dirs(folder).name
    
    for i = 1:numel(sub)

        %% Display which participant is currently processed
        X = ['Processing control participant ' sub{i}];
        disp(X);

        %% Get active con-image
        con_active_path = dir([stats_folder currDir filesep '*_sub-control' sub{i} '_active.nii']);
        con_active = [con_active_path.folder filesep con_active_path.name];

        %% Get sham con-image
        con_sham_path = dir([stats_folder currDir filesep '*_sub-control' sub{i} '_sham.nii']);
        con_sham = [con_sham_path.folder filesep con_sham_path.name];

        %% Define output directory
        output_dir = [group_folder 'Control_group_Active-Sham_OneSample_ttest/' currDir];
        output_dir = strrep(output_dir, 'conImages', 'masked');
        if ~exist(output_dir)
            mkdir(output_dir)
        end
        output_name = ['sub-control' sub{i} '_active-sham'];

        %% Run batch
        clear matlabbatch
        matlabbatch{1}.spm.util.imcalc.input = {con_active
                                                con_sham};
        matlabbatch{1}.spm.util.imcalc.output = output_name;
        matlabbatch{1}.spm.util.imcalc.outdir = {output_dir};
        matlabbatch{1}.spm.util.imcalc.expression = 'i1-i2';
        matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
        matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
        matlabbatch{1}.spm.util.imcalc.options.mask = 0;
        matlabbatch{1}.spm.util.imcalc.options.interp = -7;
        matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
        spm_jobman('run', matlabbatch)

    end
end