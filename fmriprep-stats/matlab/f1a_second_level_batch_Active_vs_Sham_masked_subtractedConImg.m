%% Batch for 2nd level analysis %%

clear

data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active-Sham_OneSample_ttest/';
spm_path = '/data/p_02221/spm12';

GM_mask = '/data/p_02221/Scripts/fmriprep-stats/matlab/GM_spm_0.3.nii';

% RT = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/RT_JE.txt';

      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');
  
%% F-test LANG
con_path = [data_path 'Ftest_LANG_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
   
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
  
%% ControlTask_FPM
con_path = [data_path 'ControlTask-FPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
   
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    
%% ControlTask_WPM
con_path = [data_path 'ControlTask-WPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    
%% ControlTask>Rest
con_path = [data_path 'ControlTask_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    
%% FPM_WPM
con_path = [data_path 'FPM-WPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    

%% FPM_rest
con_path = [data_path 'FPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    
    
%% FPM+WPM_ControlTask
con_path = [data_path 'WPM+FPM-ControlTask_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    
    
%% FPM+WPM_rest
con_path = [data_path 'WPM+FPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)
    
    

%% WPM_rest
con_path = [data_path 'WPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)  
    
    
%% Control task - WPM+FPM
con_path = [data_path 'ControlTask-WPM+FPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)  
    
    
%% FPM - Control task
con_path = [data_path 'FPM-ControlTask_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)  
    
    
%% WPM - Control task
con_path = [data_path 'WPM-ControlTask_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)  
    
    
%% WPM-FPM
con_path = [data_path 'WPM-FPM_act_sham_masked'];

con_images = dir([con_path '/sub-*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.name = 'all';
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{3}.fcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)  