%% Batch for 2nd level analysis %%

clear

con_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active_Sham_conImages_duration+RT';
data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active_vs_Sham_Derivatives_paired_ttests/';
spm_path = '/data/p_02221/spm12';

GM_mask = '/data/p_02221/Scripts/fmriprep-stats/matlab/GM_spm_0.3.nii';

% RT = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/RT_JE.txt';

n_sub = 30;
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');

%% All tasks
% con_path = [con_dir 'MainEffectStim_paired_ttest_masked/'];
% 
% con_paths = strings(30,2);
% for sub = 1:n_sub
%     active_con_img = [con_path 'con_0001_sub-control' num2str(sub, '%03.f') '_active.nii'];
%     sham_con_img = [con_path 'con_0001_sub-control' num2str(sub, '%03.f') '_sham.nii'];
%     con_paths(sub,1) = active_con_img;
%     con_paths(sub,2) = sham_con_img;
% end
% 
% clear matlabbatch
% matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
% matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
% matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
% matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
% matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
% matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
% matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
% matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
% matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
% matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
% matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
% 
% % Navigate to output directory, specify and estimate GLM
% spm_jobman('run', matlabbatch)
% cd(con_path);
% 
% load SPM;
% spm_spm(SPM);
% 
% clear matlabbatch
% spmmat = dir([con_path '/SPM.mat']);
% matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
% matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'All tasks active>sham';
% matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
% matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
% matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'All tasks sham>active';
% matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
% matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
% matlabbatch{1}.spm.stats.con.delete = 1;
% spm_jobman('run', matlabbatch)


%% WPM + FPM
con_path = [con_dir '/WPM+FPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0002_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0002_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path '/WPM+FPM_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM+FPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'WPM+FPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% WPM
con_path = [con_dir '/WPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0003_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0003_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path 'WPM_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end


clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'WPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% FPM
con_path = [con_dir '/FPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0004_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0004_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path 'FPM_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'FPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'FPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% Control task
con_path = [con_dir '/ControlTask_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0005_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0005_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path 'ControlTask_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Control task active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Control task sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% WPM + FPM - Control task
con_path = [con_dir '/WPM+FPM-ControlTask_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0006_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0006_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path 'WPM+FPM-ControlTask_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM+FPM -Control task active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'WPM+FPM - Control task sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% Control task - WPM + FPM
con_path = [con_dir '/ControlTask-WPM+FPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0007_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0007_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path 'ControlTask-WPM+FPM_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Control task - WPM+FPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Control task - WPM+FPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% FPM - WPM
con_path = [con_dir '/FPM-WPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0008_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0008_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

output_dir = [data_path 'FPM-WPM_act_sham_paired_ttest_masked/'];
if ~exist(output_dir)
    mkdir(output_dir)
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(output_dir);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([output_dir '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'FPM-WPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'FPM-WPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% WPM - FPM
con_path = [con_dir '/WPM-FPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0009_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0009_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(con_path);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([con_path '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM-FPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'WPM-FPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% WPM - Control task
con_path = [con_dir '/WPM-ControlTask_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0010_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0010_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(con_path);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([con_path '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM-Control task active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'WPM-Control task sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% Control task - WPM
con_path = [con_dir '/ControlTask-WPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0011_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0011_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(con_path);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([con_path '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Control task-WPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Control task-WPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% FPM - Control task
con_path = [con_dir '/FPM-ControlTask_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0012_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0012_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(con_path);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([con_path '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'FPM-Control task active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'FPM-Control task sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)


%% Control task - FPM
con_path = [con_dir '/ControlTask-FPM_act_sham_conImages/'];

con_paths = strings(30,2);
for sub = 1:n_sub
    active_con_img = [con_path 'con_0013_sub-control' num2str(sub, '%03.f') '_active.nii'];
    sham_con_img = [con_path 'con_0013_sub-control' num2str(sub, '%03.f') '_sham.nii'];
    con_paths(sub,1) = active_con_img;
    con_paths(sub,2) = sham_con_img;
end

clear matlabbatch
matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

% Navigate to output directory, specify and estimate GLM
spm_jobman('run', matlabbatch)
cd(con_path);

load SPM;
spm_spm(SPM);

clear matlabbatch
spmmat = dir([con_path '/SPM.mat']);
matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'Control task-FPM active>sham';
matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Control task-FPM sham>active';
matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{1}.spm.stats.con.delete = 1;
spm_jobman('run', matlabbatch)