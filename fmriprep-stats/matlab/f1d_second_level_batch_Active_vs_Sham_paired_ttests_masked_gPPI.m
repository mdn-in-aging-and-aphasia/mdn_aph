%% Batch for 2nd level analysis %%

% con_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active_Sham_conImages';
% data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_Active_vs_Sham_Derivatives_paired_ttests/';

clear

GM_mask = '/data/p_02221/Scripts/fmriprep-stats/matlab/GM_spm_0.3.nii';

% RT = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/RT_JE.txt';

n_sub = 30;

contrast = {'FPM>WPM', 'FPM>Tone', 'Tone>FPM', 'WPM>FPM'};
      
%% Initialise SPM defaults
addpath '/data/p_02221/spm12'
spm('defaults', 'FMRI');
spm_jobman('initcfg');


% define main folder
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';

% group directory
group_dir = [root_dir 'group_statistics/gPPI/gPPI_individualROI_LSPL_FPM>Rest_10mm_sphere_2023'];

GM_mask = '/data/p_02221/Scripts/fmriprep-stats/matlab/GM_spm_0.3.nii';
cov_file = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/covs_Age_Agesq_GMV_TIV_orth.txt';


for icon = 1:numel(contrast)
    con_paths = strings(30,2);
    for sub = 1:n_sub
        active_con_img = [group_dir filesep 'active' filesep contrast{icon} filesep 'con_PPI_' contrast{icon} '_sub-control' num2str(sub, '%03.f') '-active.nii'];
        sham_con_img =  [group_dir filesep 'sham' filesep contrast{icon} filesep 'con_PPI_' contrast{icon} '_sub-control' num2str(sub, '%03.f') '-sham.nii'];
        con_paths(sub,1) = active_con_img;
        con_paths(sub,2) = sham_con_img;
    end
    
    output_dir = [group_dir filesep 'paired_ttest_masked' filesep contrast{icon}];
    if ~exist(output_dir)
        mkdir(output_dir)
    end

    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {output_dir};
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(1).scans = cellstr([con_paths(1,1);con_paths(1,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(2).scans = cellstr([con_paths(2,1);con_paths(2,2)]);    
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(3).scans = cellstr([con_paths(3,1);con_paths(3,2)]);    
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(4).scans = cellstr([con_paths(4,1);con_paths(4,2)]);    
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(5).scans = cellstr([con_paths(5,1);con_paths(5,2)]);    
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(6).scans = cellstr([con_paths(6,1);con_paths(6,2)]);    
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(7).scans = cellstr([con_paths(7,1);con_paths(7,2)]);    
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(8).scans = cellstr([con_paths(8,1);con_paths(8,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(9).scans = cellstr([con_paths(9,1);con_paths(9,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(10).scans = cellstr([con_paths(10,1);con_paths(10,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(11).scans = cellstr([con_paths(11,1);con_paths(11,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(12).scans = cellstr([con_paths(12,1);con_paths(12,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(13).scans = cellstr([con_paths(13,1);con_paths(13,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(14).scans = cellstr([con_paths(14,1);con_paths(14,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(15).scans = cellstr([con_paths(15,1);con_paths(15,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(16).scans = cellstr([con_paths(16,1);con_paths(16,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(17).scans = cellstr([con_paths(17,1);con_paths(17,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(18).scans = cellstr([con_paths(18,1);con_paths(18,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(19).scans = cellstr([con_paths(19,1);con_paths(19,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(20).scans = cellstr([con_paths(20,1);con_paths(20,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(21).scans = cellstr([con_paths(21,1);con_paths(21,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(22).scans = cellstr([con_paths(22,1);con_paths(22,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(23).scans = cellstr([con_paths(23,1);con_paths(23,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(24).scans = cellstr([con_paths(24,1);con_paths(24,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(25).scans = cellstr([con_paths(25,1);con_paths(25,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(26).scans = cellstr([con_paths(26,1);con_paths(26,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(27).scans = cellstr([con_paths(27,1);con_paths(27,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(28).scans = cellstr([con_paths(28,1);con_paths(28,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(29).scans = cellstr([con_paths(29,1);con_paths(29,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.pair(30).scans = cellstr([con_paths(30,1);con_paths(30,2)]);
    matlabbatch{1}.spm.stats.factorial_design.des.pt.gmsca = 0;
    matlabbatch{1}.spm.stats.factorial_design.des.pt.ancova = 0;
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(output_dir);
    
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([output_dir '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = [1 -1];
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = [-1 1];
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 1;
    spm_jobman('run', matlabbatch)

end