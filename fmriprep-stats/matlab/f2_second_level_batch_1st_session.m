%% Batch for 2nd level analysis %%

data_path = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/Control_group_1st_session_FD07_groupGMmask_covs_Age_GMV/';
spm_path = '/data/p_02221/spm12';

GM_mask = '/data/p_02221/Scripts/fmriprep-stats/matlab/GM_spm_0.3.nii';

cov_file = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/covs_Age_Agesq_GMV_TIV.txt';

% RT = '/data/pt_02004/MDN_LANG/Derivatives/group_statistics/RT_JE.txt';

      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');
  
%% AllStimuli>Rest
con_path = [data_path 'AllStimuli-rest'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 

   
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllStimuli>Rest';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
%% AllTasks>Errors
% con_path = [data_path 'AllTasks-Errors'];
% 
% con_images = dir([con_path '/con_*.nii']);
%         Vols = {};
%         for j = 1:numel(con_images)
%             Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
%         end
% 
%     clear matlabbatch
%     matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
%     matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
% 
%     %
%     matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
%     matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
%     matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
%     matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
%     matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
%     matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
%     matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
%     matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
% 
%     % Navigate to output directory, specify and estimate GLM
%     spm_jobman('run', matlabbatch)
%     cd(con_path);
% 
%     load SPM;
%     spm_spm(SPM);
% 
%     clear matlabbatch
%     spmmat = dir([con_path '/SPM.mat']);
%     matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
%     matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'AllTasks>Errors';
%     matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
%     matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
%     matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'Errors>AllTasks';
%     matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
%     matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
%     matlabbatch{1}.spm.stats.con.delete = 0;
%     spm_jobman('run', matlabbatch)
    
%% ControlTask>Rest
con_path = [data_path 'ControlTask-rest'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'ControlTask>Rest';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
%% FPM_ControlTask
con_path = [data_path 'FPM-ControlTask'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'FPM>ControlTask';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'ControlTask>FPM';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    

%% FPM_rest
con_path = [data_path 'FPM-rest'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'FPM>rest';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
    
%% FPM_WPM
con_path = [data_path 'FPM-WPM'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'FPM>WPM';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'WPM>FPM';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
    
%% FPM+WPM_ControlTask
con_path = [data_path 'WPM+FPM-ControlTask'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM+FPM>ControlTask';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'ControlTask>WPM+FPM';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
    
%% FPM+WPM_rest
con_path = [data_path 'WPM+FPM-rest'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM+FPM>rest';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'rest>WPM+FPM';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)
    
    

%% WPM_ControlTask
con_path = [data_path 'WPM-ControlTask'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM>ControlTask';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'ControlTask>WPM';
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
    matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)    
    
    
%% WPM_rest
con_path = [data_path 'WPM-rest'];

con_images = dir([con_path '/con_*.nii']);
        Vols = {};
        for j = 1:numel(con_images)
            Vols(j,:) = cellstr([con_path '/' con_images(j).name]);
        end
        
    clear matlabbatch
    matlabbatch{1}.spm.stats.factorial_design.dir = {con_path};
    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;
                                                              
    %
    matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {cov_file};
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
    matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
    matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
    matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
    matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
    
    % Navigate to output directory, specify and estimate GLM
    spm_jobman('run', matlabbatch)
    cd(con_path);
     
    load SPM;
    spm_spm(SPM);
    
    clear matlabbatch
    spmmat = dir([con_path '/SPM.mat']);
    matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'WPM>rest';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{1}.spm.stats.con.delete = 0;
    spm_jobman('run', matlabbatch)    