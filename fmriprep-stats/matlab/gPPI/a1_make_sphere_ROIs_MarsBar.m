%% MarsBar – Create ROIs

clear all
% close all

%% Set-up

% SPM12
addpath('/data/p_02221/spm12/')
spm('defaults', 'FMRI');
spm_jobman('initcfg');

% MarsBar
addpath /data/p_02221/spm12/toolbox/marsbar-0.45/
marsbar('on')

% Group directory
group_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/';

% Output directory
out_dir = [group_dir 'peak_ROIs/'];
if ~exist(out_dir)
    mkdir(out_dir)
end


%% Set general options

sphereRadius = 10; % mm

ROI_file = [group_dir 'gPPI_ROIs_202307.csv'];
ROIs = readtable(ROI_file, 'Delimiter', ',');


%% Make ROIs
fprintf('\n');

for iROI=1:size(ROIs, 1)
    
    ROI_name = cell2mat(ROIs.ROI_name(iROI))
    con_name = cell2mat(ROIs.contrast_name(iROI));
    ROI_coordinates = [ROIs.x(iROI) ROIs.y(iROI) ROIs.z(iROI)];
%     name_coord = ROIs(i, 4);

    fprintf('Working on ROI %d/%d: %s ... ', iROI, size(ROIs,1), ROI_name);

%     % Load cluster ROI for trimming
%     cluster_roi = [spm_folder roi_label '_roi.mat'];
%     act_roi = maroi('load', cluster_roi); % FWE-corrected at peak level (p<0.05) for Categories>Counting; p<0.001, FWE-corrected at cluster level for Counting>Categories
    
    % Make sphere ROI
    sphere_roi = maroi_sphere(struct('centre', ROI_coordinates, 'radius', sphereRadius, 'label', ROI_name));

%     % Combine for trimmed ROI
%     trim_stim = sphere_roi & act_roi;
%     trim_stim = label(trim_stim, roi_label);

    outName = fullfile(out_dir, sprintf('%s_%s_sphere_%dmm_roi', ROI_name, con_name, sphereRadius));

    % save MarsBaR ROI (.mat) file
%     saveroi(sphere_roi, [outName '.mat']);
    % save the Nifti (.nii) file
    save_as_image(sphere_roi, [outName '.nii']);

    fprintf('done.\n');

end

fprintf('\nAll done. %d ROIs written to %s.', size(ROIs,1), out_dir);