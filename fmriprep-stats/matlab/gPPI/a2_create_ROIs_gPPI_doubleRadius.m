% VOI extraction based on individual peaks
% load first level first, then jump to individual peak within ROI (10 mm radius) and
% draw sphere (5 mm). save and move files to individual gppi folder. 
%*mask.nii will be used in the gPPI analysis to extract eigenvalues

clear all
close all

%% Set-up
%Setup SPM12
addpath('/data/p_02221/spm12/')

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% define main folder
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';

% group directory
group_dir = [root_dir 'group_statistics/gPPI/'];

% define subjects
sub_folders = dir([root_dir 'subjects/sub-control*']);
subjects_1run = {'sub-control006', 'sub-control027'}; 
% sub-control006_ses-2_task-semanticmatching_run-1_bold
% sub-control027_ses-1_task-semanticmatching_run-1_bold

% define first level directory
firstlevel_dir = '1st_level_semanticmatching';
ppi_folder = 'gPPI';

% sessions and runs
n_sessions = 3;
% run = {[1] [2]};

% read table with ROI information
ROI_table = [root_dir 'group_statistics/gPPI/gPPI_ROIs_202206.csv'];
ROIs = readtable(ROI_table);

% ROI_coords =  {[-9 15 51] [-31 25 4] [31 27 2] [-44 5 35] [-34 0 57] [43 35 32] [18 -80 7] [-11 -72 10] [-14 -65 51] [51 12 -31] [6 -52 38]};
sphere = 5; % to match original analysis, according to smoothing level
threshold = {[0.001] [0.01] [0.05]}; 

%% go through all subfolders and load first level SPM for later VOI extraction

for iROI = 1:numel(ROIs.ROI_name)
    
    % extract information about ROI
    ROI_coordinates = [ROIs.x(iROI) ROIs.y(iROI) ROIs.z(iROI)];
    ROI_name = cell2mat(ROIs.ROI_name(iROI));
    ROI_contrast = cell2mat(ROIs.contrast_name(iROI));
                    
    for ithresh = 1:numel(threshold)
        
        % open output files
        fid1=fopen([group_dir 'gPPI_subjects_without_ROI_thr_' num2str(threshold{ithresh}) '.txt'],'a');
        fid2=fopen([group_dir 'gPPI_indivROI_' ROI_name '_' ROI_contrast '_5mm_double_radius.txt'],'a');
        
        for isub = 1:numel(sub_folders)
        
            if isub == 1 && iROI == 1 
                fprintf(fid1, 'subject\tsession\trun\tROI\tcontrast\n');
            end

            if isub == 1 && ithresh == 1
                fprintf(fid2, 'subject\tsession\trun\tthreshold\tROI size\tx\ty\tz\n');
            end

            sub_name = sub_folders(isub).name;
        
            for isess = 1:n_sessions
                
                if strcmp(sub_name, 'sub-control006') & isess == 2                    
                    run = {[2]};
                elseif strcmp(sub_name, 'sub-control027') & isess == 1
                    run = {[2]};
                else
                    run = {[1] [2]};
                end
                
                for irun = 1:numel(run)
        
                    % Display which participant, session, and run is currently processed
                    X = ['This is participant ' sub_name ' and session ' num2str(isess) ' and run ' num2str(run{irun}) ...
                        ' and ROI ' cell2mat(ROIs.ROI_name(iROI)) ' and threshold ' num2str(threshold{ithresh})];
                    disp(X);
            
                    curr_sub_dir = [sub_folders(isub).folder '/' sub_folders(isub).name];
                    curr_sess_dir = [curr_sub_dir filesep 'ses-' num2str(isess)];
                    
                    % create gPPI directory 
                    PPI_folder_path = [curr_sess_dir filesep ppi_folder filesep 'gPPI_individualROI_' cell2mat(ROIs.ROI_name(iROI)) '_' ...
                        cell2mat(ROIs.contrast_name(iROI)) '_th' num2str(threshold{ithresh}) '_5mm'];
                    if ~exist(PPI_folder_path)
                        mkdir(PPI_folder_path)
                    end 

                    % go into subjects' first level folder
                    firstLevelFolder_dir = [curr_sess_dir filesep firstlevel_dir];
%                     cd(firstLevelFolder_dir);

                    % define SPM path
                    SPM_mat_path = [firstLevelFolder_dir filesep 'SPM.mat'];

                    % define which contrast to use, depending on the ROI
                    contrast = ROIs.contrast(iROI);

%%   % VOI: EXTRACTING TIME SERIES
%==========================================================================
%% find nearst activated voxel for each participant (give coordinate from group peak)
             
% 1. It searches the nearest voxel to group level peak
%    for specific contrast and threshold for specific subject in MASK
% 2. It creates a txt file with nearest voxel per subject
%
% IMPORTANT: number of voxels in ROI changes, according to the activation
% cluster size      

                    matlabbatch{1}.spm.util.voi.spmmat = {SPM_mat_path};
                    matlabbatch{1}.spm.util.voi.adjust = 14; % adjust for EOI FULL
                    matlabbatch{1}.spm.util.voi.session = run{irun};
                    matlabbatch{1}.spm.util.voi.name = [ROI_name '_' ROI_contrast '_double_radius'];
                    
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.spmmat = {''};
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.contrast = contrast; 
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.conjunction = 1;
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.threshdesc = 'none';
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.thresh = threshold{ithresh}; % 
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.extent = 0;
                    matlabbatch{1}.spm.util.voi.roi{1}.spm.mask = struct('contrast', {}, 'thresh', {}, 'mtype', {});

                    matlabbatch{1}.spm.util.voi.roi{2}.sphere.centre = [ROI_coordinates];
                    matlabbatch{1}.spm.util.voi.roi{2}.sphere.radius = 10;
                    matlabbatch{1}.spm.util.voi.roi{2}.sphere.move.fixed = 1;
                   
                    % Define smaller inner sphere which jumps to the peak of the outer sphere
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.centre = [0 0 0]; % Leave this at zero
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.radius = sphere;       % Set radius here (mm)
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.spm = 1;       % Index of SPM within the batch
                    matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.mask = 'i2';    % Index of the outer sphere within the batch
                    
                    matlabbatch{1}.spm.util.voi.expression = 'i1 & i3';
                    
                    spm_jobman('run',matlabbatch);
                    
%% find next voxel (give coordinate from group peak)
                    %[xyz] = spm_XYZreg('NearestXYZ',[ROI_coords{iRoi}],xSPM.XYZmm);
                    [xyz] = xY.xyz;
                    % print to txt to know the peak for each subject and
                    % session
                    
                    if isempty(xyz)
                        W = ['This is participant ' sub_name ' and session ' num2str(isess) ' and run ' num2str(run{irun}) ...
                        ' and ROI ' cell2mat(ROIs.ROI_name(iROI)) ' and threshold ' num2str(threshold{ithresh}) '. The ROI is empty.'];
                        disp(W);

                        cd(group_dir);
                        fprintf(fid1,'%s\t%d\t%d\t%s\t%s\n', sub_name, isess, run{irun}, ROI_name, ROI_contrast);                    

                        movefile([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_mask.nii'], ...
                            [firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '_mask.nii']);
                        voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '_mask.nii']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);

                        voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '_eigen.nii']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);
                    else
                        
                        cd(group_dir)
                        fprintf(fid2,'%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', sub_name, isess, run{irun}, threshold{ithresh}, size(xY.XYZmm,2), xyz(1), xyz(2), xyz(3));
 
                        %% move files from first level folder to PPI folder
                        % voi_file = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_th' num2str(threshold{ithresh}) '_nearest_6mm_FINAL_1.mat']);
                        % for some reason the name of the VOIs is not as called above...
                        voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '.mat']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);

                        movefile([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_mask.nii'], ...
                            [firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '_mask.nii']);
                        voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '_mask.nii']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);
%                         voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_mask.nii']);
%                         voi_file = [voi_folder.folder '/' voi_folder.name];
%                         movefile(voi_file, PPI_folder_path);

                        voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_' num2str(run{irun}) '_eigen.nii']);
                        voi_file = [voi_folder.folder '/' voi_folder.name];
                        movefile(voi_file, PPI_folder_path);
                    end

                end
            end
        end
    end
end

fclose(fid1);
fclose(fid2);