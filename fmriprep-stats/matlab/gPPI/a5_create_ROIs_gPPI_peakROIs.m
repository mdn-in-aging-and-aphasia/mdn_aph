%% Create individual ROIs %%
% written by Sandra Martin, 06/22
% This script uses spherical masks/ROIs based on peak coordinates (e.g., coming from a second level analysis) in standard space and extracts the
% top percent voxels above 0 (T values) with respect to a specified task contrast

clear all
close all

%% Set-up
%Setup SPM12
addpath('/data/p_02221/spm12/')

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% define main folder
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';

% group directory
group_dir = [root_dir 'group_statistics/gPPI/'];

% define subjects
sub_folders = dir([root_dir 'subjects/sub-control*']);
subjects_1run = {'sub-control006', 'sub-control027'}; 
% sub-control006_ses-2_task-semanticmatching_run-1_bold
% sub-control027_ses-1_task-semanticmatching_run-1_bold

% define first level directory
firstlevel_dir = '1st_level_semanticmatching';
ppi_folder = 'gPPI';

% sessions and runs
n_sessions = 1;
% run = {[1] [2]};

% get cluster masks/ROIs
FPM = dir([root_dir 'group_statistics/gPPI/peak_ROIs/r*_FPM>*.nii']);
FPM = FPM([1,2,6]);
WPM = dir([root_dir 'group_statistics/gPPI/peak_ROIs/r*_WPM>*.nii']);
WPM = WPM([1,3]);

ROIs = [WPM; FPM];

% ROIs = dir("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/peak_ROIs/rRParOperc_WPM>Rest_sphere_10mm_roi.nii");
% ROI_table = [root_dir 'group_statistics/gPPI/gPPI_ROIs_clusterROIs_202206.csv'];

% sphere – only relevant for p value thresholds
sphere = 5; % to match original analysis, according to smoothing level

% pattern to extract contrast
pat = optionalPattern(lettersPattern + '+') + lettersPattern + '>' + lettersPattern;

% How to threshold the ROI
threshold_type = 'top_percent'; % 'p_value' or 'top_percent' (top % of activated voxels)
if strcmp(threshold_type,'p_value')
%     T_thresholds = [3.125731 2.342528 1.651448]; % set T-values that correspond to selected p-values
    threshold = {[0.001] [0.01] [0.05]}; 
elseif strcmp(threshold_type,'top_percent')
    top_percent = 25; % percentage of voxels with highest activity
end


%% loop through ROIs

for iROI = 1:size(ROIs, 1)
    
%     extract information about ROI
    ROI_name = ROIs(iROI).name
    ROI_name = ROI_name(2:end-20);
    ROI_contrast = cell2mat(extract(ROI_name, pat));
    switch ROI_contrast
        case 'WPM+FPM>ControlTask'
            ROI_con_image = 'spmT_0006';
        case 'WPM+FPM>Rest'
            ROI_con_image = 'spmT_0002';
        case 'WPM>Rest'
            ROI_con_image = 'spmT_0003';
        case 'FPM>Rest'
            ROI_con_image = 'spmT_0004';
        case 'WPM>Tone'
            ROI_con_image = 'spmT_0010';
        case 'FPM>Tone'
            ROI_con_image = 'spmT_0012';
    end
    
    ROI_path = [ROIs(iROI).folder filesep ROIs(iROI).name];
    
     % calculate number of voxels in ROI
    V_ROI = spm_vol(ROI_path);
    [Y_ROI,XYZ] = spm_read_vols(V_ROI);
    n_voxels_in_ROI = sum(Y_ROI(:)>0)
                    
    if strcmp(threshold_type,'p_value') % threshold using p-value
    
        for ithresh = 1:numel(threshold)

            % open output files
            fid1=fopen([group_dir 'gPPI_subjects_without_ROI_thr_' num2str(threshold{ithresh}) '.txt'],'a');
            fid2=fopen([group_dir 'gPPI_indivROI_' ROI_name '_5mm_double_radius.txt'],'a');

            for isub = 1:numel(sub_folders)

                if isub == 1 && iROI == 1 
                    fprintf(fid1, 'subject\tsession\trun\tROI\tcontrast\n');
                end

                if isub == 1 && ithresh == 1
                    fprintf(fid2, 'subject\tsession\trun\tcontrast\tthreshold\tROI size\tx\ty\tz\n');
                end

                sub_name = sub_folders(isub).name;

                for isess = 1:n_sessions

                    if strcmp(sub_name, 'sub-control006') & isess == 2                    
                        run = {[2]};
                    elseif strcmp(sub_name, 'sub-control027') & isess == 1
                        run = {[2]};
                    else
                        run = {[1] [2]};
                    end

                    for irun = 1:numel(run)

                        % Display which participant, session, and run is currently processed
                        X = ['This is participant ' sub_name ' and session ' num2str(isess) ' and run ' num2str(run{irun}) ...
                            ' and ROI ' ROI_name ' and threshold ' num2str(threshold{ithresh})];
                        disp(X);

                        curr_sub_dir = [sub_folders(isub).folder '/' sub_folders(isub).name];
                        curr_sess_dir = [curr_sub_dir filesep 'ses-' num2str(isess)];

                        con = ROI_contrast;

                        % create gPPI directory 
                        PPI_folder_path = [curr_sess_dir filesep ppi_folder filesep 'gPPI_individualROI_' ROIs{iROI} '_' ...
                            con_name '_th' num2str(threshold{ithresh}) '_5mm'];
                        if ~exist(PPI_folder_path)
                            mkdir(PPI_folder_path)
                        end 

                        % go into subjects' first level folder
                        firstLevelFolder_dir = [curr_sess_dir filesep firstlevel_dir];
    %                     cd(firstLevelFolder_dir);

                        % define SPM path
                        SPM_mat_path = [firstLevelFolder_dir filesep 'SPM.mat'];

                        % define which contrast to use, depending on the ROI
%                         con = ROIs.contrast(iROI);

    %%   % VOI: EXTRACTING TIME SERIES
    %==========================================================================
    %% find nearst activated voxel for each participant (give coordinate from group peak)

    % 1. It searches the nearest voxel to group level peak
    %    for specific contrast and threshold for specific subject in MASK
    % 2. It creates a txt file with nearest voxel per subject
    %
    % IMPORTANT: number of voxels in ROI changes, according to the activation
    % cluster size      

                        clear matlabbatch
                        cd(firstLevelFolder_dir)
                        matlabbatch{1}.spm.util.voi.spmmat = {SPM_mat_path};
                        matlabbatch{1}.spm.util.voi.adjust = 14; % adjust for EOI FULL
                        matlabbatch{1}.spm.util.voi.session = run{irun};
                        matlabbatch{1}.spm.util.voi.name = [ROIs{iROI} '_' con_name '_double_radius'];

                        matlabbatch{1}.spm.util.voi.roi{1}.spm.spmmat = {''};
                        matlabbatch{1}.spm.util.voi.roi{1}.spm.contrast = con; 
                        matlabbatch{1}.spm.util.voi.roi{1}.spm.conjunction = 1;
                        matlabbatch{1}.spm.util.voi.roi{1}.spm.threshdesc = 'none';
                        matlabbatch{1}.spm.util.voi.roi{1}.spm.thresh = threshold{ithresh}; % 
                        matlabbatch{1}.spm.util.voi.roi{1}.spm.extent = 0;
                        matlabbatch{1}.spm.util.voi.roi{1}.spm.mask = struct('contrast', {}, 'thresh', {}, 'mtype', {});

                        matlabbatch{1}.spm.util.voi.roi{2}.mask.image = {[ROIs{iROI} '_mask.nii']};
    %                     matlabbatch{1}.spm.util.voi.roi{2}.sphere.centre = [ROI_coordinates];
    %                     matlabbatch{1}.spm.util.voi.roi{2}.sphere.radius = 10;
    %                     matlabbatch{1}.spm.util.voi.roi{2}.sphere.move.fixed = 1;

                        % Define smaller inner sphere which jumps to the peak of the outer sphere
                        matlabbatch{1}.spm.util.voi.roi{3}.sphere.centre = [0 0 0]; % Leave this at zero
                        matlabbatch{1}.spm.util.voi.roi{3}.sphere.radius = sphere;       % Set radius here (mm)
                        matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.spm = 1;       % Index of SPM within the batch
                        matlabbatch{1}.spm.util.voi.roi{3}.sphere.move.local.mask = 'i2';    % Index of the outer sphere within the batch

                        matlabbatch{1}.spm.util.voi.expression = 'i1 & i3';

                        spm_jobman('run',matlabbatch);

    %% find next voxel (give coordinate from group peak)
                        %[xyz] = spm_XYZreg('NearestXYZ',[ROI_coords{iRoi}],xSPM.XYZmm);
                        [xyz] = xY.xyz;
                        % print to txt to know the peak for each subject and
                        % session

                        if isempty(xyz)
                            W = ['This is participant ' sub_name ' and session ' num2str(isess) ' and run ' num2str(run{irun}) ...
                            ' and ROI ' ROIs{iROI} ' and threshold ' num2str(threshold{ithresh}) '. The ROI is empty.'];
                            disp(W);

                            cd(group_dir);
                            fprintf(fid1,'%s\t%d\t%d\t%s\t%s\n', sub_name, isess, run{irun}, ROIs{iROI}, con_name);                    

                            movefile([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_mask.nii'], ...
                                [firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '_mask.nii']);
                            voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '_mask.nii']);
                            voi_file = [voi_folder.folder '/' voi_folder.name];
                            movefile(voi_file, PPI_folder_path);

                            voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '_eigen.nii']);
                            voi_file = [voi_folder.folder '/' voi_folder.name];
                            movefile(voi_file, PPI_folder_path);
                        else

                            cd(group_dir)
                            fprintf(fid2,'%s\t%d\t%d\t%s\t%d\t%d\t%d\t%d\t%d\n', sub_name, isess, run{irun}, con_name, threshold{ithresh}, size(xY.XYZmm,2), xyz(1), xyz(2), xyz(3));

                            %% move files from first level folder to PPI folder
                            % voi_file = dir([firstLevelFolder_path filesep 'VOI_' ROI_names{iRoi} '_th' num2str(threshold{ithresh}) '_nearest_6mm_FINAL_1.mat']);
                            % for some reason the name of the VOIs is not as called above...
                            voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '.mat']);
                            voi_file = [voi_folder.folder '/' voi_folder.name];
                            movefile(voi_file, PPI_folder_path);

                            movefile([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_mask.nii'], ...
                                [firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '_mask.nii']);
                            voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '_mask.nii']);
                            voi_file = [voi_folder.folder '/' voi_folder.name];
                            movefile(voi_file, PPI_folder_path);
    %                         voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROI_name '_' ROI_contrast '_double_radius_mask.nii']);
    %                         voi_file = [voi_folder.folder '/' voi_folder.name];
    %                         movefile(voi_file, PPI_folder_path);

                            voi_folder = dir([firstLevelFolder_dir filesep 'VOI_' ROIs{iROI} '_' con_name '_double_radius_' num2str(run{irun}) '_eigen.nii']);
                            voi_file = [voi_folder.folder '/' voi_folder.name];
                            movefile(voi_file, PPI_folder_path);
                        end
                    end
                end
            end
        end
    
    elseif strcmp(threshold_type,'top_percent') % threshold using percentage of most activated voxels (top T-values)

        % open output files
        fid2=fopen([group_dir 'gPPI_indivROI_' ROI_name '_10mm_sphere_top_' num2str(top_percent) 'percent.txt'],'a');

        for isub = 1:numel(sub_folders)

            if isub == 1
                fprintf(fid2, 'subject\tsession\tcontrast\tROI_size_all_vx\tROI_size_above0\n');
            end

            sub_name = sub_folders(isub).name;

            for isess = 1:n_sessions

                % Display which participant, session, and run is currently processed
                X = ['This is participant ' sub_name ' and session ' num2str(isess) ' and ROI ' ROI_name];
                disp(X);

                curr_sub_dir = [sub_folders(isub).folder '/' sub_folders(isub).name];
                curr_sess_dir = [curr_sub_dir filesep 'ses-' num2str(isess)];

                % create gPPI directory 
                PPI_folder_path = [curr_sess_dir filesep ppi_folder filesep 'gPPI_individualROI_' ROI_name ...
                    '_10mm_sphere_top_' num2str(top_percent) 'percent_voxel'];
                if ~exist(PPI_folder_path)
                    mkdir(PPI_folder_path)
                end 

                % go into subjects' first level folder
                firstLevelFolder_dir = [curr_sess_dir filesep firstlevel_dir];
                curr_Tmap = dir([firstLevelFolder_dir '/' ROI_con_image '*nii']);

                if numel(curr_Tmap) ~= 1
                    error(['Error: Not exactly 1 T-map found at ' ...
                        firstLevelFolder_dir '/' ROI_con_image '*nii'])
                end

                curr_Tmap_path = [curr_Tmap.folder '/' curr_Tmap.name];

                %% create image of voxels in ROI
                clear matlabbatch
                matlabbatch{1}.spm.util.imcalc.input = {
                                                curr_Tmap_path
                                                ROI_path
                                                };
                matlabbatch{1}.spm.util.imcalc.output = ['VOI_' ROI_name '_T_image.nii'];
                matlabbatch{1}.spm.util.imcalc.outdir = {PPI_folder_path};
                matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
                matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
                matlabbatch{1}.spm.util.imcalc.options.mask = -1;
                matlabbatch{1}.spm.util.imcalc.options.interp = 0;
                matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

                spm_jobman('run',matlabbatch);

                % get image
                Tmap_ROI = dir([PPI_folder_path '/' 'VOI_' ROI_name '_T_image.nii']);

                if numel(Tmap_ROI) ~= 1
                error(['Error: Not exactly 1 ROI-Tmap found at ' ...
                    PPI_folder_path '/' 'VOI_' ROI_name '_T_image.nii'])
                end

                Tmap_ROI_path = [Tmap_ROI.folder '/' Tmap_ROI.name];

                % get Tmap in ROI
                clear V Y n_voxels Y_sorted idx_sorted idx_top_voxels Y_topVoxels V_ROI
                V = spm_vol(Tmap_ROI_path);
                [Y,XYZ] = spm_read_vols(V);

                n_top_voxels = round((n_voxels_in_ROI./100) .* top_percent);

                % set all non-ROI voxels to minus-Infinity to ensure that only
                % voxels within the ROI are picked
                Y(Y_ROI(:)==0) = -Inf;

                % sort the ROI-voxels by T-values in descending order
                [Y_sorted,idx_sorted] = sort(Y(:),'descend');

                % get indices of the n top voxels within the ROI with T>0
                idx_top_voxels = idx_sorted(1:n_top_voxels);
                idx_top_voxels_above0 = idx_sorted(Y_sorted(1:n_top_voxels)>0);
                Y_sorted(Y_sorted(1:n_top_voxels)>0);

                % write out mask file in individual session-related PPI
                % folder
                VOI_mask_outfile = [PPI_folder_path '/' 'VOI_' ROI_name '_mask.nii'];

                if numel(idx_top_voxels_above0) > 0
                    Y_topVoxels = Y;
                    Y_topVoxels(:) = 0;
                    Y_topVoxels(idx_top_voxels_above0) = 1;

                    V_topVoxels = struct('fname',VOI_mask_outfile,'mat',V.mat,'dim',V.dim,...
                        'dt',[spm_type('float32') spm_platform('bigend')],'pinfo',[1;0;0]);
                    spm_write_vol(V_topVoxels,Y_topVoxels); 
                end

                cd(group_dir)
                fprintf(fid2,'%s\t%d\t%s\t%d\t%d\n', sub_name, isess, ROI_contrast, numel(idx_top_voxels), numel(idx_top_voxels_above0));

            end
        end
    end
end

% fclose(fid1);
fclose(fid2);