%% gPPI – first level analysis 
% written by Sandra Martin, 06/22

clear
close all

%% Set-up

%Setup SPM12
addpath('/data/p_02221/spm12/')
spm('Defaults','fMRI');
spm_jobman('initcfg');

% PPPI
addpath /data/p_02221/spm12/toolbox/PPPI/

% define main folder
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';

% group directory
group_dir = [root_dir 'group_statistics/gPPI/'];

% define subjects
sub_folders = dir([root_dir 'subjects/sub-control*']);
% subjects_1run = {'sub-control006', 'sub-control027'}; 
% sub-control006_ses-2_task-semanticmatching_run-1_bold
% sub-control027_ses-1_task-semanticmatching_run-1_bold

% define first level directory
firstlevel = '1st_level_semanticmatching';
ppi_folder = 'gPPI';

% sessions and runs
% n_sessions = 3;
sessions = {2, 3};
stim_sessions = {'sham-active'}; %'active', 'sham', 'active-sham'

% Table with stimulation order
stim_order_tbl = readtable("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/stim_order.txt", 'ReadVariableNames', true, 'Delimiter', 'tab', 'Format', ...
    '%s%s%s%s');

% List of ROIs 
% FPM = dir([group_dir 'threshold_top_percent_voxels/*_FPM>*.txt']);
% WPM = dir([group_dir 'threshold_top_percent_voxels/*WPM>*.txt']);
% ROI_info = [WPM;FPM];

ROI1 = dir('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/*LSPL_*2023');
ROI2 = dir('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/*LOccipitalPole*_2023');
ROI_info = [ROI1;ROI2];

% ROI_info = dir("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/peak_ROIs/rRParOperc_WPM>Rest_sphere_10mm_roi.nii");

% ROI_info = dir([group_dir 'atlas_ROIs/*_25percent.txt']);
%ROI_info = dir([group_dir 'threshold_top_percent_voxels/*.txt']);

%% Loop over ROIs and subjects
for iROI = 1:numel(ROI_info)
    
    ROI_name = ROI_info(iROI).name;
%     ROI_name = ROI_name(2:end-20)

    % Set the second level directory according to ROI
    secondlevel_folder = [group_dir ROI_name];
    if ~exist(secondlevel_folder, 'dir')
        mkdir(secondlevel_folder)
    end
    
    % Create contrast folders
    cons = {'WPM+FPM>rest', 'Tone>rest', 'WPM+FPM>Tone', 'Tone>WPM+FPM', 'FPM>Tone', 'FPM>WPM', 'WPM>FPM', 'FPM>rest', 'WPM>rest', 'Tone>FPM'}; %'WPM>Tone'
%     cons = {'WPM+FPM>ControlTask'};
    for icon = 1:numel(cons)
        for istim = 1:numel(stim_sessions)
            switch stim_sessions{istim}
                case 'active-sham'
                    con_dir = [secondlevel_folder filesep stim_sessions{istim} filesep cons{icon} filesep 'con_imgs'];
                    if ~exist(con_dir, 'dir')
                        mkdir(con_dir)
                    end
                otherwise
                    con_dir = [secondlevel_folder filesep stim_sessions{istim} filesep cons{icon}];
                    if ~exist(con_dir, 'dir')
                        mkdir(con_dir)
                    end
            end
        end
    end
    
    for isub = 1:numel(sub_folders)
        
        sub_name = sub_folders(isub).name;
        
%         for isess = 1:numel(sessions)
%             % Display which participant and which run is currently processed
%             X = ['This is participant ' sub_name ' and session ' num2str(sessions{isess}) ' and ROI ' ROI_name];
%             disp(X);

%             %Set the firstlevel directory
%             curr_sub_dir = [sub_folders(isub).folder filesep sub_folders(isub).name];
%             curr_sess_dir = [curr_sub_dir filesep 'ses-' num2str(sessions{isess})];
%             firstLevel_dir = [curr_sess_dir filesep firstlevel];
% 
%             % ROI folder
%             curr_ROI_dir = dir([curr_sub_dir filesep 'ses-1/gPPI' filesep 'gPPI_individualROI_' ROI_name '_10mm_sphere_top_25percent_voxel' filesep '*mask.nii']);
%             curr_ROI = [curr_ROI_dir.folder filesep curr_ROI_dir.name];
%             if ~isfile(curr_ROI)
%                fprintf('No voxels in the ROI\n');
%                continue;
%             end 

%             if ~isfile(curr_ROI) && sessions{isess} == 2
%                 fprintf('No voxels in the ROI. Trying session 3.\n');
%                 curr_ROI_dir = dir([curr_sub_dir filesep 'ses-3/gPPI' filesep 'gPPI_individualROI_' ROI_name '_top_10percent_voxel' filesep '*mask.nii']);
%                 curr_ROI = [curr_ROI_dir.folder filesep curr_ROI_dir.name];
%                 outdir = [curr_sub_dir filesep 'ses-2/gPPI' filesep  'gPPI_individualROI_' ROI_name '_top_10percent_voxel'];   
%                 if ~isfile(curr_ROI)
%                     fprintf('Also no voxels in session 3. Skipping participant.\n')
%                     continue;
%                 end
%             elseif ~isfile(curr_ROI) && sessions{isess} == 3
%                 fprintf('No voxels in the ROI. Trying session 2.\n');
%                 curr_ROI_dir = dir([curr_sub_dir filesep 'ses-2/gPPI' filesep 'gPPI_individualROI_' ROI_name '_top_10percent_voxel' filesep '*mask.nii']);
%                 curr_ROI = [curr_ROI_dir.folder filesep curr_ROI_dir.name];
%                 outdir = [curr_sub_dir filesep 'ses-3/gPPI' filesep  'gPPI_individualROI_' ROI_name '_top_10percent_voxel'];  
%                 if ~isfile(curr_ROI)
%                     fprintf('Also no voxels in session 2. Skipping participant.\n')
%                     continue;
%                 end
%             else
%                 outdir = curr_ROI_dir.folder;   
%             end
% 
%             curr_output_dir = [curr_sess_dir filesep 'gPPI' filesep 'gPPI_individualROI_' ROI_name '_WPM+FPM>Rest_top_25percent_voxel_ROIFromSession1'];
%             if ~exist(curr_output_dir, 'dir')
%                 mkdir(curr_output_dir)
%             end
%             
%             %% Configure the PPPI Parameter structure
%             P.subject = sub_name;
%             P.directory = firstLevel_dir;
%             P.VOI = curr_ROI;
%             %P.region 
%             %P.FLmask 
%             %P.VOI = regionfile;
%             P.maskdir = curr_output_dir;
%             P.outdir = curr_output_dir;
%             P.Estimate = 1;
%             P.Contrast = 'Omnibus F-test for PPI Analyses'; % use FULL F-contrast 
%             P.extract = 'eig';
%             P.equalroi = 0;
%             P.FLmask = 1;
%             P.Tasks = {'0' 'WPM_corr' 'FPM_corr' 'tone_corr' 'Errors'}; % define all conditions of interest INCLUDING FILLERS AND ERROR TRIALS
%             P.Weights = [];
%             P.analysis = 'psy';
%             P.method = 'cond';
%             P.CompContrasts = 1;
%             P.Weighted = 0;
% %             P.GroupDir = secondlevel_folder;
%             
%             P.Contrasts(1).left = {'WPM_corr' 'FPM_corr'};
%             P.Contrasts(1).right = {'none'};
%             P.Contrasts(1).STAT='T';
%             P.Contrasts(1).Weighted=0;
%             P.Contrasts(1).MinEvents=1;
%             P.Contrasts(1).name='WPM+FPM>rest';
%             
%             P.Contrasts(2).left={'tone_corr'};
%             P.Contrasts(2).right={'none'};
%             P.Contrasts(2).STAT='T';
%             P.Contrasts(2).Weighted=0;
%             P.Contrasts(2).MinEvents=10;
%             P.Contrasts(2).name='Tone>rest';
%             
%             P.Contrasts(3).left={'WPM_corr' 'FPM_corr'};
%             P.Contrasts(3).right={'tone_corr'};
%             P.Contrasts(3).STAT='T';
%             P.Contrasts(3).Weighted=0;
%             P.Contrasts(3).MinEvents=1;
%             P.Contrasts(3).name='WPM+FPM>Tone';
%  
%             P.Contrasts(4).left={'tone_corr'};
%             P.Contrasts(4).right={'WPM_corr' 'FPM_corr'};
%             P.Contrasts(4).STAT='T';
%             P.Contrasts(4).Weighted=0;
%             P.Contrasts(4).MinEvents=10;
%             P.Contrasts(4).name='Tone>WPM+FPM';
% 
% %             P.Contrasts(5).left={'WPM_corr'};
% %             P.Contrasts(5).right={'tone_corr'};
% %             P.Contrasts(5).STAT='T';
% %             P.Contrasts(5).Weighted=0;
% %             P.Contrasts(5).MinEvents=1;
% %             P.Contrasts(5).name='WPM>Tone';
% 
%             P.Contrasts(5).left={'FPM_corr'};
%             P.Contrasts(5).right={'tone_corr'};
%             P.Contrasts(5).STAT='T';
%             P.Contrasts(5).Weighted=0;
%             P.Contrasts(5).MinEvents=1;
%             P.Contrasts(5).name='FPM>Tone';
% 
%             P.Contrasts(6).left={'FPM_corr'};
%             P.Contrasts(6).right={'WPM_corr'};
%             P.Contrasts(6).STAT='T';
%             P.Contrasts(6).Weighted=0;
%             P.Contrasts(6).MinEvents=1;
%             P.Contrasts(6).name='FPM>WPM';
% 
%             P.Contrasts(7).left={'WPM_corr'};
%             P.Contrasts(7).right={'FPM_corr'};
%             P.Contrasts(7).STAT='T';
%             P.Contrasts(7).Weighted=0;
%             P.Contrasts(7).MinEvents=1;
%             P.Contrasts(7).name='WPM>FPM';
% 
%             P.Contrasts(8).left={'FPM_corr'};
%             P.Contrasts(8).right={'none'};
%             P.Contrasts(8).STAT='T';
%             P.Contrasts(8).Weighted=0;
%             P.Contrasts(8).MinEvents=1;
%             P.Contrasts(8).name='FPM>rest';
% 
%             P.Contrasts(9).left={'WPM_corr'};
%             P.Contrasts(9).right={'none'};
%             P.Contrasts(9).STAT='T';
%             P.Contrasts(9).Weighted=0;
%             P.Contrasts(9).MinEvents=1;
%             P.Contrasts(9).name='WPM>rest';
% 
%             P.Contrasts(10).left={'tone_corr'};
%             P.Contrasts(10).right={'FPM_corr'};
%             P.Contrasts(10).STAT='T';
%             P.Contrasts(10).Weighted=0;
%             P.Contrasts(10).MinEvents=1;
%             P.Contrasts(10).name='Tone>FPM';
% 
%             %% Run PPPI with the command: PPPI(P)
%             PPPI(P)
% 
%             %% Move PPI stuff to PPI folders
%             ROI = curr_ROI_dir.name(1:end-4);
%             files = dir([firstLevel_dir filesep '*' ROI '*']);
%             
%             for f = 1:numel(files)
%                 movefile([files(f).folder filesep files(f).name], curr_output_dir);
%             end
%             
%             % Grab con images
%             con_img = dir([curr_output_dir filesep 'PPI_' ROI filesep 'con*.nii']);
%             
%             % Determine stimulation order to find out whether second or third session was active or sham, respectively
%             if contains(sub_name, stim_order_tbl.sub_number(isub))
%                 stim_order = stim_order_tbl.order(isub);
%             end
%             
%             if strcmp(stim_order, 'SA') && sessions{isess} == 2
%                 for icon_img = 1:numel(con_img)
%                     for icon = 1:numel(cons)
%                         if contains(con_img(icon_img).name, cons{icon})
% %                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'sham' filesep cons{icon} filesep ...
% %                                 con_img(icon_img).name(1:end-4) '-sham.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep ...
%                                 'con_imgs' filesep con_img(icon_img).name(1:end-4) '-sham.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'sham' filesep cons{icon} filesep ...
%                                 con_img(icon_img).name(1:end-4) '-sham.nii']);
%                         end
%                     end
%                 end
%             elseif strcmp(stim_order, 'SA') && sessions{isess} == 3
%                 for icon_img = 1:numel(con_img)
%                     for icon = 1:numel(cons)
%                         if contains(con_img(icon_img).name, cons{icon})
% %                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active' filesep cons{icon} filesep ...
% %                                 con_img(icon_img).name(1:end-4) '-active.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep ...
%                                 'con_imgs' filesep con_img(icon_img).name(1:end-4) '-active.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active' filesep cons{icon} filesep ...
%                                 con_img(icon_img).name(1:end-4) '-active.nii']);
%                         end
%                     end
%                 end
%             elseif strcmp(stim_order, 'AS') && sessions{isess} == 2
%                 for icon_img = 1:numel(con_img)
%                     for icon = 1:numel(cons)
%                         if contains(con_img(icon_img).name, cons{icon})
% %                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active' filesep cons{icon} filesep ...
% %                                 con_img(icon_img).name(1:end-4) '-active.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep ...
%                                 'con_imgs' filesep con_img(icon_img).name(1:end-4) '-active.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active' filesep cons{icon} filesep ...
%                                 con_img(icon_img).name(1:end-4) '-active.nii']);
%                         end
%                     end
%                 end
%             elseif strcmp(stim_order, 'AS') && sessions{isess} == 3
%                 for icon_img = 1:numel(con_img)
%                     for icon = 1:numel(cons)
%                         if contains(con_img(icon_img).name, cons{icon})
% %                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'sham' filesep cons{icon} filesep ...
% %                                 con_img(icon_img).name(1:end-4) '-sham.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep ...
%                                 'con_imgs' filesep con_img(icon_img).name(1:end-4) '-sham.nii']);
%                             copyfile([con_img(icon_img).folder filesep con_img(icon_img).name], [secondlevel_folder filesep 'sham' filesep cons{icon} filesep ...
%                                 con_img(icon_img).name(1:end-4) '-sham.nii']);
%                         end
%                     end
%                 end
%             end          
%             
%         end
        
%         %% Subtract active – sham image to get difference image
%         for icon = 1:numel(cons)
%             % Get active con-image
%             con_active_path = dir([secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep 'con_imgs' filesep '*_' sub_name '-active.nii']);
%             con_active = [con_active_path.folder filesep con_active_path.name];
% 
%             % Get sham con-image
%             con_sham_path = dir([secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep 'con_imgs' filesep '*_' sub_name '-sham.nii']);
%             con_sham = [con_sham_path.folder filesep con_sham_path.name];
% 
%             % Define output directory
%             output_dir = [secondlevel_folder filesep 'active-sham' filesep cons{icon}];
%             output_name = [sub_name '_active-sham'];
% 
%             if isfile(con_active) && isfile(con_sham)
%                 % Run batch
%                 clear matlabbatch
%                 matlabbatch{1}.spm.util.imcalc.input = {con_active
%                                                         con_sham};
%                 matlabbatch{1}.spm.util.imcalc.output = output_name;
%                 matlabbatch{1}.spm.util.imcalc.outdir = {output_dir};
%                 matlabbatch{1}.spm.util.imcalc.expression = 'i1-i2';
%                 matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
%                 matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
%                 matlabbatch{1}.spm.util.imcalc.options.mask = 0;
%                 matlabbatch{1}.spm.util.imcalc.options.interp = -7;
%                 matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
%                 spm_jobman('run', matlabbatch)
%             else
%                 continue;
%             end
%         end

        %% Subtract sham – active image to get difference image
        for icon = 1:numel(cons)
            % Get active con-image
            con_active_path = dir([secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep 'con_imgs' filesep '*_' sub_name '-active.nii']);
            con_active = [con_active_path.folder filesep con_active_path.name];

            % Get sham con-image
            con_sham_path = dir([secondlevel_folder filesep 'active-sham' filesep cons{icon} filesep 'con_imgs' filesep '*_' sub_name '-sham.nii']);
            con_sham = [con_sham_path.folder filesep con_sham_path.name];

            % Define output directory
            output_dir = [secondlevel_folder filesep 'sham-active' filesep cons{icon}];
            output_name = [sub_name '_sham-active'];

            if isfile(con_active) && isfile(con_sham)
                % Run batch
                clear matlabbatch
                matlabbatch{1}.spm.util.imcalc.input = {con_active
                                                        con_sham};
                matlabbatch{1}.spm.util.imcalc.output = output_name;
                matlabbatch{1}.spm.util.imcalc.outdir = {output_dir};
                matlabbatch{1}.spm.util.imcalc.expression = 'i2-i1';
                matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
                matlabbatch{1}.spm.util.imcalc.options.mask = 0;
                matlabbatch{1}.spm.util.imcalc.options.interp = -7;
                matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
                spm_jobman('run', matlabbatch)
            else
                continue;
            end
        end
        
    end
    
    % clear up folders where con files weren't matched correctly
    FPM_dir = [group_dir 'gPPI_individualROI_' ROI_name '_10mm_sphere_2023' filesep 'sham-active' filesep 'FPM>rest' filesep 'con_imgs'];
    wrong_files = dir([FPM_dir filesep '*WPM+FPM*.nii']);
    for xfile = 1:numel(wrong_files)
        delete([wrong_files(xfile).folder filesep wrong_files(xfile).name])
    end

    for isub = 1:numel(sub_folders)
        sub_name = sub_folders(isub).name;

        con_active_path = dir([FPM_dir filesep '*_' sub_name '-active.nii']);
        con_active = [con_active_path.folder filesep con_active_path.name];
    
        % Get sham con-image
        con_sham_path = dir([FPM_dir filesep '*_' sub_name '-sham.nii']);
        con_sham = [con_sham_path.folder filesep con_sham_path.name];
    
        % Define output directory
        output_dir = [secondlevel_folder filesep 'sham-active' filesep 'FPM>rest'];
        output_name = [sub_name '_active-sham'];
    
        if isfile(con_active) && isfile(con_sham)
                    % Run batch
                    clear matlabbatch
                    matlabbatch{1}.spm.util.imcalc.input = {con_active
                                                            con_sham};
                    matlabbatch{1}.spm.util.imcalc.output = output_name;
                    matlabbatch{1}.spm.util.imcalc.outdir = {output_dir};
                    matlabbatch{1}.spm.util.imcalc.expression = 'i2-i1';
                    matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                    matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
                    matlabbatch{1}.spm.util.imcalc.options.mask = 0;
                    matlabbatch{1}.spm.util.imcalc.options.interp = -7;
                    matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
                    spm_jobman('run', matlabbatch)
        else
            continue;
        end
    end

    FPM_tone_dir = [group_dir 'gPPI_individualROI_' ROI_name '_10mm_sphere_2023' filesep 'sham-active' filesep 'FPM>Tone' filesep 'con_imgs'];
    wrong_files = dir([FPM_tone_dir filesep '*WPM+FPM*.nii']);
    for xfile = 1:numel(wrong_files)
        delete([wrong_files(xfile).folder filesep wrong_files(xfile).name])
    end

    for isub = 1:numel(sub_folders)
        sub_name = sub_folders(isub).name;

        con_active_path = dir([FPM_tone_dir filesep '*_' sub_name '-active.nii']);
        con_active = [con_active_path.folder filesep con_active_path.name];
    
        % Get sham con-image
        con_sham_path = dir([FPM_tone_dir filesep '*_' sub_name '-sham.nii']);
        con_sham = [con_sham_path.folder filesep con_sham_path.name];
    
        % Define output directory
        output_dir = [secondlevel_folder filesep 'sham-active' filesep 'FPM>Tone'];
        output_name = [sub_name '_active-sham'];
    
        if isfile(con_active) && isfile(con_sham)
                    % Run batch
                    clear matlabbatch
                    matlabbatch{1}.spm.util.imcalc.input = {con_active
                                                            con_sham};
                    matlabbatch{1}.spm.util.imcalc.output = output_name;
                    matlabbatch{1}.spm.util.imcalc.outdir = {output_dir};
                    matlabbatch{1}.spm.util.imcalc.expression = 'i2-i1';
                    matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
                    matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
                    matlabbatch{1}.spm.util.imcalc.options.mask = 0;
                    matlabbatch{1}.spm.util.imcalc.options.interp = -7;
                    matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
                    spm_jobman('run', matlabbatch)
        else
            continue;
        end
    end

end


