%% 2nd level analysis for gPPI results %%
% written by Sandra Martin, 06/22

clear
close all

%% Set-up
%Setup SPM12
addpath('/data/p_02221/spm12/');
spm('Defaults','fMRI');
spm_jobman('initcfg');

% define main folder
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats/';

% group directory
ROI1 = dir('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/*LSPL_*2023');
ROI2 = dir('/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/*LOccipitalPole*_2023');
group_dir= [ROI1;ROI2];

% group_dir = dir([root_dir 'group_statistics/gPPI/gPPI_individualROI*_atlasROI_2023']);
% group_dir = dir("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/group_statistics/gPPI/gPPI_individualROI_RParOperc_WPM>Rest_10mm_sphere_2023");

GM_mask = '/data/p_02221/Scripts/fmriprep-stats/matlab/GM_spm_0.3.nii';
% sessions = {'active', 'sham', 'active-sham'};
sessions = {'sham-active'};

for i = 1:numel(group_dir)
    
    curr_dir = [group_dir(i).folder filesep group_dir(i).name];
    
    for isess = 1:numel(sessions)
        
        curr_sess_dir = [curr_dir filesep sessions{isess}];
        
        con_dir = dir(curr_sess_dir);
        con_dir = con_dir(3:end);
    
        for icon = 1:numel(con_dir)
            
            curr_con_name = con_dir(icon).name;
            curr_con = [con_dir(icon).folder filesep con_dir(icon).name];
            
            con_images = dir([curr_con filesep '*sub-*.nii']);
            Vols = {};
            for j = 1:numel(con_images)
                Vols(j,:) = cellstr([curr_con filesep con_images(j).name]);
            end

            clear matlabbatch
            matlabbatch{1}.spm.stats.factorial_design.dir = {curr_con};
            matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = Vols;

            %
            matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
            matlabbatch{1}.spm.stats.factorial_design.multi_cov.files = {};
            matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCFI = 1;
            matlabbatch{1}.spm.stats.factorial_design.multi_cov.iCC = 1;
            matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
            matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
            matlabbatch{1}.spm.stats.factorial_design.masking.em = {GM_mask};
            matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
            matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
            matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1; 

            % Navigate to output directory, specify and estimate GLM
            spm_jobman('run', matlabbatch)
            cd(curr_con);

            load SPM;
            spm_spm(SPM);

            clear matlabbatch
            spmmat = dir([curr_con '/SPM.mat']);
            matlabbatch{1}.spm.stats.con.spmmat = {[spmmat.folder '/' spmmat.name]};
            matlabbatch{1}.spm.stats.con.consess{1}.tcon.name = 'active>sham';
            matlabbatch{1}.spm.stats.con.consess{1}.tcon.weights = 1;
            matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
            matlabbatch{1}.spm.stats.con.consess{2}.tcon.name = 'sham>active';
            matlabbatch{1}.spm.stats.con.consess{2}.tcon.weights = -1;
            matlabbatch{1}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
            matlabbatch{1}.spm.stats.con.delete = 1;
            spm_jobman('run', matlabbatch) 
            
        end
    end
end