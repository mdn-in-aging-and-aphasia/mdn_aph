#!/bin/bash

ROI_dir="/data/p_02221/Masks_ROIs/HOA_ROIs"

for iROI in "$ROI_dir"/*
do
	if [ -f "$iROI" ]; then
		ROI_name=${iROI%.nii.gz}
		FSL fslmaths "$iROI" -thr 10 -bin "$ROI_name"_thr_bin
		FSL flirt -in "$ROI_name"_thr_bin -ref /data/p_02221/MDN_APH/derivatives/fmriprep-preproc/output/sub-control007/fmriprep/sub-control007/ses-1/func/sub-control007_ses-1_task-semanticmatching_run-1_space-MNI152NLin6Asym_desc-preproc_bold.nii -out "$ROI_name"_thr_bin_rs -applyxfm
		FSL fslmaths "$ROI_name"_thr_bin_rs -thr 0.3 -bin "$ROI_name"_thr_bin_rs_thr_bin
	fi
done	

	
