%% Extract parameter estimates and PSC from ROIs from Localizer

clear
close all

%% Setup
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats';
sub_folders = dir([root_dir '/subjects/sub-control*']);
gPPI_folder = 'gPPI';

% path to marsbar ROIs
% ROI_folder = '/data/pt_02004/CoPla_AG_PSC/ROIs/marsbar';
% ROIs = dir([ROI_folder '/*.mat']);

% path to ROIs
ROI_path = [root_dir '/group_statistics/gPPI/Cluster_significant_gPPI_effects'];
ROIs = dir([ROI_path '/*.nii']);

% output folder to save parameter estimates
output_folder = [root_dir '/group_statistics/gPPI/'];

% sessions
sessions = {2, 3};
stim_sessions = {'active-sham'};

% Table with stimulation order
stim_order_tbl = readtable("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/stim_order.txt", 'ReadVariableNames', true, 'Delimiter', 'tab', 'Format', ...
    '%s%s%s%s');

% pattern to extract ROI name
pat = textBoundary + lettersPattern + '_' + lettersPattern + optionalPattern('+' + lettersPattern) + '>' + lettersPattern + '_' + lettersPattern + '>' + lettersPattern;
pat2 = textBoundary + lettersPattern + '_' + lettersPattern + optionalPattern('+' + lettersPattern) + '>' + lettersPattern;
pat_stimType = lettersPattern + '>' + lettersPattern + textBoundary("end");

optionalPattern(lettersPattern + '_') + lettersPattern + '_' + lettersPattern + optionalPattern('+' + lettersPattern) + '>' + lettersPattern;

%% Add Marsbar and SPM12
%Setup SPM12
addpath('/data/p_02221/spm12/')

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

addpath('/data/p_02221/spm12/toolbox/marsbar-0.45/')
marsbar('on')

% open text file and write header information
fid=fopen([output_folder '/Marsbar_estimates_Watershed_ROIs_n=' num2str(numel(ROIs)) '_' date '.txt'],'a');
fprintf(fid,'ROI\tSubject\tSession\tStimulation\tLang>ControlTask\n');

%% Make a loop – first ROIs, then subjects
for iROI = 1:numel(ROIs)
    
    curr_ROI = ROIs(iROI);
    curr_ROI_name = curr_ROI.name(1:end-4)
    curr_ROI_path = [curr_ROI.folder '/' curr_ROI.name];
    curr_ROI_extract = cell2mat(extract(curr_ROI_name, pat));
    curr_ROI_filename = cell2mat(extract(curr_ROI_name, pat2));
    curr_stim_type = cell2mat(extract(curr_ROI_name, pat_stimType));
%     subject_ROIs = dir([curr_ROI_path '/top10pct_above0/sub-control*']);

    switch curr_ROI_extract
        case('LSPL_FPM>Rest_ControlTask>Rest')
            beta_name = 'PPI_ControlTask>rest';
        case('LSPL_FPM>Rest_Lang>ControlTask')
            beta_name = 'PPI_WPM+FPM>ControlTask';
        case('LSPL_FPM>Rest_Lang>Rest')
            beta_name = 'PPI_WPM+FPM>rest
        case('LSPL_WPM+FPM>Rest_ControlTask>rest_active>sham.nii')
            beta_name = 'PPI_ControlTask>rest';
        case('LSPL_WPM+FPM>Rest_Lang>ControlTask_sham>active.nii')
            beta_name = 'PPI_WPM+FPM>ControlTask';
        case('LSPL_WPM+FPM>Rest_Lang>Rest_sham>active.nii')
            beta_name = 'PPI_WPM+FPM>rest';
        case('RCuneus_WPM>Rest_ControlTask>Rest_active>sham.nii')
            beta_name = 'PPI_ControlTask>rest';
        case('RCuneus_WPM>Rest_Lang>ControlTask_sham_active.nii')
            beta_name = 'PPI_WPM+FPM>ControlTask';
        case('RMOG_WPM+FPM>Rest_Lang>Rest_sham>active.nii')
            beta_name = 'PPI_WPM+FPM>rest';
        case('RMTG_FPM>Rest_Lang>Rest_sham>active.nii')
            beta_name = 'PPI_WPM+FPM>rest';
        case('preSMA_WPM+FPM>Rest_ControlTask>rest')
            beta_name = "all";
    end
    
    for isub = 1:numel(sub_folders)
        
        sub_name = sub_folders(isub).name;
        sub_folder = [sub_folders(isub).folder filesep sub_folders(isub).name];
            
        % use MarsBaR to convert nifit ROI in MarsBaR object
        %marsy_ROI = maroi_image(struct('vol', spm_vol(curr_sub_ROI_path), 'binarize', 0, 'func', 'img'))

        %% loop over sessions
        for isess = 1:numel(sessions)
            
            sess_dir = dir([sub_folder filesep 'ses-' num2str(sessions{isess}) filesep 'gPPI/gPPI_individualROI_*' curr_ROI_filename '*_top_10percent_voxel_ROIFromSession1']);
            sess_dir = [sess_dir.folder filesep sess_dir.name];

            % load SPM mat file
            model_dir = dir([sess_dir '/PPI_VOI*/SPM.mat']);
            model = [model_dir.folder filesep model_dir.name];
            
            % determine if it was sham or active stimulation
            if contains(sub_name, stim_order_tbl.sub_number(isub))
                stim_order = stim_order_tbl.order(isub);
            end
            
            if strcmp(stim_order, 'SA') && sessions{isess} == 2
                stim = 'sham';
            elseif strcmp(stim_order, 'SA') && sessions{isess} == 3
                stim = 'active';
            elseif strcmp(stim_order, 'AS') && sessions{isess} == 2
                stim = 'active';
            elseif strcmp(stim_order, 'AS') && sessions{isess} == 3
                stim = 'sham';
            end

            %% Start marsbar structure
            % Make marsbar design object
            D  = mardo(model);
            % Make marsbar ROI object
            R = maroi_image(struct('vol', spm_vol(curr_ROI_path), 'binarize', 0, 'func', 'img'));
            % Fetch data into marsbar data object
            Y  = get_marsy(R, D, 'mean');
            % Get contrasts from original design
            xCon = get_contrasts(D);
            % Estimate design on ROI data
            E = estimate(D, Y);
            % Put contrasts from original design back into design object
            E = set_contrasts(E, xCon);
            % get design betas
            b = betas(E);
            % get stats and stuff for all contrasts into statistics structure
            marsS = compute_contrasts(E, 1:length(xCon));

            if ~strcmp(curr_ROI_filename, 'preSMA_WPM+FPM>Rest')
                for icon = 1:numel(marsS.rows)
                    curr_con = marsS.rows(icon);
                    curr_con_name = curr_con{1,1}.name;
                    
                    if strcmp(curr_con_name, beta_name)
                            curr_beta = marsS.con(icon);          
                    end
                end
            end
            
%% Write estimates to text file        
fprintf(fid, '%s\t%s\t%d\t%s\t%d\n', curr_ROI_name, sub_name, cell2mat(sessions(isess)), stim, marsS.con(3));

        end
    end
end
       

