% Marsbar batch for ROIs on 1st level PPIs 

clear
close all

%% Setup
root_dir = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats';
gPPI_folder = 'gPPI';

% List of ROIs 
ROI_info = dir([root_dir '/group_statistics/' gPPI_folder '/threshold_top_percent_voxels/*25percent.txt']);

% Define seed
seed = 'bilateral_preSMA';

% path to ROIs
% ROIs = dir([root_dir 'group_statistics/gPPI/peak_ROIs/r*.nii']);
% % ROI_path = [root_dir '/group_statistics/gPPI/Cluster_significant_gPPI_effects'];
% ROIs = dir([ROI_path '/*.nii']);

% define subjects
sub_folders = dir([root_dir '/subjects/sub-control*']);
% subjects_1run = {'sub-control006', 'sub-control027'}; 

% define first level directory
firstlevel = '1st_level_semanticmatching';
ppi_folder = 'gPPI';

% output folder to save parameter estimates
output_folder = [root_dir '/group_statistics/gPPI/'];

% sessions
sessions = {2, 3};
stim_sessions = {'active-sham'};

% Table with stimulation order
stim_order_tbl = readtable("/data/p_02221/MDN_APH/derivatives/fmriprep-stats/subjects/stim_order.txt", 'ReadVariableNames', true, 'Delimiter', 'tab', 'Format', ...
    '%s%s%s%s');

%% Add Marsbar and SPM12
%Setup SPM12
addpath('/data/p_02221/spm12/')

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

addpath('/data/p_02221/spm12/toolbox/marsbar-0.45/')
marsbar('on')

% open text file and write header information
fid=fopen([output_folder '/Marsbar_estimates_preSMA2sigStimEffectROIs_n=' num2str(numel(ROI_info)-1) '_' date '.txt'],'a');
fprintf(fid,'SeedROI\tTargetROI\tSubject\tSession\tStimulation\tFPM>Tone\tFPM>WPM\tFPM>Rest\tLang>Rest\tTone>Rest\tLang>Tone\n');

%% Loop over ROIs and subjects
for iROI = 1:numel(ROI_info)
    
    ROI_name = ROI_info(iROI).name;
    ROI_name = ROI_name(15:end-18)
    if contains(ROI_name, seed)
        continue
    end

    for isub = 1:numel(sub_folders)
        
        sub_name = sub_folders(isub).name;
% 
%                % open text file and write header information
%        fid=fopen(['/data/pt_02004/Beta_weights/ROIs_based_on_YA_calculated_outputs/beta_weights_' ROI_names{iRoi} '_JE.txt'],'a');
% 
%        % open text file and write header information 
%        %subjectnum = str2double(sub{isub});       
%        if isub == 1
%           fprintf(fid,'Subject\tROI_seed\tROI_target\tCats_rest\tCats_Counting\n');
%        end

        for isess = 1:numel(sessions)
            % Display which participant and which run is currently processed
            X = ['This is participant ' sub_name ' and session ' num2str(sessions{isess}) ' and ROI ' ROI_name];
            disp(X);

            %Set the  directory
            curr_sub_dir = [sub_folders(isub).folder filesep sub_folders(isub).name];
            curr_sess_dir = [curr_sub_dir filesep 'ses-' num2str(sessions{isess})];
            gPPI_dir = [curr_sess_dir filesep 'gPPI/gPPI_individualROI_bilateral_preSMA_WPM+FPM>Rest_top_25percent_voxel_ROIFromSession1/'];

            % Get SPM.mat file
            model_dir = dir([gPPI_dir filesep 'PPI_VOI_*/SPM.mat']);
            model = [model_dir.folder filesep model_dir.name];

            % determine if it was sham or active stimulation
            if contains(sub_name, stim_order_tbl.sub_number(isub))
                stim_order = stim_order_tbl.order(isub);
            end
            
            if strcmp(stim_order, 'SA') && sessions{isess} == 2
                stim = 'sham';
            elseif strcmp(stim_order, 'SA') && sessions{isess} == 3
                stim = 'active';
            elseif strcmp(stim_order, 'AS') && sessions{isess} == 2
                stim = 'active';
            elseif strcmp(stim_order, 'AS') && sessions{isess} == 3
                stim = 'sham';
            end
        
            % Get target ROI file
            ROI_dir = dir([curr_sub_dir filesep 'ses-1/gPPI/']);
            mask_name = ['gPPI_individualROI_' ROI_name '_top_25percent_voxel'];
            curr_ROI_idx = find(strcmp(mask_name, {ROI_dir.name}));
            curr_ROI_dir = dir([ROI_dir(curr_ROI_idx).folder filesep ROI_dir(curr_ROI_idx).name filesep '*_mask.nii']);

            if isempty(curr_ROI_dir)
                fprintf('No ROI here\n');
                continue;
            end

            % Check if mat file exists, i.e., if subject had any voxels in
            % this ROI
            curr_ROI = [curr_ROI_dir.folder filesep curr_ROI_dir.name];
           
            % use MarsBaR to convert nifti ROI in MarsBaR object
            marsy_ROI = maroi_image(struct('vol', spm_vol(curr_ROI), 'binarize', 1, 'func', 'img', 'label', curr_ROI_dir.name));   
    
%% Start marsbar structure

            % Make marsbar design object
            D  = mardo(model);
            % Make marsbar ROI object
            R  = maroi(marsy_ROI);
            % Fetch data into marsbar data object
            Y  = get_marsy(R, D, 'mean');
            % Get contrasts from original design
            xCon = get_contrasts(D);
            % Estimate design on ROI data
            E = estimate(D, Y);
            % Put contrasts from original design back into design object
            E = set_contrasts(E, xCon);
            % get design betas
            b = betas(E);
            % get stats and stuff for all contrasts into statistics structure
            marsS = compute_contrasts(E, 1:length(xCon));
            
%             [e_specs, e_names] = event_specs(E);
%             n_events = size(e_specs, 2);
%             dur = 3.5;
%             % Return percent signal esimate for all events in design
%             for e_s = 1:n_events
%               pct_ev(e_s) = event_signal(E, e_specs(:,e_s), dur);
%             end

%             for ibeta = 1:numel(contrasts_of_interest)
%                 for icon = 1:numel(marsS.rows)
%                     curr_con = marsS.rows(icon);
%                     curr_con_name = curr_con{1,1}.name;
%                     
%                     if strcmp(curr_con_name, contrasts_of_interest{ibeta})
%                             curr_beta = marsS.con(icon);          
%                     end
%                 end
%             end;
           
%% Write beta weights to text file
           
           fprintf(fid, 'BilateralPreSMA\t%s\t%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n', ROI_name(1:end-12), sub_name, num2str(sessions{isess}), stim, marsS.con(5), marsS.con(6), ...
               marsS.con(8), marsS.con(1), marsS.con(2), marsS.con(3));
           
       end
   end
end
