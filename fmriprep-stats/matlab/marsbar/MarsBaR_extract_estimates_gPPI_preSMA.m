%% Extract parameter estimates and PSC from pre-SMA

clear
close all

%% Setup
root_folder = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats';
sub_folders = dir([root_folder '/subjects/sub-control*']);
first_level_folder = '1st_level_semanticmatching';

ROI_session = 1;
n_sessions = 3;

% path to ROIs
ROI_path = '/data/p_02221/Masks_ROIs/rbilateral_preSMA_mask_thr0.3.nii'; % resliced bilateral preSMA mask in MNI space
ROI_name = 'bilateral_preSMA';

% define first level directory
firstlevel_dir = '1st_level_semanticmatching';
ppi_folder = 'gPPI';

top_percent = 10;
con_name = 'WPM+FPM>Rest';
con_image = 'spmT_0002';

% output folder to save parameter estimates
output_folder = [root_folder '/group_statistics/Control_group_Active-Sham_OneSample_ttest'];

% Create output folder if it doesn't exist
if ~exist(output_folder,'dir')
    mkdir(output_folder)
end


%% Add Marsbar and SPM12
%Setup SPM12
addpath('/data/p_02221/spm12/')

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

addpath('/data/p_02221/spm12/toolbox/marsbar-0.45/')
marsbar('on')

% open text file and write header information
fid=fopen([output_folder '/Marsbar_estimates_univariate_stimulation_effect_bilateral_preSMA_' date '.txt'],'a');
fprintf(fid,'ROI\tSubject\tSession\tLang>Rest\tWPM>Rest\tFPM>Rest\tControlTask>Rest\tLang>ControlTask\tControlTask>Lang\tPSC_WPM\tPSC_FPM\tPSC_tone\n');

% this is a separate output file with information on size of individual
% ROIs for bilateral preSMA mask
fid2=fopen([root_folder '/group_statistics/gPPI/gPPI_indivROI_' ROI_name '_' con_name '_top_' num2str(top_percent) 'percent.txt'],'a');
fprintf(fid2, 'subject\tsession\tcontrast\tROI_size_all_vx\tROI_size_above0\n');

%% Make a loop – first ROIs, then subjects
for isub = 1:numel(sub_folders)
        
    % calculate number of voxels in ROI
    V_ROI = spm_vol(ROI_path);
    [Y_ROI,XYZ] = spm_read_vols(V_ROI);
    n_voxels_in_ROI = sum(Y_ROI(:)>0)

    curr_sub = sub_folders(isub);
    curr_sub_name = curr_sub.name(5:end)
    curr_sub_dir = [sub_folders(isub).folder '/' sub_folders(isub).name];
    curr_sess_dir = [curr_sub_dir filesep 'ses-' num2str(ROI_session)];

    % create gPPI directory 
    % mask is directly stored in a PPI folder so to also run a PPI for this
    % seed mask
    PPI_folder_path = [curr_sess_dir filesep ppi_folder filesep 'gPPI_individualROI_' ROI_name '_' ...
        con_name '_top_' num2str(top_percent) 'percent_voxel'];
    if ~exist(PPI_folder_path)
        mkdir(PPI_folder_path)
    end 

    % go into subjects' first level folder
    firstLevelFolder_dir = [curr_sess_dir filesep firstlevel_dir];
    curr_Tmap = dir([firstLevelFolder_dir '/' con_image '*nii']);

    if numel(curr_Tmap) ~= 1
        error(['Error: Not exactly 1 T-map found at ' ...
            firstLevelFolder_dir '/' ROI_con_image '*nii'])
    end

    curr_Tmap_path = [curr_Tmap.folder '/' curr_Tmap.name];

    %% create image of voxels in ROI
    clear matlabbatch
    matlabbatch{1}.spm.util.imcalc.input = {
                                    curr_Tmap_path
                                    ROI_path
                                    };
    matlabbatch{1}.spm.util.imcalc.output = ['VOI_' ROI_name '_T_image.nii'];
    matlabbatch{1}.spm.util.imcalc.outdir = {PPI_folder_path};
    matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
    matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
    matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
    matlabbatch{1}.spm.util.imcalc.options.mask = -1;
    matlabbatch{1}.spm.util.imcalc.options.interp = 0;
    matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

    spm_jobman('run',matlabbatch);

    % get image
    Tmap_ROI = dir([PPI_folder_path '/' 'VOI_' ROI_name '_T_image.nii']);

    if numel(Tmap_ROI) ~= 1
    error(['Error: Not exactly 1 ROI-Tmap found at ' ...
        PPI_folder_path '/' 'VOI_' ROI_name '_T_image.nii'])
    end

    Tmap_ROI_path = [Tmap_ROI.folder '/' Tmap_ROI.name];

    % get Tmap in ROI
    clear V Y n_voxels Y_sorted idx_sorted idx_top_voxels Y_topVoxels V_ROI
    V = spm_vol(Tmap_ROI_path);
    [Y,XYZ] = spm_read_vols(V);

    n_top_voxels = round((n_voxels_in_ROI./100) .* top_percent);

    % set all non-ROI voxels to minus-Infinity to ensure that only
    % voxels within the ROI are picked
    Y(Y_ROI(:)==0) = -Inf;

    % sort the ROI-voxels by T-values in descending order
    [Y_sorted,idx_sorted] = sort(Y(:),'descend');

    % get indices of the n top voxels within the ROI with T>0
    idx_top_voxels = idx_sorted(1:n_top_voxels);
    idx_top_voxels_above0 = idx_sorted(Y_sorted(1:n_top_voxels)>0);
    Y_sorted(Y_sorted(1:n_top_voxels)>0);

    VOI_mask_outfile = [PPI_folder_path '/' 'VOI_' ROI_name '_mask.nii'];

    if numel(idx_top_voxels_above0) > 0
        Y_topVoxels = Y;
        Y_topVoxels(:) = 0;
        Y_topVoxels(idx_top_voxels_above0) = 1;

        V_topVoxels = struct('fname',VOI_mask_outfile,'mat',V.mat,'dim',V.dim,...
            'dt',[spm_type('float32') spm_platform('bigend')],'pinfo',[1;0;0]);
        spm_write_vol(V_topVoxels,Y_topVoxels); 
    end

    fprintf(fid2,'%s\t%d\t%s\t%d\t%d\n', curr_sub_name, 1, ROI_name, numel(idx_top_voxels), numel(idx_top_voxels_above0));


    %% Now move on to extract parameter estimates for preSMA mask, using the same mask for all sessions
    % use MarsBaR to convert preSMA mask ROI in MarsBaR object
    marsy_ROI = maroi_image(struct('vol', spm_vol(VOI_mask_outfile), 'binarize', 1, 'func', 'img', 'label', ROI_name))

    for ses = 1:n_sessions

        % load design files to estimate average duration per condition
        design_run1 = [curr_sub_dir '/ses-' num2str(ses) '/logfiles/sub-' curr_sub_name '_session-' num2str(ses) '_task-semanticmatching_run-1_1st_level_event_related_incl_length_audio.mat'];
        run1 = load(design_run1);
        design_run2 = [curr_sub_dir '/ses-' num2str(ses) '/logfiles/sub-' curr_sub_name '_session-' num2str(ses) '_task-semanticmatching_run-2_1st_level_event_related_incl_length_audio.mat'];
        run2 = load(design_run2);

        WPM = [run1.durations{1}; run2.durations{1}];
        avgDur_WPM = mean(WPM);
        FPM = [run1.durations{2}; run2.durations{2}];
        avgDur_FPM = mean(FPM);
        tone = [run1.durations{3}; run2.durations{3}];
        avgDur_tone = mean(tone);

        % load SPM mat file
        model_dir = [curr_sub_dir '/ses-' num2str(ses) '/' first_level_folder '/SPM.mat'];
    
        %% Start marsbar structure
        % Make marsbar design object
        D  = mardo(model_dir);
        % Make marsbar ROI object
        R  = maroi(marsy_ROI);
        % Fetch data into marsbar data object
        Y  = get_marsy(R, D, 'mean');
        % Get contrasts from original design
        xCon = get_contrasts(D);
        % Estimate design on ROI data
        E = estimate(D, Y);
        % Put contrasts from original design back into design object
        E = set_contrasts(E, xCon);
        % get design betas
        b = betas(E);
        % get stats and stuff for all contrasts into statistics structure
        marsS = compute_contrasts(E, 1:length(xCon));
    
  
        [e_specs, e_names] = event_specs(E);
    %         n_events = size(e_specs, 2);
        dur = 3.5;
        % Return percent signal esimate for all events in design
        for e_s = 1:numel(e_names)
          if strcmp('WPM_corr', e_names{e_s})  
              pct_ev(e_s) = event_signal(E, e_specs(:,e_s), avgDur_WPM);
          elseif strcmp('FPM_corr', e_names{e_s})
              pct_ev(e_s) = event_signal(E, e_specs(:,e_s), avgDur_FPM);
          elseif strcmp('tone_corr', e_names{e_s})
              pct_ev(e_s) = event_signal(E, e_specs(:,e_s), avgDur_tone);
          else 
              pct_ev(e_s) = event_signal(E, e_specs(:,e_s), dur);
          end
        end
    
        % average across conditions
        WPM_PSC = find(strcmp(e_names, 'WPM_corr'));
        avgPSC_WPM = mean(pct_ev(WPM_PSC));
        FPM_PSC = find(strcmp(e_names, 'FPM_corr'));
        avgPSC_FPM = mean(pct_ev(FPM_PSC));
        tone_PSC = find(strcmp(e_names, 'tone_corr'));
        avgPSC_tone = mean(pct_ev(tone_PSC));
        
    
        %% Write estimates to text file        
        fprintf(fid, '%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n', ROI_name, curr_sub_name, ses, marsS.con(2), marsS.con(3), marsS.con(4), ...
            marsS.con(5), marsS.con(6), marsS.con(7), avgPSC_WPM, avgPSC_FPM, avgPSC_tone);
                      
    end
end
       
fclose(fid);
fclose(fid2);
