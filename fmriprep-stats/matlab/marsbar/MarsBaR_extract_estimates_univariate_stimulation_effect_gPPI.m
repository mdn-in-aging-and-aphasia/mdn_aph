%% Extract parameter estimates and PSC from ROIs from Localizer

clear

%% Setup
root_folder = '/data/p_02221/MDN_APH/derivatives/fmriprep-stats';
sub_folders = dir([root_folder '/subjects/sub-control*']);
first_level_folder = '1st_level_semanticmatching';

% ROI_session = 1;
n_sessions = {'2','3'};

% path to ROIs
ROI_path = [root_folder '/group_statistics/gPPI'];
ROI_folders = dir([ROI_path '/gPPI_*_2023/active-sham/*/with_covs_*/*_bin.nii']);

% define first level directory
firstlevel_dir = '1st_level_semanticmatching';
ppi_folder = 'gPPI';


% duration of events
% dur = 43; %?

% output folder to save parameter estimates
output_folder = [root_folder '/group_statistics/gPPI'];

% Create output folder if it doesn't exist
if ~exist(output_folder,'dir')
    mkdir(output_folder)
end


%% Add Marsbar and SPM12
%Setup SPM12
addpath('/data/p_02221/spm12/')

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

addpath('/data/p_02221/spm12/toolbox/marsbar-0.45/')
marsbar('on')

% open text file and write header information
fid=fopen([output_folder '/Marsbar_estimates_gPPI_ROIs_n=' num2str(numel(ROI_folders)) '_' date '.txt'],'a');
%fprintf(fid,'ROI\tSubject\tSession\tLang>Rest\tWPM>Rest\tFPM>Rest\tControlTask>Rest\tLang>ControlTask\tControlTask>Lang\tPSC_WPM\tPSC_FPM\tPSC_tone\n');
fprintf(fid,'ROI\tSubject\tSession\tContrast\tEstimate\tPSC_WPM\tPSC_FPM\tPSC_tone\n');
%fprintf(fid,'ROI\tSubject\tSession\tContrast\tEstimate\n');

%% Make a loop – first ROIs, then subjects
for iROI = 1:numel(ROI_folders)
    
    curr_ROI = ROI_folders(iROI);
    curr_ROI_name = curr_ROI.name(1:end-4);
    curr_ROI_path = [curr_ROI.folder '/' curr_ROI.name];
    curr_ROI_short = extractBefore(curr_ROI_name, '_');

    if contains(curr_ROI_name, 'Tone_FPM')
        beta_name = {'PPI_Tone>rest', 'PPI_FPM>rest'};
    elseif contains(curr_ROI_name, 'FPM_WPM')
        beta_name = {'PPI_FPM>rest', 'PPI_WPM>rest'};
    end

    % use MarsBaR to convert nifti ROI in MarsBaR object
    marsy_ROI = maroi_image(struct('vol', spm_vol(curr_ROI_path), 'binarize', 0, 'func', 'img', 'label', curr_ROI_name))

    for isub = 1:numel(sub_folders)

        curr_sub = sub_folders(isub);
        curr_sub_path = [curr_sub.folder '/' curr_sub.name];
        curr_sub_name = curr_sub.name(5:end)
      
        if curr_ROI_short == "LOccipitalPole" && (curr_sub_name == "control001" || curr_sub_name == "control004" || ...
                curr_sub_name == "control026" || curr_sub_name == "control027" || curr_sub_name == "control028")
            continue
        end
        
        %% loop over sessions
        for ses = 1:numel(n_sessions)
            
            % load design files to estimate average duration per condition
            design_run1 = [curr_sub_path '/ses-' n_sessions{ses} '/logfiles/' curr_sub.name '_session-' n_sessions{ses} '_task-semanticmatching_run-1_1st_level_event_related_incl_length_audio.mat'];
            run1 = load(design_run1);
            design_run2 = [curr_sub_path '/ses-' n_sessions{ses} '/logfiles/' curr_sub.name '_session-' n_sessions{ses} '_task-semanticmatching_run-2_1st_level_event_related_incl_length_audio.mat'];
            run2 = load(design_run2);

            WPM = [run1.durations{1}; run2.durations{1}];
            avgDur_WPM = mean(WPM);
            FPM = [run1.durations{2}; run2.durations{2}];
            avgDur_FPM = mean(FPM);
            tone = [run1.durations{3}; run2.durations{3}];
            avgDur_tone = mean(tone);

            % load SPM mat file
            model_dir = dir([curr_sub_path '/ses-' n_sessions{ses} '/gPPI/gPPI_*' curr_ROI_short '*25*_ROIFromSession1/PPI_*' curr_ROI_short '*_mask/SPM.mat']);
            model = [model_dir.folder filesep model_dir.name];

            %% Start marsbar structure
            % Make marsbar design object
            D  = mardo(model);
            % Make marsbar ROI object
            R  = maroi(marsy_ROI);
%             R = maroi_image(struct('vol', spm_vol(curr_sub_path), 'binarize', 0, 'func', 'img'));
            % Fetch data into marsbar data object
            Y  = get_marsy(R, D, 'mean');
            % Get contrasts from original design
            xCon = get_contrasts(D);
            xCon(20:length(xCon)) = [];
            % Estimate design on ROI data
            E = estimate(D, Y);
            % Put contrasts from original design back into design object
            E = set_contrasts(E, xCon);
            % get design betas
            b = betas(E);
            % get stats and stuff for all contrasts into statistics structure
            marsS = compute_contrasts(E, 1:length(xCon));

%             for icon = 1:numel(marsS.rows)
%                 curr_con = marsS.rows(icon);
%                 curr_con_name = curr_con{1,1}.name;
%                 
%                 if strcmp(curr_con_name, beta_name)
%                         curr_beta = marsS.con(icon);          
%                 end
%             end

            beta_estimates = [];
            if contains(curr_ROI_name, 'FPM_WPM') 
                for icon = 1:numel(marsS.rows)
                    curr_con = marsS.rows(icon);
                    curr_con_name = curr_con{1,1}.name;
                    
                    for ibeta = 1:numel(beta_name)
                        if strcmp(curr_con_name, beta_name{ibeta})
                            beta_estimates(ibeta) = marsS.con(icon);
                        end
                    end
                end
            elseif contains(curr_ROI_name, 'Tone_FPM') 
                for icon = 1:numel(marsS.rows)
                    curr_con = marsS.rows(icon);
                    curr_con_name = curr_con{1,1}.name;
                    
                    for ibeta = 1:numel(beta_name)
                        if strcmp(curr_con_name, beta_name{ibeta})
                            beta_estimates(ibeta) = marsS.con(icon);
                        end
                    end
                end
            end
                

            [e_specs, e_names] = event_specs(E);
    %         n_events = size(e_specs, 2);
            dur = 3.5;
            % Return percent signal esimate for all events in design
            for e_s = 1:numel(e_names)
              if strcmp('WPM_corr', e_names{e_s})  
                  pct_ev(e_s) = event_signal(E, e_specs(:,e_s), avgDur_WPM);
              elseif strcmp('FPM_corr', e_names{e_s})
                  pct_ev(e_s) = event_signal(E, e_specs(:,e_s), avgDur_FPM);
              elseif strcmp('tone_corr', e_names{e_s})
                  pct_ev(e_s) = event_signal(E, e_specs(:,e_s), avgDur_tone);
              else 
                  pct_ev(e_s) = event_signal(E, e_specs(:,e_s), dur);
              end
            end

            % average across conditsions
            WPM_PSC = find(strcmp(e_names, 'WPM_corr'));
            avgPSC_WPM = mean(pct_ev(WPM_PSC));
            FPM_PSC = find(strcmp(e_names, 'FPM_corr'));
            avgPSC_FPM = mean(pct_ev(FPM_PSC));
            tone_PSC = find(strcmp(e_names, 'tone_corr'));
            avgPSC_tone = mean(pct_ev(tone_PSC));
            
        
            %% Write estimates to text file        
            fprintf(fid, '%s\t%s\t%s\t%s\t%d\t%d\t%d\t%d\n', curr_ROI_name, curr_sub_name, n_sessions{ses}, beta_name{1}, beta_estimates(1), avgPSC_WPM, avgPSC_FPM, avgPSC_tone);
            fprintf(fid, '%s\t%s\t%s\t%s\t%d\t%d\t%d\t%d\n', curr_ROI_name, curr_sub_name, n_sessions{ses}, beta_name{2}, beta_estimates(2), avgPSC_WPM, avgPSC_FPM, avgPSC_tone);
                           
        end
    end
end
       

